﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using System.Data.Entity;
using log4net;

namespace QL_KS
{
    public partial class frmDSKhachHang : Form
    {
        public frmDSKhachHang()
        {
            InitializeComponent();
            
        }
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmDSKhachHang).Name);
        public string TaiKhoan;
        public static string makh;
        private void frmDSKhachHang_Load(object sender, EventArgs e)
        {
            LoadKH();
        }

        private void LoadKH()
        {
            var ds = KhachHangBus.LoadDS();
            gcDSKH.DataSource = ds;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog
                {
                    CheckPathExists = true,
                    InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments),
                    OverwritePrompt = true,
                    Filter = "txt files (*.pdf)|*.pdf|All files (*.*)|*.*"
                };
                DialogResult dr = sfd.ShowDialog();
                if (dr == System.Windows.Forms.DialogResult.OK || dr == System.Windows.Forms.DialogResult.Yes)
                {
                    gvDSKH.ExportToPdf(sfd.FileName);
                }
                dr = MessageBox.Show("Xuất dữ liệu thành công! Bạn có muốn mở tập tin vừa xuất ra không?", "Xác nhận mở tập tin", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(sfd.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi xuất dữ liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            frmTimKiemKH form = new frmTimKiemKH();
            form.ShowDialog();
            LoadKH();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmThemMoiKH frm = new frmThemMoiKH();

            frm.TaiKhoan = TaiKhoan;
            frm.ShowDialog();
            LoadKH();
        }
    }
}