﻿namespace QL_KS
{
    partial class frmThem_Sua_Loai_Phong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmThem_Sua_Loai_Phong));
            this.btnHuyBo = new System.Windows.Forms.Button();
            this.btnTraPhong = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lbThongBao_TienQuaGio = new System.Windows.Forms.Label();
            this.lbThongBao_Ten = new System.Windows.Forms.Label();
            this.txtTienQuaGio = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtTenLoaiPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.pnwapDongia = new System.Windows.Forms.Panel();
            this.fllKetiep = new System.Windows.Forms.FlowLayoutPanel();
            this.fllDonGia = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaLoaiPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.pnwapDongia.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.BackColor = System.Drawing.SystemColors.Menu;
            this.btnHuyBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHuyBo.FlatAppearance.BorderSize = 0;
            this.btnHuyBo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.ForeColor = System.Drawing.Color.Crimson;
            this.btnHuyBo.Location = new System.Drawing.Point(572, 382);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(110, 40);
            this.btnHuyBo.TabIndex = 4;
            this.btnHuyBo.Text = "Hủy Bỏ";
            this.btnHuyBo.UseVisualStyleBackColor = false;
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnTraPhong
            // 
            this.btnTraPhong.BackColor = System.Drawing.SystemColors.Menu;
            this.btnTraPhong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTraPhong.FlatAppearance.BorderSize = 0;
            this.btnTraPhong.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraPhong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnTraPhong.Location = new System.Drawing.Point(456, 382);
            this.btnTraPhong.Name = "btnTraPhong";
            this.btnTraPhong.Size = new System.Drawing.Size(110, 40);
            this.btnTraPhong.TabIndex = 3;
            this.btnTraPhong.Text = "Thêm Mới";
            this.btnTraPhong.UseVisualStyleBackColor = false;
            this.btnTraPhong.Click += new System.EventHandler(this.btnTraPhong_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(9, 16);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(673, 41);
            this.panel2.TabIndex = 66;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(268, 30);
            this.label4.TabIndex = 0;
            this.label4.Text = "THÔNG TIN LOẠI PHÒNG";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbThongBao_TienQuaGio
            // 
            this.lbThongBao_TienQuaGio.AutoSize = true;
            this.lbThongBao_TienQuaGio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_TienQuaGio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_TienQuaGio.Location = new System.Drawing.Point(103, 127);
            this.lbThongBao_TienQuaGio.Name = "lbThongBao_TienQuaGio";
            this.lbThongBao_TienQuaGio.Size = new System.Drawing.Size(159, 16);
            this.lbThongBao_TienQuaGio.TabIndex = 76;
            this.lbThongBao_TienQuaGio.Text = "Hãy nhập vào tiền quá giờ";
            this.lbThongBao_TienQuaGio.Visible = false;
            // 
            // lbThongBao_Ten
            // 
            this.lbThongBao_Ten.AutoSize = true;
            this.lbThongBao_Ten.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_Ten.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_Ten.Location = new System.Drawing.Point(452, 82);
            this.lbThongBao_Ten.Name = "lbThongBao_Ten";
            this.lbThongBao_Ten.Size = new System.Drawing.Size(171, 16);
            this.lbThongBao_Ten.TabIndex = 75;
            this.lbThongBao_Ten.Text = "Hãy nhập vào tên loại phòng";
            this.lbThongBao_Ten.Visible = false;
            // 
            // txtTienQuaGio
            // 
            // 
            // 
            // 
            this.txtTienQuaGio.BackgroundStyle.Class = "TextBoxBorder";
            this.txtTienQuaGio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTienQuaGio.ButtonClear.Visible = true;
            this.txtTienQuaGio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienQuaGio.Location = new System.Drawing.Point(106, 146);
            this.txtTienQuaGio.Mask = "000000";
            this.txtTienQuaGio.Name = "txtTienQuaGio";
            this.txtTienQuaGio.Size = new System.Drawing.Size(230, 20);
            this.txtTienQuaGio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtTienQuaGio.TabIndex = 2;
            this.txtTienQuaGio.Text = "";
            this.txtTienQuaGio.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtTienQuaGio_MaskInputRejected);
            // 
            // txtTenLoaiPhong
            // 
            this.txtTenLoaiPhong.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTenLoaiPhong.Border.Class = "TextBoxBorder";
            this.txtTenLoaiPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenLoaiPhong.DisabledBackColor = System.Drawing.Color.White;
            this.txtTenLoaiPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenLoaiPhong.ForeColor = System.Drawing.Color.Black;
            this.txtTenLoaiPhong.Location = new System.Drawing.Point(452, 101);
            this.txtTenLoaiPhong.MaxLength = 30;
            this.txtTenLoaiPhong.Name = "txtTenLoaiPhong";
            this.txtTenLoaiPhong.PreventEnterBeep = true;
            this.txtTenLoaiPhong.Size = new System.Drawing.Size(230, 22);
            this.txtTenLoaiPhong.TabIndex = 1;
            // 
            // pnwapDongia
            // 
            this.pnwapDongia.AutoScroll = true;
            this.pnwapDongia.Controls.Add(this.fllKetiep);
            this.pnwapDongia.Controls.Add(this.fllDonGia);
            this.pnwapDongia.Location = new System.Drawing.Point(0, 231);
            this.pnwapDongia.Name = "pnwapDongia";
            this.pnwapDongia.Size = new System.Drawing.Size(689, 125);
            this.pnwapDongia.TabIndex = 71;
            // 
            // fllKetiep
            // 
            this.fllKetiep.Location = new System.Drawing.Point(349, 12);
            this.fllKetiep.Name = "fllKetiep";
            this.fllKetiep.Size = new System.Drawing.Size(338, 46);
            this.fllKetiep.TabIndex = 1;
            // 
            // fllDonGia
            // 
            this.fllDonGia.Location = new System.Drawing.Point(3, 12);
            this.fllDonGia.Name = "fllDonGia";
            this.fllDonGia.Size = new System.Drawing.Size(342, 46);
            this.fllDonGia.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 70;
            this.label1.Text = "Tiền Quá Giờ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(346, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 69;
            this.label3.Text = "Tên Loại Phòng:";
            // 
            // txtMaLoaiPhong
            // 
            this.txtMaLoaiPhong.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMaLoaiPhong.Border.Class = "TextBoxBorder";
            this.txtMaLoaiPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaLoaiPhong.DisabledBackColor = System.Drawing.Color.White;
            this.txtMaLoaiPhong.Enabled = false;
            this.txtMaLoaiPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLoaiPhong.ForeColor = System.Drawing.Color.Black;
            this.txtMaLoaiPhong.Location = new System.Drawing.Point(106, 100);
            this.txtMaLoaiPhong.Name = "txtMaLoaiPhong";
            this.txtMaLoaiPhong.PreventEnterBeep = true;
            this.txtMaLoaiPhong.Size = new System.Drawing.Size(230, 22);
            this.txtMaLoaiPhong.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 17);
            this.label2.TabIndex = 67;
            this.label2.Text = "Mã Loại Phòng:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(7, 181);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(673, 41);
            this.panel1.TabIndex = 67;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 30);
            this.label5.TabIndex = 0;
            this.label5.Text = "ĐƠN GIÁ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmThem_Sua_Loai_Phong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 431);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbThongBao_TienQuaGio);
            this.Controls.Add(this.lbThongBao_Ten);
            this.Controls.Add(this.txtTienQuaGio);
            this.Controls.Add(this.txtTenLoaiPhong);
            this.Controls.Add(this.pnwapDongia);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMaLoaiPhong);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnHuyBo);
            this.Controls.Add(this.btnTraPhong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmThem_Sua_Loai_Phong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmThem_Sua_Loai_Phong";
            this.Load += new System.EventHandler(this.frmThem_Sua_Loai_Phong_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnwapDongia.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnHuyBo;
        private System.Windows.Forms.Button btnTraPhong;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbThongBao_TienQuaGio;
        private System.Windows.Forms.Label lbThongBao_Ten;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtTienQuaGio;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenLoaiPhong;
        private System.Windows.Forms.Panel pnwapDongia;
        private System.Windows.Forms.FlowLayoutPanel fllKetiep;
        private System.Windows.Forms.FlowLayoutPanel fllDonGia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaLoaiPhong;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
    }
}