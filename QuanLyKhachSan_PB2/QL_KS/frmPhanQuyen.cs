﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using log4net;

namespace QL_KS
{
    public partial class frmPhanQuyen : Form
    {
        public frmPhanQuyen()
        {
            InitializeComponent();
        }
        PhanQuyenBUS pqBUS = new PhanQuyenBUS();
        public string TaiKhoan;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmPhanQuyen).Name);
        public frmMain f;

        private void LoadDL()
        {
            List<BangPhanQuyen> list = pqBUS.Load();
            gcPhanQuyen.DataSource = list;
        }

        private void frmPhanQuyen_Load(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void btnLuuPhanQuyen_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn muốn lưu sự thay đổi này không?", "Câu hỏi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    List<BangPhanQuyen> lt = (List<BangPhanQuyen>)gvData.DataSource;
                    pqBUS.CapNhat(lt);
                    MessageBox.Show("Lưu thành công sự thay đổi!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LuuNhatKyHT.Them(TaiKhoan, "Hệ Thống", "Phân quyền");
                    LoadDL();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Không thể lưu sự thay đổi!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _logger.Error(ex.ToString());
                }
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}