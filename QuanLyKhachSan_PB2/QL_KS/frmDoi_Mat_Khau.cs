﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QL_KS.DAO;
using DevExpress.XtraEditors;
using QL_KS.BUS;
using log4net;

namespace QL_KS
{
    public partial class frmDoi_Mat_Khau : Form
    {
        public frmDoi_Mat_Khau()
        {
            InitializeComponent();
        }
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmDoi_Mat_Khau).Name);
        public string MaNV;
        public string TaiKhoan;
        NhanVienBUS nvBUS = new NhanVienBUS();


        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThayDoi_Click(object sender, EventArgs e)
        {
            if (txtMatKhauHT.Text == string.Empty || txtMatKhauMoi.Text == string.Empty || txtNhapLaiMK.Text == string.Empty)
            {
                lblThongBao.Text = "Bạn chưa nhập đầy đủ thông tin";
                lblThongBao.ForeColor = Color.Red;
            }
            else
            {
                NhanVien nv = nvBUS.LoadTheoMaNV(MaNV);
                if (MaHoaMD5.md5(txtMatKhauHT.Text) == nv.MatKhau.Trim())
                {
                    if (txtMatKhauMoi.Text == txtNhapLaiMK.Text)
                    {
                        string matkhaumoi = MaHoaMD5.md5(txtMatKhauMoi.Text);
                        nv.MatKhau = matkhaumoi;
                        nvBUS.CapNhatMatKhau(nv);
                        LuuNhatKyHT.Them(TaiKhoan, "Hệ Thống", "Thay đổi mật khẩu");
                        lblThongBao.Text = "Thay đổi mật khẩu thành công";
                        lblThongBao.ForeColor = Color.Blue;
                        XtraMessageBox.Show("Thay đổi mật khẩu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        lblThongBao.Text = "Mật khẩu mới và nhập lại mật khẩu mới không khớp";
                        lblThongBao.ForeColor = Color.Red;
                    }
                }
                else
                {
                    lblThongBao.Text = "Mật khẩu hiện tại không đúng";
                    lblThongBao.ForeColor = Color.Red;
                }
            }
        }

        private void txtMatKhauHT_TextChanged(object sender, EventArgs e)
        {
            //if (txtMatKhauHT.Text == "--Mật khẩu hiện tại phải dưới 50 ký tự")
            //{
            //    txtMatKhauHT.ForeColor = Color.Black;
            //}
        }

        private void txtMatKhauHT_Enter(object sender, EventArgs e)
        {
            //if (txtMatKhauHT.Text == "--Mật khẩu hiện tại phải dưới 50 ký tự")
            //    txtMatKhauHT.Text = "";
        }

        private void txtMatKhauHT_Leave(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(txtMatKhauHT.Text))
            //{
            //    txtMatKhauHT.Text = "--Mật khẩu hiện tại phải dưới 50 ký tự";
            //    txtMatKhauHT.ForeColor = Color.Black;
            //}
            //else
            //    errorProvider1.Clear();
        }

        private void txtMatKhauMoi_TextChanged(object sender, EventArgs e)
        {
            //if (txtMatKhauMoi.Text == "--Mật khẩu mới phải dưới 50 ký tự")
            //{
            //    txtMatKhauMoi.ForeColor = Color.Black;
            //}
        }

        private void txtMatKhauMoi_Leave(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(txtMatKhauMoi.Text))
            //{
            //    errorProvider2.SetError(txtMatKhauMoi,"Bạn chưa nhập mật khẩu mới");
            //}
            //else
            //    errorProvider2.Clear();
        }

        private void txtMatKhauMoi_Enter(object sender, EventArgs e)
        {
            //if (txtMatKhauMoi.Text == "--Mật khẩu mới phải dưới 50 ký tự")
            //    txtMatKhauMoi.Text = "";
        }

        private void txtNhapLaiMK_TextChanged(object sender, EventArgs e)
        {
            //if (txtNhapLaiMK.Text == "--Mật khẩu nhập lại phải dưới 50 ký tự")
            //{
            //    txtNhapLaiMK.ForeColor = Color.Black;
            //}
        }

        private void txtNhapLaiMK_Leave(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(txtNhapLaiMK.Text))
            //{
            //    errorProvider2.SetError(txtNhapLaiMK, "Bạn chưa nhập lại mật khẩu mới");
            //}
            //else
            //    errorProvider3.Clear();
        }

        private void txtNhapLaiMK_Enter(object sender, EventArgs e)
        {
            //if (txtNhapLaiMK.Text == "--Mật khẩu nhập lại phải dưới 50 ký tự")
            //    txtNhapLaiMK.Text = "";
        }

        private void frmDoiMatKhau_Load(object sender, EventArgs e)
        {
            //txtMatKhauHT.Text = "--Nhập mật khẩu hiện tại";
            //txtMatKhauMoi.Text = "--Nhập mật khẩu mới";
            //txtNhapLaiMK.Text = "--Nhập lại mật khẩu mới";
        }



        private void txtMatKhauHT_Click(object sender, EventArgs e)
        {
            //txtMatKhauHT.Text = "";
        }

        private void txtMatKhauMoi_Click(object sender, EventArgs e)
        {
            //txtMatKhauMoi.Text = "";
        }

        private void txtNhapLaiMK_Click(object sender, EventArgs e)
        {
            //txtNhapLaiMK.Text = "";
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
