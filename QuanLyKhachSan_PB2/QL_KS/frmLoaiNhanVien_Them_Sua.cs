﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QL_KS.BUS;
using log4net;

namespace QL_KS
{
    public partial class frmLoaiNhanVien_Them_Sua : DevExpress.XtraEditors.XtraForm
    {
        public delegate void LoadData();
        public LoadData delegateLoadData;
        LoaiNhanVienBUS lnvBUS;
        LoaiNhanVien lnv;
        public string TaiKhoan;
        private string maloai;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmLoaiNhanVien_Them_Sua).Name);
        public frmLoaiNhanVien_Them_Sua()
        {
            maloai = "";
            InitializeComponent();
            textEdit1.ReadOnly = true;
            lnvBUS = new LoaiNhanVienBUS();
            lnv = new LoaiNhanVien();
            LoadDL();
        }
        public frmLoaiNhanVien_Them_Sua(string maloai)
        {
            InitializeComponent();
            this.maloai = maloai;
            lnvBUS = new LoaiNhanVienBUS();
            lnv = new LoaiNhanVien();
            LoadDL();
        }

        private void LoadDL()
        {
            if(maloai != "")
            {
                lnv = lnvBUS.TimKiem(maloai);
                textEdit1.Text = lnv.MaLoaiNV;
                textEdit2.Text = lnv.TenLoaiNV;
                this.Text = "Sửa Loại Nhân Viên";
                btnCapNhatLai.Text = "Sửa";
            }
            else
            {
                textEdit1.Text = lnvBUS.TaoMa();
                textEdit2.Text = "";
                this.Text = "Thêm Loại Nhân Viên";
                btnCapNhatLai.Text = "Thêm";
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void btnCapNhatLai_Click(object sender, EventArgs e)
        {
            lnv.MaLoaiNV = textEdit1.Text;
            lnv.TenLoaiNV = textEdit2.Text;
            if(maloai == "")
            {
                int kq = lnvBUS.Them(lnv);
                if (kq> 0)
                {
                    MessageBox.Show("Thêm Loại nhân viên thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    LuuNhatKyHT.Them(TaiKhoan, "Quản Lý Loai Nhân Viên", "Thêm Loại Nhân Viên");
                }
                else
                {
                    MessageBox.Show("Thêm thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                bool kq =  lnvBUS.Sua(lnv);
                if (kq)
                {
                    MessageBox.Show("Cập nhật Loại nhân viên thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LuuNhatKyHT.Them(TaiKhoan, "Quản Lý Loai Nhân Viên", "Cập Nhật Loại Nhân Viên");
                }
                else
                {
                    MessageBox.Show("Cập nhật thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (delegateLoadData != null)
            {
                delegateLoadData();
            }
            LoadDL();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmLoaiNhanVien_Them_Sua_Load(object sender, EventArgs e)
        {

        }
    }
}