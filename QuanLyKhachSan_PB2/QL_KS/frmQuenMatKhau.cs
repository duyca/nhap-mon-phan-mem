﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using System.Net.Mail;
using log4net;
using NhapDoAn.BUS;
using QL_KS.DAO;

namespace QL_KS
{
    public partial class frmQuenMatKhau : Form
    {
        public frmQuenMatKhau()
        {
            InitializeComponent();
        }
        NhanVienBUS nvBUS = new NhanVienBUS();
      
        List<NhanVien> list_nv = new List<NhanVien>();
        NhanVien nv;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmQuenMatKhau).Name);

        
        private void frmQuenMatKhau_Load(object sender, EventArgs e)
        {
            list_nv = nvBUS.LoadDS();
           
        }

        private void txtTaiKhoan_Leave(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(txtTaiKhoan.Text))
            //{
            //    errorProvider1.SetError(txtTaiKhoan, "Bạn chưa nhập tài khoản");
            //}
            //else
            //{
            //    errorProvider1.Clear();
            //}
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            txtEmail.Text = "";

            if (txtTaiKhoan.Text != string.Empty)
            {
                nv = new NhanVien();
                foreach (NhanVien n in list_nv)
                {
                    if (n.TaiKhoan.Trim() == txtTaiKhoan.Text && n.ConQuanLy == true)
                    {
                        nv = n;
                        break;
                    }
                }
                if (nv.TaiKhoan != null)
                {
                    lbThongBao.Text = "";
                    txtEmail.Text = nv.Email.Trim();

                }
                else
                {
                    lbThongBao.Text = "Tài khoản không tồn tại trong hệ thống";
                    lbThongBao.ForeColor = Color.Red;
                   
                }
            }
            else
            {
                lbThongBao.Text = "Bạn chưa nhập tài khoản";
                lbThongBao.ForeColor = Color.Red;
              
            }
        }

        private void btnNhanMKMoi_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text == "")
            {
                lbThongBao.Text = "Bạn chưa nhập tài khoản";
            
                lbThongBao.ForeColor = Color.Red;
            }
            else if (txtEmail.Text != "")
            {
                Random rd = new Random();
                string mk = rd.Next(99999).ToString();
                try
                {
                    // từ admin gửi đến email, tieu đề đổi mật khẩu, nd đổi mật khẩu
                    MailMessage mail = new MailMessage("phamthixuanhien1994@gmail.com", txtEmail.Text, "Đổi Mật Khẩu", mk);
                    // phương thức hoạt động
                    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

                    // bảo mật cho mật khẩu đăng nhập
                    client.EnableSsl = true;

                    // truyền vào user va pass 
                    client.Credentials = new System.Net.NetworkCredential("phamthixuanhien1994@gmail.com", "hien1987");

                    // gửi
                    client.Send(mail);

                    // cập nhật mật khẩu vào lại trong csdl
                    NhanVien n = new NhanVien();
                    n.MaNV = nv.MaNV;
                    string mahoamk = MaHoaMD5.md5(mk);
                    n.MatKhau = mahoamk;
                    nvBUS.CapNhatMatKhau(n);

                    // thông báo người dùng biết
                    MessageBox.Show("Thay đổi mật khẩu thành công, vui lòng kiểm tra mail!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LuuNhatKyHT.Them(txtTaiKhoan.Text, "Hệ thống", "Quên mật khẩu");
                    frmDang_Nhap fr = new frmDang_Nhap();
                    this.Hide();
                    fr.ShowDialog();
                }
                catch (Exception ex)
                {
                    lbThongBao.Text = "Gửi mail không thành công, vui lòng kiểm tra lại mail!";
                    lbThongBao.ForeColor = Color.Red;
                    _logger.Error(ex.ToString());
                }
            }
            else
            {
                lbThongBao.Text = "Bạn chưa bấm nút lấy mail";
                lbThongBao.ForeColor = Color.Red;
            }
   
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            frmDang_Nhap frmDN = new frmDang_Nhap();
            this.Hide();
            frmDN.ShowDialog();
        }
    }
}