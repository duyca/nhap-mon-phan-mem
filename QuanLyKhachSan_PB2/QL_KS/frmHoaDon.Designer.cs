﻿namespace QL_KS
{
    partial class frmHoaDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gcHoaDon = new DevExpress.XtraGrid.GridControl();
            this.gvHoaDon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MaHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ma_kh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoNgayThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TongTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.hoaDonsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.txtTenKhach = new System.Windows.Forms.TextBox();
            this.blbTenKhach = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.datetimepicker = new System.Windows.Forms.DateTimePicker();
            this.cbTenKH = new System.Windows.Forms.CheckBox();
            this.cbNgayThue = new System.Windows.Forms.CheckBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSoDienThoai = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCMND = new DevExpress.XtraEditors.LabelControl();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblMaKH = new DevExpress.XtraEditors.LabelControl();
            this.lblTenKH = new DevExpress.XtraEditors.LabelControl();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btmTimKiem = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gcHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hoaDonsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gcHoaDon
            // 
            this.gcHoaDon.EmbeddedNavigator.Appearance.BackColor = System.Drawing.Color.White;
            this.gcHoaDon.EmbeddedNavigator.Appearance.ForeColor = System.Drawing.Color.Black;
            this.gcHoaDon.EmbeddedNavigator.Appearance.Options.UseBackColor = true;
            this.gcHoaDon.EmbeddedNavigator.Appearance.Options.UseForeColor = true;
            gridLevelNode1.RelationName = "Level1";
            this.gcHoaDon.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcHoaDon.Location = new System.Drawing.Point(322, 2);
            this.gcHoaDon.MainView = this.gvHoaDon;
            this.gcHoaDon.Name = "gcHoaDon";
            this.gcHoaDon.Size = new System.Drawing.Size(683, 537);
            this.gcHoaDon.TabIndex = 22;
            this.gcHoaDon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvHoaDon,
            this.gridView1});
            // 
            // gvHoaDon
            // 
            this.gvHoaDon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MaHD,
            this.ma_kh,
            this.SoNgayThue,
            this.NgayTT,
            this.TongTien});
            this.gvHoaDon.GridControl = this.gcHoaDon;
            this.gvHoaDon.GroupPanelText = "Hóa đơn";
            this.gvHoaDon.Name = "gvHoaDon";
            this.gvHoaDon.OptionsBehavior.Editable = false;
            this.gvHoaDon.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.True;
            this.gvHoaDon.OptionsView.ShowAutoFilterRow = true;
            this.gvHoaDon.OptionsView.ShowGroupPanel = false;
            this.gvHoaDon.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvHoaDon_RowClick);
            this.gvHoaDon.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.gvHoaDon_MasterRowEmpty);
            this.gvHoaDon.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gvHoaDon_MasterRowGetChildList);
            this.gvHoaDon.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gvHoaDon_MasterRowGetRelationName);
            this.gvHoaDon.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gvHoaDon_MasterRowGetRelationCount);
            this.gvHoaDon.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvHoaDon_FocusedRowChanged);
            // 
            // MaHD
            // 
            this.MaHD.Caption = "Hóa Đơn";
            this.MaHD.FieldName = "MaHD";
            this.MaHD.Name = "MaHD";
            this.MaHD.Visible = true;
            this.MaHD.VisibleIndex = 0;
            // 
            // ma_kh
            // 
            this.ma_kh.Caption = "Mã KH";
            this.ma_kh.FieldName = "MaKH";
            this.ma_kh.Name = "ma_kh";
            // 
            // SoNgayThue
            // 
            this.SoNgayThue.Caption = "Số Giờ Thuê";
            this.SoNgayThue.FieldName = "SoGioThue";
            this.SoNgayThue.Name = "SoNgayThue";
            this.SoNgayThue.Visible = true;
            this.SoNgayThue.VisibleIndex = 1;
            // 
            // NgayTT
            // 
            this.NgayTT.Caption = "Ngày thanh toán";
            this.NgayTT.FieldName = "NgayThanhToan";
            this.NgayTT.Name = "NgayTT";
            this.NgayTT.Visible = true;
            this.NgayTT.VisibleIndex = 2;
            // 
            // TongTien
            // 
            this.TongTien.Caption = "Tổng Tiền";
            this.TongTien.FieldName = "TongTien";
            this.TongTien.Name = "TongTien";
            this.TongTien.Visible = true;
            this.TongTien.VisibleIndex = 3;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcHoaDon;
            this.gridView1.Name = "gridView1";
            // 
            // hoaDonsBindingSource
            // 
            this.hoaDonsBindingSource.DataSource = typeof(QL_KS.HoaDon);
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // txtTenKhach
            // 
            this.txtTenKhach.BackColor = System.Drawing.Color.White;
            this.txtTenKhach.Enabled = false;
            this.txtTenKhach.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKhach.ForeColor = System.Drawing.Color.Black;
            this.txtTenKhach.Location = new System.Drawing.Point(126, 339);
            this.txtTenKhach.Name = "txtTenKhach";
            this.txtTenKhach.Size = new System.Drawing.Size(145, 25);
            this.txtTenKhach.TabIndex = 28;
            // 
            // blbTenKhach
            // 
            this.blbTenKhach.AutoSize = true;
            this.blbTenKhach.BackColor = System.Drawing.Color.White;
            this.blbTenKhach.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blbTenKhach.ForeColor = System.Drawing.Color.Black;
            this.blbTenKhach.Location = new System.Drawing.Point(12, 340);
            this.blbTenKhach.Name = "blbTenKhach";
            this.blbTenKhach.Size = new System.Drawing.Size(105, 17);
            this.blbTenKhach.TabIndex = 29;
            this.blbTenKhach.Text = "Tên Khách Hàng:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(12, 385);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "Từ Ngày:";
            // 
            // datetimepicker
            // 
            this.datetimepicker.BackColor = System.Drawing.Color.White;
            this.datetimepicker.CustomFormat = "dd/MM/yyyy hh:mm";
            this.datetimepicker.Enabled = false;
            this.datetimepicker.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datetimepicker.ForeColor = System.Drawing.Color.Black;
            this.datetimepicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimepicker.Location = new System.Drawing.Point(126, 380);
            this.datetimepicker.Name = "datetimepicker";
            this.datetimepicker.Size = new System.Drawing.Size(145, 25);
            this.datetimepicker.TabIndex = 31;
            // 
            // cbTenKH
            // 
            this.cbTenKH.AutoSize = true;
            this.cbTenKH.BackColor = System.Drawing.Color.White;
            this.cbTenKH.ForeColor = System.Drawing.Color.Black;
            this.cbTenKH.Location = new System.Drawing.Point(288, 343);
            this.cbTenKH.Name = "cbTenKH";
            this.cbTenKH.Size = new System.Drawing.Size(15, 14);
            this.cbTenKH.TabIndex = 1;
            this.cbTenKH.UseVisualStyleBackColor = false;
            this.cbTenKH.CheckedChanged += new System.EventHandler(this.cbTenKH_CheckedChanged);
            // 
            // cbNgayThue
            // 
            this.cbNgayThue.AutoSize = true;
            this.cbNgayThue.BackColor = System.Drawing.Color.White;
            this.cbNgayThue.ForeColor = System.Drawing.Color.Black;
            this.cbNgayThue.Location = new System.Drawing.Point(288, 385);
            this.cbNgayThue.Name = "cbNgayThue";
            this.cbNgayThue.Size = new System.Drawing.Size(15, 14);
            this.cbNgayThue.TabIndex = 2;
            this.cbNgayThue.UseVisualStyleBackColor = false;
            this.cbNgayThue.CheckedChanged += new System.EventHandler(this.cbNgayThue_CheckedChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.BackColor = System.Drawing.Color.White;
            this.dateTimePicker2.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.ForeColor = System.Drawing.Color.Black;
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(126, 420);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(145, 25);
            this.dateTimePicker2.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(12, 425);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 35;
            this.label7.Text = "Đến Ngày:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.White;
            this.checkBox1.Enabled = false;
            this.checkBox1.ForeColor = System.Drawing.Color.Black;
            this.checkBox1.Location = new System.Drawing.Point(288, 425);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.Appearance.BackColor = System.Drawing.Color.White;
            this.lblDiaChi.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblDiaChi.Appearance.Options.UseBackColor = true;
            this.lblDiaChi.Appearance.Options.UseForeColor = true;
            this.lblDiaChi.Location = new System.Drawing.Point(119, 187);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(12, 13);
            this.lblDiaChi.TabIndex = 56;
            this.lblDiaChi.Text = "...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 53;
            this.label2.Text = "Tên khách hàng:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 55;
            this.label3.Text = "Địa chỉ:";
            // 
            // lblSoDienThoai
            // 
            this.lblSoDienThoai.Appearance.BackColor = System.Drawing.Color.White;
            this.lblSoDienThoai.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblSoDienThoai.Appearance.Options.UseBackColor = true;
            this.lblSoDienThoai.Appearance.Options.UseForeColor = true;
            this.lblSoDienThoai.Location = new System.Drawing.Point(119, 231);
            this.lblSoDienThoai.Name = "lblSoDienThoai";
            this.lblSoDienThoai.Size = new System.Drawing.Size(12, 13);
            this.lblSoDienThoai.TabIndex = 58;
            this.lblSoDienThoai.Text = "...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 16);
            this.label1.TabIndex = 51;
            this.label1.Text = "CMND:";
            // 
            // lblCMND
            // 
            this.lblCMND.Appearance.BackColor = System.Drawing.Color.White;
            this.lblCMND.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblCMND.Appearance.Options.UseBackColor = true;
            this.lblCMND.Appearance.Options.UseForeColor = true;
            this.lblCMND.Location = new System.Drawing.Point(119, 146);
            this.lblCMND.Name = "lblCMND";
            this.lblCMND.Size = new System.Drawing.Size(12, 13);
            this.lblCMND.TabIndex = 52;
            this.lblCMND.Text = "...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 16);
            this.label5.TabIndex = 49;
            this.label5.Text = "Mã khách hàng:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(12, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 57;
            this.label4.Text = "Số điện thoại:";
            // 
            // lblMaKH
            // 
            this.lblMaKH.Appearance.BackColor = System.Drawing.Color.White;
            this.lblMaKH.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblMaKH.Appearance.Options.UseBackColor = true;
            this.lblMaKH.Appearance.Options.UseForeColor = true;
            this.lblMaKH.Location = new System.Drawing.Point(119, 60);
            this.lblMaKH.Name = "lblMaKH";
            this.lblMaKH.Size = new System.Drawing.Size(12, 13);
            this.lblMaKH.TabIndex = 50;
            this.lblMaKH.Text = "...";
            // 
            // lblTenKH
            // 
            this.lblTenKH.Appearance.BackColor = System.Drawing.Color.White;
            this.lblTenKH.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblTenKH.Appearance.Options.UseBackColor = true;
            this.lblTenKH.Appearance.Options.UseForeColor = true;
            this.lblTenKH.Location = new System.Drawing.Point(119, 104);
            this.lblTenKH.Name = "lblTenKH";
            this.lblTenKH.Size = new System.Drawing.Size(12, 13);
            this.lblTenKH.TabIndex = 54;
            this.lblTenKH.Text = "...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(12, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 59;
            this.label8.Text = "Thông Tin Khách Hàng";
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.SystemColors.Menu;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSearch.Location = new System.Drawing.Point(39, 269);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(247, 40);
            this.btnSearch.TabIndex = 60;
            this.btnSearch.Text = "Hóa Đơn Trong Ngày";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btmTrongngay_Click);
            // 
            // btmTimKiem
            // 
            this.btmTimKiem.BackColor = System.Drawing.SystemColors.Menu;
            this.btmTimKiem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btmTimKiem.FlatAppearance.BorderSize = 0;
            this.btmTimKiem.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmTimKiem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btmTimKiem.Location = new System.Drawing.Point(41, 454);
            this.btmTimKiem.Name = "btmTimKiem";
            this.btmTimKiem.Size = new System.Drawing.Size(110, 40);
            this.btmTimKiem.TabIndex = 61;
            this.btmTimKiem.Text = "Tìm Kiếm";
            this.btmTimKiem.UseVisualStyleBackColor = false;
            this.btmTimKiem.Click += new System.EventHandler(this.btmTimKiem_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Menu;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button1.Location = new System.Drawing.Point(161, 454);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 40);
            this.button1.TabIndex = 62;
            this.button1.Text = "In Hóa Đơn";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Menu;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Crimson;
            this.button2.Location = new System.Drawing.Point(161, 500);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 40);
            this.button2.TabIndex = 63;
            this.button2.Text = "Đóng";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Menu;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button3.Location = new System.Drawing.Point(41, 500);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 40);
            this.button3.TabIndex = 64;
            this.button3.Text = "Tải Lại";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.btnLoadLai_Click);
            // 
            // frmHoaDon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1004, 551);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btmTimKiem);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblDiaChi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSoDienThoai);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCMND);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblMaKH);
            this.Controls.Add(this.lblTenKH);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbNgayThue);
            this.Controls.Add(this.cbTenKH);
            this.Controls.Add(this.datetimepicker);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.blbTenKhach);
            this.Controls.Add(this.txtTenKhach);
            this.Controls.Add(this.gcHoaDon);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Silver;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmHoaDon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "HÓA ĐƠN";
            this.Load += new System.EventHandler(this.frmHoaDon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hoaDonsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraGrid.GridControl gcHoaDon;
        private DevExpress.XtraGrid.Views.Grid.GridView gvHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn MaHD;
        private DevExpress.XtraGrid.Columns.GridColumn ma_kh;
        private DevExpress.XtraGrid.Columns.GridColumn SoNgayThue;
        private DevExpress.XtraGrid.Columns.GridColumn NgayTT;
        private DevExpress.XtraGrid.Columns.GridColumn TongTien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource hoaDonsBindingSource;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.TextBox txtTenKhach;
        private System.Windows.Forms.Label blbTenKhach;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker datetimepicker;
        private System.Windows.Forms.CheckBox cbTenKH;
        private System.Windows.Forms.CheckBox cbNgayThue;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox1;
        private DevExpress.XtraEditors.LabelControl lblDiaChi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LabelControl lblSoDienThoai;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl lblCMND;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LabelControl lblMaKH;
        private DevExpress.XtraEditors.LabelControl lblTenKH;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btmTimKiem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}