﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using DevExpress.XtraEditors;
using log4net;

namespace QL_KS
{
    public partial class frmTimKiemKH : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmTimKiemKH).Name);
        public string xacnhan;
        public frmTimKiemKH()
        {
            InitializeComponent();
        }
        public string TaiKhoan;
        int idex;
        public static string id = "";

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmTimKiemKH_Load(object sender, EventArgs e)
        {
            LoadKH();
        }

        private void LoadKH()
        {
            var ds = KhachHangBus.LoadDS();
            gcDSKH.DataSource = ds;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtHoTen.Text == "" && txtCMND.Text == "")
            {
                LoadKH();
            }

            if (txtHoTen.Text != "" && txtCMND.Text == "")
            {
                gcDSKH.DataSource = KhachHangBus.SearchName(txtHoTen.Text);
                
            }

            if (txtHoTen.Text == "" && txtCMND.Text != "")
            {
                gcDSKH.DataSource = KhachHangBus.SearchCMND(txtCMND.Text);
            }

            if (txtHoTen.Text != "" && txtCMND.Text != "")
            {
                gcDSKH.DataSource = KhachHangBus.SearchAll(txtHoTen.Text,txtCMND.Text);
            }
        }

        private void txtCMND_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }
            
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            id = gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString();
            var frm = new frmCap_Nhat_KH(id);
            frm.TaiKhoan = TaiKhoan;
            frm.ShowDialog();
            LoadKH();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (idex >= 0)
            {
                DialogResult Result = XtraMessageBox.Show("Bạn có thật sự muốn xóa?", "Hỏi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Result == DialogResult.Yes)
                {
                    try
                    {
                        if (KhachHangBus.TimKiemHoaDonCuaKH(gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString()) == true || KhachHangBus.TimKiemPhieuThueCuaKH(gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString()) == true)
                        {
                            XtraMessageBox.Show("Khách hàng đã phát sinh hóa đơn hoặc phiếu thuê, không thể xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            KhachHangBus.Xoa(gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString());
                            LoadKH();
                            XtraMessageBox.Show("Xóa khách hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LuuNhatKyHT.Them(TaiKhoan, "Quản Lý Khách hàng", "Xóa khách hàng");
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.ToString(), "Lỗi xóa khách hàng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _logger.Error(ex.ToString());
                    }
                }
            }
            else
                XtraMessageBox.Show("Chưa chọn dòng cần xóa!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
           
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtHoTen.Text = "";
            txtCMND.Text = "";

            LoadKH();
        }
        public delegate void LayMaKH(string maKH);
        public LayMaKH maKH;
        private void gcDSKH_DoubleClick(object sender, EventArgs e)
        {
            string ma = gvData.GetRowCellValue(gvData.FocusedRowHandle, "MaKH").ToString();
            
            if(xacnhan == "1")
            {
                maKH(ma);
                Close();
            }
        }
    }
}