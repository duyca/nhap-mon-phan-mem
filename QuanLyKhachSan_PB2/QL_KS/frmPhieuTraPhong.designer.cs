﻿namespace QL_KS
{
    partial class frmPhieuTraPhong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPhieuTraPhong));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDonGia = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtLoaiPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPhuThu = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lbPhuThu = new System.Windows.Forms.Label();
            this.cbbPhong = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtGioHienTai = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNgayThue = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lbNgayThue = new System.Windows.Forms.Label();
            this.lbThongBaoSoNguoi = new System.Windows.Forms.Label();
            this.txtSoNguoi = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.label13 = new System.Windows.Forms.Label();
            this.cbbHinhThucThue = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbThongBaoTen = new System.Windows.Forms.Label();
            this.txtSoDT = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtCMND = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtDiaChi = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label8 = new System.Windows.Forms.Label();
            this.cbbLoaiKhachHang = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTenKhachHang = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbSoGio = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbTongTien = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLuuThayDoi = new System.Windows.Forms.Button();
            this.btnHuyBo = new System.Windows.Forms.Button();
            this.btnTraPhong = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panel2);
            this.panelControl1.Controls.Add(this.panel1);
            this.panelControl1.Controls.Add(this.label15);
            this.panelControl1.Controls.Add(this.txtDonGia);
            this.panelControl1.Controls.Add(this.txtLoaiPhong);
            this.panelControl1.Controls.Add(this.label12);
            this.panelControl1.Controls.Add(this.txtPhuThu);
            this.panelControl1.Controls.Add(this.lbPhuThu);
            this.panelControl1.Controls.Add(this.cbbPhong);
            this.panelControl1.Controls.Add(this.txtGioHienTai);
            this.panelControl1.Controls.Add(this.label9);
            this.panelControl1.Controls.Add(this.txtNgayThue);
            this.panelControl1.Controls.Add(this.lbNgayThue);
            this.panelControl1.Controls.Add(this.lbThongBaoSoNguoi);
            this.panelControl1.Controls.Add(this.txtSoNguoi);
            this.panelControl1.Controls.Add(this.label13);
            this.panelControl1.Controls.Add(this.cbbHinhThucThue);
            this.panelControl1.Controls.Add(this.label11);
            this.panelControl1.Controls.Add(this.label10);
            this.panelControl1.Controls.Add(this.lbThongBaoTen);
            this.panelControl1.Controls.Add(this.txtSoDT);
            this.panelControl1.Controls.Add(this.txtCMND);
            this.panelControl1.Controls.Add(this.txtDiaChi);
            this.panelControl1.Controls.Add(this.label8);
            this.panelControl1.Controls.Add(this.cbbLoaiKhachHang);
            this.panelControl1.Controls.Add(this.label7);
            this.panelControl1.Controls.Add(this.txtTenKhachHang);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.lbSoGio);
            this.panelControl1.Controls.Add(this.label14);
            this.panelControl1.Controls.Add(this.lbTongTien);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.btnLuuThayDoi);
            this.panelControl1.Controls.Add(this.btnHuyBo);
            this.panelControl1.Controls.Add(this.btnTraPhong);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(648, 566);
            this.panelControl1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(9, 210);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(630, 42);
            this.panel2.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(274, 30);
            this.label2.TabIndex = 0;
            this.label2.Text = "THÔNG TIN THUÊ PHÒNG";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(9, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(630, 41);
            this.panel1.TabIndex = 65;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(280, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN KHÁCH HÀNG";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(328, 401);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 17);
            this.label15.TabIndex = 64;
            this.label15.Text = "Đơn Giá:";
            // 
            // txtDonGia
            // 
            this.txtDonGia.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDonGia.Border.Class = "TextBoxBorder";
            this.txtDonGia.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDonGia.DisabledBackColor = System.Drawing.Color.White;
            this.txtDonGia.Enabled = false;
            this.txtDonGia.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGia.ForeColor = System.Drawing.Color.Black;
            this.txtDonGia.Location = new System.Drawing.Point(444, 399);
            this.txtDonGia.MaxLength = 100;
            this.txtDonGia.Name = "txtDonGia";
            this.txtDonGia.PreventEnterBeep = true;
            this.txtDonGia.Size = new System.Drawing.Size(184, 22);
            this.txtDonGia.TabIndex = 12;
            // 
            // txtLoaiPhong
            // 
            this.txtLoaiPhong.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtLoaiPhong.Border.Class = "TextBoxBorder";
            this.txtLoaiPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtLoaiPhong.DisabledBackColor = System.Drawing.Color.White;
            this.txtLoaiPhong.Enabled = false;
            this.txtLoaiPhong.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaiPhong.ForeColor = System.Drawing.Color.Black;
            this.txtLoaiPhong.Location = new System.Drawing.Point(136, 315);
            this.txtLoaiPhong.MaxLength = 100;
            this.txtLoaiPhong.Name = "txtLoaiPhong";
            this.txtLoaiPhong.PreventEnterBeep = true;
            this.txtLoaiPhong.Size = new System.Drawing.Size(184, 22);
            this.txtLoaiPhong.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Enabled = false;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 317);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 17);
            this.label12.TabIndex = 61;
            this.label12.Text = "Loại phòng";
            // 
            // txtPhuThu
            // 
            this.txtPhuThu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPhuThu.Border.Class = "TextBoxBorder";
            this.txtPhuThu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPhuThu.DisabledBackColor = System.Drawing.Color.White;
            this.txtPhuThu.Enabled = false;
            this.txtPhuThu.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuThu.ForeColor = System.Drawing.Color.Black;
            this.txtPhuThu.Location = new System.Drawing.Point(444, 271);
            this.txtPhuThu.MaxLength = 100;
            this.txtPhuThu.Name = "txtPhuThu";
            this.txtPhuThu.PreventEnterBeep = true;
            this.txtPhuThu.Size = new System.Drawing.Size(184, 22);
            this.txtPhuThu.TabIndex = 9;
            // 
            // lbPhuThu
            // 
            this.lbPhuThu.AutoSize = true;
            this.lbPhuThu.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhuThu.Location = new System.Drawing.Point(328, 276);
            this.lbPhuThu.Name = "lbPhuThu";
            this.lbPhuThu.Size = new System.Drawing.Size(54, 17);
            this.lbPhuThu.TabIndex = 59;
            this.lbPhuThu.Text = "Phụ Thu";
            // 
            // cbbPhong
            // 
            this.cbbPhong.DisplayMember = "Text";
            this.cbbPhong.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbPhong.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPhong.ForeColor = System.Drawing.Color.Black;
            this.cbbPhong.FormattingEnabled = true;
            this.cbbPhong.ItemHeight = 17;
            this.cbbPhong.Location = new System.Drawing.Point(137, 272);
            this.cbbPhong.Name = "cbbPhong";
            this.cbbPhong.Size = new System.Drawing.Size(185, 23);
            this.cbbPhong.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbPhong.TabIndex = 5;
            this.cbbPhong.SelectedIndexChanged += new System.EventHandler(this.cbbPhong_SelectedIndexChanged);
            // 
            // txtGioHienTai
            // 
            this.txtGioHienTai.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGioHienTai.Border.Class = "TextBoxBorder";
            this.txtGioHienTai.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGioHienTai.DisabledBackColor = System.Drawing.Color.White;
            this.txtGioHienTai.Enabled = false;
            this.txtGioHienTai.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGioHienTai.ForeColor = System.Drawing.Color.Black;
            this.txtGioHienTai.Location = new System.Drawing.Point(444, 358);
            this.txtGioHienTai.MaxLength = 100;
            this.txtGioHienTai.Name = "txtGioHienTai";
            this.txtGioHienTai.PreventEnterBeep = true;
            this.txtGioHienTai.Size = new System.Drawing.Size(184, 22);
            this.txtGioHienTai.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(327, 360);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 56;
            this.label9.Text = "Giờ Hiện Tại:";
            // 
            // txtNgayThue
            // 
            this.txtNgayThue.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtNgayThue.Border.Class = "TextBoxBorder";
            this.txtNgayThue.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNgayThue.DisabledBackColor = System.Drawing.Color.White;
            this.txtNgayThue.Enabled = false;
            this.txtNgayThue.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayThue.ForeColor = System.Drawing.Color.Black;
            this.txtNgayThue.Location = new System.Drawing.Point(137, 356);
            this.txtNgayThue.MaxLength = 100;
            this.txtNgayThue.Name = "txtNgayThue";
            this.txtNgayThue.PreventEnterBeep = true;
            this.txtNgayThue.Size = new System.Drawing.Size(184, 22);
            this.txtNgayThue.TabIndex = 7;
            // 
            // lbNgayThue
            // 
            this.lbNgayThue.AutoSize = true;
            this.lbNgayThue.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayThue.Location = new System.Drawing.Point(14, 360);
            this.lbNgayThue.Name = "lbNgayThue";
            this.lbNgayThue.Size = new System.Drawing.Size(60, 17);
            this.lbNgayThue.TabIndex = 54;
            this.lbNgayThue.Text = "Giờ Thuê";
            // 
            // lbThongBaoSoNguoi
            // 
            this.lbThongBaoSoNguoi.AutoSize = true;
            this.lbThongBaoSoNguoi.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBaoSoNguoi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBaoSoNguoi.Location = new System.Drawing.Point(441, 296);
            this.lbThongBaoSoNguoi.Name = "lbThongBaoSoNguoi";
            this.lbThongBaoSoNguoi.Size = new System.Drawing.Size(144, 17);
            this.lbThongBaoSoNguoi.TabIndex = 53;
            this.lbThongBaoSoNguoi.Text = "Hãy nhập vào số người";
            this.lbThongBaoSoNguoi.Visible = false;
            // 
            // txtSoNguoi
            // 
            // 
            // 
            // 
            this.txtSoNguoi.BackgroundStyle.Class = "TextBoxBorder";
            this.txtSoNguoi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSoNguoi.ButtonClear.Visible = true;
            this.txtSoNguoi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoNguoi.Location = new System.Drawing.Point(444, 315);
            this.txtSoNguoi.Mask = "00";
            this.txtSoNguoi.Name = "txtSoNguoi";
            this.txtSoNguoi.Size = new System.Drawing.Size(184, 20);
            this.txtSoNguoi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtSoNguoi.TabIndex = 10;
            this.txtSoNguoi.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(327, 317);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 17);
            this.label13.TabIndex = 51;
            this.label13.Text = "Số Người Thuê(*):";
            // 
            // cbbHinhThucThue
            // 
            this.cbbHinhThucThue.DisplayMember = "Text";
            this.cbbHinhThucThue.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbHinhThucThue.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbHinhThucThue.ForeColor = System.Drawing.Color.Black;
            this.cbbHinhThucThue.FormattingEnabled = true;
            this.cbbHinhThucThue.ItemHeight = 17;
            this.cbbHinhThucThue.Location = new System.Drawing.Point(136, 398);
            this.cbbHinhThucThue.Name = "cbbHinhThucThue";
            this.cbbHinhThucThue.Size = new System.Drawing.Size(185, 23);
            this.cbbHinhThucThue.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbHinhThucThue.TabIndex = 8;
            this.cbbHinhThucThue.SelectedIndexChanged += new System.EventHandler(this.cbbHinhThucThue_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SteelBlue;
            this.label11.Location = new System.Drawing.Point(13, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 21);
            this.label11.TabIndex = 48;
            this.label11.Text = "Phòng: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 401);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 17);
            this.label10.TabIndex = 47;
            this.label10.Text = "Hình Thức Thuê:";
            // 
            // lbThongBaoTen
            // 
            this.lbThongBaoTen.AutoSize = true;
            this.lbThongBaoTen.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBaoTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBaoTen.Location = new System.Drawing.Point(137, 72);
            this.lbThongBaoTen.Name = "lbThongBaoTen";
            this.lbThongBaoTen.Size = new System.Drawing.Size(180, 17);
            this.lbThongBaoTen.TabIndex = 46;
            this.lbThongBaoTen.Text = "Hãy nhập vào tên khách hàng";
            this.lbThongBaoTen.Visible = false;
            // 
            // txtSoDT
            // 
            // 
            // 
            // 
            this.txtSoDT.BackgroundStyle.Class = "TextBoxBorder";
            this.txtSoDT.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSoDT.ButtonClear.Visible = true;
            this.txtSoDT.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDT.Location = new System.Drawing.Point(140, 131);
            this.txtSoDT.Mask = "999 000 0000";
            this.txtSoDT.Name = "txtSoDT";
            this.txtSoDT.Size = new System.Drawing.Size(184, 20);
            this.txtSoDT.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtSoDT.TabIndex = 1;
            this.txtSoDT.Text = "";
            // 
            // txtCMND
            // 
            // 
            // 
            // 
            this.txtCMND.BackgroundStyle.Class = "TextBoxBorder";
            this.txtCMND.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCMND.ButtonClear.Visible = true;
            this.txtCMND.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCMND.Location = new System.Drawing.Point(140, 166);
            this.txtCMND.Mask = "000000000000";
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.Size = new System.Drawing.Size(184, 20);
            this.txtCMND.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtCMND.TabIndex = 2;
            this.txtCMND.Text = "";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDiaChi.Border.Class = "TextBoxBorder";
            this.txtDiaChi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDiaChi.DisabledBackColor = System.Drawing.Color.White;
            this.txtDiaChi.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.Black;
            this.txtDiaChi.Location = new System.Drawing.Point(447, 132);
            this.txtDiaChi.MaxLength = 1000;
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.PreventEnterBeep = true;
            this.txtDiaChi.Size = new System.Drawing.Size(189, 52);
            this.txtDiaChi.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(330, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 17);
            this.label8.TabIndex = 42;
            this.label8.Text = "Địa Chỉ:";
            // 
            // cbbLoaiKhachHang
            // 
            this.cbbLoaiKhachHang.DisplayMember = "Text";
            this.cbbLoaiKhachHang.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbLoaiKhachHang.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiKhachHang.ForeColor = System.Drawing.Color.Black;
            this.cbbLoaiKhachHang.FormattingEnabled = true;
            this.cbbLoaiKhachHang.ItemHeight = 17;
            this.cbbLoaiKhachHang.Location = new System.Drawing.Point(447, 90);
            this.cbbLoaiKhachHang.Name = "cbbLoaiKhachHang";
            this.cbbLoaiKhachHang.Size = new System.Drawing.Size(189, 23);
            this.cbbLoaiKhachHang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbLoaiKhachHang.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(330, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 17);
            this.label7.TabIndex = 40;
            this.label7.Text = "Loại Khách Hàng:";
            // 
            // txtTenKhachHang
            // 
            this.txtTenKhachHang.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTenKhachHang.Border.Class = "TextBoxBorder";
            this.txtTenKhachHang.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenKhachHang.DisabledBackColor = System.Drawing.Color.White;
            this.txtTenKhachHang.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKhachHang.ForeColor = System.Drawing.Color.Black;
            this.txtTenKhachHang.Location = new System.Drawing.Point(140, 91);
            this.txtTenKhachHang.MaxLength = 100;
            this.txtTenKhachHang.Name = "txtTenKhachHang";
            this.txtTenKhachHang.PreventEnterBeep = true;
            this.txtTenKhachHang.Size = new System.Drawing.Size(184, 22);
            this.txtTenKhachHang.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 17);
            this.label5.TabIndex = 38;
            this.label5.Text = "Số Điện Thoại:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 37;
            this.label4.Text = "Số CMND:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 36;
            this.label3.Text = "Tên Khách Hàng(*):";
            // 
            // lbSoGio
            // 
            this.lbSoGio.AutoSize = true;
            this.lbSoGio.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoGio.Location = new System.Drawing.Point(441, 449);
            this.lbSoGio.Name = "lbSoGio";
            this.lbSoGio.Size = new System.Drawing.Size(46, 17);
            this.lbSoGio.TabIndex = 33;
            this.lbSoGio.Text = "12 Giờ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(327, 449);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 17);
            this.label14.TabIndex = 32;
            this.label14.Text = "Số Giờ Đã Thuê: ";
            // 
            // lbTongTien
            // 
            this.lbTongTien.AutoSize = true;
            this.lbTongTien.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongTien.ForeColor = System.Drawing.Color.Tomato;
            this.lbTongTien.Location = new System.Drawing.Point(440, 476);
            this.lbTongTien.Name = "lbTongTien";
            this.lbTongTien.Size = new System.Drawing.Size(108, 21);
            this.lbTongTien.TabIndex = 31;
            this.lbTongTien.Text = "120,000 VND";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(327, 476);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 21);
            this.label6.TabIndex = 30;
            this.label6.Text = "Tổng Tiền:";
            // 
            // btnLuuThayDoi
            // 
            this.btnLuuThayDoi.BackColor = System.Drawing.SystemColors.Menu;
            this.btnLuuThayDoi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLuuThayDoi.FlatAppearance.BorderSize = 0;
            this.btnLuuThayDoi.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuThayDoi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnLuuThayDoi.Location = new System.Drawing.Point(20, 514);
            this.btnLuuThayDoi.Name = "btnLuuThayDoi";
            this.btnLuuThayDoi.Size = new System.Drawing.Size(121, 40);
            this.btnLuuThayDoi.TabIndex = 13;
            this.btnLuuThayDoi.Text = "Lưu Thay Đổi";
            this.btnLuuThayDoi.UseVisualStyleBackColor = false;
            this.btnLuuThayDoi.Click += new System.EventHandler(this.btnLuuThayDoi_Click);
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.BackColor = System.Drawing.SystemColors.Menu;
            this.btnHuyBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHuyBo.FlatAppearance.BorderSize = 0;
            this.btnHuyBo.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.ForeColor = System.Drawing.Color.Crimson;
            this.btnHuyBo.Location = new System.Drawing.Point(529, 514);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(110, 40);
            this.btnHuyBo.TabIndex = 15;
            this.btnHuyBo.Text = "Hủy Bỏ";
            this.btnHuyBo.UseVisualStyleBackColor = false;
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnTraPhong
            // 
            this.btnTraPhong.BackColor = System.Drawing.SystemColors.Menu;
            this.btnTraPhong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTraPhong.FlatAppearance.BorderSize = 0;
            this.btnTraPhong.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraPhong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnTraPhong.Location = new System.Drawing.Point(413, 514);
            this.btnTraPhong.Name = "btnTraPhong";
            this.btnTraPhong.Size = new System.Drawing.Size(110, 40);
            this.btnTraPhong.TabIndex = 14;
            this.btnTraPhong.Text = "Trả Phòng";
            this.btnTraPhong.UseVisualStyleBackColor = false;
            this.btnTraPhong.Click += new System.EventHandler(this.btnTraPhong_Click);
            // 
            // frmPhieuTraPhong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(648, 566);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Office 2013";
            this.Name = "frmPhieuTraPhong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Phiếu Trả Phòng";
            this.Load += new System.EventHandler(this.frmPhieuTraPhong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Button btnHuyBo;
        private System.Windows.Forms.Button btnTraPhong;
        private System.Windows.Forms.Button btnLuuThayDoi;
        private System.Windows.Forms.Label lbSoGio;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbTongTien;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbThongBaoTen;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtSoDT;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtCMND;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDiaChi;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbLoaiKhachHang;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenKhachHang;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNgayThue;
        private System.Windows.Forms.Label lbNgayThue;
        private System.Windows.Forms.Label lbThongBaoSoNguoi;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtSoNguoi;
        private System.Windows.Forms.Label label13;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbHinhThucThue;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGioHienTai;
        private System.Windows.Forms.Label label9;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbPhong;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPhuThu;
        private System.Windows.Forms.Label lbPhuThu;
        private DevComponents.DotNetBar.Controls.TextBoxX txtLoaiPhong;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDonGia;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
    }
}