﻿namespace QL_KS
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.XtraBars.Ribbon.ReduceOperation reduceOperation1 = new DevExpress.XtraBars.Ribbon.ReduceOperation();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.ribbonPage6 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.img_Phong = new System.Windows.Forms.ImageList(this.components);
            this.rbbItemHeThong = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rbbSaoLuuVaPhucHoi = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbItemLuuDL = new DevExpress.XtraBars.BarButtonItem();
            this.bbItemPhucHoi = new DevExpress.XtraBars.BarButtonItem();
            this.rbbNhatKyHoatDong = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbItemNhatKyHeThong = new DevExpress.XtraBars.BarButtonItem();
            this.bbPhanQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.bbItemPhanQuye = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rbbQLNhanVien = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiNhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLoaiNhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.rbbQuanLyPhong = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbPhong = new DevExpress.XtraBars.BarButtonItem();
            this.bbLoaiPhong = new DevExpress.XtraBars.BarButtonItem();
            this.rbbQuanLyHinhThuc = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbHinhThucThue = new DevExpress.XtraBars.BarButtonItem();
            this.rbbPhanQuyen = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbItemPhanQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.rbbQuanLyKhachHang = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.btnKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnLoaiKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiBCDT = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiMatDo = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.btnDSKH = new DevExpress.XtraBars.BarButtonItem();
            this.btnTimKiemKH = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbHoaDonTT = new DevExpress.XtraBars.BarButtonItem();
            this.rbbThayDoiQuyDinh = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbThayDoiQuyDinh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbItemXemVaCapNhatThongTinCaNhan = new DevExpress.XtraBars.BarButtonItem();
            this.bbItemDoiMatKhau = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbItemDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.bbItemThoatChuongTrinh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.rbbTenDangNhap = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.bbTenDangNhap = new DevExpress.XtraBars.BarButtonItem();
            this.lv_DSPhong = new System.Windows.Forms.ListView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbLoaiPhong = new System.Windows.Forms.Label();
            this.Danh_Sach_Loai_Phong = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lbSoNguoi = new System.Windows.Forms.Label();
            this.lbpreSoNguoi = new System.Windows.Forms.Label();
            this.lbTongTien = new System.Windows.Forms.Label();
            this.lbpreTongTien = new System.Windows.Forms.Label();
            this.lbHinhThuc = new System.Windows.Forms.Label();
            this.lbpreHinhThuc = new System.Windows.Forms.Label();
            this.lbTenPhong = new System.Windows.Forms.Label();
            this.lbpreTenPhong = new System.Windows.Forms.Label();
            this.lbPhongTrong = new System.Windows.Forms.Label();
            this.lbPhongThue = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // ribbonPage6
            // 
            this.ribbonPage6.Name = "ribbonPage6";
            this.ribbonPage6.Text = "ribbonPage6";
            // 
            // img_Phong
            // 
            this.img_Phong.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("img_Phong.ImageStream")));
            this.img_Phong.TransparentColor = System.Drawing.Color.Transparent;
            this.img_Phong.Images.SetKeyName(0, "Chua_Thue.png");
            this.img_Phong.Images.SetKeyName(1, "Da_Thue.png");
            // 
            // rbbItemHeThong
            // 
            this.rbbItemHeThong.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rbbSaoLuuVaPhucHoi,
            this.rbbNhatKyHoatDong});
            this.rbbItemHeThong.Name = "rbbItemHeThong";
            this.rbbItemHeThong.Text = "HỆ THỐNG";
            // 
            // rbbSaoLuuVaPhucHoi
            // 
            this.rbbSaoLuuVaPhucHoi.ItemLinks.Add(this.bbItemLuuDL);
            this.rbbSaoLuuVaPhucHoi.ItemLinks.Add(this.bbItemPhucHoi);
            this.rbbSaoLuuVaPhucHoi.Name = "rbbSaoLuuVaPhucHoi";
            this.rbbSaoLuuVaPhucHoi.Text = "SAO LƯU VÀ PHỤC HỒI";
            // 
            // bbItemLuuDL
            // 
            this.bbItemLuuDL.Caption = "LƯU DỮ LIỆU";
            this.bbItemLuuDL.Id = 12;
            this.bbItemLuuDL.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemLuuDL.ImageOptions.LargeImage")));
            this.bbItemLuuDL.Name = "bbItemLuuDL";
            this.bbItemLuuDL.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemLuuDL_ItemClick);
            // 
            // bbItemPhucHoi
            // 
            this.bbItemPhucHoi.Caption = "PHỤC HỒI DỮ LIỆU";
            this.bbItemPhucHoi.Id = 13;
            this.bbItemPhucHoi.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemPhucHoi.ImageOptions.LargeImage")));
            this.bbItemPhucHoi.Name = "bbItemPhucHoi";
            this.bbItemPhucHoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemPhucHoi_ItemClick);
            // 
            // rbbNhatKyHoatDong
            // 
            this.rbbNhatKyHoatDong.ItemLinks.Add(this.bbItemNhatKyHeThong);
            this.rbbNhatKyHoatDong.Name = "rbbNhatKyHoatDong";
            this.rbbNhatKyHoatDong.Text = "NHẬT KÝ HOẠT ĐỘNG";
            // 
            // bbItemNhatKyHeThong
            // 
            this.bbItemNhatKyHeThong.Caption = "NHẬT KÝ HỆ THỐNG";
            this.bbItemNhatKyHeThong.Id = 14;
            this.bbItemNhatKyHeThong.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemNhatKyHeThong.ImageOptions.LargeImage")));
            this.bbItemNhatKyHeThong.Name = "bbItemNhatKyHeThong";
            this.bbItemNhatKyHeThong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemNhatKyHeThong_ItemClick);
            // 
            // bbPhanQuyen
            // 
            this.bbPhanQuyen.Caption = "Phân quyền";
            this.bbPhanQuyen.Id = 27;
            this.bbPhanQuyen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbPhanQuyen.ImageOptions.Image")));
            this.bbPhanQuyen.Name = "bbPhanQuyen";
            this.bbPhanQuyen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbPhanQuyen_ItemClick);
            // 
            // bbItemPhanQuye
            // 
            this.bbItemPhanQuye.Caption = "Phân Quyền";
            this.bbItemPhanQuye.Id = 24;
            this.bbItemPhanQuye.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemPhanQuye.ImageOptions.LargeImage")));
            this.bbItemPhanQuye.Name = "bbItemPhanQuye";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rbbQLNhanVien,
            this.rbbQuanLyPhong,
            this.rbbQuanLyHinhThuc,
            this.rbbPhanQuyen,
            this.rbbQuanLyKhachHang});
            this.ribbonPage3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage3.Image")));
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Quản Lý";
            // 
            // rbbQLNhanVien
            // 
            this.rbbQLNhanVien.ItemLinks.Add(this.bbiNhanVien);
            this.rbbQLNhanVien.ItemLinks.Add(this.bbiLoaiNhanVien);
            this.rbbQLNhanVien.Name = "rbbQLNhanVien";
            this.rbbQLNhanVien.Text = "Quản Lý Nhân Viên";
            // 
            // bbiNhanVien
            // 
            this.bbiNhanVien.Caption = "Nhân Viên";
            this.bbiNhanVien.Id = 10;
            this.bbiNhanVien.ImageOptions.Image = global::QL_KS.Properties.Resources.Aha_Soft_Free_Large_Boss_Manager;
            this.bbiNhanVien.ImageOptions.LargeImage = global::QL_KS.Properties.Resources.Aha_Soft_Free_Large_Boss_Manager;
            this.bbiNhanVien.Name = "bbiNhanVien";
            this.bbiNhanVien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNhanVien_ItemClick);
            // 
            // bbiLoaiNhanVien
            // 
            this.bbiLoaiNhanVien.Caption = "Loại Nhân Viên";
            this.bbiLoaiNhanVien.Id = 11;
            this.bbiLoaiNhanVien.ImageOptions.Image = global::QL_KS.Properties.Resources.Aha_Soft_Free_Large_Boss_Manager;
            this.bbiLoaiNhanVien.ImageOptions.LargeImage = global::QL_KS.Properties.Resources.Icons_Land_Vista_People_Groups_Meeting_Dark;
            this.bbiLoaiNhanVien.Name = "bbiLoaiNhanVien";
            this.bbiLoaiNhanVien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoaiNhanVien_ItemClick);
            // 
            // rbbQuanLyPhong
            // 
            this.rbbQuanLyPhong.ItemLinks.Add(this.bbPhong);
            this.rbbQuanLyPhong.ItemLinks.Add(this.bbLoaiPhong);
            this.rbbQuanLyPhong.Name = "rbbQuanLyPhong";
            this.rbbQuanLyPhong.Text = "Quản Lý Phòng";
            // 
            // bbPhong
            // 
            this.bbPhong.Caption = "Phòng";
            this.bbPhong.Id = 19;
            this.bbPhong.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbPhong.ImageOptions.LargeImage")));
            this.bbPhong.Name = "bbPhong";
            this.bbPhong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbPhong_ItemClick);
            // 
            // bbLoaiPhong
            // 
            this.bbLoaiPhong.Caption = "Loại Phòng";
            this.bbLoaiPhong.Id = 20;
            this.bbLoaiPhong.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbLoaiPhong.ImageOptions.LargeImage")));
            this.bbLoaiPhong.Name = "bbLoaiPhong";
            this.bbLoaiPhong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbLoaiPhong_ItemClick);
            // 
            // rbbQuanLyHinhThuc
            // 
            this.rbbQuanLyHinhThuc.ItemLinks.Add(this.bbHinhThucThue);
            this.rbbQuanLyHinhThuc.Name = "rbbQuanLyHinhThuc";
            this.rbbQuanLyHinhThuc.Text = "Quản Lý Hình Thức Thuê Phòng";
            // 
            // bbHinhThucThue
            // 
            this.bbHinhThucThue.Caption = "Hình Thức Thuê";
            this.bbHinhThucThue.Id = 21;
            this.bbHinhThucThue.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbHinhThucThue.ImageOptions.Image")));
            this.bbHinhThucThue.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbHinhThucThue.ImageOptions.LargeImage")));
            this.bbHinhThucThue.Name = "bbHinhThucThue";
            this.bbHinhThucThue.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // rbbPhanQuyen
            // 
            this.rbbPhanQuyen.ItemLinks.Add(this.bbItemPhanQuyen);
            this.rbbPhanQuyen.Name = "rbbPhanQuyen";
            this.rbbPhanQuyen.Text = "Phân Quyền";
            // 
            // bbItemPhanQuyen
            // 
            this.bbItemPhanQuyen.Caption = "Phân Quyền";
            this.bbItemPhanQuyen.Id = 23;
            this.bbItemPhanQuyen.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemPhanQuyen.ImageOptions.LargeImage")));
            this.bbItemPhanQuyen.Name = "bbItemPhanQuyen";
            this.bbItemPhanQuyen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemPhanQuyen_ItemClick);
            // 
            // rbbQuanLyKhachHang
            // 
            this.rbbQuanLyKhachHang.ItemLinks.Add(this.btnKhachHang);
            this.rbbQuanLyKhachHang.ItemLinks.Add(this.btnLoaiKhachHang);
            this.rbbQuanLyKhachHang.Name = "rbbQuanLyKhachHang";
            this.rbbQuanLyKhachHang.Text = "Quản Lý Khách Hàng";
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Caption = "Khách Hàng";
            this.btnKhachHang.Id = 25;
            this.btnKhachHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnKhachHang.ImageOptions.LargeImage")));
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhachHang_ItemClick);
            // 
            // btnLoaiKhachHang
            // 
            this.btnLoaiKhachHang.Caption = "Loại Khách Hàng";
            this.btnLoaiKhachHang.Id = 26;
            this.btnLoaiKhachHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnLoaiKhachHang.ImageOptions.LargeImage")));
            this.btnLoaiKhachHang.Name = "btnLoaiKhachHang";
            this.btnLoaiKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLoaiKhachHang_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "barButtonItem8";
            this.barButtonItem8.Id = 32;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup5,
            this.ribbonPageGroup4});
            this.ribbonPage2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage2.Image")));
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Báo Cáo - Thống Kê";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.bbiBCDT);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Báo Cáo";
            // 
            // bbiBCDT
            // 
            this.bbiBCDT.Caption = "Báo cáo doanh thu";
            this.bbiBCDT.Id = 7;
            this.bbiBCDT.ImageOptions.LargeImage = global::QL_KS.Properties.Resources.Wwalczyszyn_Android_Style_Calendar;
            this.bbiBCDT.Name = "bbiBCDT";
            this.bbiBCDT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBCDT_ItemClick);
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.bbiMatDo);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Báo Cáo Mật Độ";
            // 
            // bbiMatDo
            // 
            this.bbiMatDo.Caption = "Mật độ sử dụng phòng";
            this.bbiMatDo.Id = 9;
            this.bbiMatDo.ImageOptions.LargeImage = global::QL_KS.Properties.Resources.Custom_Icon_Design_Pretty_Office_9_Edit_file;
            this.bbiMatDo.Name = "bbiMatDo";
            this.bbiMatDo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMatDo_ItemClick);
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Thống Kê";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Thống Kê Doanh Thu";
            this.barButtonItem7.Id = 6;
            this.barButtonItem7.ImageOptions.LargeImage = global::QL_KS.Properties.Resources.Aha_Soft_Large_Seo_SEO;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3,
            this.ribbonPageGroup1,
            this.ribbonPageGroup6,
            this.rbbThayDoiQuyDinh,
            this.ribbonPageGroup10,
            this.ribbonPageGroup11});
            this.ribbonPage1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage1.Image")));
            this.ribbonPage1.Name = "ribbonPage1";
            reduceOperation1.Behavior = DevExpress.XtraBars.Ribbon.ReduceOperationBehavior.Single;
            reduceOperation1.Group = null;
            reduceOperation1.ItemLinkIndex = 0;
            reduceOperation1.ItemLinksCount = 0;
            reduceOperation1.Operation = DevExpress.XtraBars.Ribbon.ReduceOperationType.LargeButtons;
            this.ribbonPage1.ReduceOperations.Add(reduceOperation1);
            this.ribbonPage1.Text = "Khách sạn";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Giao Dien";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 2;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Glyph = global::QL_KS.Properties.Resources.home_icon;
            this.ribbonPageGroup1.ItemLinks.Add(this.btnDSKH);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnTimKiemKH);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Khách hàng";
            // 
            // btnDSKH
            // 
            this.btnDSKH.Caption = "Danh sách khách hàng";
            this.btnDSKH.Id = 1;
            this.btnDSKH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDSKH.ImageOptions.Image")));
            this.btnDSKH.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnDSKH.ImageOptions.LargeImage")));
            this.btnDSKH.Name = "btnDSKH";
            this.btnDSKH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDSKH_ItemClick);
            // 
            // btnTimKiemKH
            // 
            this.btnTimKiemKH.Caption = "Tìm kiếm khách hàng";
            this.btnTimKiemKH.Id = 2;
            this.btnTimKiemKH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiemKH.ImageOptions.Image")));
            this.btnTimKiemKH.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTimKiemKH.ImageOptions.LargeImage")));
            this.btnTimKiemKH.Name = "btnTimKiemKH";
            this.btnTimKiemKH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTimKiemKH_ItemClick);
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.bbHoaDonTT);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Quản Lý Hóa Đơn";
            // 
            // bbHoaDonTT
            // 
            this.bbHoaDonTT.Caption = "Hóa đơn thanh toán";
            this.bbHoaDonTT.Id = 28;
            this.bbHoaDonTT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbHoaDonTT.ImageOptions.Image")));
            this.bbHoaDonTT.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbHoaDonTT.ImageOptions.LargeImage")));
            this.bbHoaDonTT.Name = "bbHoaDonTT";
            this.bbHoaDonTT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // rbbThayDoiQuyDinh
            // 
            this.rbbThayDoiQuyDinh.ItemLinks.Add(this.bbThayDoiQuyDinh);
            this.rbbThayDoiQuyDinh.Name = "rbbThayDoiQuyDinh";
            this.rbbThayDoiQuyDinh.Text = "Thay Đổi Quy ĐỊnh";
            // 
            // bbThayDoiQuyDinh
            // 
            this.bbThayDoiQuyDinh.Caption = "Thay đổi quy định";
            this.bbThayDoiQuyDinh.Id = 29;
            this.bbThayDoiQuyDinh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbThayDoiQuyDinh.ImageOptions.Image")));
            this.bbThayDoiQuyDinh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbThayDoiQuyDinh.ImageOptions.LargeImage")));
            this.bbThayDoiQuyDinh.Name = "bbThayDoiQuyDinh";
            this.bbThayDoiQuyDinh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick_1);
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.bbItemXemVaCapNhatThongTinCaNhan);
            this.ribbonPageGroup10.ItemLinks.Add(this.bbItemDoiMatKhau);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "Tài khoản";
            // 
            // bbItemXemVaCapNhatThongTinCaNhan
            // 
            this.bbItemXemVaCapNhatThongTinCaNhan.Caption = "Xem và cập nhật thông tin ";
            this.bbItemXemVaCapNhatThongTinCaNhan.Id = 15;
            this.bbItemXemVaCapNhatThongTinCaNhan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbItemXemVaCapNhatThongTinCaNhan.ImageOptions.Image")));
            this.bbItemXemVaCapNhatThongTinCaNhan.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemXemVaCapNhatThongTinCaNhan.ImageOptions.LargeImage")));
            this.bbItemXemVaCapNhatThongTinCaNhan.Name = "bbItemXemVaCapNhatThongTinCaNhan";
            this.bbItemXemVaCapNhatThongTinCaNhan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemXemVaCapNhatThongTinCaNhan_ItemClick);
            // 
            // bbItemDoiMatKhau
            // 
            this.bbItemDoiMatKhau.Caption = "Đổi mật khẩu";
            this.bbItemDoiMatKhau.Id = 16;
            this.bbItemDoiMatKhau.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbItemDoiMatKhau.ImageOptions.Image")));
            this.bbItemDoiMatKhau.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemDoiMatKhau.ImageOptions.LargeImage")));
            this.bbItemDoiMatKhau.Name = "bbItemDoiMatKhau";
            this.bbItemDoiMatKhau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemDoiMatKhau_ItemClick);
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.bbItemDangXuat);
            this.ribbonPageGroup11.ItemLinks.Add(this.bbItemThoatChuongTrinh);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.Text = "Trạng Thái Đăng Nhập";
            // 
            // bbItemDangXuat
            // 
            this.bbItemDangXuat.Caption = "Đăng xuất";
            this.bbItemDangXuat.Id = 17;
            this.bbItemDangXuat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbItemDangXuat.ImageOptions.Image")));
            this.bbItemDangXuat.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemDangXuat.ImageOptions.LargeImage")));
            this.bbItemDangXuat.Name = "bbItemDangXuat";
            this.bbItemDangXuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemDangXuat_ItemClick);
            // 
            // bbItemThoatChuongTrinh
            // 
            this.bbItemThoatChuongTrinh.Caption = "Thoát chương trình";
            this.bbItemThoatChuongTrinh.Id = 18;
            this.bbItemThoatChuongTrinh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbItemThoatChuongTrinh.ImageOptions.Image")));
            this.bbItemThoatChuongTrinh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbItemThoatChuongTrinh.ImageOptions.LargeImage")));
            this.bbItemThoatChuongTrinh.Name = "bbItemThoatChuongTrinh";
            this.bbItemThoatChuongTrinh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbItemThoatChuongTrinh_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 33;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Đổi mật khâu";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.LargeImage")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Đăng Xuất";
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "InplaceGallery1";
            // 
            // 
            // 
            galleryItemGroup1.Caption = "Group1";
            this.ribbonGalleryBarItem1.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.ribbonGalleryBarItem1.Id = 1;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Báo cáo doanh thu";
            this.barButtonItem4.Id = 3;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.btnDSKH,
            this.btnTimKiemKH,
            this.barButtonItem2,
            this.barButtonItem3,
            this.ribbonGalleryBarItem1,
            this.skinRibbonGalleryBarItem1,
            this.barButtonItem4,
            this.barButtonItem7,
            this.bbiBCDT,
            this.bbiMatDo,
            this.bbiNhanVien,
            this.bbiLoaiNhanVien,
            this.bbItemLuuDL,
            this.bbItemPhucHoi,
            this.bbItemNhatKyHeThong,
            this.bbItemXemVaCapNhatThongTinCaNhan,
            this.bbItemDoiMatKhau,
            this.bbItemDangXuat,
            this.bbItemThoatChuongTrinh,
            this.bbPhong,
            this.bbLoaiPhong,
            this.bbHinhThucThue,
            this.bbItemPhanQuyen,
            this.bbItemPhanQuye,
            this.btnKhachHang,
            this.btnLoaiKhachHang,
            this.bbPhanQuyen,
            this.bbHoaDonTT,
            this.bbThayDoiQuyDinh,
            this.barButtonItem5,
            this.barButtonItem8});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 34;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage3,
            this.rbbItemHeThong});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(1358, 162);
            this.ribbonControl1.StatusBar = this.rbbTenDangNhap;
            this.ribbonControl1.Click += new System.EventHandler(this.ribbonControl1_Click);
            // 
            // rbbTenDangNhap
            // 
            this.rbbTenDangNhap.ItemLinks.Add(this.bbTenDangNhap);
            this.rbbTenDangNhap.Location = new System.Drawing.Point(0, 728);
            this.rbbTenDangNhap.Name = "rbbTenDangNhap";
            this.rbbTenDangNhap.Ribbon = this.ribbonControl1;
            this.rbbTenDangNhap.Size = new System.Drawing.Size(1358, 31);
            // 
            // bbTenDangNhap
            // 
            this.bbTenDangNhap.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbTenDangNhap.Caption = "barButtonItem3";
            this.bbTenDangNhap.Id = 37;
            this.bbTenDangNhap.Name = "bbTenDangNhap";
            // 
            // lv_DSPhong
            // 
            this.lv_DSPhong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv_DSPhong.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_DSPhong.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lv_DSPhong.LargeImageList = this.img_Phong;
            this.lv_DSPhong.Location = new System.Drawing.Point(0, 0);
            this.lv_DSPhong.Name = "lv_DSPhong";
            this.lv_DSPhong.Size = new System.Drawing.Size(1062, 566);
            this.lv_DSPhong.TabIndex = 0;
            this.lv_DSPhong.UseCompatibleStateImageBehavior = false;
            this.lv_DSPhong.Click += new System.EventHandler(this.lv_DSPhong_Click);
            this.lv_DSPhong.DoubleClick += new System.EventHandler(this.lv_DSPhong_DoubleClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 162);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lv_DSPhong);
            this.splitContainer1.Size = new System.Drawing.Size(1358, 566);
            this.splitContainer1.SplitterDistance = 322;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.panel2);
            this.splitContainer2.Panel1.Controls.Add(this.Danh_Sach_Loai_Phong);
            this.splitContainer2.Panel1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panel1);
            this.splitContainer2.Size = new System.Drawing.Size(322, 566);
            this.splitContainer2.SplitterDistance = 271;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel2.Controls.Add(this.lbLoaiPhong);
            this.panel2.Location = new System.Drawing.Point(1, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(319, 34);
            this.panel2.TabIndex = 0;
            // 
            // lbLoaiPhong
            // 
            this.lbLoaiPhong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbLoaiPhong.AutoSize = true;
            this.lbLoaiPhong.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoaiPhong.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbLoaiPhong.Location = new System.Drawing.Point(106, 7);
            this.lbLoaiPhong.Name = "lbLoaiPhong";
            this.lbLoaiPhong.Size = new System.Drawing.Size(109, 19);
            this.lbLoaiPhong.TabIndex = 0;
            this.lbLoaiPhong.Text = "LOẠI PHÒNG";
            // 
            // Danh_Sach_Loai_Phong
            // 
            this.Danh_Sach_Loai_Phong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Danh_Sach_Loai_Phong.AutoScroll = true;
            this.Danh_Sach_Loai_Phong.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Danh_Sach_Loai_Phong.ForeColor = System.Drawing.Color.MidnightBlue;
            this.Danh_Sach_Loai_Phong.Location = new System.Drawing.Point(0, 35);
            this.Danh_Sach_Loai_Phong.Name = "Danh_Sach_Loai_Phong";
            this.Danh_Sach_Loai_Phong.Size = new System.Drawing.Size(320, 240);
            this.Danh_Sach_Loai_Phong.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.lbSoNguoi);
            this.panel1.Controls.Add(this.lbpreSoNguoi);
            this.panel1.Controls.Add(this.lbTongTien);
            this.panel1.Controls.Add(this.lbpreTongTien);
            this.panel1.Controls.Add(this.lbHinhThuc);
            this.panel1.Controls.Add(this.lbpreHinhThuc);
            this.panel1.Controls.Add(this.lbTenPhong);
            this.panel1.Controls.Add(this.lbpreTenPhong);
            this.panel1.Controls.Add(this.lbPhongTrong);
            this.panel1.Controls.Add(this.lbPhongThue);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(320, 319);
            this.panel1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(141, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Phòng Trống";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(141, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Phòng Đang Thuê";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(189)))), ((int)(((byte)(194)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(15, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 40);
            this.button2.TabIndex = 18;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(103)))), ((int)(((byte)(85)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(15, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 40);
            this.button1.TabIndex = 17;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // lbSoNguoi
            // 
            this.lbSoNguoi.AutoSize = true;
            this.lbSoNguoi.Location = new System.Drawing.Point(144, 224);
            this.lbSoNguoi.Name = "lbSoNguoi";
            this.lbSoNguoi.Size = new System.Drawing.Size(0, 13);
            this.lbSoNguoi.TabIndex = 11;
            this.lbSoNguoi.Visible = false;
            // 
            // lbpreSoNguoi
            // 
            this.lbpreSoNguoi.AutoSize = true;
            this.lbpreSoNguoi.Location = new System.Drawing.Point(12, 224);
            this.lbpreSoNguoi.Name = "lbpreSoNguoi";
            this.lbpreSoNguoi.Size = new System.Drawing.Size(54, 13);
            this.lbpreSoNguoi.TabIndex = 10;
            this.lbpreSoNguoi.Text = "Số Người:";
            this.lbpreSoNguoi.Visible = false;
            // 
            // lbTongTien
            // 
            this.lbTongTien.AutoSize = true;
            this.lbTongTien.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongTien.Location = new System.Drawing.Point(141, 251);
            this.lbTongTien.Name = "lbTongTien";
            this.lbTongTien.Size = new System.Drawing.Size(106, 19);
            this.lbTongTien.TabIndex = 9;
            this.lbTongTien.Text = "120,000 VND";
            this.lbTongTien.Visible = false;
            // 
            // lbpreTongTien
            // 
            this.lbpreTongTien.AutoSize = true;
            this.lbpreTongTien.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbpreTongTien.Location = new System.Drawing.Point(9, 251);
            this.lbpreTongTien.Name = "lbpreTongTien";
            this.lbpreTongTien.Size = new System.Drawing.Size(96, 19);
            this.lbpreTongTien.TabIndex = 8;
            this.lbpreTongTien.Text = "Tổng Tiền: ";
            this.lbpreTongTien.Visible = false;
            // 
            // lbHinhThuc
            // 
            this.lbHinhThuc.AutoSize = true;
            this.lbHinhThuc.Location = new System.Drawing.Point(144, 198);
            this.lbHinhThuc.Name = "lbHinhThuc";
            this.lbHinhThuc.Size = new System.Drawing.Size(49, 13);
            this.lbHinhThuc.TabIndex = 7;
            this.lbHinhThuc.Text = "Theo Giờ";
            this.lbHinhThuc.Visible = false;
            // 
            // lbpreHinhThuc
            // 
            this.lbpreHinhThuc.AutoSize = true;
            this.lbpreHinhThuc.Location = new System.Drawing.Point(12, 198);
            this.lbpreHinhThuc.Name = "lbpreHinhThuc";
            this.lbpreHinhThuc.Size = new System.Drawing.Size(59, 13);
            this.lbpreHinhThuc.TabIndex = 6;
            this.lbpreHinhThuc.Text = "Hình Thức:";
            this.lbpreHinhThuc.Visible = false;
            // 
            // lbTenPhong
            // 
            this.lbTenPhong.AutoSize = true;
            this.lbTenPhong.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenPhong.Location = new System.Drawing.Point(144, 170);
            this.lbTenPhong.Name = "lbTenPhong";
            this.lbTenPhong.Size = new System.Drawing.Size(36, 19);
            this.lbTenPhong.TabIndex = 5;
            this.lbTenPhong.Text = "001";
            this.lbTenPhong.Visible = false;
            // 
            // lbpreTenPhong
            // 
            this.lbpreTenPhong.AutoSize = true;
            this.lbpreTenPhong.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbpreTenPhong.Location = new System.Drawing.Point(12, 170);
            this.lbpreTenPhong.Name = "lbpreTenPhong";
            this.lbpreTenPhong.Size = new System.Drawing.Size(66, 19);
            this.lbpreTenPhong.TabIndex = 4;
            this.lbpreTenPhong.Text = "Phòng:";
            this.lbpreTenPhong.Visible = false;
            // 
            // lbPhongTrong
            // 
            this.lbPhongTrong.AutoSize = true;
            this.lbPhongTrong.Location = new System.Drawing.Point(144, 16);
            this.lbPhongTrong.Name = "lbPhongTrong";
            this.lbPhongTrong.Size = new System.Drawing.Size(19, 13);
            this.lbPhongTrong.TabIndex = 3;
            this.lbPhongTrong.Text = "12";
            // 
            // lbPhongThue
            // 
            this.lbPhongThue.AutoSize = true;
            this.lbPhongThue.Location = new System.Drawing.Point(144, 41);
            this.lbPhongThue.Name = "lbPhongThue";
            this.lbPhongThue.Size = new System.Drawing.Size(19, 13);
            this.lbPhongThue.TabIndex = 2;
            this.lbPhongThue.Text = "16";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số Phòng Đang Thuê:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số Phòng Trống:";
            // 
            // frmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1358, 759);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.rbbTenDangNhap);
            this.Controls.Add(this.ribbonControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.rbbTenDangNhap;
            this.Text = "QUẢN LÝ KHÁCH SẠN";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage6;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ImageList img_Phong;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView lv_DSPhong;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar rbbTenDangNhap;
        private DevExpress.XtraBars.BarButtonItem bbTenDangNhap;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem btnDSKH;
        private DevExpress.XtraBars.BarButtonItem btnTimKiemKH;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem bbiBCDT;
        private DevExpress.XtraBars.BarButtonItem bbiMatDo;
        private DevExpress.XtraBars.BarButtonItem bbiNhanVien;
        private DevExpress.XtraBars.BarButtonItem bbiLoaiNhanVien;
        private DevExpress.XtraBars.BarButtonItem bbItemLuuDL;
        private DevExpress.XtraBars.BarButtonItem bbItemPhucHoi;
        private DevExpress.XtraBars.BarButtonItem bbItemNhatKyHeThong;
        private DevExpress.XtraBars.BarButtonItem bbItemXemVaCapNhatThongTinCaNhan;
        private DevExpress.XtraBars.BarButtonItem bbItemDoiMatKhau;
        private DevExpress.XtraBars.BarButtonItem bbItemDangXuat;
        private DevExpress.XtraBars.BarButtonItem bbItemThoatChuongTrinh;
        private DevExpress.XtraBars.BarButtonItem bbPhong;
        private DevExpress.XtraBars.BarButtonItem bbLoaiPhong;
        private DevExpress.XtraBars.BarButtonItem bbHinhThucThue;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbQLNhanVien;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbQuanLyPhong;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbQuanLyHinhThuc;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbPhanQuyen;
        private DevExpress.XtraBars.Ribbon.RibbonPage rbbItemHeThong;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbSaoLuuVaPhucHoi;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbNhatKyHoatDong;
        private DevExpress.XtraBars.BarButtonItem bbItemPhanQuyen;
        private DevExpress.XtraBars.BarButtonItem bbItemPhanQuye;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbTongTien;
        private System.Windows.Forms.Label lbpreTongTien;
        private System.Windows.Forms.Label lbHinhThuc;
        private System.Windows.Forms.Label lbpreHinhThuc;
        private System.Windows.Forms.Label lbTenPhong;
        private System.Windows.Forms.Label lbpreTenPhong;
        private System.Windows.Forms.Label lbPhongTrong;
        private System.Windows.Forms.Label lbPhongThue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel Danh_Sach_Loai_Phong;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbLoaiPhong;
        private System.Windows.Forms.Label lbSoNguoi;
        private System.Windows.Forms.Label lbpreSoNguoi;
        private DevExpress.XtraBars.BarButtonItem btnKhachHang;
        private DevExpress.XtraBars.BarButtonItem btnLoaiKhachHang;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbQuanLyKhachHang;
        private DevExpress.XtraBars.BarButtonItem bbPhanQuyen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraBars.BarButtonItem bbHoaDonTT;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem bbThayDoiQuyDinh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbbThayDoiQuyDinh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
    }
}

