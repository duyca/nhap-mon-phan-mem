﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QL_KS.BUS;
using DevComponents.DotNetBar;
using log4net;

namespace QL_KS
{
    public partial class frmPhieuThuePhong : DevExpress.XtraEditors.XtraForm
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmPhieuThuePhong).Name);
        public string frmMaNV;
        public string frmMaKH;
        public string TaiKhoan;
        Hinh_Thuc_BUS bhinhthuc = new Hinh_Thuc_BUS();
        Loai_Khach_HangBUS bloaikhach = new Loai_Khach_HangBUS();
        KhachHangBus bkhach = new KhachHangBus();
        PhieuThueBUS bphieu = new PhieuThueBUS();
        Phong_BUS bphong = new Phong_BUS();
        public string frmMaPhong;
        public frmPhieuThuePhong()
        {
            InitializeComponent();
        }
        private void frmPhieuThuePhong_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            frmMaKH = null;
            Load_data();
        }
        private void Load_data()
        {
            if(frmMaKH==null)
            {
                foreach (HinhThuc ht in bhinhthuc.Load_DS_Hinh_Thuc())
                {
                    ComboBoxItem item = new ComboBoxItem();
                    item.Text = ht.TenHinhThuc;
                    item.Name = ht.MaHinhThuc;
                    cbbHinhThucThue.Items.Add(item);
                    cbbHinhThucThue.SelectedIndex = 0;
                }
                foreach (LoaiKhachHang lk in bloaikhach.Load_DS_Loai_Khach())
                {
                    ComboBoxItem item = new ComboBoxItem();
                    item.Text = lk.TenLoaiKH;
                    item.Name = lk.MaLoaiKH.ToString();
                    cbbLoaiKhachHang.Items.Add(item);
                    cbbLoaiKhachHang.SelectedIndex = 0;
                }
                txtNgayThue.Text = DateTime.Now.ToString();
            }
            else
            {
                KhachHang khachtam = bkhach.TimKiemkhachHang(frmMaKH);
                txtTenKhachHang.Text = khachtam.TenKH;
                txtSoDT.Text = khachtam.SDT;
                txtDiaChi.Text = khachtam.DiaChi;
                txtCMND.Text = khachtam.CMND;
                foreach (LoaiKhachHang lk in bloaikhach.Load_DS_Loai_Khach())
                {
                    ComboBoxItem item = new ComboBoxItem();
                    item.Text = lk.TenLoaiKH;
                    item.Name = lk.MaLoaiKH.ToString();
                    cbbLoaiKhachHang.Items.Add(item);
                    if(lk.MaLoaiKH == khachtam.LoaiKH)
                    {
                        cbbLoaiKhachHang.SelectedItem = item;
                    }
                }
            }
            txtDonGia.Text = HinhThuc_LoaiPhongBUS.Tim_HT_LP((cbbHinhThucThue.SelectedItem as ComboBoxItem).Name,bphong.Tim_Phong(frmMaPhong).LoaiPhong).DonGia.ToString();
            txtDonGia.Text = string.Format("{0:#,##0 VND}", double.Parse(txtDonGia.Text));
        }
        private void btnThuePhong_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (txtTenKhachHang.Text == string.Empty)
            {
                lbThongBaoTen.Visible = true;
                error++;
            }
            else
                lbThongBaoTen.Visible = false;
            if (txtSoNguoi.Text == string.Empty)
            {
                lbThongBaoSoNguoi.Text = "Hãy nhập vào số người:";
                lbThongBaoSoNguoi.Visible = true;
                error++;
            }
            else
            {
                if (bphong.Tim_Phong(frmMaPhong).SoNguoiToiDa < Convert.ToInt32(txtSoNguoi.Text))
                {
                    lbThongBaoSoNguoi.Text = "Số người vượt quá cho phép";
                    lbThongBaoSoNguoi.Visible = true;
                    error++;
                }
                else
                {
                    lbThongBaoSoNguoi.Visible = false;
                }
            }
            //----------------Khong loi--------------------//
            if (error == 0)
            {
                try
                {
                    PhieuThue phieumoi = new PhieuThue();
                    KhachHang khachmoi = new KhachHang();

                    if(frmMaKH == null)
                    {
                        khachmoi.MaKH = KhachHangBus.ID();
                    }
                    else
                    {
                        khachmoi.MaKH = frmMaKH;
                    }
                    khachmoi.TenKH = txtTenKhachHang.Text.ToString();
                    khachmoi.LoaiKH = Convert.ToInt32((cbbLoaiKhachHang.SelectedItem as ComboBoxItem).Name);
                    khachmoi.DiaChi = txtDiaChi.Text;
                    khachmoi.SDT = txtSoDT.Text;
                    khachmoi.CMND = txtCMND.Text;

                    phieumoi.MaPhieuThue = bphieu.Tao_Ma_Moi();
                    phieumoi.MaHinhThuc = (cbbHinhThucThue.SelectedItem as ComboBoxItem).Name;
                    phieumoi.SoNguoiThue = Convert.ToInt32(txtSoNguoi.Text);
                    phieumoi.NgayBatDauThue = DateTime.Now;
                    phieumoi.MaPhong = frmMaPhong.ToString();
                    phieumoi.MaKH = khachmoi.MaKH.ToString();
                    phieumoi.NgayTraPhong = null;
                    phieumoi.TienChuyenPhong = 0;
                    bphong.Cap_Nhat_Tinh_Trang("MTT0000001", frmMaPhong);
                    if (frmMaKH == null)
                    {
                        bkhach.Them_Khach_Hang(khachmoi);
                    }
                    bphieu.Them_Phieu_Thue(phieumoi);
                    LuuNhatKyHTBUS.Them();
                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    _logger.Error(ex.Message);
                }

            }
            else
            {
                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void LayMaKH(string maKH)
        {
            frmMaKH = maKH;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            frmTimKiemKH frmtimKH = new frmTimKiemKH();
            frmtimKH.xacnhan = "1";
            frmtimKH.maKH = new frmTimKiemKH.LayMaKH(LayMaKH);
            frmtimKH.ShowDialog();
            Load_data();
        }

        private void cbbHinhThucThue_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDonGia.Text = HinhThuc_LoaiPhongBUS.Tim_HT_LP((cbbHinhThucThue.SelectedItem as ComboBoxItem).Name, bphong.Tim_Phong(frmMaPhong).LoaiPhong).DonGia.ToString();
            txtDonGia.Text = string.Format("{0:#,##0 VND}", double.Parse(txtDonGia.Text));
        }
    }
}