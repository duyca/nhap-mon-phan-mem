﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QL_KS.BUS;
using log4net;

namespace QL_KS
{
    public partial class frmCap_Nhat_KH : Form
    {
        public static string maKH = "";
        public static bool kqCN;
        public string TaiKhoan;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmCap_Nhat_KH).Name);
        public frmCap_Nhat_KH(string ma)
        {
            InitializeComponent();
            maKH = ma;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            frmTimKiemKH frm = new frmTimKiemKH();
            this.Close();
        }

        private void frmCapNhatKH_Load(object sender, EventArgs e)
        {
            cbbLoaiKH.DataSource = KhachHangBus.LoadKH();
            cbbLoaiKH.DisplayMember = "TenLoaiKH";
            cbbLoaiKH.ValueMember = "MaLoaiKH";


            foreach (var k in KhachHangBus.SearchID(maKH))
            {
                txtMaKH.Text = k.MaKH;
                txtHoTen.Text = k.TenKH;
                txtCMND.Text = k.CMND;
                txtSDT.Text = k.SDT;
                mmDC.Text = k.DC;
                cbbLoaiKH.Text = k.LoaiKH;
            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (txtHoTen.Text == "" || txtCMND.Text == "")
            {
                MessageBox.Show("Nhập đầy đủ các thông tin bắt buộc.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else if (txtHoTen.Text != "" && txtCMND.Text != "")
            {
                KhachHang kh = new KhachHang();
                kh.MaKH = txtMaKH.Text;
                kh.TenKH = txtHoTen.Text;
                kh.CMND = txtCMND.Text;
                kh.SDT = txtSDT.Text;
                kh.DiaChi = mmDC.Text;
                kh.LoaiKH = Convert.ToInt32(KhachHangBus.TimLoaiKH(cbbLoaiKH.Text));
                bool kq = false;
                try
                {
                    kq = KhachHangBus.Update(kh);
                    LuuNhatKyHTBUS.Them(TaiKhoan, "Hệ thống", "Cập nhật thông tin khách hàng");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    _logger.Error(ex.Message);
                }
                if (kq == true)
                {
                    MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
                else
                {
                    MessageBox.Show("Cập nhật thất bại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void txtCMND_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

    }
}
