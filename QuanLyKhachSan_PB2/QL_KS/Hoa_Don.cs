﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QL_KS.BUS;
using System.Drawing.Printing;

namespace QL_KS
{
    public partial class Hoa_Don : Form
    {
        PhieuThueBUS bPhieu = new PhieuThueBUS();
        HoaDonBUS bHoaDon = new HoaDonBUS();
        KhachHangBus bKhach = new KhachHangBus();
        NhanVienBUS bNhanVien = new NhanVienBUS();
        public string frmMaHoaDon;
        public Hoa_Don()
        {
            InitializeComponent();
        }

        private void Hoa_Don_Load(object sender, EventArgs e)
        {
            //In Hoa Don
            //PrintDialog _PrintDialog = new PrintDialog();
            PrintDocument _PrintDocument = new PrintDocument();
            _PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(Tao_Hoa_Don);

            //if (result == DialogResult.OK)
            //{
            _PrintDocument.Print();
            // }
            Close();
        }
        private void Tao_Hoa_Don(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            HoaDon hoadontam = bHoaDon.Tim_Hoa_Don(frmMaHoaDon);
            PhieuThue phieutam = bPhieu.Tim_Phieu_Thue_Theo_Ma(hoadontam.MaPhieuThue);
            Graphics graphic = e.Graphics;
            Font font = new Font("Courier New", 12);
            float FontHeight = font.GetHeight();
            int startX = 10;
            int startY = 10;
            int offset = 40;

            graphic.DrawString("KHÁCH SẠN QUEEN WYOM'S", new Font("Courier New", 18), new SolidBrush(Color.Black), startX, startY);
            string top = "Khách Hàng : ".PadRight(24) + bKhach.TimKiemkhachHang(hoadontam.MaKH).TenKH.ToString();
            graphic.DrawString(top, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)FontHeight; //make the spacing consistent

            graphic.DrawString("Tiền Phòng ", new Font("Courier New", 12), new SolidBrush(Color.Black), startX, startY + offset);
            graphic.DrawString(String.Format("{0:0,0 VND}",bPhieu.tinh_tien_phong(phieutam.MaPhieuThue)).ToString(), new Font("Courier New", 12), new SolidBrush(Color.Black), startX + 250, startY + offset);
            offset = offset + (int)FontHeight; //make the spacing consistent

            graphic.DrawString("Tiền Phụ Thu ", new Font("Courier New", 12), new SolidBrush(Color.Black), startX, startY + offset);
            graphic.DrawString(String.Format("{0:0,0 VND}", phieutam.TienChuyenPhong).ToString(), new Font("Courier New", 12), new SolidBrush(Color.Black), startX + 250, startY + offset);
            offset = offset + (int)FontHeight; //make the spacing consistent

            graphic.DrawString("----------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)FontHeight + 5; //make the spacing consistent

            offset = offset + 20; //make some room so that the total stands out.


            graphic.DrawString("TỔNG TIỀN TRẢ ", new Font("Courier New", 12, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
            graphic.DrawString(String.Format("{0:0,0 VND}",hoadontam.TongTien).ToString(), new Font("Courier New", 12, FontStyle.Bold), new SolidBrush(Color.Black), startX + 250, startY + offset);

            offset = offset + (int)FontHeight + 5; //make the spacing consistent              
            graphic.DrawString("CẢM ƠN QUÝ KHÁCH ĐÃ SƯ DỤNG DỊCH VỤ CỦA KHÁCH SẠN QUEEN WYOM'S", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)FontHeight + 5; //make the spacing consistent              
            graphic.DrawString("HẸN GẶP LẠI QUÝ KHÁCH!", font, new SolidBrush(Color.Black), startX, startY + offset);
        }
    }
}
