﻿namespace QL_KS
{
    partial class frmThayDoiQuyDinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtTenPhutThem = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhutThem = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenTienChuyenPhong = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtTienChuyenPhong = new DevExpress.XtraEditors.TextEdit();
            this.btnThayDoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhutThem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhutThem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTienChuyenPhong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienChuyenPhong.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.groupControl1.Appearance.Options.UseBackColor = true;
            this.groupControl1.Appearance.Options.UseForeColor = true;
            this.groupControl1.Controls.Add(this.txtTenPhutThem);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtPhutThem);
            this.groupControl1.Location = new System.Drawing.Point(-6, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(367, 120);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Giới Hạn Quá Giờ";
            // 
            // txtTenPhutThem
            // 
            this.txtTenPhutThem.Enabled = false;
            this.txtTenPhutThem.Location = new System.Drawing.Point(134, 39);
            this.txtTenPhutThem.Name = "txtTenPhutThem";
            this.txtTenPhutThem.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTenPhutThem.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtTenPhutThem.Properties.Appearance.Options.UseBackColor = true;
            this.txtTenPhutThem.Properties.Appearance.Options.UseForeColor = true;
            this.txtTenPhutThem.Properties.Mask.EditMask = "f1";
            this.txtTenPhutThem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTenPhutThem.Size = new System.Drawing.Size(148, 20);
            this.txtTenPhutThem.TabIndex = 0;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(288, 74);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(30, 16);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Phút";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.Location = new System.Drawing.Point(21, 40);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(90, 16);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Tên Quy Định:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.Location = new System.Drawing.Point(21, 77);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(55, 16);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Số Phút:";
            // 
            // txtPhutThem
            // 
            this.txtPhutThem.Location = new System.Drawing.Point(134, 73);
            this.txtPhutThem.Name = "txtPhutThem";
            this.txtPhutThem.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPhutThem.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtPhutThem.Properties.Appearance.Options.UseBackColor = true;
            this.txtPhutThem.Properties.Appearance.Options.UseForeColor = true;
            this.txtPhutThem.Properties.Mask.EditMask = "f1";
            this.txtPhutThem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPhutThem.Size = new System.Drawing.Size(148, 20);
            this.txtPhutThem.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.groupControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.groupControl2.Appearance.Options.UseBackColor = true;
            this.groupControl2.Appearance.Options.UseForeColor = true;
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.txtTenTienChuyenPhong);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.txtTienChuyenPhong);
            this.groupControl2.Location = new System.Drawing.Point(-6, 129);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(367, 120);
            this.groupControl2.TabIndex = 16;
            this.groupControl2.Text = "Tiền Chuyển Phòng";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(288, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(26, 16);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "VND";
            // 
            // txtTenTienChuyenPhong
            // 
            this.txtTenTienChuyenPhong.Enabled = false;
            this.txtTenTienChuyenPhong.Location = new System.Drawing.Point(134, 32);
            this.txtTenTienChuyenPhong.Name = "txtTenTienChuyenPhong";
            this.txtTenTienChuyenPhong.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTenTienChuyenPhong.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtTenTienChuyenPhong.Properties.Appearance.Options.UseBackColor = true;
            this.txtTenTienChuyenPhong.Properties.Appearance.Options.UseForeColor = true;
            this.txtTenTienChuyenPhong.Properties.Mask.EditMask = "f1";
            this.txtTenTienChuyenPhong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTenTienChuyenPhong.Size = new System.Drawing.Size(148, 20);
            this.txtTenTienChuyenPhong.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(21, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 16);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "Tên Quy Định:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(25, 75);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 16);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "Đơn Giá:";
            // 
            // txtTienChuyenPhong
            // 
            this.txtTienChuyenPhong.Location = new System.Drawing.Point(134, 71);
            this.txtTienChuyenPhong.Name = "txtTienChuyenPhong";
            this.txtTienChuyenPhong.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTienChuyenPhong.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtTienChuyenPhong.Properties.Appearance.Options.UseBackColor = true;
            this.txtTienChuyenPhong.Properties.Appearance.Options.UseForeColor = true;
            this.txtTienChuyenPhong.Properties.Mask.EditMask = "n0";
            this.txtTienChuyenPhong.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTienChuyenPhong.Size = new System.Drawing.Size(148, 20);
            this.txtTienChuyenPhong.TabIndex = 1;
            // 
            // btnThayDoi
            // 
            this.btnThayDoi.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnThayDoi.Appearance.Options.UseForeColor = true;
            this.btnThayDoi.Location = new System.Drawing.Point(103, 276);
            this.btnThayDoi.Name = "btnThayDoi";
            this.btnThayDoi.Size = new System.Drawing.Size(75, 23);
            this.btnThayDoi.TabIndex = 0;
            this.btnThayDoi.Text = "Thay đổi";
            this.btnThayDoi.Click += new System.EventHandler(this.btnThayDoi_Click);
            // 
            // btnDong
            // 
            this.btnDong.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnDong.Appearance.Options.UseForeColor = true;
            this.btnDong.Location = new System.Drawing.Point(201, 276);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 1;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // frmThayDoiQuyDinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 322);
            this.Controls.Add(this.btnThayDoi);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmThayDoiQuyDinh";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "THAY ĐỔI QUY ĐỊNH";
            this.Load += new System.EventHandler(this.frmThayDoiQuyDinh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhutThem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhutThem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTienChuyenPhong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTienChuyenPhong.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtPhutThem;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtTienChuyenPhong;
        private DevExpress.XtraEditors.SimpleButton btnThayDoi;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.TextEdit txtTenPhutThem;
        private DevExpress.XtraEditors.TextEdit txtTenTienChuyenPhong;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}