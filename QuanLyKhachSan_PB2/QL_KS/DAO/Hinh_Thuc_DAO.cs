﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class Hinh_Thuc_DAO
    {
        public HinhThuc Tim_Hinh_Thuc(string maHT)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var HinhThuc = (from ht in data.HinhThucs
                             where ht.MaHinhThuc == maHT
                             select ht).FirstOrDefault();
                return HinhThuc;
            }
        }
        public List<HinhThuc> Load_DS_Hinh_Thuc()
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var DS_Hinh_Thuc = from ht in data.HinhThucs
                                   where ht.active == true
                                   select ht;
                return DS_Hinh_Thuc.ToList();
            }
        }
        public void Them_Hinh_Thuc(HinhThuc hinhthucmoi)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.HinhThucs.Add(hinhthucmoi);
                data.SaveChanges();
            }
        }
        public void Sua_Hinh_Thuc(HinhThuc hinhthucmoi)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                HinhThuc hinhthuctam = data.HinhThucs.Find(hinhthucmoi.MaHinhThuc);
                hinhthuctam.TenHinhThuc = hinhthucmoi.TenHinhThuc;
                hinhthuctam.GioKetThuc = hinhthucmoi.GioKetThuc;
                hinhthuctam.ThoiGianKeoDai = hinhthucmoi.ThoiGianKeoDai;
                hinhthuctam.active = hinhthucmoi.active;
                data.SaveChanges();
            }
        }
        public static string ID()
        {
            string g = "MHT000";
            string id = null;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                id = data.HinhThucs.Max(ma => ma.MaHinhThuc);
            }
            if (id == null)
            {
                g = "MHT0000001";
            }
            else
            {
                int n = 0;
                n = Convert.ToInt32(id.Substring(6, 4));
                n = n + 1;
                if (n < 10)
                {
                    g = g + "000";
                    g = g + n.ToString();
                }
                else if (n < 100)
                {
                    g = g + "00";
                    g = g + n.ToString();
                }
                else if (n < 1000)
                {
                    g = g + "0";
                    g = g + n.ToString();
                }
            }
            return g;
        }
    }
}
