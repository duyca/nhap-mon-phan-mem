﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_KS
{
    public static class LuuNhatKyHT
    {
        public static  QLKhachSanEntities data;
        public static void Them(string tendn, string chucnang, string hanhdong)
        {
            data = new QLKhachSanEntities();
            NhatKyHoatDong nk = new NhatKyHoatDong();
            {
                nk.ID = tendn;
                nk.TenMayTinh = SystemInformation.ComputerName;
                nk.ThoiGian = DateTime.Now;
                nk.ChucNang = chucnang;
                nk.HanhDong = hanhdong;
                data.NhatKyHoatDongs.Add(nk);
                data.SaveChanges();
            }
        }
    }
}
