﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class LoaiNhanVienDAO
    {
        QLKhachSanEntities data;

        public List<LoaiNhanVien> LoadDS()
        {
            data = new QLKhachSanEntities();
            return data.LoaiNhanViens.ToList();
        }

        public String TaoMa()
        {
            data = new QLKhachSanEntities();
            String max = data.LoaiNhanViens.Max(lnv => lnv.MaLoaiNV);
            int index = max.IndexOf('0');
            string chu = max.Substring(0, index);
            int so = Int32.Parse(max.Substring(index, max.Length - index));
            so++;
            return chu + so.ToString("D"+ (max.Length - index).ToString());
        }


        public LoaiNhanVien TimKiem(string ma)
        {
            data = new QLKhachSanEntities();
            var kq = data.LoaiNhanViens.Find(ma);
            return kq;
        }

        public int Them(LoaiNhanVien lnv)
        {
            data = new QLKhachSanEntities();
            data.LoaiNhanViens.Add(lnv);
            return data.SaveChanges();
        }

        public bool Sua(LoaiNhanVien nv)
        {
            string manv = nv.MaLoaiNV;
            var nhanvien = data.LoaiNhanViens.SingleOrDefault(b => b.MaLoaiNV == manv);
            if (nhanvien != null)
            {
                nhanvien.MaLoaiNV = nv.MaLoaiNV;
                nhanvien.TenLoaiNV = nv.TenLoaiNV;
                data.SaveChanges();
                return true;
            }
            else
                return false;
        }
        public int Xoa(string ma)
        {
            data = new QLKhachSanEntities();
            var nvXoa = data.LoaiNhanViens.First(nv => nv.MaLoaiNV == ma);
            data.LoaiNhanViens.Remove(nvXoa);
            return data.SaveChanges();
        }
    }
}
