﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class Phong_DAO
    {
        QLKhachSanEntities data;
        public List<Phong> Load_DS_Phong(string MalP)
        {
            if (MalP != null)
            {
                using (QLKhachSanEntities data = new QLKhachSanEntities())
                {
                    var DS_Phong = from p in data.Phongs
                                   where p.LoaiPhong == MalP && p.active == true 
                                   orderby p.TenPhong
                                   select p;
                    if (DS_Phong.Count() != 0)
                        return DS_Phong.ToList();
                    else
                        return null;
                }
            }
            else
            {
                using (QLKhachSanEntities data = new QLKhachSanEntities())
                {
                    var DS_Phong = from p in data.Phongs
                                   where p.active == true
                                   orderby p.TenPhong
                                   select p;
                    return DS_Phong.ToList();
                }
            }
        }
        public int DemSoPhongTrong()
        {
            int Dem = 0;
            List<Phong> Ds_phong;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var var_DS_Phong = from p in data.Phongs
                                   where p.active == true
                                   select p;
                Ds_phong = var_DS_Phong.ToList();
            }
            foreach (Phong p in Ds_phong)
            {
                if (p.TinhTrang == "MTT0000002")
                    Dem++;
            }
            return Dem;
        }
        public int DemSoPhongThue()
        {
            int Dem = 0;
            List<Phong> Ds_phong;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var DS_Phong = from p in data.Phongs
                               select p;
                Ds_phong = DS_Phong.ToList();
            }
            foreach (Phong p in Ds_phong)
            {
                if (p.TinhTrang == "MTT0000001")
                    Dem++;
            }
            return Dem;
        }
        public void Cap_Nhat_Tinh_Trang(string maTinhtrang, string map)
        {
            data = new QLKhachSanEntities();
            var kq = data.Phongs.Find(map);
            //kq.MatKhau = MD5(nv.MatKhau);
            kq.TinhTrang = maTinhtrang;
            data.SaveChanges();
        }
        public Phong Tim_Phong(string map)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var rphong = (from p in data.Phongs
                              where p.MaPhong == map
                              select p).FirstOrDefault();
                if (rphong != null)
                    return rphong;
                else
                    return null;
            }
        }
        public List<Phong> DS_Phong_Trong()
        {
            List<Phong> Ds_phong;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var var_DS_Phong = from p in data.Phongs
                                   where p.TinhTrang == "MTT0000002" && p.active == true
                                   select p;
                Ds_phong = var_DS_Phong.ToList();
            }
            return Ds_phong;
        }
        public void Them_Phong(Phong phong)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.Phongs.Add(phong);
                data.SaveChanges();
            }
        }
        public void Xoa_phong(string map)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                Phong phongtam = data.Phongs.Find(map);
                phongtam.active = false;
                data.SaveChanges();
            }
        }
        public void Sua_phong(Phong phong)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                Phong phongtam = data.Phongs.Find(phong.MaPhong);
                phongtam.TenPhong = phong.TenPhong;
                phongtam.GhiChu = phong.GhiChu;
                phongtam.LoaiPhong = phong.LoaiPhong;
                phongtam.active = phong.active;
                phongtam.SoNguoiToiDa = phong.SoNguoiToiDa;
                phongtam.TinhTrang = phong.TinhTrang;
                data.SaveChanges();
            }
        }
        public static string ID()
        {
            string g = "MP0000";
            string id = null;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                id = data.Phongs.Max(ma => ma.MaPhong);
            }
            if (id == null)
            {
                g = "MP00000001";
            }
            else
            {
                int n = 0;
                n = Convert.ToInt32(id.Substring(6, 4));
                n = n + 1;
                if (n < 10)
                {
                    g = g + "000";
                    g = g + n.ToString();
                }
                else if (n < 100)
                {
                    g = g + "00";
                    g = g + n.ToString();
                }
                else if (n < 1000)
                {
                    g = g + "0";
                    g = g + n.ToString();
                }
            }
            return g;
        }
    }
}
