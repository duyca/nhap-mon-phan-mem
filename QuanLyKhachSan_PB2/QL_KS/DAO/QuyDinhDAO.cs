﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class QuyDinhDAO
    {
        public static QuyDinh Lay_Quy_Dinh(int sothutu)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var quydinh = (from qd in data.QuyDinhs
                              where qd.STT == sothutu
                              select qd).FirstOrDefault();
                return quydinh;
            }
        }
        public static void Thay_Doi(QuyDinh tam)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.QuyDinhs.Find(tam.STT).GiaTri = tam.GiaTri;
                data.SaveChanges();
            }
        }
    }
}
