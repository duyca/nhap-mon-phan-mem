﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class Loai_Phong_DAO
    {
        public List<LoaiPhong> Load_DS_Loai_Phong()
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var DS_Loai_Phong = from lp in data.LoaiPhongs
                                    where lp.active == true
                                    select lp;
                return DS_Loai_Phong.ToList();
            }
        }
        public LoaiPhong Tim_Loai_Phong( string maLoai)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var Loai_Phong = (from lp in data.LoaiPhongs
                                 where lp.MaLoaiPhong == maLoai
                                 select lp).FirstOrDefault();
                if (Loai_Phong != null)
                    return Loai_Phong;
                else
                    return null;
            }
        }
        public void Them_Loai_Phong(LoaiPhong lphong)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.LoaiPhongs.Add(lphong);
                data.SaveChanges();
            }
        }
        public void Sua_Loai_Phong(LoaiPhong lphong)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                LoaiPhong loaitam = (from lp in data.LoaiPhongs
                                          where lp.MaLoaiPhong == lphong.MaLoaiPhong
                                          select lp).FirstOrDefault();
                loaitam.TenLoaiPhong = lphong.TenLoaiPhong;
                loaitam.TienQuaGio = lphong.TienQuaGio;
                loaitam.active = lphong.active;
                data.SaveChanges();
            }
        }
        public static string ID()
        {
            string g = "MLP000";
            string id = null;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                id = data.LoaiPhongs.Max(ma => ma.MaLoaiPhong);
            }
            if (id == null)
            {
                g = "MLP0000001";
            }
            else
            {
                int n = 0;
                n = Convert.ToInt32(id.Substring(6, 4));
                n = n + 1;
                if (n < 10)
                {
                    g = g + "000";
                    g = g + n.ToString();
                }
                else if (n < 100)
                {
                    g = g + "00";
                    g = g + n.ToString();
                }
                else if (n < 1000)
                {
                    g = g + "0";
                    g = g + n.ToString();
                }
            }
            return g;
        }
    }
}
