﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class PhieuThueDAO
    {
        double TongTien = 0;
        public PhieuThue Tim_Phieu_Thue(string ma)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from pt in data.PhieuThues
                             join ph in data.Phongs on pt.MaPhong equals ph.MaPhong
                             where ph.MaPhong == ma && pt.NgayTraPhong == null
                             select pt).FirstOrDefault();
                if (phieu != null)
                    return phieu;
                else
                    return null;
            }
        }
        public PhieuThue Tim_Phieu_Thue_Theo_Ma(string ma)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from pt in data.PhieuThues
                             where pt.MaPhieuThue == ma
                             select pt).FirstOrDefault();
                return phieu;
            }
        }
        public double tinh_tien(string maPhieuThue)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from pt in data.PhieuThues
                             join hinhthuc in data.HinhThucs on pt.MaHinhThuc equals hinhthuc.MaHinhThuc
                             join phong in data.Phongs on pt.MaPhong equals phong.MaPhong
                             join loaiphong in data.LoaiPhongs on phong.LoaiPhong equals loaiphong.MaLoaiPhong
                             from hinhthuc_loaiphong in data.HinhThuc_LoaiPhong
                             where pt.MaPhieuThue == maPhieuThue && hinhthuc.MaHinhThuc == hinhthuc_loaiphong.MaHinhThuc && hinhthuc_loaiphong.MaLoaiPhong == loaiphong.MaLoaiPhong
                             select new
                             {
                                 pt.NgayBatDauThue,
                                 loaiphong.TienQuaGio,
                                 hinhthuc.ThoiGianKeoDai,
                                 hinhthuc.GioKetThuc,
                                 hinhthuc_loaiphong.DonGia,
                                 hinhthuc_loaiphong.TiepTheo,
                                 pt.TienChuyenPhong
                             }).FirstOrDefault();

                var qd = (from quydinh in data.QuyDinhs
                          where quydinh.STT == 1
                          select quydinh).FirstOrDefault();
                DateTime giotrongphieu = (DateTime)phieu.NgayBatDauThue;
                DateTime Hientai = DateTime.Now;
                TimeSpan thoigianlech = Hientai.Subtract(giotrongphieu);
                int sogio = (thoigianlech.Days * 24) + thoigianlech.Hours;
                int sophut = thoigianlech.Minutes;
                int boisoThoigian = 0;
                int soduThoigian = 0;
                //Tinh tien theo thoi gian o
                if (phieu.ThoiGianKeoDai != null)
                {
                    if (sophut >= qd.GiaTri)
                    {
                        sogio++;
                    }
                    if (sogio >= phieu.ThoiGianKeoDai)
                    {
                        boisoThoigian = Convert.ToInt32((sogio - phieu.ThoiGianKeoDai) / phieu.ThoiGianKeoDai);
                        soduThoigian = Convert.ToInt32((sogio - phieu.ThoiGianKeoDai) % phieu.ThoiGianKeoDai);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * phieu.TienQuaGio + phieu.TienChuyenPhong);
                    }
                    else
                    {
                        double sogioMien = Convert.ToDouble(phieu.GioKetThuc);
                        TongTien = Convert.ToDouble(phieu.DonGia + phieu.TienChuyenPhong);
                    }
                }
                //Tinh tien theo moc thoi gian tra phong
                else
                {
                    if (sophut + giotrongphieu.Minute >= qd.GiaTri)
                    {
                        if ((sophut + giotrongphieu.Minute) >= (60 + qd.GiaTri))
                            sogio += 2;
                        sogio++;
                    }
                    int khoangthoigianfree;
                    if (giotrongphieu.Hour < phieu.GioKetThuc)
                    {
                        khoangthoigianfree = Convert.ToInt32(phieu.GioKetThuc - giotrongphieu.Hour);
                    }
                    else
                    {
                        khoangthoigianfree = Convert.ToInt32(phieu.GioKetThuc + 24 - giotrongphieu.Hour);
                    }
                    if (sogio <= khoangthoigianfree)
                    {
                        TongTien = Convert.ToDouble(phieu.DonGia + phieu.TienChuyenPhong);
                    }
                    else
                    {
                        boisoThoigian = Convert.ToInt32((sogio - khoangthoigianfree) / 24);
                        soduThoigian = Convert.ToInt32((sogio - khoangthoigianfree) % 24);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * phieu.TienQuaGio + phieu.TienChuyenPhong);
                    }
                }

                return TongTien;
            }
        }
        public double tinh_tien(string maPhieuThue,string sHinhThuc, string sloaiphong)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from pt in data.PhieuThues
                             join hinhthuc in data.HinhThucs on pt.MaHinhThuc equals hinhthuc.MaHinhThuc
                             join phong in data.Phongs on pt.MaPhong equals phong.MaPhong
                             join loaiphong in data.LoaiPhongs on phong.LoaiPhong equals loaiphong.MaLoaiPhong
                             from hinhthuc_loaiphong in data.HinhThuc_LoaiPhong
                             where pt.MaPhieuThue == maPhieuThue && sHinhThuc == hinhthuc_loaiphong.MaHinhThuc && hinhthuc_loaiphong.MaLoaiPhong == sloaiphong
                             select new
                             {
                                 pt.NgayBatDauThue,
                                 hinhthuc_loaiphong.DonGia,
                                 hinhthuc_loaiphong.TiepTheo,
                                 pt.TienChuyenPhong
                             }).FirstOrDefault();

                var vhinhthuc = (from ht in data.HinhThucs
                                where ht.MaHinhThuc == sHinhThuc
                                select ht).FirstOrDefault();

                var vloaiphong = (from lp in data.LoaiPhongs
                                 where lp.MaLoaiPhong == sloaiphong
                                  select lp).FirstOrDefault();

     
                var qd = (from quydinh in data.QuyDinhs
                          where quydinh.STT == 1
                          select quydinh).FirstOrDefault();
                DateTime giotrongphieu = (DateTime)phieu.NgayBatDauThue;
                DateTime Hientai = DateTime.Now;
                TimeSpan thoigianlech = Hientai.Subtract(giotrongphieu);
                int sogio = (thoigianlech.Days * 24) + thoigianlech.Hours;
                int sophut = thoigianlech.Minutes;
                int boisoThoigian = 0;
                int soduThoigian = 0;
                //Tinh tien theo thoi gian o
                if (vhinhthuc.ThoiGianKeoDai != null)
                {
                    if (sophut >= qd.GiaTri)
                    {
                        sogio++;
                    }
                    if (sogio >= vhinhthuc.ThoiGianKeoDai)
                    {
                        boisoThoigian = Convert.ToInt32((sogio - vhinhthuc.ThoiGianKeoDai) / vhinhthuc.ThoiGianKeoDai);
                        Console.WriteLine(boisoThoigian);
                        soduThoigian = Convert.ToInt32((sogio - vhinhthuc.ThoiGianKeoDai) % vhinhthuc.ThoiGianKeoDai);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * vloaiphong.TienQuaGio + phieu.TienChuyenPhong);
                    }
                    else
                    {
                        double sogioMien = Convert.ToDouble(vhinhthuc.GioKetThuc);
                        TongTien = Convert.ToDouble(phieu.DonGia + phieu.TienChuyenPhong);
                    }
                }
                //Tinh tien theo moc thoi gian tra phong
                else
                {
                    if (sophut + giotrongphieu.Minute >= qd.GiaTri)
                    {
                        if ((sophut + giotrongphieu.Minute) >= (60 + qd.GiaTri))
                            sogio += 2;
                        sogio++;
                    }
                    int khoangthoigianfree;
                    if (giotrongphieu.Hour < vhinhthuc.GioKetThuc)
                    {
                        khoangthoigianfree = Convert.ToInt32(vhinhthuc.GioKetThuc - giotrongphieu.Hour);
                    }
                    else
                    {
                        khoangthoigianfree = Convert.ToInt32(vhinhthuc.GioKetThuc + 24 - giotrongphieu.Hour);
                    }
                    if (sogio <= khoangthoigianfree)
                    {
                        TongTien = Convert.ToDouble(phieu.DonGia + phieu.TienChuyenPhong);
                    }
                    else
                    {
                        boisoThoigian = Convert.ToInt32((sogio - khoangthoigianfree) / 24);
                        soduThoigian = Convert.ToInt32((sogio - khoangthoigianfree) % 24);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * vloaiphong.TienQuaGio + phieu.TienChuyenPhong);
                    }
                }
                return TongTien;
            }
        }

        public double tinh_tien_phong(string maPhieuThue)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from pt in data.PhieuThues
                             join hinhthuc in data.HinhThucs on pt.MaHinhThuc equals hinhthuc.MaHinhThuc
                             join phong in data.Phongs on pt.MaPhong equals phong.MaPhong
                             join loaiphong in data.LoaiPhongs on phong.LoaiPhong equals loaiphong.MaLoaiPhong
                             from hinhthuc_loaiphong in data.HinhThuc_LoaiPhong
                             where pt.MaPhieuThue == maPhieuThue && hinhthuc.MaHinhThuc == hinhthuc_loaiphong.MaHinhThuc && hinhthuc_loaiphong.MaLoaiPhong == loaiphong.MaLoaiPhong
                             select new
                             {
                                 pt.NgayBatDauThue,
                                 loaiphong.TienQuaGio,
                                 hinhthuc.ThoiGianKeoDai,
                                 hinhthuc.GioKetThuc,
                                 hinhthuc_loaiphong.DonGia,
                                 hinhthuc_loaiphong.TiepTheo,
                                 pt.TienChuyenPhong
                             }).FirstOrDefault();

                var qd = (from quydinh in data.QuyDinhs
                          where quydinh.STT == 1
                          select quydinh).FirstOrDefault();
                DateTime giotrongphieu = (DateTime)phieu.NgayBatDauThue;
                DateTime Hientai = DateTime.Now;
                TimeSpan thoigianlech = Hientai.Subtract(giotrongphieu);
                int sogio = (thoigianlech.Days * 24) + thoigianlech.Hours;
                int sophut = thoigianlech.Minutes;
                int boisoThoigian = 0;
                int soduThoigian = 0;
                //Tinh tien theo thoi gian o
                if (phieu.ThoiGianKeoDai != null)
                {
                    if (sophut >= qd.GiaTri)
                    {
                        sogio++;
                    }
                    if (sogio >= phieu.ThoiGianKeoDai)
                    {
                        boisoThoigian = Convert.ToInt32((sogio - phieu.ThoiGianKeoDai) / phieu.ThoiGianKeoDai);
                        soduThoigian = Convert.ToInt32((sogio - phieu.ThoiGianKeoDai) % phieu.ThoiGianKeoDai);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * phieu.TienQuaGio);
                    }
                    else
                    {
                        double sogioMien = Convert.ToDouble(phieu.GioKetThuc);
                        TongTien = Convert.ToDouble(phieu.DonGia);
                    }
                }
                //Tinh tien theo moc thoi gian tra phong
                else
                {
                    if (sophut + giotrongphieu.Minute >= qd.GiaTri)
                    {
                        if ((sophut + giotrongphieu.Minute) >= (60 + qd.GiaTri))
                            sogio += 2;
                        sogio++;
                    }
                    int khoangthoigianfree;
                    if (giotrongphieu.Hour < phieu.GioKetThuc)
                    {
                        khoangthoigianfree = Convert.ToInt32(phieu.GioKetThuc - giotrongphieu.Hour);
                    }
                    else
                    {
                        khoangthoigianfree = Convert.ToInt32(phieu.GioKetThuc + 24 - giotrongphieu.Hour);
                    }
                    if (sogio <= khoangthoigianfree)
                    {
                        TongTien = Convert.ToDouble(phieu.DonGia);
                    }
                    else
                    {
                        boisoThoigian = Convert.ToInt32((sogio - khoangthoigianfree) / 24);
                        soduThoigian = Convert.ToInt32((sogio - khoangthoigianfree) % 24);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * phieu.TienQuaGio);
                    }
                }

                return TongTien;
            }
        }
        public void Them_Phieu_Thue(PhieuThue phieu)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.PhieuThues.Add(phieu);
                data.SaveChanges();
            }
        }
        public void Cap_Nhat_Ngay_Tra(string maphieu)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from p in data.PhieuThues
                            where p.MaPhieuThue == maphieu
                            select p).FirstOrDefault();
                phieu.NgayTraPhong = DateTime.Now;
                data.SaveChanges();
            }
        }
        public TimeSpan Gio_Da_Thue(string maphieu)
        {
            TimeSpan gio_chenh_lech;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from p in data.PhieuThues
                             where p.MaPhieuThue == maphieu
                             select p).FirstOrDefault();
                gio_chenh_lech = DateTime.Now - (DateTime)phieu.NgayBatDauThue;
            }
            return gio_chenh_lech;
        }
        public void Cap_Nhat_Phieu_Thue(PhieuThue phieu)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                PhieuThue phieutam =  data.PhieuThues.Find(phieu.MaPhieuThue);
                if (phieutam != null)
                {
                    phieutam.MaPhong = phieu.MaPhong;
                    phieutam.MaHinhThuc = phieu.MaHinhThuc;
                    phieutam.MaKH = phieu.MaKH;
                    phieutam.SoNguoiThue = phieu.SoNguoiThue;
                    phieutam.TienChuyenPhong = phieu.TienChuyenPhong;
                }
                data.SaveChanges();
            }
        }
        public string Tao_Ma_Moi()
        {
            string g = "MPT000";
            string id = null;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                id = data.PhieuThues.Max(ma => ma.MaPhieuThue);
            }
            if (id == null)
            {
                g = "MPT0000001";
            }
            else
            {
                int n = 0;
                n = Convert.ToInt32(id.Substring(6, 4));
                n = n + 1;
                if (n < 10)
                {
                    g = g + "000";
                    g = g + n.ToString();
                }
                else if (n < 100)
                {
                    g = g + "00";
                    g = g + n.ToString();
                }
                else if (n < 1000)
                {
                    g = g + "0";
                    g = g + n.ToString();
                }
            }
            return g;
        }
    }
}
