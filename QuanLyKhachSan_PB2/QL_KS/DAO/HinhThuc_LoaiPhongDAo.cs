﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class HinhThuc_LoaiPhongDAO
    {
        public void Them(HinhThuc_LoaiPhong ht_lp)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.HinhThuc_LoaiPhong.Add(ht_lp);
                data.SaveChanges();
            }
        }
        public void Sua(HinhThuc_LoaiPhong ht_lp)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                HinhThuc_LoaiPhong tam = (from htlp in data.HinhThuc_LoaiPhong
                                          where htlp.MaHinhThuc == ht_lp.MaHinhThuc && htlp.MaLoaiPhong == ht_lp.MaLoaiPhong
                                          select htlp).FirstOrDefault();
                tam.TiepTheo = ht_lp.TiepTheo;
                tam.DonGia = ht_lp.DonGia;
                data.SaveChanges();
            }
        }
        public static HinhThuc_LoaiPhong Tim_HT_LP(string maHT, string maLP)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                HinhThuc_LoaiPhong hinhthuc = (from ht_lp in data.HinhThuc_LoaiPhong
                                               where ht_lp.MaHinhThuc == maHT && ht_lp.MaLoaiPhong == maLP
                                               select ht_lp).FirstOrDefault();
                if (hinhthuc != null)
                    return hinhthuc;
                else
                    return null;
            }
        }
    }
}
