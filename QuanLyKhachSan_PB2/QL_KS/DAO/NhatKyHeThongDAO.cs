﻿using QL_KS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NhapDoAn.DAO
{
    public class NhatKyHeThongDAO
    {
        QLKhachSanEntities data;

        public List<NhatKyHoatDong> LoadNhatKyHoatDong()
        {
            data = new QLKhachSanEntities();
            return data.NhatKyHoatDongs.ToList();
        }
       
        // Xóa nhật ký
        public void XoaNhatKy(NhatKyHoatDong nhatky)
        {
            data = new QLKhachSanEntities();
            foreach (var kq in data.NhatKyHoatDongs.ToList())
            {
                if (kq.STT == nhatky.STT)
                {
                    data.NhatKyHoatDongs.Remove(kq);
                    data.SaveChanges();
                }
            }
        }
        // Xóa toàn bộ nhật ký
        public void XoaToanBoNhatKy()
        {
            data = new QLKhachSanEntities();
            foreach (var kq in data.NhatKyHoatDongs.ToList())
            {
                data.NhatKyHoatDongs.Remove(kq);
                data.SaveChanges();
            }
        }
      
        public void ThemNhatKy(NhatKyHoatDong nk)
        {
            data = new QLKhachSanEntities();
            data.NhatKyHoatDongs.Add(nk);
            data.SaveChanges();
        }
    }
}
