﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class ChiTietHoaDonDAO
    {
        public static void Them_Moi(ChiTietHoaDon chitietnew)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.ChiTietHoaDons.Add(chitietnew);
                data.SaveChanges();
            }
        }
        public static string ID()
        {
            string g = "MCTHD0";
            string id = null;
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                id = data.ChiTietHoaDons.Max(ma => ma.MaCTHD);
            }
            if (id == null)
            {
                g = "MCTHD00001";
            }
            else
            {
                int n = 0;
                n = Convert.ToInt32(id.Substring(6, 4));
                n = n + 1;
                if (n < 10)
                {
                    g = g + "000";
                    g = g + n.ToString();
                }
                else if (n < 100)
                {
                    g = g + "00";
                    g = g + n.ToString();
                }
                else if (n < 1000)
                {
                    g = g + "0";
                    g = g + n.ToString();
                }
            }
            return g;
        }
    }
}
