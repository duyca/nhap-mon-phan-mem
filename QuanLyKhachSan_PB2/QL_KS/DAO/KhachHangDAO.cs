﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    public class KhachHangDAO
    {
        public KhachHang TimKiemkhachHang(string maKH)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var khachhang = (from kh in data.KhachHangs
                                where kh.MaKH == maKH
                                 select kh).FirstOrDefault();
                if (khachhang != null)
                    return khachhang;
                else
                    return null;
            }
        }
        public void Them_Khach_Hang(KhachHang khach)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                data.KhachHangs.Add(khach);
                data.SaveChanges();
            }
        }
        public void Cap_Nhat_Khach(KhachHang khach)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                KhachHang khachtam =  data.KhachHangs.Find(khach.MaKH);
                khachtam.TenKH = khach.TenKH;
                khachtam.CMND = khach.CMND;
                khachtam.DiaChi = khach.DiaChi;
                khachtam.SDT = khach.SDT;
                khachtam.LoaiKH = khach.LoaiKH;
                data.SaveChanges();
            }
        }

        //TrangTHB - Quản lý khách hàng
        static QLKhachSanEntities db = new QLKhachSanEntities();

        public static IEnumerable<KHNEW> LoadDS()
        {
            IEnumerable<KHNEW> ds = (from k in db.KhachHangs
                                     join l in db.LoaiKhachHangs on k.LoaiKH equals l.MaLoaiKH
                                     select new KHNEW()
                                     {
                                         MaKH = k.MaKH,
                                         TenKH = k.TenKH,
                                         CMND = k.CMND,
                                         DC = k.DiaChi,
                                         SDT = k.SDT,
                                         LoaiKH = l.TenLoaiKH
                                     }).ToList<KHNEW>();

            return ds;
        }

        public static IEnumerable<KHNEW> SearchName(string name)
        {
            IEnumerable<KHNEW> ds = (from k in db.KhachHangs
                                     join l in db.LoaiKhachHangs on k.LoaiKH equals l.MaLoaiKH
                                     where k.TenKH.Contains(name)
                                     select new KHNEW()
                                     {
                                         MaKH = k.MaKH,
                                         TenKH = k.TenKH,
                                         CMND = k.CMND,
                                         DC = k.DiaChi,
                                         SDT = k.SDT,
                                         LoaiKH = l.TenLoaiKH
                                     }).ToList<KHNEW>();

            return ds;
        }

        public static IEnumerable<KHNEW> SearchCMND(string cmnd)
        {
            IEnumerable<KHNEW> ds = (from k in db.KhachHangs
                                     join l in db.LoaiKhachHangs on k.LoaiKH equals l.MaLoaiKH
                                     where k.CMND.Contains(cmnd)
                                     select new KHNEW()
                                     {
                                         MaKH = k.MaKH,
                                         TenKH = k.TenKH,
                                         CMND = k.CMND,
                                         DC = k.DiaChi,
                                         SDT = k.SDT,
                                         LoaiKH = l.TenLoaiKH
                                     }).ToList<KHNEW>();

            return ds;
        }

        public static IEnumerable<KHNEW> SearchAll(string name, string cmnd)
        {
            IEnumerable<KHNEW> ds = (from k in db.KhachHangs
                                     join l in db.LoaiKhachHangs on k.LoaiKH equals l.MaLoaiKH
                                     where k.TenKH.Contains(name) && k.CMND.Contains(cmnd)
                                     select new KHNEW()
                                     {
                                         MaKH = k.MaKH,
                                         TenKH = k.TenKH,
                                         CMND = k.CMND,
                                         DC = k.DiaChi,
                                         SDT = k.SDT,
                                         LoaiKH = l.TenLoaiKH
                                     }).ToList<KHNEW>();

            return ds;
        }

        public static List<LoaiKhachHang> LoadKH()
        {
            return db.LoaiKhachHangs.ToList();
        }

        public static string ID()
        {
            string g = "MKH000";
            string id = db.KhachHangs.Max(ma => ma.MaKH);
            if (id == null)
            {
                g = "MKH0000001";
            }
            else
            {
                int n = 0;
                n = Convert.ToInt32(id.Substring(6, 4));
                n = n + 1;
                if (n < 10)
                {
                    g = g + "000";
                    g = g + n.ToString();
                }
                else if (n < 100)
                {
                    g = g + "00";
                    g = g + n.ToString();
                }
                else if (n < 1000)
                {
                    g = g + "0";
                    g = g + n.ToString();
                }
            }
            return g;
        }

        public static string TimLoaiKH(string loai)
        {
            var type = (from l in db.LoaiKhachHangs
                        join k in db.KhachHangs on l.MaLoaiKH equals k.LoaiKH
                        where l.TenLoaiKH == loai
                        select k.LoaiKH).FirstOrDefault();

            return type.ToString();
        }

        public static bool Insert(KhachHang kh)
        {
            db.KhachHangs.Add(kh);
            db.SaveChanges();

            return true;
        }

        public static bool Update(KhachHang kh)
        {
            var k = db.KhachHangs.Where(ma => ma.MaKH == kh.MaKH).FirstOrDefault();

            k.MaKH = kh.MaKH;
            k.TenKH = kh.TenKH;
            k.CMND = kh.CMND;
            k.SDT = kh.SDT;
            k.DiaChi = kh.DiaChi;
            k.LoaiKH = kh.LoaiKH;

            db.SaveChanges();

            return true;
        }

        public static IList<KHNEW> SearchID(string id)
        {
            IList<KHNEW> ds = (from k in db.KhachHangs
                               join l in db.LoaiKhachHangs on k.LoaiKH equals l.MaLoaiKH
                               where k.MaKH == id
                               select new KHNEW()
                               {
                                   MaKH = k.MaKH,
                                   TenKH = k.TenKH,
                                   CMND = k.CMND,
                                   DC = k.DiaChi,
                                   SDT = k.SDT,
                                   LoaiKH = l.TenLoaiKH
                               }).ToList<KHNEW>();

            return ds;
        }

        public static void Xoa(string makh)
        {
            db = new QLKhachSanEntities();
            var kq = db.KhachHangs.Find(makh);
            db.KhachHangs.Remove(kq);
            db.SaveChanges();
        }

        public static bool TimKiemHoaDonCuaKH(string makh)
        {
            db = new QLKhachSanEntities();

            var check = db.HoaDons.Any(l => l.MaKH == makh);

            if (check == false)
            {
                return false;
            }
            return true;
        }
        public static bool TimKiemPhieuThueCuaKH(string makh)
        {
            db = new QLKhachSanEntities();

            var check = db.PhieuThues.Any(l => l.MaKH == makh);

            if (check == false)
            {
                return false;
            }
            return true;
        }
        public static bool Delete(string ma)
        {
            bool kq = false;
            var check = db.HoaDons.Any(kh => kh.MaKH == ma);

            if (check)
            {
                kq = false;
            }
            else if (!check)
            {
                var de = db.KhachHangs.Where(kh => kh.MaKH == ma).FirstOrDefault();
                db.KhachHangs.Remove(de);
                db.SaveChanges();

                kq = true;
            }

            return kq;
        }
    }

}

