﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    public class LoaiKHDAO
    {
        static QLKhachSanEntities db = new QLKhachSanEntities();

        public static List<LoaiKhachHang> LoadLKH()
        {
            return db.LoaiKhachHangs.ToList();
        }

        public static bool Delete(int ma)
        {
            bool kq = false;
            var check = db.KhachHangs.Any(l => l.LoaiKH == ma);

            if (check)
            {
                kq = false;
            }
            else if (!check)
            {
                var de = db.LoaiKhachHangs.Where(l => l.MaLoaiKH == ma).FirstOrDefault();
                db.LoaiKhachHangs.Remove(de);
                db.SaveChanges();

                kq = true;
            }
            return kq;
        }

        public static bool Update(LoaiKhachHang l)
        {
            var k = db.LoaiKhachHangs.Where(ma => ma.MaLoaiKH == l.MaLoaiKH).FirstOrDefault();

            //k.MaLoaiKH = l.MaLoaiKH;
            k.TenLoaiKH = l.TenLoaiKH;
            k.HeSo = l.HeSo;

            db.SaveChanges();

            return true;
        }

        public static bool Insert(LoaiKhachHang l)
        {
            db.LoaiKhachHangs.Add(l);
            db.SaveChanges();

            return true;
        }
        public static bool TimKiem(string ten)
        {
            db = new QLKhachSanEntities();
            var check = db.LoaiKhachHangs.Any(l => l.TenLoaiKH == ten);
            if(check == true)
                return true;
            return false;
        }
        public static bool TimKhachHangCoCungLoai(int maloai)
        {
            db = new QLKhachSanEntities();
           
            var check = db.KhachHangs.Any(l => l.LoaiKH == maloai);

            if (check == false)
            {
                return false;
            }
            return true;
        }
        public static void Xoa(int maloai)
        {
            db = new QLKhachSanEntities();
            var kq = db.LoaiKhachHangs.Find(maloai);
            db.LoaiKhachHangs.Remove(kq);
            db.SaveChanges();
        }
    }
}
 