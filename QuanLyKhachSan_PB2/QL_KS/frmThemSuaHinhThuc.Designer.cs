﻿namespace QL_KS
{
    partial class frmThemSuaHinhThuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmThemSuaHinhThuc));
            this.btnHuyBo = new System.Windows.Forms.Button();
            this.btnTraPhong = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbGioKetThuc = new System.Windows.Forms.RadioButton();
            this.rbKeoDai = new System.Windows.Forms.RadioButton();
            this.txtGioKetThuc = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.label4 = new System.Windows.Forms.Label();
            this.lbThongBao_ThoiGian = new System.Windows.Forms.Label();
            this.lbThongBao_Ten = new System.Windows.Forms.Label();
            this.txtGioKeoDai = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtTenHinhThuc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaHinhThuc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.pnwapDongia = new System.Windows.Forms.Panel();
            this.fllKetiep = new System.Windows.Forms.FlowLayoutPanel();
            this.fllDonGia = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.pnwapDongia.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.BackColor = System.Drawing.SystemColors.Menu;
            this.btnHuyBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHuyBo.FlatAppearance.BorderSize = 0;
            this.btnHuyBo.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.ForeColor = System.Drawing.Color.Crimson;
            this.btnHuyBo.Location = new System.Drawing.Point(567, 512);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(110, 40);
            this.btnHuyBo.TabIndex = 5;
            this.btnHuyBo.Text = "Hủy Bỏ";
            this.btnHuyBo.UseVisualStyleBackColor = false;
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnTraPhong
            // 
            this.btnTraPhong.BackColor = System.Drawing.SystemColors.Menu;
            this.btnTraPhong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTraPhong.FlatAppearance.BorderSize = 0;
            this.btnTraPhong.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraPhong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnTraPhong.Location = new System.Drawing.Point(451, 512);
            this.btnTraPhong.Name = "btnTraPhong";
            this.btnTraPhong.Size = new System.Drawing.Size(110, 40);
            this.btnTraPhong.TabIndex = 4;
            this.btnTraPhong.Text = "Thêm Mới";
            this.btnTraPhong.UseVisualStyleBackColor = false;
            this.btnTraPhong.Click += new System.EventHandler(this.btnTraPhong_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(9, 16);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(671, 41);
            this.panel2.TabIndex = 68;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(256, 30);
            this.label6.TabIndex = 0;
            this.label6.Text = "THÔNG TIN HÌNH THỨC";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(61, 463);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(525, 17);
            this.label9.TabIndex = 83;
            this.label9.Text = "Chọn \"Giờ Kết Thúc: \" Để định nghĩa giờ kết thúc một đơn vị hình thức vào ngày hô" +
    "m sau";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(61, 443);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(459, 17);
            this.label8.TabIndex = 82;
            this.label8.Text = "Chọn \"Kéo Dài Trong: \" Để định nghĩa số giờ tồn tại của một đơn vị hình thức";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 442);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 17);
            this.label5.TabIndex = 81;
            this.label5.Text = "Lưu ý:";
            // 
            // rbGioKetThuc
            // 
            this.rbGioKetThuc.AutoSize = true;
            this.rbGioKetThuc.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbGioKetThuc.Location = new System.Drawing.Point(321, 187);
            this.rbGioKetThuc.Name = "rbGioKetThuc";
            this.rbGioKetThuc.Size = new System.Drawing.Size(14, 13);
            this.rbGioKetThuc.TabIndex = 80;
            this.rbGioKetThuc.UseVisualStyleBackColor = true;
            // 
            // rbKeoDai
            // 
            this.rbKeoDai.AutoSize = true;
            this.rbKeoDai.Checked = true;
            this.rbKeoDai.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbKeoDai.Location = new System.Drawing.Point(321, 147);
            this.rbKeoDai.Name = "rbKeoDai";
            this.rbKeoDai.Size = new System.Drawing.Size(14, 13);
            this.rbKeoDai.TabIndex = 79;
            this.rbKeoDai.TabStop = true;
            this.rbKeoDai.UseVisualStyleBackColor = true;
            // 
            // txtGioKetThuc
            // 
            // 
            // 
            // 
            this.txtGioKetThuc.BackgroundStyle.Class = "TextBoxBorder";
            this.txtGioKetThuc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGioKetThuc.ButtonClear.Visible = true;
            this.txtGioKetThuc.Enabled = false;
            this.txtGioKetThuc.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGioKetThuc.Location = new System.Drawing.Point(105, 183);
            this.txtGioKetThuc.Mask = "00";
            this.txtGioKetThuc.Name = "txtGioKetThuc";
            this.txtGioKetThuc.Size = new System.Drawing.Size(210, 20);
            this.txtGioKetThuc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtGioKetThuc.TabIndex = 2;
            this.txtGioKetThuc.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 77;
            this.label4.Text = "Giờ Kết Thúc";
            // 
            // lbThongBao_ThoiGian
            // 
            this.lbThongBao_ThoiGian.AutoSize = true;
            this.lbThongBao_ThoiGian.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_ThoiGian.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_ThoiGian.Location = new System.Drawing.Point(102, 124);
            this.lbThongBao_ThoiGian.Name = "lbThongBao_ThoiGian";
            this.lbThongBao_ThoiGian.Size = new System.Drawing.Size(215, 17);
            this.lbThongBao_ThoiGian.TabIndex = 76;
            this.lbThongBao_ThoiGian.Text = "Hãy nhập vào một trong hai ô dưới";
            this.lbThongBao_ThoiGian.Visible = false;
            // 
            // lbThongBao_Ten
            // 
            this.lbThongBao_Ten.AutoSize = true;
            this.lbThongBao_Ten.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_Ten.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_Ten.Location = new System.Drawing.Point(451, 79);
            this.lbThongBao_Ten.Name = "lbThongBao_Ten";
            this.lbThongBao_Ten.Size = new System.Drawing.Size(170, 17);
            this.lbThongBao_Ten.TabIndex = 75;
            this.lbThongBao_Ten.Text = "Hãy nhập vào tên hình thức:";
            this.lbThongBao_Ten.Visible = false;
            // 
            // txtGioKeoDai
            // 
            // 
            // 
            // 
            this.txtGioKeoDai.BackgroundStyle.Class = "TextBoxBorder";
            this.txtGioKeoDai.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGioKeoDai.ButtonClear.Visible = true;
            this.txtGioKeoDai.Enabled = false;
            this.txtGioKeoDai.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGioKeoDai.Location = new System.Drawing.Point(105, 143);
            this.txtGioKeoDai.Mask = "0000";
            this.txtGioKeoDai.Name = "txtGioKeoDai";
            this.txtGioKeoDai.Size = new System.Drawing.Size(210, 20);
            this.txtGioKeoDai.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtGioKeoDai.TabIndex = 1;
            this.txtGioKeoDai.Text = "";
            // 
            // txtTenHinhThuc
            // 
            this.txtTenHinhThuc.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTenHinhThuc.Border.Class = "TextBoxBorder";
            this.txtTenHinhThuc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenHinhThuc.DisabledBackColor = System.Drawing.Color.White;
            this.txtTenHinhThuc.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHinhThuc.ForeColor = System.Drawing.Color.Black;
            this.txtTenHinhThuc.Location = new System.Drawing.Point(451, 98);
            this.txtTenHinhThuc.MaxLength = 30;
            this.txtTenHinhThuc.Name = "txtTenHinhThuc";
            this.txtTenHinhThuc.PreventEnterBeep = true;
            this.txtTenHinhThuc.Size = new System.Drawing.Size(230, 25);
            this.txtTenHinhThuc.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 72;
            this.label1.Text = "Kéo Dài Trong:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(351, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 17);
            this.label3.TabIndex = 71;
            this.label3.Text = "Tên Hình Thức:";
            // 
            // txtMaHinhThuc
            // 
            this.txtMaHinhThuc.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMaHinhThuc.Border.Class = "TextBoxBorder";
            this.txtMaHinhThuc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaHinhThuc.DisabledBackColor = System.Drawing.Color.White;
            this.txtMaHinhThuc.Enabled = false;
            this.txtMaHinhThuc.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHinhThuc.ForeColor = System.Drawing.Color.Black;
            this.txtMaHinhThuc.Location = new System.Drawing.Point(105, 97);
            this.txtMaHinhThuc.Name = "txtMaHinhThuc";
            this.txtMaHinhThuc.PreventEnterBeep = true;
            this.txtMaHinhThuc.Size = new System.Drawing.Size(230, 25);
            this.txtMaHinhThuc.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 69;
            this.label2.Text = "Mã Hình Thức:";
            // 
            // pnwapDongia
            // 
            this.pnwapDongia.AutoScroll = true;
            this.pnwapDongia.Controls.Add(this.fllKetiep);
            this.pnwapDongia.Controls.Add(this.fllDonGia);
            this.pnwapDongia.Location = new System.Drawing.Point(0, 265);
            this.pnwapDongia.Name = "pnwapDongia";
            this.pnwapDongia.Size = new System.Drawing.Size(689, 125);
            this.pnwapDongia.TabIndex = 84;
            // 
            // fllKetiep
            // 
            this.fllKetiep.Location = new System.Drawing.Point(349, 12);
            this.fllKetiep.Name = "fllKetiep";
            this.fllKetiep.Size = new System.Drawing.Size(338, 46);
            this.fllKetiep.TabIndex = 1;
            // 
            // fllDonGia
            // 
            this.fllDonGia.Location = new System.Drawing.Point(3, 12);
            this.fllDonGia.Name = "fllDonGia";
            this.fllDonGia.Size = new System.Drawing.Size(342, 46);
            this.fllDonGia.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(7, 211);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(673, 41);
            this.panel1.TabIndex = 85;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 30);
            this.label7.TabIndex = 0;
            this.label7.Text = "ĐƠN GIÁ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmThemSuaHinhThuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 565);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnwapDongia);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rbGioKetThuc);
            this.Controls.Add(this.rbKeoDai);
            this.Controls.Add(this.txtGioKetThuc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbThongBao_ThoiGian);
            this.Controls.Add(this.lbThongBao_Ten);
            this.Controls.Add(this.txtGioKeoDai);
            this.Controls.Add(this.txtTenHinhThuc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMaHinhThuc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnHuyBo);
            this.Controls.Add(this.btnTraPhong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmThemSuaHinhThuc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Hình Thức";
            this.Load += new System.EventHandler(this.frmThemSuaHinhThuc_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnwapDongia.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHuyBo;
        private System.Windows.Forms.Button btnTraPhong;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbGioKetThuc;
        private System.Windows.Forms.RadioButton rbKeoDai;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtGioKetThuc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbThongBao_ThoiGian;
        private System.Windows.Forms.Label lbThongBao_Ten;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtGioKeoDai;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenHinhThuc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaHinhThuc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnwapDongia;
        private System.Windows.Forms.FlowLayoutPanel fllKetiep;
        private System.Windows.Forms.FlowLayoutPanel fllDonGia;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
    }
}