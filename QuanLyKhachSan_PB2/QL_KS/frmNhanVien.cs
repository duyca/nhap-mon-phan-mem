﻿using QL_KS.BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace QL_KS
{
    public partial class frmNhanVien : Form
    {
        private NhanVienBUS nvBUS;
        LoaiNhanVienBUS lnvBUS;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmNhanVien).Name);
        public frmNhanVien()
        {
            InitializeComponent();
            nvBUS = new NhanVienBUS();
            lnvBUS = new LoaiNhanVienBUS();
        }

        private void frmNhanVien_Load(object sender, EventArgs e)
        {
            LoadDS();
        }
        private void LoadDS()
        {
            repositoryItemLookUpEdit1.DataSource = lnvBUS.LoadDS();
            gridControl1.DataSource = nvBUS.LoadDS();
        }
        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns[0]).ToString();
            DialogResult d = MessageBox.Show("Bạn có muốn xóa nhân viên mã "+ ma + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (d == DialogResult.Yes)
            {
                try
                {
                    int kq = nvBUS.Xoa(ma);
                    if(kq > 0)
                    {
                       MessageBox.Show("xóa nhân viên thành công ","thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("xóa nhân viên không thành công ", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    _logger.Error(ex.Message);
                }
                gridControl1.DataSource = nvBUS.LoadDS();
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            frmNhanVien_Them_Sua frm = new frmNhanVien_Them_Sua();
            frm.delegateLoadData += LoadDS;
            //frm.status = 0;
            frm.ShowDialog();

        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns[0]).ToString();

            frmNhanVien_Them_Sua frm = new frmNhanVien_Them_Sua(ma);
            frm.delegateLoadData += LoadDS;
            //frm.status = 0;
            frm.ShowDialog();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns[0]).ToString();

            frmNhanVien_Them_Sua frm = new frmNhanVien_Them_Sua(ma);
            frm.delegateLoadData += LoadDS;
            //frm.status = 0;
            frm.ShowDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns[0]).ToString();
            DialogResult d = MessageBox.Show("Bạn có muốn xóa nhân viên mã " + ma + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (d == DialogResult.Yes)
            {
                try
                {
                    int kq = nvBUS.Xoa(ma);
                    if (kq > 0)
                    {
                        MessageBox.Show("xóa nhân viên thành công ", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("xóa nhân viên không thành công do còn sử dụng ", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    _logger.Error(ex.Message);
                }
                gridControl1.DataSource = nvBUS.LoadDS();
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmNhanVien_Them_Sua frm = new frmNhanVien_Them_Sua();
            frm.delegateLoadData += LoadDS;
            //frm.status = 0;
            frm.ShowDialog();
        }
    }
}
