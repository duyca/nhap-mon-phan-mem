﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using System.Data.Entity;
using log4net;

namespace QL_KS
{
    public partial class frmHoaDon : Form
    {
        public frmHoaDon()
        {
            InitializeComponent();
            
        }
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmHoaDon).Name);
        HoaDonBUS bushoadon = new HoaDonBUS();
        KhachHangBus khachbus = new KhachHangBus();
        public int index;
        public string TaiKhoan;
        public void LoadDL()
        {
            lblMaKH.Text = "";
            lblTenKH.Text = "";
            lblCMND.Text = "";
            lblDiaChi.Text = "";
            lblSoDienThoai.Text = "";
            gcHoaDon.DataSource = bushoadon.LoadDL();
        }

        private void btnLoadLai_Click(object sender, EventArgs e)
        {
            LoadDL();
            gvHoaDon.ClearColumnsFilter();
        }

        private void frmHoaDon_Load(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            //if (index >= 0)
            //{
            //    string mahd = gvHoaDon.GetFocusedRowCellValue("MaHD").ToString().Trim();
            //    InHoaDon fr = new InHoaDon();
            //    fr.DataSource = bushoadon.InHoaDon(mahd);
            //    fr.ShowPreviewDialog();
            //}
            //else
            //{
            //    XtraMessageBox.Show("Chưa chọn hóa đơn cần in!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
            Hoa_Don hoadonmoi = new Hoa_Don();
            hoadonmoi.frmMaHoaDon = gvHoaDon.GetFocusedRowCellValue("MaHD").ToString().Trim();
            hoadonmoi.Show();
            LuuNhatKyHTBUS.Them(TaiKhoan, "Quản Lý Hóa Đơn", "In lại Hóa Đơn");
        }

        private void gvHoaDon_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }

        private void gvHoaDon_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (index >= 0)
            {
                lblMaKH.Text = gvHoaDon.GetRowCellValue(index, "MaKH").ToString();
                KhachHang khachtam = khachbus.TimKiemkhachHang(lblMaKH.Text);
                lblTenKH.Text = khachtam.TenKH.ToString();
                lblCMND.Text = khachtam.CMND.ToString();
                lblDiaChi.Text = khachtam.DiaChi.ToString();
                lblSoDienThoai.Text = khachtam.SDT.ToString();
            }
            else
            {
                lblMaKH.Text = "";
                lblTenKH.Text = "";
                lblCMND.Text = "";
                lblDiaChi.Text = "";
                lblSoDienThoai.Text = "";
            }
        }

        private void gvHoaDon_MasterRowEmpty(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventArgs e)
        {
            HoaDon hd = (HoaDon)gvHoaDon.GetRow(e.RowHandle);
            e.IsEmpty = hd.ChiTietHoaDons.Count == 0;
               
        }

        private void gvHoaDon_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void gvHoaDon_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Chi tiết hóa đơn";
        }

        private void gvHoaDon_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            HoaDon hd = (HoaDon)gvHoaDon.GetRow(e.RowHandle);
            e.ChildList = new BindingSource(hd, "ChiTietHoaDons");
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void cbTenKH_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTenKH.Checked)
            {
                txtTenKhach.Enabled = true;
                btmTimKiem.Enabled = true;
            }
            else
            {
                txtTenKhach.Enabled = false;
                if (!cbNgayThue.Checked)
                {
                    btmTimKiem.Enabled = false;
                }
            }
        }

        private void cbNgayThue_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNgayThue.Checked)
            {
                datetimepicker.Enabled = true;
                dateTimePicker2.Enabled = true;
                btmTimKiem.Enabled = true;
            }
            else
            {
                datetimepicker.Enabled = false;
                dateTimePicker2.Enabled = false;
                if (!cbTenKH.Checked)
                {
                    btmTimKiem.Enabled = false;
                }
            }
        }

        private void btmTimKiem_Click(object sender, EventArgs e)
        {
            string tenkh = null;
            DateTime tungay, denngay;
            tungay = datetimepicker.Value;
            denngay = dateTimePicker2.Value;
            if (cbTenKH.Checked)
            {
                tenkh = txtTenKhach.Text;
            }
            gcHoaDon.DataSource = bushoadon.LoadDL(tenkh, tungay, denngay);
        }

        private void btmTrongngay_Click(object sender, EventArgs e)
        {
            string tenkh = null;
            gcHoaDon.DataSource = bushoadon.LoadDL(tenkh, DateTime.Today, DateTime.Now);
        }

    }
}