﻿namespace QL_KS
{
    partial class frmThem_Sua_Phong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmThem_Sua_Phong));
            this.btnThuePhong = new System.Windows.Forms.Button();
            this.btnHuyBo = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_ThongBao_SoNguoi = new System.Windows.Forms.Label();
            this.lbThongbao_ten = new System.Windows.Forms.Label();
            this.txtSoNguoi = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtGhiChu = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cbbLoaiPhong = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtMaPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnThuePhong
            // 
            this.btnThuePhong.BackColor = System.Drawing.SystemColors.Menu;
            this.btnThuePhong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnThuePhong.FlatAppearance.BorderSize = 0;
            this.btnThuePhong.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThuePhong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnThuePhong.Location = new System.Drawing.Point(395, 270);
            this.btnThuePhong.Name = "btnThuePhong";
            this.btnThuePhong.Size = new System.Drawing.Size(110, 40);
            this.btnThuePhong.TabIndex = 5;
            this.btnThuePhong.Text = "Thêm Phòng";
            this.btnThuePhong.UseVisualStyleBackColor = false;
            this.btnThuePhong.Click += new System.EventHandler(this.btnDongY_Click);
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.BackColor = System.Drawing.SystemColors.Menu;
            this.btnHuyBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHuyBo.FlatAppearance.BorderSize = 0;
            this.btnHuyBo.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.ForeColor = System.Drawing.Color.Crimson;
            this.btnHuyBo.Location = new System.Drawing.Point(511, 270);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(110, 40);
            this.btnHuyBo.TabIndex = 6;
            this.btnHuyBo.Text = "Hủy Bỏ";
            this.btnHuyBo.UseVisualStyleBackColor = false;
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(9, 16);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(613, 41);
            this.panel2.TabIndex = 67;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN LOẠI PHÒNG";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_ThongBao_SoNguoi
            // 
            this.lb_ThongBao_SoNguoi.AutoSize = true;
            this.lb_ThongBao_SoNguoi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lb_ThongBao_SoNguoi.Location = new System.Drawing.Point(409, 135);
            this.lb_ThongBao_SoNguoi.Name = "lb_ThongBao_SoNguoi";
            this.lb_ThongBao_SoNguoi.Size = new System.Drawing.Size(117, 13);
            this.lb_ThongBao_SoNguoi.TabIndex = 79;
            this.lb_ThongBao_SoNguoi.Text = "Hãy nhập vào số người";
            this.lb_ThongBao_SoNguoi.Visible = false;
            // 
            // lbThongbao_ten
            // 
            this.lbThongbao_ten.AutoSize = true;
            this.lbThongbao_ten.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongbao_ten.Location = new System.Drawing.Point(409, 76);
            this.lbThongbao_ten.Name = "lbThongbao_ten";
            this.lbThongbao_ten.Size = new System.Drawing.Size(125, 13);
            this.lbThongbao_ten.TabIndex = 78;
            this.lbThongbao_ten.Text = "Hãy nhập vào tên phòng";
            this.lbThongbao_ten.Visible = false;
            // 
            // txtSoNguoi
            // 
            // 
            // 
            // 
            this.txtSoNguoi.BackgroundStyle.Class = "TextBoxBorder";
            this.txtSoNguoi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSoNguoi.ButtonClear.Visible = true;
            this.txtSoNguoi.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoNguoi.Location = new System.Drawing.Point(412, 154);
            this.txtSoNguoi.Mask = "00";
            this.txtSoNguoi.Name = "txtSoNguoi";
            this.txtSoNguoi.Size = new System.Drawing.Size(184, 20);
            this.txtSoNguoi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtSoNguoi.TabIndex = 4;
            this.txtSoNguoi.Text = "";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGhiChu.Border.Class = "TextBoxBorder";
            this.txtGhiChu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGhiChu.DisabledBackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.ForeColor = System.Drawing.Color.Black;
            this.txtGhiChu.Location = new System.Drawing.Point(114, 198);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.PreventEnterBeep = true;
            this.txtGhiChu.Size = new System.Drawing.Size(184, 42);
            this.txtGhiChu.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(53, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 75;
            this.label6.Text = "Ghi Chú:";
            // 
            // txtTenPhong
            // 
            this.txtTenPhong.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTenPhong.Border.Class = "TextBoxBorder";
            this.txtTenPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenPhong.DisabledBackColor = System.Drawing.Color.White;
            this.txtTenPhong.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPhong.ForeColor = System.Drawing.Color.Black;
            this.txtTenPhong.Location = new System.Drawing.Point(412, 95);
            this.txtTenPhong.Name = "txtTenPhong";
            this.txtTenPhong.PreventEnterBeep = true;
            this.txtTenPhong.Size = new System.Drawing.Size(184, 25);
            this.txtTenPhong.TabIndex = 3;
            // 
            // cbbLoaiPhong
            // 
            this.cbbLoaiPhong.DisplayMember = "Text";
            this.cbbLoaiPhong.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbLoaiPhong.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiPhong.ForeColor = System.Drawing.Color.Black;
            this.cbbLoaiPhong.FormattingEnabled = true;
            this.cbbLoaiPhong.ItemHeight = 20;
            this.cbbLoaiPhong.Location = new System.Drawing.Point(114, 151);
            this.cbbLoaiPhong.Name = "cbbLoaiPhong";
            this.cbbLoaiPhong.Size = new System.Drawing.Size(184, 26);
            this.cbbLoaiPhong.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbLoaiPhong.TabIndex = 1;
            // 
            // txtMaPhong
            // 
            this.txtMaPhong.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMaPhong.Border.Class = "TextBoxBorder";
            this.txtMaPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaPhong.DisabledBackColor = System.Drawing.Color.White;
            this.txtMaPhong.Enabled = false;
            this.txtMaPhong.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhong.ForeColor = System.Drawing.Color.Black;
            this.txtMaPhong.Location = new System.Drawing.Point(114, 95);
            this.txtMaPhong.Name = "txtMaPhong";
            this.txtMaPhong.PreventEnterBeep = true;
            this.txtMaPhong.Size = new System.Drawing.Size(184, 25);
            this.txtMaPhong.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(35, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 71;
            this.label5.Text = "Loại Phòng:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(323, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 70;
            this.label4.Text = "Tên Phòng(*):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(304, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 17);
            this.label3.TabIndex = 69;
            this.label3.Text = "Số Người Tối Đa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 68;
            this.label2.Text = "Mã Phòng:";
            // 
            // frmThem_Sua_Phong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 320);
            this.Controls.Add(this.lb_ThongBao_SoNguoi);
            this.Controls.Add(this.lbThongbao_ten);
            this.Controls.Add(this.txtSoNguoi);
            this.Controls.Add(this.txtGhiChu);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTenPhong);
            this.Controls.Add(this.cbbLoaiPhong);
            this.Controls.Add(this.txtMaPhong);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnHuyBo);
            this.Controls.Add(this.btnThuePhong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmThem_Sua_Phong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thêm Phòng";
            this.Load += new System.EventHandler(this.frmThem_Sua_Phong_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnThuePhong;
        private System.Windows.Forms.Button btnHuyBo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_ThongBao_SoNguoi;
        private System.Windows.Forms.Label lbThongbao_ten;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtSoNguoi;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGhiChu;
        private System.Windows.Forms.Label label6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenPhong;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbLoaiPhong;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaPhong;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}