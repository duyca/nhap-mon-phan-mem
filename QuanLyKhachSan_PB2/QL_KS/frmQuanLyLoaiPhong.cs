﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using System.Data.Entity;
using QL_KS.BUS;
using log4net;

namespace QL_KS
{
    public partial class frmQuanLyLoaiPhong : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmQuanLyLoaiPhong).Name);
        Loai_Phong_BUS bloaiphong = new Loai_Phong_BUS();
        Phong_BUS bphong = new Phong_BUS();
        public frmQuanLyLoaiPhong()
        {
            InitializeComponent();
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        private void load_data()
        {
            loaiPhongsBindingSource.DataSource = bloaiphong.Load_DS_Loai_Phong();
        }
        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmThem_Sua_Loai_Phong Them_Sua_Loai_Phong = new frmThem_Sua_Loai_Phong();
            Them_Sua_Loai_Phong.ShowDialog();
            load_data();
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmThem_Sua_Loai_Phong Them_Sua_Loai_Phong = new frmThem_Sua_Loai_Phong();
            Them_Sua_Loai_Phong.frmMaLoaiPhong = gv_DSLoaiPhong.GetRowCellValue(gv_DSLoaiPhong.FocusedRowHandle, "MaLoaiPhong").ToString();
            Them_Sua_Loai_Phong.ShowDialog();
            load_data();
        }

        private void frmQuanLyLoaiPhong_Load(object sender, EventArgs e)
        {
            load_data();
        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn muốn xóa loại phòng này?", "Xóa Loại Phòng", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (bphong.Load_DS_Phong(gv_DSLoaiPhong.GetRowCellValue(gv_DSLoaiPhong.FocusedRowHandle, "MaLoaiPhong").ToString()) == null)
                {
                    LoaiPhong loaitam = bloaiphong.Tim_Loai_Phong(gv_DSLoaiPhong.GetRowCellValue(gv_DSLoaiPhong.FocusedRowHandle, "MaLoaiPhong").ToString());
                    loaitam.active = false;
                    try
                    {
                        bloaiphong.Sua_Loai_Phong(loaitam);
                        MessageBox.Show("Xóa thành công");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        _logger.Error(ex.Message);
                    }
                    load_data();
                }
                else
                {
                    MessageBox.Show("Không thể xóa loại phòng này vì vẫn còn phòng trong hệ thống có loại phòng này");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }
    }
}