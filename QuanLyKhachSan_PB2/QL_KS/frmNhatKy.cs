﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using NhapDoAn.BUS;
using log4net;
using DevExpress.XtraEditors;

namespace QL_KS
{
    public partial class frmNhatKy : Form
    {
        public frmNhatKy()
        {
            InitializeComponent();
        }
        NhatKyHeThongBUS nkbus = new NhatKyHeThongBUS();
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmNhatKy).Name);

        public void LoadDL()
        {
            List<NhatKyHoatDong> list_nk = nkbus.LoadNhatKyHoatDong();
            gcHeThong.DataSource = list_nk;
        }

        private void btnXoaNhatKy_Click(object sender, EventArgs e)
        {
            if (gvData.State != DevExpress.XtraGrid.Views.Grid.GridState.Editing)
            {
                if (XtraMessageBox.Show("Bạn có chắc chắn xóa không?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (var nk in gvData.GetSelectedRows())
                    {
                        NhatKyHoatDong row = (NhatKyHoatDong)gvData.GetRow(nk);
                        nkbus.XoaNhatKy(row);
                    }
                    LoadDL();
                }
            }
        }

        private void frmNhatKy_Load(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void btnLuuXML_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "XML file (*.xml) | *.xml";
            save.RestoreDirectory = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                dt.Columns.Add("ID");
                dt.Columns.Add("TenMayTinh");
                dt.Columns.Add("ThoiGian");
                dt.Columns.Add("ChucNang");
                dt.Columns.Add("HanhDong");
                dt.Columns.Add("DoiTuong");
                ds.Tables.Add(dt);
                List<NhatKyHoatDong> list_nk = nkbus.LoadNhatKyHoatDong();
                foreach (var nk in list_nk)
                {
                    DataRow r = dt.NewRow();
                    r[0] = nk.ID.Trim();
                    r[1] = nk.TenMayTinh;
                    r[2] = nk.ThoiGian;
                    r[3] = nk.ChucNang;
                    r[4] = nk.HanhDong;
                    dt.Rows.Add(r);
                }
                ds.Tables[0].WriteXml(save.FileName);
                XtraMessageBox.Show("Lưu vào tập tin thành công!", "Thông báo");
            }
        }

        private void btnDocTapTinXML_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML File|*.xml";
            openFileDialog.Title = "Restore XML";
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DataSet ds = new DataSet();
                ds.ReadXml(openFileDialog.FileName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    NhatKyHoatDong nk = new NhatKyHoatDong();
                    nk.ID = r[0].ToString();
                    nk.TenMayTinh = r[1].ToString();
                    nk.ThoiGian = DateTime.Parse(r[2].ToString());
                    nk.ChucNang = r[3].ToString();
                    nk.HanhDong = r[4].ToString();
                    nkbus.ThemNhatKy(nk);
                }
                LoadDL();
            }
        }

        private void btnXuatTTExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel|*.xlsx";
                sfd.Title = "Xuất File Excel";
                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    gcHeThong.ExportToXlsx(sfd.FileName);
                    DialogResult dr = MessageBox.Show("Bạn xuất dữ liệu thành công. Bạn có muốn mở tập tin vừa xuất ra không?", "Xác nhận mở tập tin", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi xuất dữ liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _logger.Error(ex.ToString());
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoaToanBoNhatKy_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Bạn muốn xóa toàn bộ nhật ký hệ thống?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                nkbus.XoaToanBoNhatKy();
                LoadDL();
            }
        }

      
    }
}