﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QL_KS.BUS;
using DevComponents.DotNetBar;
using System.Drawing.Printing;
using log4net;

namespace QL_KS
{
    public partial class frmPhieuTraPhong : DevExpress.XtraEditors.XtraForm
    {

        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmPhieuTraPhong).Name);
        public string frmMaPhong;
        public string frmMaNV;
        private string sHinhThuc;
        private string frmMaPhieuThue;
        public string TaiKhoan;
        Phong_BUS bphong = new Phong_BUS();
        PhieuThueBUS bphieu = new PhieuThueBUS();
        KhachHangBus bKhach = new KhachHangBus();
        Hinh_Thuc_BUS bHinhThuc = new Hinh_Thuc_BUS();
        Loai_Khach_HangBUS bloaikhach = new Loai_Khach_HangBUS();
        HoaDonBUS bHoaDon = new HoaDonBUS();
        Loai_Phong_BUS bloaiphong = new Loai_Phong_BUS();
        QuyDinhBUS bquydinh = new QuyDinhBUS();

        public frmPhieuTraPhong()
        {
            InitializeComponent();
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPhieuTraPhong_Load(object sender, EventArgs e)
        {
            frmMaPhieuThue = null;
            cbbHinhThucThue.Items.Clear();
            cbbLoaiKhachHang.Items.Clear();
            cbbPhong.Items.Clear();
            PhieuThue phieu_tam = bphieu.Tim_Phieu_Thue(frmMaPhong);
            KhachHang khach_tam = bKhach.TimKiemkhachHang(phieu_tam.MaKH);
            txtTenKhachHang.Text = khach_tam.TenKH;
            txtCMND.Text = khach_tam.CMND;
            txtDiaChi.Text = khach_tam.DiaChi;
            txtSoDT.Text = khach_tam.SDT;
            txtNgayThue.Text = phieu_tam.NgayBatDauThue.ToString();
            txtGioHienTai.Text = DateTime.Now.ToString();
            txtSoNguoi.Text = phieu_tam.SoNguoiThue.ToString();
            txtPhuThu.Text = String.Format("{0:0,0 VND}", phieu_tam.TienChuyenPhong).ToString();

            Phong phonghientai = bphong.Tim_Phong(frmMaPhong);
            sHinhThuc = phieu_tam.MaHinhThuc;

            txtLoaiPhong.Text = bloaiphong.Tim_Loai_Phong(phonghientai.LoaiPhong.ToString()).TenLoaiPhong;
            ComboBoxItem item_hientai = new ComboBoxItem();
            item_hientai.Text = phonghientai.TenPhong;
            item_hientai.Name = phonghientai.MaPhong;
            cbbPhong.Items.Add(item_hientai);
            cbbPhong.SelectedItem = item_hientai;

            //don gia phong
            txtDonGia.Text = HinhThuc_LoaiPhongBUS.Tim_HT_LP(phieu_tam.MaHinhThuc, phonghientai.LoaiPhong).DonGia.ToString();

            foreach (Phong ph in bphong.DS_Phong_Trong())
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Text = ph.TenPhong;
                item.Name = ph.MaPhong;
                cbbPhong.Items.Add(item);
            }
            foreach (LoaiKhachHang lkh in bloaikhach.Load_DS_Loai_Khach())
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Text = lkh.TenLoaiKH;
                item.Name = lkh.MaLoaiKH.ToString();
                cbbLoaiKhachHang.Items.Add(item);
                if (lkh.MaLoaiKH == khach_tam.LoaiKH)
                {
                    cbbLoaiKhachHang.SelectedItem = item;
                }
            }
            foreach (HinhThuc ht in bHinhThuc.Load_DS_Hinh_Thuc())
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Text = ht.TenHinhThuc;
                item.Name = ht.MaHinhThuc.ToString();
                cbbHinhThucThue.Items.Add(item);
                if (ht.MaHinhThuc == phieu_tam.MaHinhThuc)
                {
                    cbbHinhThucThue.SelectedItem = item;
                    sHinhThuc = item.Name;
                }
            }

            lbTongTien.Text = String.Format("{0:0,0 VND}", bphieu.tinh_tien(phieu_tam.MaPhieuThue)).ToString();
            if (bphieu.Gio_Da_Thue(phieu_tam.MaPhieuThue).Days > 0)
            {
                lbSoGio.Text = bphieu.Gio_Da_Thue(phieu_tam.MaPhieuThue).Days.ToString() + " Ngày " + bphieu.Gio_Da_Thue(phieu_tam.MaPhieuThue).Hours.ToString() + " Giờ ";
            }
            else
            {
                lbSoGio.Text = bphieu.Gio_Da_Thue(phieu_tam.MaPhieuThue).Hours.ToString() + " Giờ ";
            }
        }

        private void btnTraPhong_Click(object sender, EventArgs e)
        {
            PhieuThue phieu_tam = bphieu.Tim_Phieu_Thue(frmMaPhong);
            int error = 0;
            if (txtTenKhachHang.Text == string.Empty)
            {
                lbThongBaoTen.Visible = true;
                error++;
            }
            else
                lbThongBaoTen.Visible = false;
            if (txtSoNguoi.Text == string.Empty)
            {
                lbThongBaoSoNguoi.Text = "Hãy nhập vào số người:";
                lbThongBaoSoNguoi.Visible = true;
                error++;
            }
            else
            {
                if (bphong.Tim_Phong(frmMaPhong).SoNguoiToiDa < Convert.ToInt32(txtSoNguoi.Text))
                {
                    lbThongBaoSoNguoi.Text = "Số người vượt quá cho phép";
                    lbThongBaoSoNguoi.Visible = true;
                    error++;
                }
                else
                {
                    lbThongBaoSoNguoi.Visible = false;
                }
            }
            if (error == 0)
            {
                string rmaHoadon;

                rmaHoadon = bHoaDon.Tao_Ma();

                HoaDon hoadon_moi = new HoaDon();
                hoadon_moi.MaNV = frmMaNV;
                hoadon_moi.MaHD = rmaHoadon;
                hoadon_moi.MaKH = phieu_tam.MaKH;
                hoadon_moi.SoGioThue = bphieu.Gio_Da_Thue(phieu_tam.MaPhieuThue).Days * 24 + bphieu.Gio_Da_Thue(phieu_tam.MaPhieuThue).Hours;
                hoadon_moi.TongTien = bphieu.tinh_tien(phieu_tam.MaPhieuThue);
                hoadon_moi.NgayThanhToan = DateTime.Now;
                hoadon_moi.MaPhieuThue = phieu_tam.MaPhieuThue;
                bphong.Cap_Nhat_Tinh_Trang("MTT0000002", frmMaPhong);
                try
                {
                    bphieu.Cap_Nhat_Ngay_Tra(phieu_tam.MaPhieuThue);
                    bHoaDon.Them_Hoa_Don(hoadon_moi);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    _logger.Error(ex.Message);
                }

                //Them chi tiet hoa don
                ChiTietHoaDon chitiet = new ChiTietHoaDon();
                chitiet.MaCTHD = ChiTietHoaDonBUS.ID();
                chitiet.MaHD = hoadon_moi.MaHD;
                chitiet.MaPhong = frmMaPhong;
                chitiet.NgayDen = phieu_tam.NgayBatDauThue;
                chitiet.NgayDi = phieu_tam.NgayTraPhong;
                chitiet.PhuThu = phieu_tam.TienChuyenPhong;
                chitiet.ThanhTien = phieu_tam.TienChuyenPhong;
                ChiTietHoaDonBUS.Them_Moi(chitiet);
                Hoa_Don hoadon = new Hoa_Don();
                hoadon.frmMaHoaDon = hoadon_moi.MaHD;
                hoadon.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
        }
        private void cbbHinhThucThue_SelectedIndexChanged(object sender, EventArgs e)
        {
            PhieuThue phieu_tam = bphieu.Tim_Phieu_Thue(frmMaPhong);
            Phong phong_tam = bphong.Tim_Phong((cbbPhong.SelectedItem as ComboBoxItem).Name);
            sHinhThuc = (cbbHinhThucThue.SelectedItem as ComboBoxItem).Name;
            lbTongTien.Text = String.Format("{0:0,0 VND}", bphieu.tinh_tien(phieu_tam.MaPhieuThue, sHinhThuc, phong_tam.LoaiPhong)).ToString();
            txtDonGia.Text = String.Format("{0:0,0 VND}", HinhThuc_LoaiPhongBUS.Tim_HT_LP(sHinhThuc, phong_tam.LoaiPhong).DonGia).ToString();
        }

        private void cbbPhong_SelectedIndexChanged(object sender, EventArgs e)
        {
            PhieuThue phieu_tam = bphieu.Tim_Phieu_Thue(frmMaPhong);
            Phong phong_tam = bphong.Tim_Phong((cbbPhong.SelectedItem as ComboBoxItem).Name);
            txtLoaiPhong.Text = bloaiphong.Tim_Loai_Phong(phong_tam.LoaiPhong).TenLoaiPhong;
            lbTongTien.Text = String.Format("{0:0,0 VND}", bphieu.tinh_tien(phieu_tam.MaPhieuThue, sHinhThuc, phong_tam.LoaiPhong)).ToString();
            txtDonGia.Text = String.Format("{0:0,0 VND}", HinhThuc_LoaiPhongBUS.Tim_HT_LP(sHinhThuc, phong_tam.LoaiPhong).DonGia).ToString();
        }

        private void btnLuuThayDoi_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (txtTenKhachHang.Text == string.Empty)
            {
                lbThongBaoTen.Visible = true;
                error++;
            }
            else
                lbThongBaoTen.Visible = false;
            if (txtSoNguoi.Text == string.Empty)
            {
                lbThongBaoSoNguoi.Text = "Hãy nhập vào số người:";
                lbThongBaoSoNguoi.Visible = true;
                error++;
            }
            else
            {
                if (bphong.Tim_Phong(frmMaPhong).SoNguoiToiDa < Convert.ToInt32(txtSoNguoi.Text))
                {
                    lbThongBaoSoNguoi.Text = "Số người vượt quá cho phép";
                    lbThongBaoSoNguoi.Visible = true;
                    error++;
                }
                else
                {
                    lbThongBaoSoNguoi.Visible = false;
                }
            }
            if (error == 0)
            {

                // Chỉnh sửa khách hàng.
                KhachHang khachtam = new KhachHang();
                khachtam.TenKH = txtTenKhachHang.Text;
                khachtam.LoaiKH = Convert.ToInt32((cbbLoaiKhachHang.SelectedItem as ComboBoxItem).Name);
                khachtam.SDT = txtSoDT.Text;
                khachtam.CMND = txtCMND.Text;
                khachtam.DiaChi = txtDiaChi.Text;
                khachtam.MaKH = bphieu.Tim_Phieu_Thue(frmMaPhong).MaKH;

                // Chỉnh sửa phiếu thuê phòng
                PhieuThue phieutam = new PhieuThue();
                phieutam.MaPhong = (cbbPhong.SelectedItem as ComboBoxItem).Name;
                phieutam.SoNguoiThue = Convert.ToInt32(txtSoNguoi.Text);
                phieutam.MaHinhThuc = (cbbHinhThucThue.SelectedItem as ComboBoxItem).Name;
                phieutam.MaPhong = frmMaPhong;
                phieutam.MaKH = khachtam.MaKH;
                phieutam.TienChuyenPhong = bphieu.Tim_Phieu_Thue(frmMaPhong).TienChuyenPhong;
                phieutam.MaPhieuThue = bphieu.Tim_Phieu_Thue(frmMaPhong).MaPhieuThue;
                if ((cbbPhong.SelectedItem as ComboBoxItem).Name != frmMaPhong)
                {
                    bphong.Cap_Nhat_Tinh_Trang("MTT0000002", frmMaPhong);
                    frmMaPhong = (cbbPhong.SelectedItem as ComboBoxItem).Name;
                    bphong.Cap_Nhat_Tinh_Trang("MTT0000001", frmMaPhong);
                    phieutam.MaPhong = frmMaPhong;
                    phieutam.TienChuyenPhong += QuyDinhBUS.Lay_Quy_Dinh(2).GiaTri;
                }
                bphieu.Cap_Nhat_Phieu_Thue(phieutam);
                bKhach.Cap_Nhat_Khach(khachtam);
                MessageBox.Show("Thay đổi thành công");
            }
            else
            {

                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
            frmPhieuTraPhong_Load(sender, e);
        }
    }
}