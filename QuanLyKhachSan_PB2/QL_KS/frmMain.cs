﻿using QL_KS.BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using log4net;

namespace QL_KS
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
        }
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmMain).Name);
        public string MaNV;
        public string TenNV;
        public string LoaiNV;
        public string TaiKhoan;
        PhanQuyenBUS pqBUS = new PhanQuyenBUS();
        Phong_BUS pBus = new Phong_BUS();
        Loai_Phong_BUS Loai_Phong = new Loai_Phong_BUS();
        PhieuThueBUS ptBUS = new PhieuThueBUS();
        Hinh_Thuc_BUS htBUS = new Hinh_Thuc_BUS();
        // kiểm tra tồn tại form đó chưa
        private bool ExistForm(DevComponents.DotNetBar.Metro.MetroForm form)
        {
            foreach (var child in MdiChildren)
            {
                if (child.Name == form.Name)
                {
                    child.Activate();
                    return true;
                }
            }
            return false;
        }

        private void bbiMatDo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            rptMDSD rpt = new rptMDSD();
            rpt.ShowPreviewDialog();
        }

        private void bbiNhanVien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmNhanVien nv = new frmNhanVien();
            nv.ShowDialog();
            this.frmMain_Load(sender, e);
        }

        private void bbiLoaiNhanVien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmLoaiNhanVien type = new frmLoaiNhanVien();
            type.TaiKhoan = TaiKhoan;
            type.ShowDialog();
            this.frmMain_Load(sender, e);
        }
        private void PhanQuyenNguoiDung()
        {
            BangPhanQuyen phanquyen = new BangPhanQuyen();
            phanquyen = pqBUS.LoadTheoLoai(LoaiNV);

            if (phanquyen.DanhMucPhong == false)
            {
                bbPhong.Enabled = false;
                bbPhong.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }

            if (phanquyen.ThayDoiQuyDinh == false)
            {
                bbThayDoiQuyDinh.Enabled = false;
                bbThayDoiQuyDinh.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                rbbThayDoiQuyDinh.Visible = false;
            }
            if (phanquyen.NhatKyHeThong == false)
            {
                bbItemNhatKyHeThong.Enabled = false;
                bbItemNhatKyHeThong.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                rbbNhatKyHoatDong.Visible = false;
            }
            if (phanquyen.QuanLyLoaiPhong == false)
            {
                bbLoaiPhong.Enabled = false;
                bbLoaiPhong.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (phanquyen.DanhMucPhong == false && phanquyen.QuanLyLoaiPhong == false)
            {
                rbbQuanLyPhong.Visible = false;
            }

            if (phanquyen.QuanLyKhachHang == false)
            {
                btnKhachHang.Enabled = false;
                btnKhachHang.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (phanquyen.QuanLyLoaiKhachHang == false)
            {
                btnLoaiKhachHang.Enabled = false;
                btnLoaiKhachHang.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (phanquyen.QuanLyKhachHang == false && phanquyen.QuanLyLoaiKhachHang == false)
            {
                rbbQuanLyKhachHang.Visible = false;
            }
            if (phanquyen.QuanLyNhanVien == false)
            {
                bbiNhanVien.Enabled = false;
                bbiNhanVien.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (phanquyen.QuanLyLoaiNV == false)
            {
                bbiLoaiNhanVien.Enabled = false;
                bbiLoaiNhanVien.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (phanquyen.QuanLyNhanVien == false && phanquyen.QuanLyLoaiNV == false)
            {
                rbbQLNhanVien.Visible = false;
            }
            if (phanquyen.SaoLuuDL == false)
            {
                bbItemLuuDL.Enabled = false;
                bbItemLuuDL.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (phanquyen.PhucHoiDL == false)
            {
                bbItemPhucHoi.Enabled = false;
                bbItemPhucHoi.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            if (phanquyen.SaoLuuDL == false && phanquyen.PhucHoiDL == false)
            {
                rbbSaoLuuVaPhucHoi.Visible = false;
            }
            if (phanquyen.PhanQuyen == false)
            {
                bbPhanQuyen.Enabled = false;
                bbPhanQuyen.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                rbbPhanQuyen.Visible = false;
            }

            if (phanquyen.HinhThucThue == false)
            {
                bbHinhThucThue.Enabled = false;
                bbHinhThucThue.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                rbbQuanLyHinhThuc.Visible = false;
            }
        }

        void btn_Click(object sender, EventArgs e)
        {
            string ma = ((sender as Button).Name);
            this.lv_DSPhong.Items.Clear();
            if (pBus.Load_DS_Phong(ma) != null)
            {
                foreach (Phong P in pBus.Load_DS_Phong(ma))
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = P.TenPhong.ToString();
                    item.Name = P.MaPhong.ToString();
                    if (P.TinhTrang == "MTT0000002")
                    {
                        item.ImageIndex = 0;
                    }
                    else
                    {
                        item.ImageIndex = 1;
                    }
                    this.lv_DSPhong.Items.Add(item);
                }
            }

        }
        void btnAll_Click(object sender, EventArgs e)
        {
            string ma = null;
            this.lv_DSPhong.Items.Clear();
            foreach (Phong P in pBus.Load_DS_Phong(ma))
            {
                ListViewItem item = new ListViewItem();
                item.Text = P.TenPhong.ToString();
                item.Name = P.MaPhong.ToString();
                if (P.TinhTrang == "MTT0000002")
                {
                    item.ImageIndex = 0;
                }
                else
                {
                    item.ImageIndex = 1;
                }
                this.lv_DSPhong.Items.Add(item);
            }
        }
        private void bbItemLuuDL_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmSaoLuu saoluu = new frmSaoLuu();
            saoluu.TaiKhoan = TaiKhoan;
            saoluu.ShowDialog();
        }

        private void bbItemPhucHoi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmPhuc_Hoi phuchoi = new frmPhuc_Hoi();
            phuchoi.TaiKhoan = TaiKhoan;
            phuchoi.ShowDialog();
        }

        private void bbItemNhatKyHeThong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmNhatKy frmNhatKy = new frmNhatKy();
            frmNhatKy.ShowDialog();
        }

        private void bbItemXemVaCapNhatThongTinCaNhan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmXemVaCapNhatThongTinNhanVien frmXem = new frmXemVaCapNhatThongTinNhanVien();
            frmXem.MaNV = MaNV;
            frmXem.LoaiNV = LoaiNV;
            frmXem.TaiKhoan = TaiKhoan;
            frmXem.ShowDialog();
        }

        private void bbItemDoiMatKhau_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmDoi_Mat_Khau frmDoiMK = new frmDoi_Mat_Khau();
            frmDoiMK.MaNV = MaNV;
            frmDoiMK.TaiKhoan = TaiKhoan;
            //frmDoiMK.MdiParent = this;
            frmDoiMK.ShowDialog();
        }

        private void bbItemDangXuat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmDang_Nhap frm = new frmDang_Nhap();
            this.Hide();
            frm.ShowDialog();
        }

        private void bbItemThoatChuongTrinh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Application.Exit();
        }
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            PhanQuyenNguoiDung();
            this.Width = 1366;
            this.Height = 768;
            this.lv_DSPhong.Items.Clear();
            bbTenDangNhap.Caption = TenNV;
            timer1.Enabled = true;
            string ma = null;
            foreach (Phong P in pBus.Load_DS_Phong(ma))
            {
                ListViewItem item = new ListViewItem();
                item.Text = P.TenPhong.ToString();
                item.Name = P.MaPhong.ToString();
                if (P.TinhTrang == "MTT0000002")
                {
                    item.ImageIndex = 0;
                }
                else
                {
                    item.ImageIndex = 1;
                }
                this.lv_DSPhong.Items.Add(item);
            }
            this.lbPhongTrong.Text = pBus.DemSoPhongTrong().ToString();
            this.lbPhongThue.Text = pBus.DemSoPhongThue().ToString();
            Danh_Sach_Loai_Phong.Controls.Clear();
            Button btnAll = new Button() { Width = Danh_Sach_Loai_Phong.Width - 20, Height = 50 };
            btnAll.Text = "Tất Cả";
            btnAll.Name = "All";
            btnAll.Click += btnAll_Click;
            Danh_Sach_Loai_Phong.Controls.Add(btnAll);
            // Phòng theo loại phòng
            foreach (LoaiPhong lP in Loai_Phong.Load_DS_Loai_Phong())
            {
                Button btn = new Button() { Width = Danh_Sach_Loai_Phong.Width - 20, Height = 50 };
                btn.Click += btn_Click;
                btn.Text = lP.TenLoaiPhong;
                btn.Name = lP.MaLoaiPhong;
                Danh_Sach_Loai_Phong.Controls.Add(btn);
            }
        }
        private void lv_DSPhong_Click(object sender, EventArgs e)
        {
            if (lv_DSPhong.SelectedItems[0].ImageIndex == 1 && lv_DSPhong.SelectedItems.Count > 0)
            {
                if (lv_DSPhong.SelectedItems[0] != null)
                {
                    lbTenPhong.Text = lv_DSPhong.SelectedItems[0].Text;
                    lbpreTenPhong.Visible = true;
                    lbTenPhong.Visible = true;
                }
                string maht = ptBUS.Tim_Phieu_Thue(lv_DSPhong.SelectedItems[0].Name.ToString()).MaHinhThuc.ToString();
                string mapt = ptBUS.Tim_Phieu_Thue(lv_DSPhong.SelectedItems[0].Name.ToString()).MaPhieuThue.ToString();
                lbHinhThuc.Text = htBUS.Tim_Hinh_Thuc(maht).TenHinhThuc.ToString();
                lbHinhThuc.Visible = true;
                lbpreHinhThuc.Visible = true;

                lbTongTien.Text = String.Format("{0:0,0 VND}", ptBUS.tinh_tien(mapt)).ToString();
                lbTongTien.ForeColor = Color.OrangeRed;
                lbpreTongTien.Visible = true;
                lbTongTien.Visible = true;

                lbSoNguoi.Text = ptBUS.Tim_Phieu_Thue(lv_DSPhong.SelectedItems[0].Name.ToString()).SoNguoiThue.ToString();
                lbpreSoNguoi.Visible = true;
                lbSoNguoi.Visible = true;


            }
            else
            {
                lbpreTongTien.Visible = false;
                lbTongTien.Visible = false;
                lbpreSoNguoi.Visible = false;
                lbSoNguoi.Visible = false;
                lbpreTenPhong.Visible = false;
                lbTenPhong.Visible = false;
                lbHinhThuc.Visible = false;
                lbpreHinhThuc.Visible = false;
            }

        }
        private void lv_DSPhong_DoubleClick(object sender, EventArgs e)
        {
            if (lv_DSPhong.SelectedItems[0].ImageIndex == 0 && lv_DSPhong.SelectedItems.Count > 0)
            {
                frmPhieuThuePhong PhieuThue = new frmPhieuThuePhong();
                PhieuThue.frmMaPhong = lv_DSPhong.SelectedItems[0].Name.ToString();
                PhieuThue.TaiKhoan = TaiKhoan;
                PhieuThue.ShowDialog();
                frmMain_Load(sender, e);
            }
            else
            {
                frmPhieuTraPhong PhieuTra = new frmPhieuTraPhong();
                PhieuTra.frmMaPhong = lv_DSPhong.SelectedItems[0].Name.ToString();
                PhieuTra.TaiKhoan = TaiKhoan;
                PhieuTra.ShowDialog();
                frmMain_Load(sender, e);
            }
        }

        private void bbPhanQuyen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void bbPhong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmQuanLyPhong frm_Quanly_phong = new frmQuanLyPhong();
            frm_Quanly_phong.ShowDialog();
            this.frmMain_Load(sender, e);
        }

        private void bbLoaiPhong_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmQuanLyLoaiPhong frm_Quanly_Loai_phong = new frmQuanLyLoaiPhong();
            frm_Quanly_Loai_phong.ShowDialog();
            this.frmMain_Load(sender, e);
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmQuanLyHinhThucThue frm_Quanly_Hinh_Thuc = new frmQuanLyHinhThucThue();
            frm_Quanly_Hinh_Thuc.ShowDialog();
            this.frmMain_Load(sender, e);
        }

        private void btnKhachHang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmTimKiemKH frm = new frmTimKiemKH();
            frm.ShowDialog();
        }

        private void btnLoaiKhachHang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmLoaiKhachHang frm = new frmLoaiKhachHang();
            
            frm.ShowDialog();
            this.frmMain_Load(sender, e);
        }

        private void btnDSKH_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmDSKhachHang frm = new frmDSKhachHang();
            frm.ShowDialog();
        }

        private void btnTimKiemKH_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmTimKiemKH frm = new frmTimKiemKH();
            frm.ShowDialog();
        }

        private void bbItemPhanQuyen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmPhanQuyen frmPhanQuyen = new frmPhanQuyen();
            frmPhanQuyen.ShowDialog();
        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmHoaDon frmHoaDon = new frmHoaDon();
            frmHoaDon.TaiKhoan = TaiKhoan;
            frmHoaDon.ShowDialog();
        }
        NhanVienBUS nvbus = new NhanVienBUS();
        private void barButtonItem1_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmThayDoiQuyDinh frmThayDoiQuyDinh = new frmThayDoiQuyDinh();
            frmThayDoiQuyDinh.ShowDialog();
        }

        private void bbiBCDT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            rptBCDT rpt = new rptBCDT();
            rpt.ShowPreviewDialog();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            rptGraph rpt = new rptGraph();
            rpt.ShowPreviewDialog();
        }
    }
}
