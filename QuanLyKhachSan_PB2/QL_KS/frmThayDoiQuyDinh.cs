﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using log4net;

namespace QL_KS
{
    public partial class frmThayDoiQuyDinh : DevComponents.DotNetBar.Metro.MetroForm
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmThayDoiQuyDinh).Name);
        public frmThayDoiQuyDinh()
        {
            InitializeComponent();
        }

        private void btnThayDoi_Click(object sender, EventArgs e)
        {
            if(txtPhutThem.Text == "" || txtTienChuyenPhong.Text == "")
            {
                MessageBox.Show("Bạn phải nhập đầy đủ các giá trị của quy định");
            }
            else
            {
                QuyDinh quydinhtam = new QuyDinh();
                quydinhtam.STT = 1;
                quydinhtam.GiaTri = Convert.ToInt32(txtPhutThem.Text);
                QuyDinhBUS.Thay_Doi(quydinhtam);
                quydinhtam.STT = 2;
                quydinhtam.GiaTri = Convert.ToInt32(txtTienChuyenPhong.Text);
                QuyDinhBUS.Thay_Doi(quydinhtam);
                MessageBox.Show("Thay đổi thành công");
            }
        }

        private void frmThayDoiQuyDinh_Load(object sender, EventArgs e)
        {
            txtTenPhutThem.Text = QuyDinhBUS.Lay_Quy_Dinh(1).TenQuyDinh.ToString();
            txtPhutThem.Text = QuyDinhBUS.Lay_Quy_Dinh(1).GiaTri.ToString();
            txtTenTienChuyenPhong.Text = QuyDinhBUS.Lay_Quy_Dinh(2).TenQuyDinh.ToString();
            txtTienChuyenPhong.Text = QuyDinhBUS.Lay_Quy_Dinh(2).GiaTri.ToString();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}