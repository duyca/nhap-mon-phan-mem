﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data.SqlClient;
using log4net;
using NhapDoAn.BUS;
using QL_KS.BUS;
using DevExpress.XtraEditors;
namespace QL_KS
{
    public partial class frmPhuc_Hoi : Form
    {
        public frmPhuc_Hoi()
        {
            InitializeComponent();
        }

        int i = 0;
        public string TaiKhoan;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmPhuc_Hoi).Name);

        private void btnDuongDan_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Backup File|*.bak";
            openFileDialog.Title = "Restore Database";
            openFileDialog.ShowDialog();
            txtTenTapTin.Text = openFileDialog.FileName;
        }

        private void frmPhucHoi_Load(object sender, EventArgs e)
        {
            progressBar1.Maximum = 100;
            progressBar1.Minimum = 0;
        }

        private void btnPhucHoi_Click_1(object sender, EventArgs e)
        {
            if (txtTenTapTin.Text == string.Empty)
            {
                lblErrorDuongDan.Text = "Bạn chưa chọn tên tập tin cần phục hồi";
                lblErrorDuongDan.ForeColor = Color.Red;
            }
            else
            {
                try
                {
                    //xóa tất cả kết nối hiện có
                    System.Data.SqlClient.SqlConnection.ClearAllPools();
                    string sqlRestore = "Use master Restore Database [QLKhachSan] from disk='" + txtTenTapTin.Text + "' WITH REPLACE;";
                    SqlConnection con = new SqlConnection(@"Data Source=CA\SQLEXPRESS;Initial Catalog=QLKhachSan;Integrated Security=True");
                    con.Open();
                    SqlCommand cmd = new SqlCommand(sqlRestore, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    timer.Start();
                }
                catch (SqlException ex)
                {
                    XtraMessageBox.Show(ex.Message, "Restore Database");
                    
                    _logger.Error(ex.ToString());
                    return;
                }
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_Tick_1(object sender, EventArgs e)
        {
            progressBar1.Increment(10);
            i += 10;
            lblPhanTram.Text = i.ToString() + "%";
            if (progressBar1.Value == 100)
            {
                timer.Stop();
                XtraMessageBox.Show("Dữ liệu đã được phục hồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
                LuuNhatKyHT.Them(TaiKhoan, "Cơ sở dữ liệu", "Phục hồi dữ liệu");
                this.Close();
                lblErrorDuongDan.Text = "";
            }
        }
    }
}
