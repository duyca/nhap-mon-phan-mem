﻿using QL_KS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.BUS;
using NhapDoAn.DAO;

namespace NhapDoAn.BUS
{
    public class NhatKyHeThongBUS
    {
        NhatKyHeThongDAO nhatKyDAO = new NhatKyHeThongDAO();

        public List<NhatKyHoatDong> LoadNhatKyHoatDong()
        {
            return nhatKyDAO.LoadNhatKyHoatDong();
        }

        public void XoaNhatKy(NhatKyHoatDong nhatky)
        {
            nhatKyDAO.XoaNhatKy(nhatky);
        }

        public void XoaToanBoNhatKy()
        {
            nhatKyDAO.XoaToanBoNhatKy();
        }

       

        public void ThemNhatKy(NhatKyHoatDong nk)
        {
            nhatKyDAO.ThemNhatKy(nk);
        }
    }
}
