﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;

namespace QL_KS.BUS
{
    class QuyDinhBUS
    {
        QuyDinhDAO daoquydinh = new QuyDinhDAO();
        public static QuyDinh Lay_Quy_Dinh(int sothutu)
        {
            return QuyDinhDAO.Lay_Quy_Dinh(sothutu);
        }
        public static void Thay_Doi(QuyDinh tam)
        {
            QuyDinhDAO.Thay_Doi(tam);
        }
    }
}
