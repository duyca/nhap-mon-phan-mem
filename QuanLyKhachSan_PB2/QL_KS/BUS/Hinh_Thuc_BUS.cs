﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;
namespace QL_KS.BUS
{
    class Hinh_Thuc_BUS
    {
        Hinh_Thuc_DAO hinhthuc = new Hinh_Thuc_DAO();
        public HinhThuc Tim_Hinh_Thuc(string maHT)
        {
            return hinhthuc.Tim_Hinh_Thuc(maHT);
        }
        public List<HinhThuc> Load_DS_Hinh_Thuc()
        {
            return hinhthuc.Load_DS_Hinh_Thuc();
        }
        public void Them_Hinh_Thuc(HinhThuc hinhthucmoi)
        {
            hinhthuc.Them_Hinh_Thuc(hinhthucmoi);
        }
        public void Sua_Hinh_Thuc(HinhThuc hinhthucmoi)
        {
            hinhthuc.Sua_Hinh_Thuc(hinhthucmoi);
        }
        public static string ID()
        {
            return Hinh_Thuc_DAO.ID();
        }
    }
}
