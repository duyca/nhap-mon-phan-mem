﻿using QL_KS.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.BUS
{
    public class NhanVienBUS
    {
        NhanVienDAO nvDao = new NhanVienDAO();
        public List<NhanVien> LoadDS()
        {
            return nvDao.LoadDS();
        }

        public void CapNhatMatKhau(NhanVien nv)
        {
            nvDao.CapNhatMatKhau(nv);
        }

        public String TaoMa()
        {
            return nvDao.TaoMa();
        }
        public NhanVien LoadTheoMaNV(string MaNV)
        {
            return nvDao.LoadTheoMaNV(MaNV);
        }

        public bool CapNhatThongTin(NhanVien nv, bool taikhoan = false)
        {
            return nvDao.CapNhatThongTin(nv, taikhoan);
        }
        public bool Them(NhanVien nv)
        {
            return nvDao.Them(nv);
        }
        public int Xoa(string ma)
        {
            return nvDao.Xoa(ma);
        }
        public NhanVien TimKiem(string ma)
        {
            return nvDao.TimKiem(ma);
        }
        public DataTable getNV(string TaiKhoan)
        {
            return nvDao.getNV(TaiKhoan);
        }

    }
}
