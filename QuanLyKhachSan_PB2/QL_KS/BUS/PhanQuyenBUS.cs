﻿using QL_KS.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.BUS
{
    public class PhanQuyenBUS
    {
        PhanQuyenDAO pqDAO = new PhanQuyenDAO();

        public List<BangPhanQuyen> Load()
        {
            return pqDAO.Load();
        }

        public BangPhanQuyen LoadTheoLoai(string loainv)
        {
            return pqDAO.LoadTheoLoai(loainv);
        }

        public void Them(BangPhanQuyen p)
        {
            pqDAO.Them(p);
        }

        public void Xoa(string loainv)
        {
            pqDAO.Xoa(loainv);
        }

        public void CapNhat(List<BangPhanQuyen> list)
        {
            pqDAO.CapNhat(list);
        }
    }
}
