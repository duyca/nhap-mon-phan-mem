﻿using QL_KS.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.BUS
{
    class Loai_Phong_BUS
    {
        Loai_Phong_DAO Loai_Phong = new Loai_Phong_DAO();
        public List<LoaiPhong> Load_DS_Loai_Phong()
        {
            return Loai_Phong.Load_DS_Loai_Phong();
        }
        public LoaiPhong Tim_Loai_Phong(string maloai)
        {
            return Loai_Phong.Tim_Loai_Phong(maloai);
        }
        public void Them_Loai_Phong(LoaiPhong lphong)
        {
            Loai_Phong.Them_Loai_Phong(lphong);
        }
        public void Sua_Loai_Phong(LoaiPhong lphong)
        {
            Loai_Phong.Sua_Loai_Phong(lphong);
        }
        public  static string ID()
        {
            return Loai_Phong_DAO.ID();
        }
    }
}
