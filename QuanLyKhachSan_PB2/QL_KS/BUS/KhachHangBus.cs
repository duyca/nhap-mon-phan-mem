﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;

namespace QL_KS.BUS
{
    class KhachHangBus
    {
        public KhachHangDAO khach_dao = new KhachHangDAO();
        public KhachHang TimKiemkhachHang(string maKH)
        {
            return khach_dao.TimKiemkhachHang(maKH);
        }
        public void Them_Khach_Hang(KhachHang khach)
        {
            khach_dao.Them_Khach_Hang(khach);
        }
        public void Cap_Nhat_Khach(KhachHang khach)
        {
            khach_dao.Cap_Nhat_Khach(khach);
        }

        //TrangTHB - Quản lý khách hàng
        public static IEnumerable<KHNEW> LoadDS()
        {
            return KhachHangDAO.LoadDS();
        }

        public static IEnumerable<KHNEW> SearchName(string name)
        {
            return KhachHangDAO.SearchName(name);
        }

        public static IEnumerable<KHNEW> SearchCMND(string cmnd)
        {
            return KhachHangDAO.SearchCMND(cmnd);
        }

        public static IEnumerable<KHNEW> SearchAll(string name, string cmnd)
        {
            return KhachHangDAO.SearchAll(name, cmnd);
        }

        public static List<LoaiKhachHang> LoadKH()
        {
            return KhachHangDAO.LoadKH();
        }

        public static string ID()
        {
            return KhachHangDAO.ID();
        }

        public static string TimLoaiKH(string loai)
        {
            return KhachHangDAO.TimLoaiKH(loai);
        }

        public static bool Insert(KhachHang kh)
        {
            return KhachHangDAO.Insert(kh);
        }

        public static bool Update(KhachHang kh)
        {
            return KhachHangDAO.Update(kh);
        }

        public static IList<KHNEW> SearchID(string id)
        {
            return KhachHangDAO.SearchID(id);
        }

        public static bool Delete(string ma)
        {
            return KhachHangDAO.Delete(ma);
        }
        public static void Xoa(string makh)
        {
            KhachHangDAO.Xoa(makh);
        }
        public static bool TimKiemHoaDonCuaKH(string makh)
        {
            return KhachHangDAO.TimKiemHoaDonCuaKH(makh);
        }
        public static bool TimKiemPhieuThueCuaKH(string makh)
        {
            return KhachHangDAO.TimKiemPhieuThueCuaKH(makh);
        }
    }
}
