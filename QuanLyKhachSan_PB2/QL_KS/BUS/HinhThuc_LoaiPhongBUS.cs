﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;

namespace QL_KS.BUS
{
    class HinhThuc_LoaiPhongBUS
    {
        HinhThuc_LoaiPhongDAO daoHT_LP = new HinhThuc_LoaiPhongDAO();
        public void Them(HinhThuc_LoaiPhong ht_lp)
        {
            daoHT_LP.Them(ht_lp);
        }
        public static HinhThuc_LoaiPhong Tim_HT_LP(string maHT, string maLP)
        {
            return HinhThuc_LoaiPhongDAO.Tim_HT_LP(maHT, maLP);
        }
        public void Sua(HinhThuc_LoaiPhong ht_lp)
        {
            daoHT_LP.Sua(ht_lp);
        }
    }
}
