﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;

namespace QL_KS.BUS
{
    class Loai_Khach_HangBUS
    {
        Loai_Khach_HangDAO loaikhach = new Loai_Khach_HangDAO();
        public List<LoaiKhachHang> Load_DS_Loai_Khach()
        {
            return loaikhach.Load_DS_Loai_Khach();
        }
        
    }
}
