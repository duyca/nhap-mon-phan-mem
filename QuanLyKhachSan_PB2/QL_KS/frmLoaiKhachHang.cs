﻿using DevExpress.XtraEditors;
using log4net;
using QL_KS.BUS;
using QL_KS.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_KS
{
    public partial class frmLoaiKhachHang : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmLoaiKhachHang).Name);
        public frmLoaiKhachHang()
        {
            InitializeComponent();
        }
        Loai_Khach_HangBUS bus = new Loai_Khach_HangBUS();
        int idex;
        public string TaiKhoan;

        private void frmLoaiKhachHang_Load(object sender, EventArgs e)
        {
            LoadDL();
            if (idex >= 0)
            {
                txtTenLoai.Text = gvLoaiKH.GetFocusedRowCellValue("TenLoaiKH").ToString();
                txtHeSo.Text = gvLoaiKH.GetFocusedRowCellValue("HeSo").ToString();
            }
        }
        public void LoadDL()
        {
            gcLKH.DataSource = bus.Load_DS_Loai_Khach();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtTenLoai.Text == "" || txtHeSo.Text == "")
            {
                XtraMessageBox.Show("Chưa nhập đầy đủ thông tin!", "Lỗi nhập", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                LoaiKhachHang lkh = new LoaiKhachHang();
                lkh.TenLoaiKH = txtTenLoai.Text;
                lkh.HeSo = double.Parse(txtHeSo.Text);
                try
                {
                    if (LoaiKHBUS.TimKiem(txtTenLoai.Text) == true)
                    {
                        XtraMessageBox.Show("Loại khách hàng này đã tồn tại trong hệ thống", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        bool kq = LoaiKHBUS.Insert(lkh);
                        if (kq == true)
                        {
                            XtraMessageBox.Show("Thêm loại khách hàng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LuuNhatKyHT.Them(TaiKhoan, "Quản Lý Loại khách hàng", "Thêm");
                            LoadDL();
                        }
                        else
                        {
                            XtraMessageBox.Show("Thêm loại khách hàng thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.ToString(), "Lỗi thêm loại khách hàng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _logger.Error(ex.ToString());
                }
            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (idex >= 0)
            {
                LoaiKhachHang lkh = new LoaiKhachHang();
                lkh.MaLoaiKH = int.Parse(gvLoaiKH.GetRowCellValue(idex, "MaLoaiKH").ToString());
                lkh.TenLoaiKH = txtTenLoai.Text;
                lkh.HeSo = double.Parse(txtHeSo.Text);
                try
                {

                    bool kq = LoaiKHBUS.Update(lkh);
                    if (kq == true)
                    {
                        XtraMessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDL();
                        LuuNhatKyHT.Them(TaiKhoan, "Quản Lý Loại Khách hàng", "Cập nhật");
                    }
                    else
                    {
                        XtraMessageBox.Show("Cập nhật loại khách hàng thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.ToString(), "Lỗi cập nhật loại khách hàng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _logger.Error(ex.ToString());
                }
            }
            else
                XtraMessageBox.Show("Chưa chọn dòng cần cập nhật!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (idex >= 0)
            {
                DialogResult Result = XtraMessageBox.Show("Bạn có thật sự muốn xóa?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Result == DialogResult.Yes)
                {
                    try
                    {
                        if (LoaiKHBUS.TimKhachHangCoCungLoai(int.Parse(gvLoaiKH.GetRowCellValue(idex, "MaLoaiKH").ToString())) == true)
                        {
                            XtraMessageBox.Show("Loại khách hàng đã tồn tại trong hệ thống, không thể xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            LoaiKHBUS.Xoa(int.Parse(gvLoaiKH.GetRowCellValue(idex, "MaLoaiKH").ToString()));
                            XtraMessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LoadDL();
                            LuuNhatKyHT.Them(TaiKhoan, "Quản Lý Loại Khách hàng", "Xóa");
                        }
                     }

                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.ToString(), "Lỗi xóa loại khách hàng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _logger.Error(ex.ToString());
                    }
                }
            }
            else
                XtraMessageBox.Show("Chưa chọn dòng cần xóa!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvLoaiKH_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (idex >= 0)
            {
                txtTenLoai.Text = gvLoaiKH.GetFocusedRowCellValue("TenLoaiKH").ToString();
                txtHeSo.Text = gvLoaiKH.GetFocusedRowCellValue("HeSo").ToString();
            }
        }

        private void gvLoaiKH_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            idex = e.FocusedRowHandle;
            if (idex < 0)
            {
                txtTenLoai.Text = "";
                txtHeSo.Text = "";
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtTenLoai.Text = "";
            txtHeSo.Text = "";
        }

    }
}
