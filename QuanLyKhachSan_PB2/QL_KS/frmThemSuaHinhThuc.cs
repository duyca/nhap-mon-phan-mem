﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QL_KS.BUS;
using log4net;


namespace QL_KS
{
    public partial class frmThemSuaHinhThuc : Form
    {
        Hinh_Thuc_BUS bhinh_thuc = new Hinh_Thuc_BUS();
        Loai_Phong_BUS bloai_phong = new Loai_Phong_BUS();
        HinhThuc_LoaiPhongBUS bht_ph = new HinhThuc_LoaiPhongBUS();
        public string frmMaHinhThuc;
        public frmThemSuaHinhThuc()
        {
            InitializeComponent();
        }
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmThemSuaHinhThuc).Name);
        private void rbKeoDai_CheckedChanged(object sender, EventArgs e)
        {
            if(rbKeoDai.Checked == true)
            {
                txtGioKeoDai.Enabled = true;
                rbGioKetThuc.Checked = false;
                txtGioKetThuc.Enabled = false;
            }
        }

        private void rbGioKetThuc_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGioKetThuc.Checked == true)
            {
                txtGioKetThuc.Enabled = true;
                rbKeoDai.Checked = false;
                txtGioKeoDai.Enabled = false;
            }
        }

        private void frmThemSuaHinhThuc_Load(object sender, EventArgs e)
        {

            // loai don gia

            if (frmMaHinhThuc == null)
            {
                fllDonGia.Padding = new Padding(0, 0, 0, 0);
                fllKetiep.Padding = new Padding(0, 0, 0, 0);
                foreach (LoaiPhong lp in bloai_phong.Load_DS_Loai_Phong())
                {
                    fllDonGia.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = lp.TenLoaiPhong;
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0, 6, 0, 0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = lp.MaLoaiPhong;
                    txttam.Text = "0";
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    fllDonGia.Controls.Add(lbtam);
                    fllDonGia.Controls.Add(txttam);
                }
                foreach (LoaiPhong lp in bloai_phong.Load_DS_Loai_Phong())
                {
                    fllKetiep.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = "Tiếp Theo";
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0, 6, 0, 0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = lp.MaLoaiPhong;
                    txttam.Text = "0";
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    fllKetiep.Controls.Add(lbtam);
                    fllKetiep.Controls.Add(txttam);
                }
            }
            //co ma hinh thuc
            else
            {
                btnTraPhong.Text = "Lưu";
                fllDonGia.Padding = new Padding(0, 0, 0, 0);
                fllKetiep.Padding = new Padding(0, 0, 0, 0);
                HinhThuc hihnhthuctam = bhinh_thuc.Tim_Hinh_Thuc(frmMaHinhThuc);
                foreach (LoaiPhong lp in bloai_phong.Load_DS_Loai_Phong())
                {
                    fllDonGia.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = lp.TenLoaiPhong;
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0, 6, 0, 0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = lp.MaLoaiPhong;
                    txttam.Text = HinhThuc_LoaiPhongBUS.Tim_HT_LP(hihnhthuctam.MaHinhThuc, lp.MaLoaiPhong).DonGia.ToString();
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    fllDonGia.Controls.Add(lbtam);
                    fllDonGia.Controls.Add(txttam);
                }
                foreach (LoaiPhong lp in bloai_phong.Load_DS_Loai_Phong())
                {
                    fllKetiep.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = "Tiếp Theo";
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0, 6, 0, 0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = lp.MaLoaiPhong;
                    txttam.Text = HinhThuc_LoaiPhongBUS.Tim_HT_LP(hihnhthuctam.MaHinhThuc, lp.MaLoaiPhong).TiepTheo.ToString();
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    fllKetiep.Controls.Add(lbtam);
                    fllKetiep.Controls.Add(txttam);
                }
            }
            // ket thuc load don gia
            if (rbGioKetThuc.Checked == true)
            {
                txtGioKetThuc.Enabled = true;
                rbKeoDai.Checked = false;
                txtGioKeoDai.Enabled = false;
                txtGioKeoDai.Text = "";
            }
            if (rbKeoDai.Checked == true)
            {
                txtGioKeoDai.Enabled = true;
                rbGioKetThuc.Checked = false;
                txtGioKetThuc.Enabled = false;
                txtGioKetThuc.Text = "";
            }
            this.Text = "Thêm Hình Thức";
            if (frmMaHinhThuc !=null)
            {
                btnTraPhong.Text = "Lưu";
                this.Text = "Sửa Hình Thức";
                HinhThuc hinhthuctam = bhinh_thuc.Tim_Hinh_Thuc(frmMaHinhThuc);
                txtMaHinhThuc.Text = hinhthuctam.MaHinhThuc;
                txtTenHinhThuc.Text = hinhthuctam.TenHinhThuc;
                txtGioKeoDai.Text = hinhthuctam.ThoiGianKeoDai.ToString();
                txtGioKetThuc.Text = hinhthuctam.GioKetThuc.ToString();
                if (txtGioKeoDai.Text != "")
                {
                    rbKeoDai.Checked = true;
                    txtGioKeoDai.Enabled = true;
                    rbGioKetThuc.Checked = false;
                    txtGioKetThuc.Enabled = false;
                }
                else
                {
                    rbKeoDai.Checked = false;
                    txtGioKeoDai.Enabled = false;
                    rbGioKetThuc.Checked = true;
                    txtGioKetThuc.Enabled = true;
                }
            }
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnTraPhong_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (txtTenHinhThuc.Text == string.Empty)
            {
                error++;
                lbThongBao_Ten.Visible = true;
            }
            else
            {
                lbThongBao_Ten.Visible = false;
            }
            if (txtGioKeoDai.Text == string.Empty && txtGioKetThuc.Text == string.Empty)
            {
                error++;
                lbThongBao_ThoiGian.Visible = true;
            }
            else
            {
                lbThongBao_ThoiGian.Visible = false;
            }
            if(error!=0)
            {
                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
            else
            {
                HinhThuc Hinh_Thuc_Moi = new HinhThuc();
                if(frmMaHinhThuc == null)
                {

                      Hinh_Thuc_Moi.MaHinhThuc = Hinh_Thuc_BUS.ID();
  
                }
                else
                {
                    Hinh_Thuc_Moi.MaHinhThuc = frmMaHinhThuc;
                }
                Hinh_Thuc_Moi.TenHinhThuc = txtTenHinhThuc.Text;
                if(rbGioKetThuc.Checked)
                {
                    Hinh_Thuc_Moi.GioKetThuc = Convert.ToInt32(txtGioKetThuc.Text);
                    Hinh_Thuc_Moi.ThoiGianKeoDai = null;
                }
                else
                {
                    Hinh_Thuc_Moi.ThoiGianKeoDai = Convert.ToInt32(txtGioKeoDai.Text);
                    Hinh_Thuc_Moi.GioKetThuc = null;
                }
                Hinh_Thuc_Moi.active = true;
                if (frmMaHinhThuc == null)
                {
                    bhinh_thuc.Them_Hinh_Thuc(Hinh_Thuc_Moi);
                    MessageBox.Show("Thêm hình thức thành công");
                }
                else
                {
                    bhinh_thuc.Sua_Hinh_Thuc(Hinh_Thuc_Moi);
                    MessageBox.Show("Sửa hình thức thành công");
                }

                foreach (LoaiPhong lp in bloai_phong.Load_DS_Loai_Phong())
                {
                    HinhThuc_LoaiPhong ht_lp = new HinhThuc_LoaiPhong();
                    ht_lp.MaLoaiPhong = lp.MaLoaiPhong;
                    ht_lp.MaHinhThuc = Hinh_Thuc_Moi.MaHinhThuc;
                    foreach (Control ct in fllDonGia.Controls)
                    {
                        if (ct is MaskedTextBox)
                        {
                            if (ct.Name == ht_lp.MaLoaiPhong)
                            {
                                ht_lp.DonGia = Convert.ToInt32(ct.Text);
                            }
                        }
                    }
                    foreach (Control ct in fllKetiep.Controls)
                    {
                        if (ct is MaskedTextBox)
                        {
                            if (ct.Name == ht_lp.MaLoaiPhong)
                            {
                                ht_lp.TiepTheo = Convert.ToInt32(ct.Text);
                            }
                        }
                    }
                    try
                    {
                        if (frmMaHinhThuc == null)
                        {
                            bht_ph.Them(ht_lp);
                        }
                        else
                        {
                            bht_ph.Sua(ht_lp);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        _logger.Error(ex.Message);
                    }

                }
                Close();
            }
        }
    }

}
