//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QL_KS
{
    using System;
    using System.Collections.Generic;
    
    public partial class NhanVien
    {
        public NhanVien()
        {
            this.HoaDons = new HashSet<HoaDon>();
        }
    
        public string MaNV { get; set; }
        public string TenNV { get; set; }
        public string DiaChi { get; set; }
        public Nullable<System.DateTime> NgaySinh { get; set; }
        public string CMND { get; set; }
        public string SDT { get; set; }
        public string TaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public string LoaiNV { get; set; }
        public string Email { get; set; }
        public Nullable<bool> ConQuanLy { get; set; }
        public Nullable<int> GioiTinh { get; set; }
    
        public virtual ICollection<HoaDon> HoaDons { get; set; }
        public virtual LoaiNhanVien LoaiNhanVien { get; set; }
    }
}
