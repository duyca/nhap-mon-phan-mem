﻿using QL_KS.BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_KS
{
    public partial class frmLoaiNhanVien : Form
    {

        private LoaiNhanVienBUS nvBUS;
        public frmLoaiNhanVien()
        {
            InitializeComponent();
            nvBUS = new LoaiNhanVienBUS();
        }

        private void frmLoaiNhanVien_Load(object sender, EventArgs e)
        {
            bindingSource1.DataSource = nvBUS.LoadDS();
            gridControl1.DataSource = bindingSource1;
            LoadDS();
        }
        private void LoadDS()
        {
            bindingSource1.DataSource = nvBUS.LoadDS();
            // gridControl1.DataSource = nvBUS.LoadDS().First();
            //gridControl1.RefreshDataSource();
            //gridView1.RefreshData();
        }
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmLoaiNhanVien_Them_Sua frm = new frmLoaiNhanVien_Them_Sua();
            frm.delegateLoadData += LoadDS;
            frm.ShowDialog();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns[0]).ToString();
            frmLoaiNhanVien_Them_Sua frm = new frmLoaiNhanVien_Them_Sua(ma);
            frm.delegateLoadData += LoadDS;
            frm.ShowDialog();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns[0]).ToString();
            DialogResult d = MessageBox.Show("Bạn có muốn xóa loại nhân viên "+ ma + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (d == DialogResult.Yes)
            {
                LoaiNhanVien nv = nvBUS.TimKiem(ma);
                if(nv.NhanViens.Count >= 1)
                {
                    MessageBox.Show("Bạn không thể xóa do còn nhân viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }else
                {
                    nvBUS.Xoa(ma);
                    LoadDS();
                }
            }
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
