﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using QL_KS.BUS;

namespace QL_KS
{
    public partial class frmThem_Sua_Loai_Phong : Form
    {
        public string frmMaLoaiPhong = null;
        Hinh_Thuc_BUS bhinhthuc = new Hinh_Thuc_BUS();
        Loai_Phong_BUS bloaiphong = new Loai_Phong_BUS();
        HinhThuc_LoaiPhongBUS bHT_LP = new HinhThuc_LoaiPhongBUS();
        public frmThem_Sua_Loai_Phong()
        {
            InitializeComponent();
        }

        private void frmThem_Sua_Loai_Phong_Load(object sender, EventArgs e)
        {
            if(frmMaLoaiPhong == null)
            {
                fllDonGia.Padding = new Padding(0, 0, 0, 0);
                fllKetiep.Padding = new Padding(0, 0, 0, 0);
                foreach (HinhThuc ht in bhinhthuc.Load_DS_Hinh_Thuc())
                {
                    fllDonGia.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = ht.TenHinhThuc;
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0,6,0,0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = ht.MaHinhThuc;
                    txttam.Text = "0";
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial",9.75f, System.Drawing.FontStyle.Regular);
                    fllDonGia.Controls.Add(lbtam);
                    fllDonGia.Controls.Add(txttam);
                }
                foreach (HinhThuc ht in bhinhthuc.Load_DS_Hinh_Thuc())
                {
                    fllKetiep.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = "Tiếp Theo";
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0, 6, 0, 0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = ht.MaHinhThuc;
                    txttam.Text = "0";
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    fllKetiep.Controls.Add(lbtam);
                    fllKetiep.Controls.Add(txttam);
                }
            }
            // Co ma loai phong
            else
            {
                btnTraPhong.Text = "Lưu";
                fllDonGia.Padding = new Padding(0, 0, 0, 0);
                fllKetiep.Padding = new Padding(0, 0, 0, 0);
                LoaiPhong loaitam = bloaiphong.Tim_Loai_Phong(frmMaLoaiPhong);
                txtMaLoaiPhong.Text = loaitam.MaLoaiPhong;
                txtTenLoaiPhong.Text = loaitam.TenLoaiPhong;
                txtTienQuaGio.Text = loaitam.TienQuaGio.ToString();
                foreach (HinhThuc ht in bhinhthuc.Load_DS_Hinh_Thuc())
                {
                    fllDonGia.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = ht.TenHinhThuc;
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0, 6, 0, 0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = ht.MaHinhThuc;
                    txttam.Text = bHT_LP.Tim_HT_LP(ht.MaHinhThuc, loaitam.MaLoaiPhong).DonGia.ToString();
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    fllDonGia.Controls.Add(lbtam);
                    fllDonGia.Controls.Add(txttam);
                }
                foreach (HinhThuc ht in bhinhthuc.Load_DS_Hinh_Thuc())
                {
                    fllKetiep.Height += 21;
                    Label lbtam = new Label();
                    lbtam.Text = "Tiếp Theo";
                    lbtam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    lbtam.Width = 93;
                    lbtam.Height = 21;
                    lbtam.Padding = new Padding(0, 6, 0, 0);
                    MaskedTextBox txttam = new MaskedTextBox();
                    txttam.Mask = "000000";
                    txttam.Name = ht.MaHinhThuc;
                    txttam.Text = bHT_LP.Tim_HT_LP(ht.MaHinhThuc, loaitam.MaLoaiPhong).TiepTheo.ToString();
                    txttam.Width = 230;
                    txttam.Height = 21;
                    txttam.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular);
                    fllKetiep.Controls.Add(lbtam);
                    fllKetiep.Controls.Add(txttam);
                }
            }
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnTraPhong_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (txtTenLoaiPhong.Text == string.Empty)
            {
                lbThongBao_Ten.Visible = true;
                error++;
            }
            else
                lbThongBao_Ten.Visible = false;
            if (txtTienQuaGio.Text == string.Empty)
            {
                lbThongBao_TienQuaGio.Visible = true;
                error++;
            }
            else
            {
                lbThongBao_TienQuaGio.Visible = false;
            }
            if(error!= 0 )
            {
                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
            else
            {
                int error2 = 0;
                foreach(Control ct in fllDonGia.Controls)
                {
                    if(ct is MaskedTextBox)
                    {
                        if (ct.Text == string.Empty)
                        {
                            error2++;
                        }
                    }
                    
                }
                foreach (Control ct in fllKetiep.Controls)
                {
                    if (ct is MaskedTextBox)
                    {
                        if (ct.Text == string.Empty)
                        {
                            error2++;
                        }
                    }

                }
                if (error2!= 0 )
                {
                    MessageBox.Show("Hãy nhập đầy đủ đơn giá và giá cho đơn vị kế tiếp");
                }
                else
                {
                    LoaiPhong loaiphongtam = new LoaiPhong();
                    loaiphongtam.TenLoaiPhong = txtTenLoaiPhong.Text;
                    int tienquagio = Convert.ToInt32(txtTienQuaGio.Text);
                    loaiphongtam.TienQuaGio = Convert.ToInt32(txtTienQuaGio.Text);              
                    if(frmMaLoaiPhong==null)
                    {
                        do
                        {
                            loaiphongtam.MaLoaiPhong = RandomString(10);
                        }
                        while (bloaiphong.Tim_Loai_Phong(loaiphongtam.MaLoaiPhong) != null);
                        bloaiphong.Them_Loai_Phong(loaiphongtam);
                    }
                    else
                    {
                        loaiphongtam.MaLoaiPhong = frmMaLoaiPhong;
                        bloaiphong.Sua_Loai_Phong(loaiphongtam);
                    }

                    foreach (HinhThuc ht in bhinhthuc.Load_DS_Hinh_Thuc())
                    {
                        HinhThuc_LoaiPhong ht_lp = new HinhThuc_LoaiPhong();
                        ht_lp.MaLoaiPhong = loaiphongtam.MaLoaiPhong;
                        ht_lp.MaHinhThuc = ht.MaHinhThuc;
                        foreach (Control ct in fllDonGia.Controls)
                        {
                            if (ct is MaskedTextBox)
                            {
                                if (ct.Name == ht_lp.MaHinhThuc)
                                {
                                    ht_lp.DonGia = Convert.ToInt32(ct.Text);
                                }
                            }
                        }
                        foreach (Control ct in fllKetiep.Controls)
                        {
                            if (ct is MaskedTextBox)
                            {
                                if (ct.Name == ht_lp.MaHinhThuc)
                                {
                                    ht_lp.TiepTheo = Convert.ToInt32(ct.Text);
                                }
                            }
                        }
                        if(frmMaLoaiPhong == null)
                            bHT_LP.Them(ht_lp);
                        else
                        {
                            bHT_LP.Sua(ht_lp);
                        }

                    }
                    if (frmMaLoaiPhong == null)
                        MessageBox.Show("Thêm loại phòng thành công");
                    else
                    {
                        MessageBox.Show("Sửa loại phòng thành công");
                    }
                    
                    Close();
                }
            }
        }
        private string RandomString(int size)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 87)));
                sb.Append(c);
            }
            return sb.ToString();

        }
    }
}
