﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils.Menu;

namespace QL_KS
{
    public partial class frmTraCuuPhong : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmTraCuuPhong()
        {
            InitializeComponent();
        }
        int idex;
        public delegate void ThuePhongConTrong();
        public event ThuePhongConTrong KhiThue = null;
        public string HoTen, chucvu, ngaydi = "", ngayden = "";
        public string maphieu, maphongdachuyen;

        private void frmTraCuuPhong_Load(object sender, EventArgs e)
        {
            lkLoaiPhong.Properties.DataSource = Loai_Phong_BUS.getTable();
            dtNgayDen.CustomFormat = "hh:mm:ss tt dd/MM/yyyy";
            dtNgayDi.CustomFormat = "hh:mm:ss tt dd/MM/yyyy";
            if (ngayden != "")
            {
                if (ngaydi != "")
                {
                    dtNgayDen.Value = DateTime.Now;
                    dtNgayDi.Value = DateTime.Parse(ngaydi);
                    radioGroup1.SelectedIndex = 1;
                    btnTimKiem_Click(sender, e);
                }
                else
                {
                    dtNgayDen.Value = DateTime.Now;
                    btnTimKiem_Click(sender, e);
                }
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            //if (radioGroup1.SelectedIndex == 0)
            //{
            //    List<Phong> listp = Phong_BUS.TimPhongTrong();
            //    DataTable TbPhongTrong = new DataTable();
            //    TbPhongTrong.Columns.Add("MaPhong");
            //    TbPhongTrong.Columns.Add("TenPhong");
            //    TbPhongTrong.Columns.Add("LoaiPhong");
            //    TbPhongTrong.Columns.Add("NgayBatDauThue");
            //    TbPhongTrong.Columns.Add("NgayTraPhong");
            //    TbPhongTrong.Columns.Add("GioTrong");
            //    TbPhongTrong.Columns.Add("SoNguoiToiDa");
            //    TbPhongTrong.Columns.Add("DonGia");
            //    DataRow r;
            //    double giotrong;
            //    for (int i = 0; i < listp.Count(); i++)
            //    {
            //        var kq = PhieuThueBUS.LoadPhieuTheoMP(listp[i].MaPhong);
            //        if (kq.Count() > 1) //phòng đang có khách hàng thuê
            //        {
            //            for (int j = 0; j < kq.Count() - 1; j++)
            //            {
            //                var dt = kq[j + 1].NgayBatDauThue - dtNgayDen.Value;
            //                giotrong = Math.Round(double.Parse(dt.Value.TotalHours.ToString()), 1);
            //                if ((dtNgayDen.Value >= kq[j].NgayTraPhong || dtNgayDen.Value < kq[j].NgayBatDauThue) && giotrong >= 0.7)
            //                {
            //                    r = TbPhongTrong.NewRow();
            //                    r["MaPhong"] = listp[i].MaPhong;
            //                    r["TenPhong"] = listp[i].TenPhong;
            //                    r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                    r["NgayBatDauThue"] = dtNgayDen.Value;
            //                    r["NgayTraPhong"] = kq[j + 1].NgayBatDauThue;
            //                    r["GioTrong"] = dt.Value;
            //                    r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                    r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                    TbPhongTrong.Rows.Add(r);
            //                }
            //            }
            //            if (dtNgayDen.Value >= kq[kq.Count() - 1].NgayTra)
            //            {
            //                r = TbPhongTrong.NewRow();
            //                r["MaPhong"] = listp[i].MaPhong;
            //                r["TenPhong"] = listp[i].TenPhong;
            //                r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                if (Math.Round(double.Parse((dtNgayDen.Value - kq[kq.Count() - 1].NgayTra).Value.TotalMinutes.ToString()), 1) >= 15)
            //                    r["NgayBatDauThue"] = dtNgayDen.Value;
            //                else
            //                    r["NgayBatDauThue"] = kq[kq.Count() - 1].NgayTra.Value.AddMinutes(15);
            //                r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                TbPhongTrong.Rows.Add(r);
            //            }
            //        }
            //        else if (kq.Count() == 1) //phòng đang kiểm tra có 1 khách hàng đang thuê trong phiếu thuê
            //        {
            //            var tam = Math.Round(double.Parse((kq[0].NgayBatDauThue - dtNgayDen.Value).Value.TotalHours.ToString()), 1);

            //            if (dtNgayDen.Value >= kq[0].NgayTraPhong)
            //            {
            //                r = TbPhongTrong.NewRow();
            //                r["MaPhong"] = listp[i].MaPhong;
            //                r["TenPhong"] = listp[i].TenPhong;
            //                r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                if (Math.Round(double.Parse((dtNgayDen.Value - kq[0].NgayTraPhong).Value.TotalMinutes.ToString()), 1) >= 15)
            //                    r["NgayBatDauThue"] = dtNgayDen.Value;
            //                else
            //                    r["NgayBatDauThue"] = kq[0].NgayTraPhong.Value.AddMinutes(15);

            //                r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                TbPhongTrong.Rows.Add(r);
            //            }
            //            else if (tam >= 0.7) //nếu ngày đến < ngày thuê phòng và cách nhau 45'
            //            {
            //                r = TbPhongTrong.NewRow();
            //                r["MaPhong"] = listp[i].MaPhong;
            //                r["TenPhong"] = listp[i].TenPhong;
            //                r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                r["NgayBatDauThue"] = dtNgayDen.Value;
            //                r["NgayTraPhong"] = kq[0].NgayBatDauThue.Value.AddMinutes(-15);
            //                r["GioTrong"] = kq[0].NgayBatDauThue.Value.AddMinutes(-15) - dtNgayDen.Value;
            //                r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                TbPhongTrong.Rows.Add(r);
            //            }

            //        }
            //        else //phòng đang trống
            //        {
            //            r = TbPhongTrong.NewRow();
            //            r["MaPhong"] = listp[i].MaPhong;
            //            r["TenPhong"] = listp[i].TenPhong;
            //            r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //            r["NgayBatDauThue"] = dtNgayDen.Value;
            //            r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //            r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //            TbPhongTrong.Rows.Add(r);
            //        }
            //    }
            //    gcTraCuuPhong.DataSource = TbPhongTrong;
            //}
            //if (radioGroup1.SelectedIndex == 1)
            //{
            //    if (dtNgayDi.Value <= dtNgayDen.Value)
            //    {
            //        XtraMessageBox.Show("Ngày đi phải lớn hơn ngày đến", "Lỗi Nhập", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    }
            //    else
            //    {
            //        List<Phong> listp = Phong_BUS.TimPhongTrong();
            //        List<PhieuThue> listpt = PhieuThueBUS.LoadPhieuThue();
            //        DataTable TbPhongTrong = new DataTable();
            //        TbPhongTrong.Columns.Add("MaPhong");
            //        TbPhongTrong.Columns.Add("TenPhong");
            //        TbPhongTrong.Columns.Add("LoaiPhong");
            //        TbPhongTrong.Columns.Add("NgayBatDauThue");
            //        TbPhongTrong.Columns.Add("NgayTraPhong");
            //        TbPhongTrong.Columns.Add("GioTrong");
            //        TbPhongTrong.Columns.Add("SoNguoiToiDa");
            //        TbPhongTrong.Columns.Add("DonGia");
            //        DataRow r;
            //        double giotrong;
            //        double phuttrong;
            //        for (int i = 0; i < listp.Count(); i++)
            //        {
            //            var kq = PhieuThueBUS.LoadPhieuTheoMP(listp[i].MaPhong);
            //            if (kq.Count() > 1)
            //            {
            //                for (int j = 0; j < kq.Count() - 1; j++)
            //                {
            //                    var dt = kq[j + 1].NgayBatDauThue - dtNgayDen.Value;
            //                    giotrong = Math.Round(double.Parse(dt.Value.TotalHours.ToString()), 1);
            //                    var pt = dtNgayDi.Value - kq[j + 1].NgayBatDauThue;
            //                    phuttrong = Math.Round(double.Parse(pt.Value.TotalMinutes.ToString()), 1);
            //                    if (dtNgayDen.Value >= kq[j].NgayTraPhong && dtNgayDi.Value <= kq[j + 1].NgayBatDauThue)
            //                    {
            //                        r = TbPhongTrong.NewRow();
            //                        r["MaPhong"] = listp[i].MaPhong;
            //                        r["TenPhong"] = listp[i].TenPhong;
            //                        r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                        r["NgayBatDauThue"] = dtNgayDen.Value;
            //                        if (Math.Round(double.Parse((kq[j + 1].NgayBatDauThue - dtNgayDi.Value).Value.TotalMinutes.ToString()), 1) >= 15)
            //                            r["NgayTraPhong"] = dtNgayDi.Value;
            //                        else
            //                            r["NgayTraPhong"] = kq[j + 1].NgayBatDauThue.Value.AddMinutes(-15);
            //                        r["GioTrong"] = dt.Value;
            //                        r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                        r["DonGia"] = listp[i].loaiphong1.DonGia;
            //                        TbPhongTrong.Rows.Add(r);
            //                    }
            //                }
            //                if (dtNgayDen.Value >= kq[kq.Count() - 1].NgayTraPhong)
            //                {
            //                    r = TbPhongTrong.NewRow();
            //                    r["MaPhong"] = listp[i].MaPhong;
            //                    r["TenPhong"] = listp[i].TenPhong;
            //                    r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                    if (Math.Round(double.Parse((dtNgayDen.Value - kq[kq.Count() - 1].NgayTra).Value.TotalMinutes.ToString()), 1) >= 15)
            //                        r["NgayBatDauThue"] = dtNgayDen.Value;
            //                    else
            //                        r["NgayBatDauThue"] = kq[kq.Count() - 1].NgayTra.Value.AddMinutes(15);
            //                    r["NgayTraPhong"] = dtNgayDi.Value;
            //                    r["GioTrong"] = dtNgayDi.Value - dtNgayDen.Value;
            //                    r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                    r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                    TbPhongTrong.Rows.Add(r);
            //                }
            //            }
            //            else if (kq.Count() == 1)
            //            {
            //                if (dtNgayDi.Value >= kq[0].NgayTraPhong)
            //                {
            //                    r = TbPhongTrong.NewRow();
            //                    r["MaPhong"] = listp[i].MaPhong;
            //                    r["TenPhong"] = listp[i].TenPhong;
            //                    r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                    if (Math.Round(double.Parse((dtNgayDen.Value - kq[0].NgayTraPhong).Value.TotalMinutes.ToString()), 1) >= 15)
            //                        r["NgayBatDauThue"] = dtNgayDen.Value;
            //                    else
            //                        r["NgayBatDauThue"] = kq[0].NgayTraPhong.Value.AddMinutes(15);
            //                    r["NgayTra"] = dtNgayDi.Value;
            //                    r["GioTrong"] = dtNgayDi.Value - dtNgayDen.Value;
            //                    r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                    r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                    TbPhongTrong.Rows.Add(r);
            //                }
            //                else if (dtNgayDi.Value <= kq[0].NgayBatDauThue)
            //                {
            //                    r = TbPhongTrong.NewRow();
            //                    r["MaPhong"] = listp[i].MaPhong;
            //                    r["TenPhong"] = listp[i].TenPhong;
            //                    r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                    r["NgayThue"] = dtNgayDen.Value;
            //                    if (Math.Round(double.Parse((kq[0].NgayBatDauThue - dtNgayDi.Value).Value.TotalMinutes.ToString()), 1) >= 15)
            //                        r["NgayTra"] = dtNgayDi.Value;
            //                    else
            //                        r["NgayTra"] = kq[0].NgayBatDauThue.Value.AddMinutes(-15);
            //                    r["GioTrong"] = dtNgayDi.Value - dtNgayDen.Value;
            //                    r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                    r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                    TbPhongTrong.Rows.Add(r);
            //                }

            //            }
            //            else
            //            {
            //                r = TbPhongTrong.NewRow();
            //                r["MaPhong"] = listp[i].MaPhong;
            //                r["TenPhong"] = listp[i].TenPhong;
            //                r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                r["NgayThue"] = dtNgayDen.Value;
            //                r["NgayTra"] = dtNgayDi.Value;
            //                //r["GioTrong"] = Math.Round(double.Parse((dtngdi.Value - dtngayden.Value).TotalHours.ToString()), 1);
            //                r["GioTrong"] = dtNgayDi.Value - dtNgayDen.Value;
            //                r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                TbPhongTrong.Rows.Add(r);
            //            }
            //        }
            //        gcTraCuuPhong.DataSource = TbPhongTrong;
            //    }
            //}
        }
        private bool ExistForm(DevComponents.DotNetBar.Metro.MetroForm form)
        {
            foreach (var child in this.MdiParent.MdiChildren)
            {
                if (child.Name == form.Name)
                {
                    child.Activate();
                    return true;
                }
            }
            return false;
        }
        private void btnThuePhong_Click(object sender, EventArgs e)
        {
            if (gvTraCuuPhong.RowCount > 0)
            {
                if (idex >= 0)
                {
                    if (ngayden != "")
                    {
                        frmPhieuThue.checkChuyenPhong = 1;
                        frmPhieuThue.Maphieuthue = maphieu;
                        frmPhieuThue.maphongdachuyen = maphongdachuyen;
                    }
                    frmPhieuThue.MaP = gvTraCuuPhong.GetRowCellValue(idex, "MaPhong").ToString();
                    frmPhieuThue.TenP = gvTraCuuPhong.GetRowCellValue(idex, "TenPhong").ToString();
                    frmPhieuThue.SLTD = int.Parse(gvTraCuuPhong.GetRowCellValue(idex, "SoNguoiToiDa").ToString());
                    frmPhieuThue.NgayDen = gvTraCuuPhong.GetRowCellValue(idex, "NgayThue").ToString();
                    frmPhieuThue.NgayDi = gvTraCuuPhong.GetRowCellValue(idex, "NgayTra").ToString();
                    frmPhieuThue dmp = new frmPhieuThue();
                    if (KhiThue != null)
                        KhiThue();
                    if (ExistForm(dmp)) return;
                    dmp.MdiParent = this.MdiParent;
                    dmp.Show();
                }
                else
                    XtraMessageBox.Show("Chưa chọn phòng cho thuê!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                XtraMessageBox.Show("Chưa chọn phòng cho thuê!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        private void gvTraCuuPhong_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            idex = e.FocusedRowHandle;
        }

        private void gvTraCuuPhong_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                GridViewMenu menu = e.Menu as GridViewMenu;
                menu.Items.Clear();

                DXMenuItem itemThue = new DXMenuItem("Thuê Phòng");
                itemThue.Click += new EventHandler(btnThuePhong_Click);
                menu.Items.Add(itemThue);

            }
        }
        Phong_BUS phongbus = new Phong_BUS();
        private void lkLoaiPhong_EditValueChanged(object sender, EventArgs e)
        {
            //string maloai = lkLoaiPhong.GetColumnValue("MaLP").ToString().Trim();

            //if (maloai != null)
            //{
            //    List<Phong> listp = Phong_BUS.TimPhongTrong(maloai);
            //    List<PhieuThue> listpt = PhieuThueBUS.LoadPhieuThue();
            //    DataTable TbPhongTrong = new DataTable();
            //    TbPhongTrong.Columns.Add("MaPhong");
            //    TbPhongTrong.Columns.Add("TenPhong");
            //    TbPhongTrong.Columns.Add("LoaiPhong");
            //    TbPhongTrong.Columns.Add("NgayBatDauThue");
            //    TbPhongTrong.Columns.Add("NgayTraPhong");
            //    TbPhongTrong.Columns.Add("GioTrong");
            //    TbPhongTrong.Columns.Add("SoNguoiToiDa");
            //    TbPhongTrong.Columns.Add("DonGia");
            //    DataRow r;
            //    double giotrong;
            //    for (int i = 0; i < listp.Count(); i++)
            //    {
            //        var kq = PhieuThueBUS.LoadPhieuTheoMP(listp[i].MaPhong);
            //        if (kq.Count() >= 1)
            //        {
            //            for (int j = 0; j < kq.Count() - 1; j++)
            //            {
            //                var dt = kq[j + 1].NgayBatDauThue - kq[j].NgayTraPhong;
            //                giotrong = Math.Round(double.Parse(dt.Value.TotalHours.ToString()), 1);
            //                if (giotrong >= 0.7)
            //                {
            //                    r = TbPhongTrong.NewRow();
            //                    r["MaPhong"] = listp[i].MaPhong;
            //                    r["TenPhong"] = listp[i].TenPhong;
            //                    r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                    r["NgayBatDauThue"] = kq[j].NgayTraPhong;
            //                    r["NgayTraPhong"] = kq[j + 1].NgayBatDauThue;
            //                    r["GioTrong"] = dt.Value;
            //                    r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                    r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                    TbPhongTrong.Rows.Add(r);
            //                }
            //            }
            //            if (DateTime.Now >= kq[kq.Count() - 1].NgayTra)
            //            {

            //                var tam = DateTime.Now - kq[kq.Count() - 1].NgayTra;
            //                var phuttrong = Math.Round(double.Parse(tam.Value.TotalMinutes.ToString()), 1);
            //                if (phuttrong >= 15)
            //                {
            //                    r = TbPhongTrong.NewRow();
            //                    r["MaPhong"] = listp[i].MaPhong;
            //                    r["TenPhong"] = listp[i].TenPhong;
            //                    r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                    r["NgayBatDauThue"] = DateTime.Now;
            //                    r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                    r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                    TbPhongTrong.Rows.Add(r);
            //                }
            //                else
            //                {
            //                    r = TbPhongTrong.NewRow();
            //                    r["MaPhong"] = listp[i].MaPhong;
            //                    r["TenPhong"] = listp[i].TenPhong;
            //                    r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                    r["NgayBatDauThue"] = DateTime.Now.AddMinutes(15);
            //                    r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                    r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                    TbPhongTrong.Rows.Add(r);
            //                }
            //            }
            //            else
            //            {
            //                r = TbPhongTrong.NewRow();
            //                r["MaPhong"] = listp[i].MaPhong;
            //                r["TenPhong"] = listp[i].TenPhong;
            //                r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //                r["NgayBatDauThue"] = kq[kq.Count() - 1].NgayTra.Value.AddMinutes(15);
            //                r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //                r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //                TbPhongTrong.Rows.Add(r);
            //            }
            //        }
            //        else
            //        {
            //            r = TbPhongTrong.NewRow();
            //            r["MaPhong"] = listp[i].MaPhong;
            //            r["TenPhong"] = listp[i].TenPhong;
            //            r["LoaiPhong"] = listp[i].LoaiPhong1.TenLoaiPhong;
            //            r["NgayBatDauThue"] = DateTime.Now;
            //            r["SoNguoiToiDa"] = listp[i].SoNguoiToiDa;
            //            r["DonGia"] = listp[i].LoaiPhong1.DonGia;
            //            TbPhongTrong.Rows.Add(r);
            //        }
            //    }
            //    gcTraCuuPhong.DataSource = TbPhongTrong;
            //}
        }
    }
}