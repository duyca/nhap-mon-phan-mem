﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using DevExpress.XtraEditors;
using log4net;

namespace QL_KS
{
    public partial class frmPTraPhong : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmPTraPhong()
        {
            InitializeComponent();
        }
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmPTraPhong).Name);
        public string MaKH, TenKH, NGDen, NGDi, TenPhong, SoNguoiO, TaiKhoan;
        public string MaPhong, MaNV, MaPhieuThue;
        public double tienchuyenphong;
        public static int checkchuyenphong = 0;
        double HeSo = 1, PhuThu = 1;
        
        double DonGia1h, tongtien;

        private void frmPTraPhong_Load(object sender, EventArgs e)
        {
            txtTenPhong.Text = TenPhong;
            txtTenKH.Text = TenKH;
            txtNGDen.Text = NGDen;
            if (DateTime.Now > DateTime.Parse(NGDi))
                txtNGDi.Text = NGDi;
            else
                txtNGDi.Text = DateTime.Now.ToString();
            txtSoNguoiO.Text = SoNguoiO;
            //???
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        HoaDonBUS hoadonbus = new HoaDonBUS();
      
        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            HoaDon hd = new HoaDon();
            hd.MaKH = MaKH;
            hd.SoGioThue = Math.Round(double.Parse((DateTime.Parse(txtNGDi.Text) - DateTime.Parse(txtNGDen.Text)).TotalHours.ToString()), 1);
            hd.TongTien = tongtien;
            hd.MaNV = MaNV;
            hd.NgayThanhToan = DateTime.Now;
           
            // cấp phát mã hóa đơn ????
            hd.MaHD = "HD";
            
            ChiTietHoaDon cthd = new ChiTietHoaDon();
            cthd.MaHD = hd.MaHD;
            cthd.MaPhong = MaPhong;
            cthd.NgayDen = DateTime.Parse(NGDen);
            cthd.NgayDi = DateTime.Parse(NGDi);
            
            //?????
            //cthd.SoNguoiThue = int.Parse(SoNguoiO.ToString());
            //cthd.HeSo = HeSo;
            cthd.PhuThu = PhuThu;
            cthd.DonGia = DonGia1h;
            cthd.ThanhTien = tongtien;
            try
            {
                if (checkchuyenphong != 0)
                {
                    PhieuThue p = new PhieuThue();
                    p.MaPhieuThue = MaPhieuThue;
                    p.TienChuyenPhong = tongtien;
                   // PhieuThueBUS.CapNhatTienChuyenPhong(p);
                    XtraMessageBox.Show("Thanh toán thành công!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LuuNhatKyHT.Them(TaiKhoan, "Phiếu Thuê", "Thanh Toán " + tongtien.ToString());
                    checkchuyenphong = 0;
                    this.Close();
                }
                else
                {
                    hoadonbus.Them_Hoa_Don(hd);
                   // ctBus.Them(cthd);
                    PhieuThueBUS.XoaPhieu(MaPhieuThue);
                    if (PhieuThueBUS.LoadPhieuTheoMP(MaPhong).Count == 0)
                      //  Phong_BUS.CapNhatTrangThai(MaPhong, "Còn Trống");
                   
                    
                    XtraMessageBox.Show("Thanh toán thành công!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LuuNhatKyHT.Them(TaiKhoan, "Phiếu Thuê", "Thanh Toán " + tongtien.ToString());
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Thanh toán không thành công!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _logger.Error(ex.ToString());
            }
        }
    }
}