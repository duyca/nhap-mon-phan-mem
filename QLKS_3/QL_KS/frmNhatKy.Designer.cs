﻿namespace QL_KS
{
    partial class frmNhatKy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gcHeThong = new DevExpress.XtraGrid.GridControl();
            this.gvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenMayTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThoiGian = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ChucNang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HanhDong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDocTapTinXML = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaToanBoNhatKy = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuXML = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatTTExcel = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaNhatKy = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcHeThong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.Appearance.Options.UseForeColor = true;
            this.panelControl2.Controls.Add(this.gcHeThong);
            this.panelControl2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panelControl2.Location = new System.Drawing.Point(-112, 23);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(857, 303);
            this.panelControl2.TabIndex = 57;
            // 
            // gcHeThong
            // 
            this.gcHeThong.EmbeddedNavigator.Appearance.BackColor = System.Drawing.Color.White;
            this.gcHeThong.EmbeddedNavigator.Appearance.ForeColor = System.Drawing.Color.Black;
            this.gcHeThong.EmbeddedNavigator.Appearance.Options.UseBackColor = true;
            this.gcHeThong.EmbeddedNavigator.Appearance.Options.UseForeColor = true;
            this.gcHeThong.Location = new System.Drawing.Point(112, 0);
            this.gcHeThong.MainView = this.gvData;
            this.gcHeThong.Name = "gcHeThong";
            this.gcHeThong.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit3,
            this.repositoryItemDateEdit3});
            this.gcHeThong.Size = new System.Drawing.Size(740, 303);
            this.gcHeThong.TabIndex = 1;
            this.gcHeThong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvData});
            // 
            // gvData
            // 
            this.gvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.ID,
            this.TenMayTinh,
            this.ThoiGian,
            this.ChucNang,
            this.HanhDong});
            this.gvData.GridControl = this.gcHeThong;
            this.gvData.Name = "gvData";
            this.gvData.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.ID, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.ChucNang, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // STT
            // 
            this.STT.FieldName = "STT";
            this.STT.Name = "STT";
            this.STT.OptionsColumn.AllowEdit = false;
            this.STT.OptionsColumn.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.Caption = "ID";
            this.ID.FieldName = "ID";
            this.ID.Name = "ID";
            this.ID.OptionsColumn.AllowEdit = false;
            this.ID.Visible = true;
            this.ID.VisibleIndex = 0;
            // 
            // TenMayTinh
            // 
            this.TenMayTinh.Caption = "Máy tính";
            this.TenMayTinh.FieldName = "TenMayTinh";
            this.TenMayTinh.Name = "TenMayTinh";
            this.TenMayTinh.OptionsColumn.AllowEdit = false;
            this.TenMayTinh.OptionsColumn.ReadOnly = true;
            this.TenMayTinh.Visible = true;
            this.TenMayTinh.VisibleIndex = 1;
            // 
            // ThoiGian
            // 
            this.ThoiGian.Caption = "Thời gian";
            this.ThoiGian.FieldName = "ThoiGian";
            this.ThoiGian.Name = "ThoiGian";
            this.ThoiGian.Visible = true;
            this.ThoiGian.VisibleIndex = 3;
            // 
            // ChucNang
            // 
            this.ChucNang.Caption = "Chức năng";
            this.ChucNang.FieldName = "ChucNang";
            this.ChucNang.Name = "ChucNang";
            this.ChucNang.Visible = true;
            this.ChucNang.VisibleIndex = 2;
            // 
            // HanhDong
            // 
            this.HanhDong.Caption = "Hành động";
            this.HanhDong.FieldName = "HanhDong";
            this.HanhDong.Name = "HanhDong";
            this.HanhDong.Visible = true;
            this.HanhDong.VisibleIndex = 4;
            // 
            // repositoryItemImageEdit3
            // 
            this.repositoryItemImageEdit3.AutoHeight = false;
            this.repositoryItemImageEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit3.Name = "repositoryItemImageEdit3";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(645, 371);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(67, 23);
            this.simpleButton1.TabIndex = 67;
            this.simpleButton1.Text = "Đóng";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnDocTapTinXML
            // 
            this.btnDocTapTinXML.Location = new System.Drawing.Point(404, 371);
            this.btnDocTapTinXML.Name = "btnDocTapTinXML";
            this.btnDocTapTinXML.Size = new System.Drawing.Size(111, 23);
            this.btnDocTapTinXML.TabIndex = 66;
            this.btnDocTapTinXML.Text = "Đọc từ tập tin XML";
            this.btnDocTapTinXML.Click += new System.EventHandler(this.btnDocTapTinXML_Click);
            // 
            // btnXoaToanBoNhatKy
            // 
            this.btnXoaToanBoNhatKy.Location = new System.Drawing.Point(124, 371);
            this.btnXoaToanBoNhatKy.Name = "btnXoaToanBoNhatKy";
            this.btnXoaToanBoNhatKy.Size = new System.Drawing.Size(133, 23);
            this.btnXoaToanBoNhatKy.TabIndex = 65;
            this.btnXoaToanBoNhatKy.Text = "Xóa toàn bộ nhật ký";
            this.btnXoaToanBoNhatKy.Click += new System.EventHandler(this.btnXoaToanBoNhatKy_Click);
            // 
            // btnLuuXML
            // 
            this.btnLuuXML.Location = new System.Drawing.Point(277, 371);
            this.btnLuuXML.Name = "btnLuuXML";
            this.btnLuuXML.Size = new System.Drawing.Size(111, 23);
            this.btnLuuXML.TabIndex = 64;
            this.btnLuuXML.Text = "Lưu vào tập tin XML";
            this.btnLuuXML.Click += new System.EventHandler(this.btnLuuXML_Click);
            // 
            // btnXuatTTExcel
            // 
            this.btnXuatTTExcel.Location = new System.Drawing.Point(533, 371);
            this.btnXuatTTExcel.Name = "btnXuatTTExcel";
            this.btnXuatTTExcel.Size = new System.Drawing.Size(95, 23);
            this.btnXuatTTExcel.TabIndex = 63;
            this.btnXuatTTExcel.Text = "Xuất tập tin Excel";
            this.btnXuatTTExcel.Click += new System.EventHandler(this.btnXuatTTExcel_Click);
            // 
            // btnXoaNhatKy
            // 
            this.btnXoaNhatKy.Location = new System.Drawing.Point(12, 371);
            this.btnXoaNhatKy.Name = "btnXoaNhatKy";
            this.btnXoaNhatKy.Size = new System.Drawing.Size(95, 23);
            this.btnXoaNhatKy.TabIndex = 62;
            this.btnXoaNhatKy.Text = "Xóa nhật ký";
            this.btnXoaNhatKy.Click += new System.EventHandler(this.btnXoaNhatKy_Click);
            // 
            // frmNhatKy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 421);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnDocTapTinXML);
            this.Controls.Add(this.btnXoaToanBoNhatKy);
            this.Controls.Add(this.btnLuuXML);
            this.Controls.Add(this.btnXuatTTExcel);
            this.Controls.Add(this.btnXoaNhatKy);
            this.Controls.Add(this.panelControl2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmNhatKy";
            this.Text = "NHẬT KÝ HỆ THỐNG";
            this.Load += new System.EventHandler(this.frmNhatKy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcHeThong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gcHeThong;
        private DevExpress.XtraGrid.Views.Grid.GridView gvData;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn ID;
        private DevExpress.XtraGrid.Columns.GridColumn TenMayTinh;
        private DevExpress.XtraGrid.Columns.GridColumn ThoiGian;
        private DevExpress.XtraGrid.Columns.GridColumn ChucNang;
        private DevExpress.XtraGrid.Columns.GridColumn HanhDong;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnDocTapTinXML;
        private DevExpress.XtraEditors.SimpleButton btnXoaToanBoNhatKy;
        private DevExpress.XtraEditors.SimpleButton btnLuuXML;
        private DevExpress.XtraEditors.SimpleButton btnXuatTTExcel;
        private DevExpress.XtraEditors.SimpleButton btnXoaNhatKy;
    }
}