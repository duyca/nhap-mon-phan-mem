﻿namespace QL_KS
{
    partial class frmPhanQuyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPhanQuyen));
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemImageEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.gcPhanQuyen = new DevExpress.XtraGrid.GridControl();
            this.gvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LoaiNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenLoaiNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DanhMucPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LapPhieuThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TraCuuPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThayDoiQuyDinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NhatKyHeThong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QuanLyLoaiPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QuanLyKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QuanLyLoaiKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QuanLyNhanVien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QuanLyLoaiNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PhanQuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SaoLuuDL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PhucHoiDL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HinhThucThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.btnThoat = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuuPhanQuyen = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPhanQuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:ss";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:ss";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            // 
            // repositoryItemImageEdit2
            // 
            this.repositoryItemImageEdit2.AutoHeight = false;
            this.repositoryItemImageEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit2.Name = "repositoryItemImageEdit2";
            // 
            // gcPhanQuyen
            // 
            this.gcPhanQuyen.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcPhanQuyen.Location = new System.Drawing.Point(0, 0);
            this.gcPhanQuyen.MainView = this.gvData;
            this.gcPhanQuyen.Name = "gcPhanQuyen";
            this.gcPhanQuyen.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit3,
            this.repositoryItemDateEdit3});
            this.gcPhanQuyen.Size = new System.Drawing.Size(994, 243);
            this.gcPhanQuyen.TabIndex = 64;
            this.gcPhanQuyen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvData});
            // 
            // gvData
            // 
            this.gvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.LoaiNV,
            this.TenLoaiNV,
            this.DanhMucPhong,
            this.LapPhieuThue,
            this.TraCuuPhong,
            this.ThayDoiQuyDinh,
            this.NhatKyHeThong,
            this.QuanLyLoaiPhong,
            this.QuanLyKhachHang,
            this.QuanLyLoaiKhachHang,
            this.QuanLyNhanVien,
            this.QuanLyLoaiNV,
            this.PhanQuyen,
            this.SaoLuuDL,
            this.PhucHoiDL,
            this.HinhThucThue});
            this.gvData.GridControl = this.gcPhanQuyen;
            this.gvData.Name = "gvData";
            this.gvData.OptionsView.ShowGroupPanel = false;
            // 
            // STT
            // 
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            // 
            // LoaiNV
            // 
            this.LoaiNV.Caption = "gridColumn1";
            this.LoaiNV.FieldName = "LoaiNV";
            this.LoaiNV.Name = "LoaiNV";
            // 
            // TenLoaiNV
            // 
            this.TenLoaiNV.Caption = "Loại nhân viên";
            this.TenLoaiNV.FieldName = "LoaiNhanVien.TenLoaiNV";
            this.TenLoaiNV.Name = "TenLoaiNV";
            this.TenLoaiNV.OptionsColumn.ReadOnly = true;
            this.TenLoaiNV.Visible = true;
            this.TenLoaiNV.VisibleIndex = 0;
            this.TenLoaiNV.Width = 79;
            // 
            // DanhMucPhong
            // 
            this.DanhMucPhong.Caption = "Danh mục phòng";
            this.DanhMucPhong.FieldName = "DanhMucPhong";
            this.DanhMucPhong.Name = "DanhMucPhong";
            this.DanhMucPhong.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.DanhMucPhong.Visible = true;
            this.DanhMucPhong.VisibleIndex = 1;
            this.DanhMucPhong.Width = 90;
            // 
            // LapPhieuThue
            // 
            this.LapPhieuThue.Caption = "Lập phiếu thuê";
            this.LapPhieuThue.FieldName = "LapPhieuThue";
            this.LapPhieuThue.Name = "LapPhieuThue";
            this.LapPhieuThue.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.LapPhieuThue.Visible = true;
            this.LapPhieuThue.VisibleIndex = 2;
            this.LapPhieuThue.Width = 81;
            // 
            // TraCuuPhong
            // 
            this.TraCuuPhong.Caption = "Tra cứu phòng";
            this.TraCuuPhong.FieldName = "TraCuuPhong";
            this.TraCuuPhong.Name = "TraCuuPhong";
            this.TraCuuPhong.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.TraCuuPhong.Visible = true;
            this.TraCuuPhong.VisibleIndex = 3;
            this.TraCuuPhong.Width = 80;
            // 
            // ThayDoiQuyDinh
            // 
            this.ThayDoiQuyDinh.Caption = "Thay đổi quy định";
            this.ThayDoiQuyDinh.FieldName = "ThayDoiQuyDinh";
            this.ThayDoiQuyDinh.Name = "ThayDoiQuyDinh";
            this.ThayDoiQuyDinh.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.ThayDoiQuyDinh.Visible = true;
            this.ThayDoiQuyDinh.VisibleIndex = 4;
            this.ThayDoiQuyDinh.Width = 95;
            // 
            // NhatKyHeThong
            // 
            this.NhatKyHeThong.Caption = "Nhật ký hệ thống";
            this.NhatKyHeThong.FieldName = "NhatKyHeThong";
            this.NhatKyHeThong.Name = "NhatKyHeThong";
            this.NhatKyHeThong.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.NhatKyHeThong.Visible = true;
            this.NhatKyHeThong.VisibleIndex = 5;
            this.NhatKyHeThong.Width = 93;
            // 
            // QuanLyLoaiPhong
            // 
            this.QuanLyLoaiPhong.Caption = "Quản lý loại phòng";
            this.QuanLyLoaiPhong.FieldName = "QuanLyLoaiPhong";
            this.QuanLyLoaiPhong.Name = "QuanLyLoaiPhong";
            this.QuanLyLoaiPhong.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.QuanLyLoaiPhong.Visible = true;
            this.QuanLyLoaiPhong.VisibleIndex = 6;
            this.QuanLyLoaiPhong.Width = 99;
            // 
            // QuanLyKhachHang
            // 
            this.QuanLyKhachHang.Caption = "Quản lý khách hàng";
            this.QuanLyKhachHang.FieldName = "QuanLyKhachHang";
            this.QuanLyKhachHang.Name = "QuanLyKhachHang";
            this.QuanLyKhachHang.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.QuanLyKhachHang.Visible = true;
            this.QuanLyKhachHang.VisibleIndex = 7;
            this.QuanLyKhachHang.Width = 105;
            // 
            // QuanLyLoaiKhachHang
            // 
            this.QuanLyLoaiKhachHang.Caption = "Quản lý loại khách hàng";
            this.QuanLyLoaiKhachHang.FieldName = "QuanLyLoaiKhachHang";
            this.QuanLyLoaiKhachHang.Name = "QuanLyLoaiKhachHang";
            this.QuanLyLoaiKhachHang.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.QuanLyLoaiKhachHang.Visible = true;
            this.QuanLyLoaiKhachHang.VisibleIndex = 8;
            this.QuanLyLoaiKhachHang.Width = 124;
            // 
            // QuanLyNhanVien
            // 
            this.QuanLyNhanVien.Caption = "Quản lý nhân viên";
            this.QuanLyNhanVien.FieldName = "QuanLyNhanVien";
            this.QuanLyNhanVien.Name = "QuanLyNhanVien";
            this.QuanLyNhanVien.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.QuanLyNhanVien.Visible = true;
            this.QuanLyNhanVien.VisibleIndex = 9;
            this.QuanLyNhanVien.Width = 97;
            // 
            // QuanLyLoaiNV
            // 
            this.QuanLyLoaiNV.Caption = "QuanLyLoaiNhanVien";
            this.QuanLyLoaiNV.FieldName = "QuanLyLoaiNV";
            this.QuanLyLoaiNV.Name = "QuanLyLoaiNV";
            this.QuanLyLoaiNV.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.QuanLyLoaiNV.Visible = true;
            this.QuanLyLoaiNV.VisibleIndex = 10;
            this.QuanLyLoaiNV.Width = 111;
            // 
            // PhanQuyen
            // 
            this.PhanQuyen.Caption = "Phân quyền";
            this.PhanQuyen.FieldName = "PhanQuyen";
            this.PhanQuyen.Name = "PhanQuyen";
            this.PhanQuyen.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.PhanQuyen.Visible = true;
            this.PhanQuyen.VisibleIndex = 11;
            this.PhanQuyen.Width = 67;
            // 
            // SaoLuuDL
            // 
            this.SaoLuuDL.Caption = "Sao lưu";
            this.SaoLuuDL.FieldName = "SaoLuuDL";
            this.SaoLuuDL.Name = "SaoLuuDL";
            this.SaoLuuDL.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.SaoLuuDL.Visible = true;
            this.SaoLuuDL.VisibleIndex = 12;
            this.SaoLuuDL.Width = 46;
            // 
            // PhucHoiDL
            // 
            this.PhucHoiDL.Caption = "Phục hồi";
            this.PhucHoiDL.FieldName = "PhucHoiDL";
            this.PhucHoiDL.Name = "PhucHoiDL";
            this.PhucHoiDL.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.PhucHoiDL.Visible = true;
            this.PhucHoiDL.VisibleIndex = 13;
            this.PhucHoiDL.Width = 50;
            // 
            // HinhThucThue
            // 
            this.HinhThucThue.Caption = "Hình thức thuê";
            this.HinhThucThue.FieldName = "HinhThucThue";
            this.HinhThucThue.Name = "HinhThucThue";
            this.HinhThucThue.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.HinhThucThue.Visible = true;
            this.HinhThucThue.VisibleIndex = 14;
            this.HinhThucThue.Width = 81;
            // 
            // repositoryItemImageEdit3
            // 
            this.repositoryItemImageEdit3.AutoHeight = false;
            this.repositoryItemImageEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit3.Name = "repositoryItemImageEdit3";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(886, 266);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(67, 23);
            this.btnThoat.TabIndex = 69;
            this.btnThoat.Text = "Đóng";
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnLuuPhanQuyen
            // 
            this.btnLuuPhanQuyen.Location = new System.Drawing.Point(796, 266);
            this.btnLuuPhanQuyen.Name = "btnLuuPhanQuyen";
            this.btnLuuPhanQuyen.Size = new System.Drawing.Size(73, 23);
            this.btnLuuPhanQuyen.TabIndex = 68;
            this.btnLuuPhanQuyen.Text = "Lưu";
            this.btnLuuPhanQuyen.Click += new System.EventHandler(this.btnLuuPhanQuyen_Click);
            // 
            // frmPhanQuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 328);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnLuuPhanQuyen);
            this.Controls.Add(this.gcPhanQuyen);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPhanQuyen";
            this.Text = "PHÂN QUYỀN NGƯỜI DÙNG";
            this.Load += new System.EventHandler(this.frmPhanQuyen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPhanQuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit2;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraGrid.GridControl gcPhanQuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gvData;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn LoaiNV;
        private DevExpress.XtraGrid.Columns.GridColumn TenLoaiNV;
        private DevExpress.XtraGrid.Columns.GridColumn DanhMucPhong;
        private DevExpress.XtraGrid.Columns.GridColumn LapPhieuThue;
        private DevExpress.XtraGrid.Columns.GridColumn TraCuuPhong;
        private DevExpress.XtraGrid.Columns.GridColumn ThayDoiQuyDinh;
        private DevExpress.XtraGrid.Columns.GridColumn NhatKyHeThong;
        private DevExpress.XtraGrid.Columns.GridColumn QuanLyLoaiPhong;
        private DevExpress.XtraGrid.Columns.GridColumn QuanLyKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn QuanLyLoaiKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn QuanLyNhanVien;
        private DevExpress.XtraGrid.Columns.GridColumn QuanLyLoaiNV;
        private DevExpress.XtraGrid.Columns.GridColumn PhanQuyen;
        private DevExpress.XtraGrid.Columns.GridColumn SaoLuuDL;
        private DevExpress.XtraGrid.Columns.GridColumn PhucHoiDL;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.SimpleButton btnThoat;
        private DevExpress.XtraEditors.SimpleButton btnLuuPhanQuyen;
        private DevExpress.XtraGrid.Columns.GridColumn HinhThucThue;
    }
}