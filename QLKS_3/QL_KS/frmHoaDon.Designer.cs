﻿namespace QL_KS
{
    partial class frmHoaDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHoaDon));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lblSoDienThoai = new DevExpress.XtraEditors.LabelControl();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTenKH = new DevExpress.XtraEditors.LabelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCMND = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMaKH = new DevExpress.XtraEditors.LabelControl();
            this.label5 = new System.Windows.Forms.Label();
            this.gcHoaDon = new DevExpress.XtraGrid.GridControl();
            this.gvHoaDon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TenKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaHD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ma_kh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoNgayThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TongTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnXemChiTiet = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnIn = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadLai = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXemChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lblSoDienThoai);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.lblDiaChi);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.lblTenKH);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.lblCMND);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.lblMaKH);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Location = new System.Drawing.Point(-2, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(291, 275);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin khách hàng:";
            // 
            // lblSoDienThoai
            // 
            this.lblSoDienThoai.Location = new System.Drawing.Point(121, 236);
            this.lblSoDienThoai.Name = "lblSoDienThoai";
            this.lblSoDienThoai.Size = new System.Drawing.Size(12, 13);
            this.lblSoDienThoai.TabIndex = 48;
            this.lblSoDienThoai.Text = "...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 47;
            this.label4.Text = "Số điện thoại:";
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.Location = new System.Drawing.Point(121, 188);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(12, 13);
            this.lblDiaChi.TabIndex = 46;
            this.lblDiaChi.Text = "...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 45;
            this.label3.Text = "Địa chỉ:";
            // 
            // lblTenKH
            // 
            this.lblTenKH.Location = new System.Drawing.Point(121, 91);
            this.lblTenKH.Name = "lblTenKH";
            this.lblTenKH.Size = new System.Drawing.Size(12, 13);
            this.lblTenKH.TabIndex = 44;
            this.lblTenKH.Text = "...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 43;
            this.label2.Text = "Tên khách hàng:";
            // 
            // lblCMND
            // 
            this.lblCMND.Location = new System.Drawing.Point(121, 141);
            this.lblCMND.Name = "lblCMND";
            this.lblCMND.Size = new System.Drawing.Size(12, 13);
            this.lblCMND.TabIndex = 42;
            this.lblCMND.Text = "...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "CMND:";
            // 
            // lblMaKH
            // 
            this.lblMaKH.Location = new System.Drawing.Point(121, 47);
            this.lblMaKH.Name = "lblMaKH";
            this.lblMaKH.Size = new System.Drawing.Size(12, 13);
            this.lblMaKH.TabIndex = 40;
            this.lblMaKH.Text = "...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 16);
            this.label5.TabIndex = 39;
            this.label5.Text = "Mã khách hàng:";
            // 
            // gcHoaDon
            // 
            this.gcHoaDon.Dock = System.Windows.Forms.DockStyle.Right;
            this.gcHoaDon.Location = new System.Drawing.Point(284, 0);
            this.gcHoaDon.MainView = this.gvHoaDon;
            this.gcHoaDon.Name = "gcHoaDon";
            this.gcHoaDon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnXemChiTiet,
            this.repositoryItemCheckEdit1});
            this.gcHoaDon.Size = new System.Drawing.Size(615, 481);
            this.gcHoaDon.TabIndex = 22;
            this.gcHoaDon.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvHoaDon,
            this.gridView1});
            // 
            // gvHoaDon
            // 
            this.gvHoaDon.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TenKH,
            this.MaHD,
            this.ma_kh,
            this.SoNgayThue,
            this.NgayTT,
            this.MaNV,
            this.TongTien,
            this.DiaChi,
            this.SDT,
            this.CMND});
            this.gvHoaDon.GridControl = this.gcHoaDon;
            this.gvHoaDon.GroupPanelText = "Hóa đơn";
            this.gvHoaDon.Name = "gvHoaDon";
            this.gvHoaDon.OptionsBehavior.Editable = false;
            this.gvHoaDon.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.True;
            this.gvHoaDon.OptionsView.ShowAutoFilterRow = true;
            this.gvHoaDon.OptionsView.ShowGroupPanel = false;
            this.gvHoaDon.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvHoaDon_RowClick);
            this.gvHoaDon.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.gvHoaDon_MasterRowEmpty);
            this.gvHoaDon.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gvHoaDon_MasterRowGetChildList);
            this.gvHoaDon.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gvHoaDon_MasterRowGetRelationName);
            this.gvHoaDon.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gvHoaDon_MasterRowGetRelationCount);
            this.gvHoaDon.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvHoaDon_FocusedRowChanged);
            // 
            // TenKH
            // 
            this.TenKH.Caption = "Khách Hàng";
            this.TenKH.FieldName = "KhachHang.TenKH";
            this.TenKH.Name = "TenKH";
            this.TenKH.Visible = true;
            this.TenKH.VisibleIndex = 0;
            // 
            // MaHD
            // 
            this.MaHD.Caption = "Hóa Đơn";
            this.MaHD.FieldName = "MaHD";
            this.MaHD.Name = "MaHD";
            this.MaHD.Visible = true;
            this.MaHD.VisibleIndex = 1;
            // 
            // ma_kh
            // 
            this.ma_kh.Caption = "Mã KH";
            this.ma_kh.FieldName = "MaKH";
            this.ma_kh.Name = "ma_kh";
            // 
            // SoNgayThue
            // 
            this.SoNgayThue.Caption = "Số ngày thuê";
            this.SoNgayThue.FieldName = "SoNgayThue";
            this.SoNgayThue.Name = "SoNgayThue";
            this.SoNgayThue.Visible = true;
            this.SoNgayThue.VisibleIndex = 2;
            // 
            // NgayTT
            // 
            this.NgayTT.Caption = "Ngày thanh toán";
            this.NgayTT.FieldName = "NgayThanhToan";
            this.NgayTT.Name = "NgayTT";
            this.NgayTT.Visible = true;
            this.NgayTT.VisibleIndex = 3;
            // 
            // MaNV
            // 
            this.MaNV.Caption = "Người thu tiền";
            this.MaNV.FieldName = "MaNV";
            this.MaNV.Name = "MaNV";
            this.MaNV.Visible = true;
            this.MaNV.VisibleIndex = 5;
            // 
            // TongTien
            // 
            this.TongTien.Caption = "Tổng Tiền";
            this.TongTien.FieldName = "TongTien";
            this.TongTien.Name = "TongTien";
            this.TongTien.Visible = true;
            this.TongTien.VisibleIndex = 4;
            // 
            // DiaChi
            // 
            this.DiaChi.Caption = "gridColumn1";
            this.DiaChi.FieldName = "KhachHang.DiaChi";
            this.DiaChi.Name = "DiaChi";
            // 
            // SDT
            // 
            this.SDT.Caption = "gridColumn1";
            this.SDT.FieldName = "KhachHang.SDT";
            this.SDT.Name = "SDT";
            // 
            // CMND
            // 
            this.CMND.Caption = "gridColumn1";
            this.CMND.FieldName = "KhachHang.CMND";
            this.CMND.Name = "CMND";
            // 
            // btnXemChiTiet
            // 
            this.btnXemChiTiet.AutoHeight = false;
            this.btnXemChiTiet.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnXemChiTiet.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnXemChiTiet.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnXemChiTiet.Name = "btnXemChiTiet";
            this.btnXemChiTiet.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcHoaDon;
            this.gridView1.Name = "gridView1";
            // 
            // btnDong
            // 
            this.btnDong.Location = new System.Drawing.Point(149, 357);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(89, 23);
            this.btnDong.TabIndex = 23;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(149, 300);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(89, 23);
            this.btnIn.TabIndex = 24;
            this.btnIn.Text = "In hóa đơn";
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // btnLoadLai
            // 
            this.btnLoadLai.Location = new System.Drawing.Point(42, 300);
            this.btnLoadLai.Name = "btnLoadLai";
            this.btnLoadLai.Size = new System.Drawing.Size(89, 23);
            this.btnLoadLai.TabIndex = 25;
            this.btnLoadLai.Text = "Load lại";
            this.btnLoadLai.Click += new System.EventHandler(this.btnLoadLai_Click);
            // 
            // frmHoaDon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 481);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnIn);
            this.Controls.Add(this.btnLoadLai);
            this.Controls.Add(this.gcHoaDon);
            this.Controls.Add(this.groupControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmHoaDon";
            this.Text = "HÓA ĐƠN";
            this.Load += new System.EventHandler(this.frmHoaDon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnXemChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl lblSoDienThoai;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LabelControl lblDiaChi;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LabelControl lblTenKH;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl lblCMND;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl lblMaKH;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraGrid.GridControl gcHoaDon;
        private DevExpress.XtraGrid.Views.Grid.GridView gvHoaDon;
        private DevExpress.XtraGrid.Columns.GridColumn TenKH;
        private DevExpress.XtraGrid.Columns.GridColumn MaHD;
        private DevExpress.XtraGrid.Columns.GridColumn ma_kh;
        private DevExpress.XtraGrid.Columns.GridColumn SoNgayThue;
        private DevExpress.XtraGrid.Columns.GridColumn NgayTT;
        private DevExpress.XtraGrid.Columns.GridColumn MaNV;
        private DevExpress.XtraGrid.Columns.GridColumn TongTien;
        private DevExpress.XtraGrid.Columns.GridColumn DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn SDT;
        private DevExpress.XtraGrid.Columns.GridColumn CMND;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnXemChiTiet;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnIn;
        private DevExpress.XtraEditors.SimpleButton btnLoadLai;
    }
}