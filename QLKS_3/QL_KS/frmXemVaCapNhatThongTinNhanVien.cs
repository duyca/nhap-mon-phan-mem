﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using System.Text.RegularExpressions;
using log4net;
using NhapDoAn.BUS;
using DevExpress.XtraEditors;

namespace QL_KS
{
    public partial class frmXemVaCapNhatThongTinNhanVien : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmXemVaCapNhatThongTinNhanVien()
        {
            InitializeComponent();
        }
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmSaoLuu).Name);
        public string MaNV;
        public string LoaiNV;
        public string TaiKhoan;
        NhanVienBUS nvBUS = new NhanVienBUS();

        private void LoadDL()
        {
            NhanVien nv = nvBUS.LoadTheoMaNV(MaNV);
            txtMaNhanVien.Text = nv.MaNV.Trim();
            txtTaiKhoan.Text = nv.TaiKhoan.Trim();
            txtTenNV.Text = nv.TenNV.Trim();
            txtEmail.Text = nv.Email.Trim();
            txtSoDienThoai.Text = nv.SDT.Trim();
            txtDiaChi.Text = nv.DiaChi.Trim();
            txtCMND.Text = nv.CMND.Trim();
        }

        private void Reset()
        {
            txtTenNV.Text = "";
            txtDiaChi.Text = "";
            txtSoDienThoai.Text = "";
            txtEmail.Text = "";
            txtCMND.Text = "";
        }

        private void ResetLabel()
        {
            lbTenNhanVien.Text = "";
            lbEmail.Text = "";
            lbSDT.Text = "";
            lbDiaChi.Text = "";
            lbCMND.Text = "";
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            bool isEmail = Regex.IsMatch(txtEmail.Text.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            ResetLabel();
            try
            {
                if (txtTenNV.Text == string.Empty || txtEmail.Text == string.Empty || isEmail == false || txtSoDienThoai.Text == string.Empty
                    || txtDiaChi.Text == string.Empty || txtCMND.Text == string.Empty)
                {
                    lblThongBao.Text = "Vui lòng nhập đầy đủ các thông tin";
                    lblThongBao.ForeColor = Color.Red;
                }
                if (txtTenNV.Text == string.Empty)
                {
                    lblThongBao.Text = "Vui lòng nhập tên nhân viên";
                    lblThongBao.ForeColor = Color.Red;
                    lbTenNhanVien.Text = "*";
                    lbTenNhanVien.ForeColor = Color.Red;
                    txtTenNV.Focus();
                    return;
                }
                if (txtEmail.Text == string.Empty)
                {
                    lblThongBao.Text = "Vui lòng nhập email";
                    lblThongBao.ForeColor = Color.Red;
                    lbEmail.Text = "*";
                    lbEmail.ForeColor = Color.Red;
                    txtEmail.Focus();
                    return;
                }
                if (isEmail == false)
                {
                    lblThongBao.Text = "Email không hợp lệ";
                    lblThongBao.ForeColor = Color.Red;
                    lbEmail.Text = "*";
                    lbEmail.ForeColor = Color.Red;
                    txtEmail.Focus();
                    return;
                }
                if (txtSoDienThoai.Text == string.Empty)
                {
                    lblThongBao.Text = "Vui lòng nhập số điện thoại";
                    lblThongBao.ForeColor = Color.Red;
                    lbSDT.Text = "*";
                    lbSDT.ForeColor = Color.Red;
                    txtSoDienThoai.Focus();
                    return;
                }
                if (txtDiaChi.Text == string.Empty)
                {
                    lblThongBao.Text = "Vui lòng nhập địa chỉ";
                    lblThongBao.ForeColor = Color.Red;
                    lbDiaChi.Text = "*";
                    lbDiaChi.ForeColor = Color.Red;
                    txtDiaChi.Focus();
                    return;
                }
                if (txtCMND.Text == string.Empty)
                {
                    lblThongBao.Text = "Vui lòng nhập số CMND";
                    lblThongBao.ForeColor = Color.Red;
                    lbCMND.Text = "*";
                    lbCMND.ForeColor = Color.Red;
                    txtCMND.Focus();
                    return;
                }
                else
                {
                    NhanVien nv = new NhanVien();
                    nv.MaNV = MaNV;
                    nv.TaiKhoan = TaiKhoan;
                    nv.TenNV = txtTenNV.Text.Trim();
                    nv.Email = txtEmail.Text.Trim();
                    nv.SDT = txtSoDienThoai.Text.Trim();
                    nv.DiaChi = txtDiaChi.Text.Trim();
                    nv.CMND = txtCMND.Text.Trim();
                    nv.ConQuanLy = true;

                    bool result = nvBUS.CapNhatThongTin(nv);
                    if (result == true)
                    {
                        XtraMessageBox.Show("Cập nhật thông tin nhân viên thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                       
                        LoadDL();
                        LuuNhatKyHT.Them(TaiKhoan, "Hệ thống", "Cập nhật thông tin cá nhân");
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                _logger.Error(ex.Message);
            }
        }

        private void frmXemVaCapNhatThongTinNhanVien_Load(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
            ResetLabel();
        }

        private void txtSoDienThoai_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtCMND_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}