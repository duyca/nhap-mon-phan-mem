﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using QL_KS.DAO;

namespace QL_KS
{
    public partial class frmDangNhap : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmDangNhap()
        {
            InitializeComponent();
        }
        NhanVienBUS nvBUS = new NhanVienBUS();
        List<NhanVien> list_nv = new List<NhanVien>();
        NhanVien nvDTO;
        public frmMain frm;
        private void lbQuenMatKhau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmQuenMatKhau quen = new frmQuenMatKhau();
            this.Hide();
            quen.ShowDialog();
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (txtMatKhau.Text == string.Empty && txtTenDangNhap.Text == string.Empty)
            {
                lbThongBao.Text = "Tên tài khoản và mật khẩu không được bỏ trống";
                errorProvider1.SetError(txtTenDangNhap, "Bạn chưa nhập tên đăng nhập");
                errorProvider2.SetError(txtMatKhau, "Bạn chưa nhập mật khẩu");
                lbThongBao.ForeColor = Color.Red;
            }
            else if (txtMatKhau.Text == string.Empty || txtTenDangNhap.Text == string.Empty)
            {
                lbThongBao.Text = "Tên tài khoản hoặc mật khẩu không được bỏ trống";
                lbThongBao.ForeColor = Color.Red;
            }
            else
            {
                nvDTO = new NhanVien();
                foreach (NhanVien nv in list_nv)
                {
                    if (nv.TaiKhoan.Trim() == txtTenDangNhap.Text && nv.ConQuanLy == true)
                    {
                        nvDTO = nv;
                        break;
                    }
                }
                if (nvDTO.TaiKhoan != null)
                {
                    if (MaHoaMD5.md5(txtMatKhau.Text) == nvDTO.MatKhau.Trim())
                    {
                        lbThongBao.Text = "";
                        frm = new frmMain();
                        frm.MaNV = nvDTO.MaNV;
                        frm.TaiKhoan = nvDTO.TaiKhoan;
                        frm.LoaiNV = nvDTO.LoaiNV;
                        frm.TenNV = nvDTO.TenNV;
                        this.Hide();
                        LuuNhatKyHT.Them(frm.TaiKhoan, "Hệ thống", "Đăng nhập");
                        frm.ShowDialog();
                    }
                    else
                    {
                        lbThongBao.Text = "Thông tin đăng nhập không chính xác";
                        lbThongBao.ForeColor = Color.Red;
                    }
                }
                else
                {
                    lbThongBao.Text = "Tài khoản không tồn tại trong hệ thống";
                    errorProvider1.SetError(txtTenDangNhap, "Tài khoản không tồn tại");
                    lbThongBao.ForeColor = Color.Red;
                }
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmDangNhap_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnDangNhap_Click(sender, e);
        }

        private void frmDangNhap_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            list_nv = nvBUS.LoadDS();
        
        }

        private void txtMatKhau_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnDangNhap_Click(sender, e);
        }

        private void txtTenDangNhap_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtTenDangNhap.Text))
            {
                errorProvider1.SetError(txtTenDangNhap, "Bạn chưa nhập tên đăng nhập");
            }
            else
            {
                errorProvider1.Clear();
            }
        }

        private void txtMatKhau_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMatKhau.Text))
            {
                errorProvider2.SetError(txtMatKhau, "Bạn chưa nhập mật khẩu");
            }
            else
            {
                errorProvider2.Clear();
            }
        }
    }
}