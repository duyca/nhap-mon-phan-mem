﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;

namespace QL_KS
{
    public partial class frmThemMoiKH : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmThemMoiKH()
        {
            InitializeComponent();
        }

        private void frmThemMoiKH_Load(object sender, EventArgs e)
        {
            cbbLoaiKH.DataSource = KhachHangBus.LoadKH();
            cbbLoaiKH.DisplayMember = "TenLoaiKH";
            cbbLoaiKH.ValueMember = "MaLoaiKH";

            txtMaKH.Text = KhachHangBus.ID();
            btnNew.Enabled = false;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {

            if (txtHoTen.Text == "" || txtCMND.Text == "")
            {
                MessageBox.Show("Nhập đầy đủ các thông tin bắt buộc.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else if (txtHoTen.Text != "" && txtCMND.Text != "")
            {
                KhachHang kh = new KhachHang();
                kh.MaKH = txtMaKH.Text;
                kh.TenKH = txtHoTen.Text;
                kh.CMND = txtCMND.Text;
                kh.SDT = txtSDT.Text;
                kh.DiaChi = mmDC.Text;
                kh.LoaiKH = Convert.ToInt32(KhachHangBus.TimLoaiKH(cbbLoaiKH.Text));

                bool kq = KhachHangBus.Insert(kh);
                if (kq == true)
                {
                    MessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtHoTen.Enabled = false;
                    txtCMND.Enabled = false;
                    txtSDT.Enabled = false;
                    mmDC.Enabled = false;

                    btnNew.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Lưu thất bại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void txtCMND_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtSDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            txtMaKH.Text = KhachHangBus.ID();
            txtHoTen.Text = "";
            txtCMND.Text = "";
            txtSDT.Text = "";
            mmDC.Text = "";

            btnNew.Enabled = false;
        }
    }
}