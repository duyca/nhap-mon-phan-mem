﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using NhapDoAn.BUS;
using System.Data.SqlClient;
using log4net;
using QL_KS.BUS;

namespace QL_KS
{
    public partial class frmSaoLuu : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmSaoLuu()
        {
            InitializeComponent();
        }
        public string TaiKhoan;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmSaoLuu).Name);
        QLKhachSanEntities1 data = new QLKhachSanEntities1();
       
        int i = 0;

        private void btnDuongDan_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folder = new FolderBrowserDialog();
            if (folder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtDuongDan.Text = folder.SelectedPath;
            }
        }
        private void frmSaoLuu_Load(object sender, EventArgs e)
        {
            txtTenTapTin.Text = "QLKhachSan.bak";
            progressBar1.Maximum = 100;
            progressBar1.Minimum = 0;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(10);
            i += 10;
            lblPhanTram.Text = i.ToString() + "%";
            if (progressBar1.Value == 100)
            {
                timer.Stop();
                MessageBox.Show("Đã sao lưu cơ sở dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void btnThayDoi_Click(object sender, EventArgs e)
        {
            if (txtDuongDan.Text == string.Empty)
            {
                lblErrorDuongDan.Text = "Bạn vui lòng chọn đường dẫn";
                lblErrorDuongDan.ForeColor = Color.Red;
            }
            else
            {
                try
                {
                    string path = txtDuongDan.Text + "\\" + txtTenTapTin.Text;
                    string sqlBackup = string.Format("BACKUP DATABASE [QLKhachSan] TO DISK='{0}' WITH INIT", path);
                    SqlConnection con = new SqlConnection(@"Data Source=USER-VAIO\SQLEXPRESS;Initial Catalog=QLKhachSan;Integrated Security=True");
                    con.Open();
                    SqlCommand cmd = new SqlCommand(sqlBackup, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    timer.Start();
                    // ghi lại nhật ký hoạt động
                    LuuNhatKyHT.Them(TaiKhoan, "Cơ sở dữ liệu", "Sao lưu dữ liệu");
                    data.SaveChanges();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Backup Database");
                    data.SaveChanges();
                    _logger.Error(ex.Message);
                    return;
                }
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}