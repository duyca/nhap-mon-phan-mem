﻿namespace QL_KS
{
    partial class frmSuaPhieuThue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbGHG = new DevExpress.XtraEditors.LabelControl();
            this.dtngdi = new System.Windows.Forms.DateTimePicker();
            this.dtngayden = new System.Windows.Forms.DateTimePicker();
            this.cbxKhachHang = new DevExpress.XtraEditors.LookUpEdit();
            this.cbxsoluong = new System.Windows.Forms.ComboBox();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cbxKhachHang.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(16, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 17);
            this.label8.TabIndex = 91;
            this.label8.Text = "Số điện thoại:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 17);
            this.label6.TabIndex = 90;
            this.label6.Text = "CMND:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 17);
            this.label5.TabIndex = 89;
            this.label5.Text = "Email:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 17);
            this.label3.TabIndex = 88;
            this.label3.Text = "Tên nhân viên:";
            // 
            // lbGHG
            // 
            this.lbGHG.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGHG.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbGHG.Appearance.Options.UseFont = true;
            this.lbGHG.Appearance.Options.UseForeColor = true;
            this.lbGHG.Location = new System.Drawing.Point(127, 134);
            this.lbGHG.Name = "lbGHG";
            this.lbGHG.Size = new System.Drawing.Size(0, 14);
            this.lbGHG.TabIndex = 98;
            // 
            // dtngdi
            // 
            this.dtngdi.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtngdi.Location = new System.Drawing.Point(127, 167);
            this.dtngdi.Name = "dtngdi";
            this.dtngdi.Size = new System.Drawing.Size(191, 22);
            this.dtngdi.TabIndex = 95;
            // 
            // dtngayden
            // 
            this.dtngayden.Enabled = false;
            this.dtngayden.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtngayden.Location = new System.Drawing.Point(127, 98);
            this.dtngayden.Name = "dtngayden";
            this.dtngayden.Size = new System.Drawing.Size(191, 22);
            this.dtngayden.TabIndex = 94;
            // 
            // cbxKhachHang
            // 
            this.cbxKhachHang.Location = new System.Drawing.Point(127, 26);
            this.cbxKhachHang.Name = "cbxKhachHang";
            this.cbxKhachHang.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cbxKhachHang.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxKhachHang.Properties.Appearance.Options.UseBackColor = true;
            this.cbxKhachHang.Properties.Appearance.Options.UseFont = true;
            this.cbxKhachHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxKhachHang.Properties.DisplayMember = "TenKH";
            this.cbxKhachHang.Properties.NullText = "";
            this.cbxKhachHang.Properties.ValueMember = "MaKH";
            this.cbxKhachHang.Size = new System.Drawing.Size(191, 20);
            this.cbxKhachHang.TabIndex = 93;
            // 
            // cbxsoluong
            // 
            this.cbxsoluong.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxsoluong.FormattingEnabled = true;
            this.cbxsoluong.Location = new System.Drawing.Point(127, 62);
            this.cbxsoluong.Name = "cbxsoluong";
            this.cbxsoluong.Size = new System.Drawing.Size(191, 21);
            this.cbxsoluong.TabIndex = 92;
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(138, 212);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(89, 23);
            this.btnCapNhat.TabIndex = 110;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnDong
            // 
            this.btnDong.Location = new System.Drawing.Point(242, 212);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(76, 23);
            this.btnDong.TabIndex = 109;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // frmSuaPhieuThue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 258);
            this.Controls.Add(this.btnCapNhat);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.lbGHG);
            this.Controls.Add(this.dtngdi);
            this.Controls.Add(this.dtngayden);
            this.Controls.Add(this.cbxKhachHang);
            this.Controls.Add(this.cbxsoluong);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmSuaPhieuThue";
            this.Text = "CẬP NHẬT PHIẾU THUÊ";
            this.Load += new System.EventHandler(this.frmSuaPhieuThue_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbxKhachHang.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LabelControl lbGHG;
        private System.Windows.Forms.DateTimePicker dtngdi;
        private System.Windows.Forms.DateTimePicker dtngayden;
        private DevExpress.XtraEditors.LookUpEdit cbxKhachHang;
        private System.Windows.Forms.ComboBox cbxsoluong;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private DevExpress.XtraEditors.SimpleButton btnDong;
    }
}