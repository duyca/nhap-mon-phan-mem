﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QL_KS.BUS;

namespace QL_KS
{
    public partial class frmThemSuaHinhThuc : Form
    {
        Hinh_Thuc_BUS bhinh_thuc = new Hinh_Thuc_BUS();
        public string frmMaHinhThuc;
        public frmThemSuaHinhThuc()
        {
            InitializeComponent();
        }

        private void rbKeoDai_CheckedChanged(object sender, EventArgs e)
        {
            if(rbKeoDai.Checked == true)
            {
                txtGioKeoDai.Enabled = true;
                rbGioKetThuc.Checked = false;
                txtGioKetThuc.Enabled = false;
            }
        }

        private void rbGioKetThuc_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGioKetThuc.Checked == true)
            {
                txtGioKetThuc.Enabled = true;
                rbKeoDai.Checked = false;
                txtGioKeoDai.Enabled = false;
            }
        }

        private void frmThemSuaHinhThuc_Load(object sender, EventArgs e)
        {
            if (rbGioKetThuc.Checked == true)
            {
                txtGioKetThuc.Enabled = true;
                rbKeoDai.Checked = false;
                txtGioKeoDai.Enabled = false;
                txtGioKeoDai.Text = "";
            }
            if (rbKeoDai.Checked == true)
            {
                txtGioKeoDai.Enabled = true;
                rbGioKetThuc.Checked = false;
                txtGioKetThuc.Enabled = false;
                txtGioKetThuc.Text = "";
            }
            this.Text = "Thêm Hình Thức";
            if (frmMaHinhThuc !=null)
            {
                btnTraPhong.Text = "Lưu";
                this.Text = "Sửa Hình Thức";
                HinhThuc hinhthuctam = bhinh_thuc.Tim_Hinh_Thuc(frmMaHinhThuc);
                txtMaLoaiPhong.Text = hinhthuctam.MaHinhThuc;
                txtTenHinhThuc.Text = hinhthuctam.TenHinhThuc;
                txtGioKeoDai.Text = hinhthuctam.ThoiGianKeoDai.ToString();
                txtGioKetThuc.Text = hinhthuctam.GioKetThuc.ToString();
                if (txtGioKeoDai.Text != "")
                {
                    rbKeoDai.Checked = true;
                    txtGioKeoDai.Enabled = true;
                    rbGioKetThuc.Checked = false;
                    txtGioKetThuc.Enabled = false;
                }
                else
                {
                    rbKeoDai.Checked = false;
                    txtGioKeoDai.Enabled = false;
                    rbGioKetThuc.Checked = true;
                    txtGioKetThuc.Enabled = true;
                }
            }
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnTraPhong_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (txtTenHinhThuc.Text == string.Empty)
            {
                error++;
                lbThongBao_Ten.Visible = true;
            }
            else
            {
                lbThongBao_Ten.Visible = false;
            }
            if (txtGioKeoDai.Text == string.Empty && txtGioKetThuc.Text == string.Empty)
            {
                error++;
                lbThongBao_ThoiGian.Visible = true;
            }
            else
            {
                lbThongBao_ThoiGian.Visible = false;
            }
            if(error!=0)
            {
                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
            else
            {
                HinhThuc Hinh_Thuc_Moi = new HinhThuc();
                if(frmMaHinhThuc == null)
                {
                    do
                    {
                        Hinh_Thuc_Moi.MaHinhThuc = RandomString(10);
                    }
                    while (bhinh_thuc.Tim_Hinh_Thuc(Hinh_Thuc_Moi.MaHinhThuc) != null);
                }
                else
                {
                    Hinh_Thuc_Moi.MaHinhThuc = frmMaHinhThuc;
                }
                Hinh_Thuc_Moi.TenHinhThuc = txtTenHinhThuc.Text;
                if(rbGioKetThuc.Checked)
                {
                    Hinh_Thuc_Moi.GioKetThuc = Convert.ToInt32(txtGioKetThuc.Text);
                    Hinh_Thuc_Moi.ThoiGianKeoDai = null;
                }
                else
                {
                    Hinh_Thuc_Moi.ThoiGianKeoDai = Convert.ToInt32(txtGioKeoDai.Text);
                    Hinh_Thuc_Moi.GioKetThuc = null;
                }
                if (frmMaHinhThuc == null)
                {
                    bhinh_thuc.Them_Hinh_Thuc(Hinh_Thuc_Moi);
                    MessageBox.Show("Thêm hình thức thành công");
                }
                else
                {
                    bhinh_thuc.Sua_Hinh_Thuc(Hinh_Thuc_Moi);
                    MessageBox.Show("Sửa hình thức thành công");
                }
                Close();
            }
        }
        private string RandomString(int size)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 87)));
                sb.Append(c);
            }
            return sb.ToString();

        }
    }

}
