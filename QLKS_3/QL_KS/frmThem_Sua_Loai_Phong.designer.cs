﻿namespace QL_KS
{
    partial class frmThem_Sua_Loai_Phong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbThongBao_TienQuaGio = new System.Windows.Forms.Label();
            this.lbThongBao_Ten = new System.Windows.Forms.Label();
            this.txtTienQuaGio = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenLoaiPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.pnwapDongia = new System.Windows.Forms.Panel();
            this.fllKetiep = new System.Windows.Forms.FlowLayoutPanel();
            this.fllDonGia = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaLoaiPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnHuyBo = new System.Windows.Forms.Button();
            this.btnTraPhong = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pnwapDongia.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.lbThongBao_TienQuaGio);
            this.panel1.Controls.Add(this.lbThongBao_Ten);
            this.panel1.Controls.Add(this.txtTienQuaGio);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtTenLoaiPhong);
            this.panel1.Controls.Add(this.pnwapDongia);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtMaLoaiPhong);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 70);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(689, 300);
            this.panel1.TabIndex = 1;
            // 
            // lbThongBao_TienQuaGio
            // 
            this.lbThongBao_TienQuaGio.AutoSize = true;
            this.lbThongBao_TienQuaGio.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_TienQuaGio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_TienQuaGio.Location = new System.Drawing.Point(103, 67);
            this.lbThongBao_TienQuaGio.Name = "lbThongBao_TienQuaGio";
            this.lbThongBao_TienQuaGio.Size = new System.Drawing.Size(139, 16);
            this.lbThongBao_TienQuaGio.TabIndex = 48;
            this.lbThongBao_TienQuaGio.Text = "Hãy nhập vào tiền quá giờ";
            this.lbThongBao_TienQuaGio.Visible = false;
            // 
            // lbThongBao_Ten
            // 
            this.lbThongBao_Ten.AutoSize = true;
            this.lbThongBao_Ten.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_Ten.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_Ten.Location = new System.Drawing.Point(452, 22);
            this.lbThongBao_Ten.Name = "lbThongBao_Ten";
            this.lbThongBao_Ten.Size = new System.Drawing.Size(149, 16);
            this.lbThongBao_Ten.TabIndex = 47;
            this.lbThongBao_Ten.Text = "Hãy nhập vào tên loại phòng";
            this.lbThongBao_Ten.Visible = false;
            // 
            // txtTienQuaGio
            // 
            // 
            // 
            // 
            this.txtTienQuaGio.BackgroundStyle.Class = "TextBoxBorder";
            this.txtTienQuaGio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTienQuaGio.ButtonClear.Visible = true;
            this.txtTienQuaGio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienQuaGio.Location = new System.Drawing.Point(106, 86);
            this.txtTienQuaGio.Mask = "0000000";
            this.txtTienQuaGio.Name = "txtTienQuaGio";
            this.txtTienQuaGio.Size = new System.Drawing.Size(230, 20);
            this.txtTienQuaGio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtTienQuaGio.TabIndex = 46;
            this.txtTienQuaGio.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label8.Location = new System.Drawing.Point(299, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 24);
            this.label8.TabIndex = 25;
            this.label8.Text = "Đơn giá";
            // 
            // txtTenLoaiPhong
            // 
            // 
            // 
            // 
            this.txtTenLoaiPhong.Border.Class = "TextBoxBorder";
            this.txtTenLoaiPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenLoaiPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenLoaiPhong.Location = new System.Drawing.Point(452, 41);
            this.txtTenLoaiPhong.MaxLength = 30;
            this.txtTenLoaiPhong.Name = "txtTenLoaiPhong";
            this.txtTenLoaiPhong.PreventEnterBeep = true;
            this.txtTenLoaiPhong.Size = new System.Drawing.Size(230, 22);
            this.txtTenLoaiPhong.TabIndex = 19;
            // 
            // pnwapDongia
            // 
            this.pnwapDongia.AutoScroll = true;
            this.pnwapDongia.Controls.Add(this.fllKetiep);
            this.pnwapDongia.Controls.Add(this.fllDonGia);
            this.pnwapDongia.Location = new System.Drawing.Point(0, 161);
            this.pnwapDongia.Name = "pnwapDongia";
            this.pnwapDongia.Size = new System.Drawing.Size(689, 125);
            this.pnwapDongia.TabIndex = 17;
            // 
            // fllKetiep
            // 
            this.fllKetiep.Location = new System.Drawing.Point(349, 12);
            this.fllKetiep.Name = "fllKetiep";
            this.fllKetiep.Size = new System.Drawing.Size(338, 46);
            this.fllKetiep.TabIndex = 1;
            // 
            // fllDonGia
            // 
            this.fllDonGia.Location = new System.Drawing.Point(3, 12);
            this.fllDonGia.Name = "fllDonGia";
            this.fllDonGia.Size = new System.Drawing.Size(342, 46);
            this.fllDonGia.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "Tiền Quá Giờ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(352, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tên Loại Phòng:";
            // 
            // txtMaLoaiPhong
            // 
            // 
            // 
            // 
            this.txtMaLoaiPhong.Border.Class = "TextBoxBorder";
            this.txtMaLoaiPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaLoaiPhong.Enabled = false;
            this.txtMaLoaiPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLoaiPhong.Location = new System.Drawing.Point(106, 40);
            this.txtMaLoaiPhong.Name = "txtMaLoaiPhong";
            this.txtMaLoaiPhong.PreventEnterBeep = true;
            this.txtMaLoaiPhong.Size = new System.Drawing.Size(230, 22);
            this.txtMaLoaiPhong.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mã Loại Phòng:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label6.Location = new System.Drawing.Point(239, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(217, 22);
            this.label6.TabIndex = 15;
            this.label6.Text = "Thông Tin Loại Phòng";
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnHuyBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHuyBo.FlatAppearance.BorderSize = 0;
            this.btnHuyBo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHuyBo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.ForeColor = System.Drawing.Color.Crimson;
            this.btnHuyBo.Location = new System.Drawing.Point(572, 397);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(110, 40);
            this.btnHuyBo.TabIndex = 17;
            this.btnHuyBo.Text = "Hủy Bỏ";
            this.btnHuyBo.UseVisualStyleBackColor = false;
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnTraPhong
            // 
            this.btnTraPhong.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnTraPhong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTraPhong.FlatAppearance.BorderSize = 0;
            this.btnTraPhong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraPhong.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraPhong.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.btnTraPhong.Location = new System.Drawing.Point(456, 397);
            this.btnTraPhong.Name = "btnTraPhong";
            this.btnTraPhong.Size = new System.Drawing.Size(110, 40);
            this.btnTraPhong.TabIndex = 16;
            this.btnTraPhong.Text = "Thêm Mới";
            this.btnTraPhong.UseVisualStyleBackColor = false;
            this.btnTraPhong.Click += new System.EventHandler(this.btnTraPhong_Click);
            // 
            // frmThem_Sua_Loai_Phong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 449);
            this.Controls.Add(this.btnHuyBo);
            this.Controls.Add(this.btnTraPhong);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Name = "frmThem_Sua_Loai_Phong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmThem_Sua_Loai_Phong";
            this.Load += new System.EventHandler(this.frmThem_Sua_Loai_Phong_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnwapDongia.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaLoaiPhong;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnwapDongia;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenLoaiPhong;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FlowLayoutPanel fllKetiep;
        private System.Windows.Forms.FlowLayoutPanel fllDonGia;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtTienQuaGio;
        private System.Windows.Forms.Label lbThongBao_Ten;
        private System.Windows.Forms.Label lbThongBao_TienQuaGio;
        private System.Windows.Forms.Button btnHuyBo;
        private System.Windows.Forms.Button btnTraPhong;
    }
}