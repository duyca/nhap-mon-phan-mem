﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;

namespace QL_KS
{
    public partial class frmTimKiemKH : DevComponents.DotNetBar.Metro.MetroForm
    {

        public frmTimKiemKH()
        {
            InitializeComponent();
        }

        public static string id = "";

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmTimKiemKH_Load(object sender, EventArgs e)
        {
           
        }

        private void LoadKH()
        {
            var ds = KhachHangBus.LoadDS();
            gcDSKH.DataSource = ds;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtHoTen.Text == "" && txtCMND.Text == "")
            {
                LoadKH();
            }

            if (txtHoTen.Text != "" && txtCMND.Text == "")
            {
                gcDSKH.DataSource = KhachHangBus.SearchName(txtHoTen.Text);
                
            }

            if (txtHoTen.Text == "" && txtCMND.Text != "")
            {
                gcDSKH.DataSource = KhachHangBus.SearchCMND(txtCMND.Text);
            }

            if (txtHoTen.Text != "" && txtCMND.Text != "")
            {
                gcDSKH.DataSource = KhachHangBus.SearchAll(txtHoTen.Text,txtCMND.Text);
            }
        }

        private void txtCMND_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }
            
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            id = gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString();
   
            var frm = new frmCapNhatKH(id);
            frm.Show();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            id = gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString();

            bool kq = KhachHangBus.Delete(id);
            if (kq == true)
            {
                MessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                LoadKH();
            }
            else
            {
                MessageBox.Show("Khách hàng đã phát sinh hóa đơn, không thể xóa.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtHoTen.Text = "";
            txtCMND.Text = "";

            LoadKH();
        }
    }
}