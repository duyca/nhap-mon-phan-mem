﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QL_KS.BUS;
using DevComponents.DotNetBar;


namespace QL_KS
{
    public partial class frmPhieuThuePhong : DevExpress.XtraEditors.XtraForm
    {
        public string frmMaNV;
        Hinh_Thuc_BUS bhinhthuc = new Hinh_Thuc_BUS();
        Loai_Khach_HangBUS bloaikhach = new Loai_Khach_HangBUS();
        KhachHangBus bkhach = new KhachHangBus();
        PhieuThueBUS bphieu = new PhieuThueBUS();
        Phong_BUS bphong = new Phong_BUS();
        public string frmMaPhong;
        public frmPhieuThuePhong()
        {
            InitializeComponent();
        }
        private void frmPhieuThuePhong_Load(object sender, EventArgs e)
        {
            foreach (HinhThuc ht in bhinhthuc.Load_DS_Hinh_Thuc())
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Text = ht.TenHinhThuc;
                item.Name = ht.MaHinhThuc;
                cbbHinhThucThue.Items.Add(item);
                cbbHinhThucThue.SelectedIndex = 0;
            }
            foreach (LoaiKhachHang lk in bloaikhach.Load_DS_Loai_Khach())
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Text = lk.TenLoaiKH;
                item.Name = lk.MaLoaiKH.ToString();
                cbbLoaiKhachHang.Items.Add(item);
                cbbLoaiKhachHang.SelectedIndex = 0;
            }
            txtNgayThue.Text = DateTime.Now.ToString();
        }
        private void btnThuePhong_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (txtTenKhachHang.Text == string.Empty)
            {
                lbThongBaoTen.Visible = true;
                error++;
            }
            else
                lbThongBaoTen.Visible = false;
            if (txtSoNguoi.Text == string.Empty)
            {
                lbThongBaoSoNguoi.Text = "Hãy nhập vào số người:";
                lbThongBaoSoNguoi.Visible = true;
                error++;
            }
            else
            {
                if (bphong.Tim_Phong(frmMaPhong).SoNguoiToiDa < Convert.ToInt32(txtSoNguoi.Text))
                {
                    lbThongBaoSoNguoi.Text = "Số người vượt quá cho phép";
                    lbThongBaoSoNguoi.Visible = true;
                    error++;
                }
                else
                {
                    lbThongBaoSoNguoi.Visible = false;
                }
            }
            //----------------Khong loi--------------------//
            if (error == 0)
            {
                try
                {
                    PhieuThue phieumoi = new PhieuThue();
                    KhachHang khachmoi = new KhachHang();

                    do
                    {
                        khachmoi.MaKH = RandomString(10);
                    }
                    while (bkhach.TimKiemkhachHang(khachmoi.MaKH) != null);
                    
                    khachmoi.TenKH = txtTenKhachHang.Text.ToString();
                    khachmoi.LoaiKH = Convert.ToInt32((cbbLoaiKhachHang.SelectedItem as ComboBoxItem).Name);
                    khachmoi.DiaChi = txtDiaChi.Text;
                    khachmoi.SDT = txtSoDT.Text;
                    khachmoi.CMND = txtCMND.Text;
                    do
                    {
                        phieumoi.MaPhieuThue = RandomString(10);
                    }
                    while (bphieu.Tim_Phieu_Thue_Theo_Ma(phieumoi.MaPhieuThue) != null);
                    
                    phieumoi.MaHinhThuc = (cbbHinhThucThue.SelectedItem as ComboBoxItem).Name;
                    phieumoi.SoNguoiThue = Convert.ToInt32(txtSoNguoi.Text);
                    phieumoi.NgayBatDauThue = DateTime.Now;
                    phieumoi.MaPhong = frmMaPhong.ToString();
                    phieumoi.MaKH = khachmoi.MaKH.ToString();
                    phieumoi.NgayTraPhong = null;
                    phieumoi.TienChuyenPhong = 0;
                    bphong.Cap_Nhat_Tinh_Trang("MTT0000001", frmMaPhong);
                    bkhach.Them_Khach_Hang(khachmoi);
                    bphieu.Them_Phieu_Thue(phieumoi);
                    Close();
                }
                catch (Exception)
                {
                    Console.WriteLine("wtf");
                }

            }
            else
            {
                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private string RandomString(int size)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 87)));
                sb.Append(c);
            }
            return sb.ToString();

        }
    }
}