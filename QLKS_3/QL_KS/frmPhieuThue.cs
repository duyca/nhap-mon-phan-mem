﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using log4net;
using QL_KS.BUS;
using DevExpress.XtraEditors;

namespace QL_KS
{
    public partial class frmPhieuThue : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmPhieuThue()
        {
            InitializeComponent();
        }
        public string TaiKhoan;
        public string ganhoten;
        public string chucvu;
        public string MaNV;
        public static string TenP, NgayDen, NgayDi;
        public static int  SLTD, idex;

        public static int checkChuyenPhong ;
        public static string Maphieuthue, MaP, maphongdachuyen;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmPhieuThue).Name);
      
        void Lay()
        {
            gcphieuThue.DataSource = PhieuThueBUS.getTable();
            cbxKhachHang.Properties.DataSource = KhachHangBus.getTable();
            cbxKhachHang.Properties.DisplayMember = "TenKH";
            cbxKhachHang.Properties.ValueMember = "MaKH";
            cbxKhachHang.ItemIndex = 0;
        }
       
        public void LoadDL()
        {
            Lay();
            lbHoTen.Text = ganhoten;
            lbChucVu.Text = chucvu;
            lbTenPhong.Text = TenP;
            txtNgayDen.Text = NgayDen;
            txtNgayDi.Text = NgayDi;
            List<int> sl = new List<int>();
            for (int i = 1; i <= SLTD; i++)
            {
                sl.Add(i);
            }
            cbxsoluong.DataSource = sl;
        }
        private void frmPhieuThue_Load(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void ResetDl()
        {
            MaP = "";
            TenP = "";
            SLTD = 0;
            NgayDen = "";
            NgayDi = "";
            checkChuyenPhong = 0;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            ResetDl();
            this.Close();
        }
        PhieuThueBUS busphieuthue = new PhieuThueBUS();
        Phong_BUS busphong = new Phong_BUS();
        private void btnThuePhong_Click(object sender, EventArgs e)
        {
            if (lbTenPhong.Text != string.Empty)
            {
                Phong p = new Phong();
                if (checkChuyenPhong != 0)
                {
                    XtraMessageBox.Show("Vui lòng thanh toán trước khi chuyển phòng!", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    frmPTraPhong.checkchuyenphong = 1;
                    btnTraPhong_Click(sender, e);
                    if (frmPTraPhong.checkchuyenphong == 0)
                    {
                        PhieuThue phieuthue = new PhieuThue();
                        phieuthue.MaPhieuThue = Maphieuthue;
                        phieuthue.MaPhong = MaP;
                        phieuthue.NgayBatDauThue = DateTime.Parse(txtNgayDen.Text);
                        if (NgayDi != "")
                            phieuthue.NgayTraPhong = DateTime.Parse(txtNgayDi.Text);
                        else
                            phieuthue.NgayTraPhong = null;
                        phieuthue.MaKH = cbxKhachHang.EditValue.ToString();
                        phieuthue.SoNguoiThue = Convert.ToInt32(cbxsoluong.SelectedValue);
                        p.MaPhong = phieuthue.MaPhong;
                        p.TinhTrang = "MTT0000001";
                        phieuthue.MaPhieuThue = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhieuThue").ToString();
                        busphieuthue.Cap_Nhat_Phieu_Thue(phieuthue);
                        if (PhieuThueBUS.LoadPhieuTheoMP(maphongdachuyen).Count == 0)
                        {
                            busphong.Cap_Nhat_Tinh_Trang(maphongdachuyen, "MTT0000002");
                        }
                        Phong_BUS.CapNhatPhongTheoTrangThai1(p);
                        XtraMessageBox.Show("Chuyển phòng cho khách thành công", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        LuuNhatKyHT.Them(TaiKhoan, "Thuê Phòng", "Chuyển Phòng" + maphongdachuyen.ToString() + " sang phòng " + p.MaPhong.ToString());
                        lbTenPhong.Text = "";
                        txtNgayDen.Text = "";
                        txtNgayDi.Text = "";
                        Lay();
                        checkChuyenPhong = 0;
                    }
                    else
                    {
                        lbTenPhong.Text = "";
                        txtNgayDen.Text = "";
                        txtNgayDi.Text = "";
                        checkChuyenPhong = 0;

                    }
                }
                else
                {
                    try
                    {
                        
                        PhieuThue ph = new PhieuThue();
                        ph.MaPhong = MaP;
                        ph.NgayBatDauThue = DateTime.Parse(txtNgayDen.Text);
                        if (NgayDi != "")
                            ph.NgayTraPhong = DateTime.Parse(txtNgayDi.Text);
                        else
                            ph.NgayTraPhong = null;
                        ph.MaKH = cbxKhachHang.EditValue.ToString();
                        ph.SoNguoiThue = Convert.ToInt32(cbxsoluong.SelectedValue);
                        ph.TienChuyenPhong = 0;
                        p.MaPhong = ph.MaPhong;
                        p.TinhTrang = "MTT0000001";

                        busphieuthue.Them_Phieu_Thue(ph);
                        Phong_BUS.CapNhatPhongTheoTrangThai1(p);
                        XtraMessageBox.Show("Thuê phòng thành công", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        lbTenPhong.Text = "";
                        txtNgayDen.Text = "";
                        txtNgayDi.Text = "";
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show("Thuê phòng không thành công !", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        _logger.Error(ex.ToString());
                    }
                    Lay();
                }
            }
            else
                XtraMessageBox.Show("Nên tìm phòng trống!", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnTraPhong_Click(object sender, EventArgs e)
        {
            if (idex >= 0)
            {
                if (checkChuyenPhong == 1)
                {
                    DataTable dtpt = PhieuThueBUS.getTable();
                    for (int i = 0; i < dtpt.Rows.Count; i++)
                    {
                        if (dtpt.Rows[i]["MaPhieuThue"].ToString() == Maphieuthue)
                        {
                            idex = i;
                            break;
                        }

                    }
                }
                frmPTraPhong fr = new frmPTraPhong();
                fr.MaKH = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaKH").ToString();
                fr.TenKH = gvDSNguoiThuePhong.GetRowCellValue(idex, "TenKH").ToString();
                fr.TenPhong = gvDSNguoiThuePhong.GetRowCellValue(idex, "TenPhong").ToString();
                fr.MaPhong = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhong").ToString();
                fr.SoNguoiO = gvDSNguoiThuePhong.GetRowCellValue(idex, "SoNguoiThue").ToString();
                fr.NGDen = gvDSNguoiThuePhong.GetRowCellValue(idex, "NgayBatDauThue").ToString();
                fr.MaNV = MaNV;
                fr.MaPhieuThue = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhieuThue").ToString();
                fr.tienchuyenphong = double.Parse(gvDSNguoiThuePhong.GetRowCellValue(idex, "TienChuyenPhong").ToString());
                fr.TaiKhoan = TaiKhoan;
                string nd = gvDSNguoiThuePhong.GetRowCellValue(idex, "NgayTraPhong").ToString();
                if (nd == "")
                {
                    fr.NGDi = DateTime.Now.ToString();
                }
                else
                {
                    if (DateTime.Parse(nd) >= DateTime.Now)
                        fr.NGDi = DateTime.Now.ToString();
                    else
                        fr.NGDi = nd;
                }
                fr.ShowDialog();
                Lay();
            }
            else
                XtraMessageBox.Show("Chưa chọn phòng cần trả!", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            Lay();
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog
                {
                    CheckPathExists = true,
                    InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyComputer),
                    OverwritePrompt = true,
                    Title = "Xuất dữ liệu thành tập tin định dạng *.xlsx",
                    Filter = "*.xlsx|*.xlsx"
                };
                DialogResult dr = sfd.ShowDialog();
                if (dr == System.Windows.Forms.DialogResult.OK || dr == System.Windows.Forms.DialogResult.Yes)
                {
                    gcphieuThue.ExportToXlsx(sfd.FileName);
                    dr = XtraMessageBox.Show("Xuất dữ liệu thành công! Bạn có muốn mở tập tin vừa xuất ra không?", "Xác nhận mở tập tin", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Lỗi xuất dữ liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _logger.Error(ex.ToString());
            }
        }

        private void btnHuyThuePhong_Click(object sender, EventArgs e)
        {
            string map = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhong").ToString();
            try
            {
                PhieuThueBUS.XoaPhieu(gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhieuThue").ToString());
                if (PhieuThueBUS.LoadPhieuTheoMP(map).Count == 0)
                    busphong.Cap_Nhat_Tinh_Trang(map, "MTT0000002");
                XtraMessageBox.Show("Hủy thuê phòng thành công", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LuuNhatKyHT.Them(TaiKhoan, "Phiếu Thuê", "Hủy Thuê Phòng");
                Lay();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Hủy thuê phòng không thành công", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _logger.Error(ex.ToString());
            }
        }
        private bool ExistForm(DevComponents.DotNetBar.Metro.MetroForm form)
        {
            foreach (var child in this.MdiParent.MdiChildren)
            {
                if (child.Name == form.Name)
                {
                    child.Activate();
                    return true;
                }
            }
            return false;
        }

        private void btnChuyenPhong_Click(object sender, EventArgs e)
        {
            if (idex >= 0)
            {
                frmTraCuuPhong frm = new frmTraCuuPhong();
                frm.KhiThue += Dmp_KhiThue;
                frm.HoTen = ganhoten;
                frm.chucvu = chucvu;
                frm.ngayden = gvDSNguoiThuePhong.GetRowCellValue(idex, "NgayBatDauThue").ToString();
                frm.ngaydi = gvDSNguoiThuePhong.GetRowCellValue(idex, "NgayTraPhong").ToString();
                frm.maphieu = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhieuThue").ToString();
                frm.maphongdachuyen = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhong").ToString();
                if (ExistForm(frm)) return;
                frm.MdiParent = this.MdiParent;
                frm.Show();
            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (idex >= 0)
            {
                frmSuaPhieuThue frm = new frmSuaPhieuThue();
                frm.MaPhieuThue = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhieuThue").ToString();
                frm.ngayden = gvDSNguoiThuePhong.GetRowCellValue(idex, "NgayBatDauThue").ToString();
                frm.ngaydi = gvDSNguoiThuePhong.GetRowCellValue(idex, "NgayTraPhong").ToString();
                frm.SLNHT = int.Parse(gvDSNguoiThuePhong.GetRowCellValue(idex, "SoNguoiThue").ToString());
                frm.makh = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaKH").ToString();
                frm.maphong = gvDSNguoiThuePhong.GetRowCellValue(idex, "MaPhong").ToString();
                frm.TenDN = TaiKhoan;
                frm.ShowDialog();
                Lay();
            }
            else
                XtraMessageBox.Show("Chưa chọn dòng cần cập nhật", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
      
        private void btnTimPhong_Click(object sender, EventArgs e)
        {
            frmTraCuuPhong dmp = new frmTraCuuPhong();
            dmp.HoTen = ganhoten;
            dmp.chucvu = chucvu;
            dmp.KhiThue += Dmp_KhiThue;
            
            dmp.MdiParent = this.MdiParent;
            dmp.Show();
        }
        private void Dmp_KhiThue()
        {
            LoadDL();
        }
        
        private void gvDSNguoiThuePhong_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            idex = e.FocusedRowHandle;
            if (idex >= 0)
            {
                if (DateTime.Parse(gvDSNguoiThuePhong.GetRowCellValue(idex, "NgayBatDauThue").ToString()) > DateTime.Now)
                {
                    btnTraPhong.Enabled = false;
                    btnHuyThuePhong.Enabled = true;
                }

                else
                {
                    btnTraPhong.Enabled = true;
                    btnHuyThuePhong.Enabled = false;
                }

            }
        }

        private void frmPhieuThue_FormClosing(object sender, FormClosingEventArgs e)
        {
            ResetDl();
        }

        private void gvDSNguoiThuePhong_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.RowHandle % 2 == 0)
                {
                    e.Appearance.BackColor = Color.Gold;
                }
            }
        }

    }
}