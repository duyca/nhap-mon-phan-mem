﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using log4net;
using QL_KS.BUS;
using System.Text.RegularExpressions;
using QL_KS.DAO;

namespace QL_KS
{
    public partial class frmNhanVien_Them_Sua : DevExpress.XtraEditors.XtraForm
    {
        public delegate void LoadData();
        public LoadData delegateLoadData;
        private static readonly ILog _logger;
        private string manv;
        public string MaNV;
        public string LoaiNV;
        public string TaiKhoan;
        NhanVienBUS nvBUS = new NhanVienBUS();
        LoaiNhanVienBUS lnvBUS = new LoaiNhanVienBUS();
        NhanVien nv = new NhanVien();

        public frmNhanVien_Them_Sua()
        {
            manv = "";
            InitializeComponent();

            gridLookUpEdit1.Properties.DisplayMember = "TenLoaiNV";
            gridLookUpEdit1.Properties.ValueMember = "MaLoaiNV";
            gridLookUpEdit1.Properties.DataSource = lnvBUS.LoadDS();
            ILog _logger = LogManager.GetLogger(typeof(frmSaoLuu).Name);
        }
        public frmNhanVien_Them_Sua(string manv)
        {
            InitializeComponent();
            this.manv = manv;
            gridLookUpEdit1.Properties.DisplayMember = "TenLoaiNV";
            gridLookUpEdit1.Properties.ValueMember = "MaLoaiNV";
            gridLookUpEdit1.Properties.DataSource = lnvBUS.LoadDS();
            ILog _logger = LogManager.GetLogger(typeof(frmSaoLuu).Name);
        }
        
        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
            ResetLabel();
            LoadDL();
        }
        private void Reset()
        {
            txtTaiKhoan.Text = "";
            txtTenNhanVien.Text = "";
            txtDiaChi.Text = "";
            txtSoDienThoai.Text = "";
            txtEmail.Text = "";
            txtCMND.Text = "";
        }
        private void ResetLabel()
        {
            labelX1.Text = "";
            lblTenNV.Text = "";
            lblEmail.Text = "";
            lblSoDienThoai.Text = "";
            lblDiaChi.Text = "";
            lblCMND.Text = "";
        }

        private void btnCapNhatLai_Click(object sender, EventArgs e)
        {

            ResetLabel();
            try
            {
                if (txtTaiKhoan.Text.Trim() == string.Empty)
                {
                    labelX1.Text = "Vui lòng nhập tài khoản";
                    lblTenNV.ForeColor = Color.Red;
                    txtTaiKhoan.Focus();
                    return;
                }
                if (txtTenNhanVien.Text == string.Empty)
                {
                    lblTenNV.Text = "Vui lòng nhập tên nhân viên";
                    lblTenNV.ForeColor = Color.Red;
                    txtTenNhanVien.Focus();
                    return;
                }
                if (txtEmail.Text == string.Empty)
                {
                    lblEmail.Text = "Vui lòng nhập email";
                    lblTenNV.ForeColor = Color.Red;
                    txtEmail.Focus();
                    return;
                }
                bool isEmail = Regex.IsMatch(txtEmail.Text.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

                if (isEmail == false)
                {
                    lblEmail.Text = "Email không hợp lệ";
                    lblEmail.ForeColor = Color.Red;
                    txtEmail.Focus();
                    return;
                }
                if (txtSoDienThoai.Text == string.Empty)
                {
                    lblSoDienThoai.Text = "Vui lòng nhập số điện thoại";
                    lblSoDienThoai.ForeColor = Color.Red;
                    txtSoDienThoai.Focus();
                    return;
                }
                if (txtCMND.Text == string.Empty)
                {
                    lblCMND.Text = "Vui lòng nhập số CMND";
                    lblCMND.ForeColor = Color.Red;
                    txtCMND.Focus();
                    return;
                }
                if (txtDiaChi.Text == string.Empty)
                {
                    lblDiaChi.Text = "Vui lòng nhập địa chỉ";
                    lblDiaChi.ForeColor = Color.Red;
                    txtDiaChi.Focus();
                    return;
                }
                else
                {
                    nv = new NhanVien();
                    nv.MaNV = txtMaNV.Text.Trim();
                    nv.TaiKhoan = txtTaiKhoan.Text.Trim();
                    nv.TenNV = txtTenNhanVien.Text.Trim();
                    nv.Email = txtEmail.Text.Trim();
                    nv.SDT = txtSoDienThoai.Text.Trim();
                    nv.DiaChi = txtDiaChi.Text.Trim();
                    nv.CMND = txtCMND.Text.Trim();
                    nv.ConQuanLy = true;
                    nv.LoaiNV = gridLookUpEdit1.EditValue.ToString();
                    nv.NgaySinh = dateEdit1.DateTime;
                    bool result;
                    if (manv == "")
                    {
                        nv.MatKhau = MaHoaMD5.md5(nv.CMND);
                        result = nvBUS.Them(nv);
                        if (result == true)
                        {
                            MessageBox.Show("Thêm nhân viên thành công!", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            LoadDL();
                            LuuNhatKyHT.Them(TaiKhoan, "Hệ thống", "Thêm nhân viên");
                        }
                        else
                        {
                            MessageBox.Show("Có lỗi xảy ra !", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        result = nvBUS.CapNhatThongTin(nv);
                        if (result == true)
                        {
                            MessageBox.Show("Cập nhật thông tin nhân viên thành công!", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            LoadDL();
                            LuuNhatKyHT.Them(TaiKhoan, "Hệ thống", "Cập nhật thông tin cá nhân");
                        }
                        else
                        {
                            MessageBox.Show("Có lỗi xảy ra !", "Thông Báo !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                _logger.Error(ex.Message);
            }
            if (delegateLoadData != null)
            {
                delegateLoadData();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmNhanVien_Sua_Load(object sender, EventArgs e)
        {
            LoadDL();
        }
  
        private void LoadDL()
        {
            if (manv != "")
            {
                btnCapNhatLai.Text = "Sửa";
                this.Text = "Sửa Nhân Viên";
                nv = nvBUS.LoadTheoMaNV(manv);
                txtMaNV.Text = nv.MaNV;
                txtTaiKhoan.Text = nv.TaiKhoan.Trim();
                txtTenNhanVien.Text = nv.TenNV.Trim();
                txtEmail.Text = nv.Email.Trim();
                txtSoDienThoai.Text = nv.SDT.Trim();
                txtDiaChi.Text = nv.DiaChi.Trim();
                txtCMND.Text = nv.CMND.Trim();
                gridLookUpEdit1.EditValue = nv.LoaiNV;
                dateEdit1.EditValue = nv.NgaySinh;

            }
            else
            {
                btnCapNhatLai.Text = "Thêm";
                this.Text = "Thêm Nhân Viên";
                txtMaNV.Text = nvBUS.TaoMa();
                txtTaiKhoan.Text = "";
                txtTenNhanVien.Text = "";
                txtEmail.Text = "";
                txtSoDienThoai.Text = "";
                txtDiaChi.Text = "";
                txtCMND.Text = "";
                gridLookUpEdit1.EditValue = "MLNV000001";
                dateEdit1.EditValue = null;
            }
        }
    }
}