﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class HoaDonDAO
    {
        public HoaDon Tim_Hoa_Don(string maHD)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var vHoaDon = (from hd in data.HoaDons
                             where hd.MaHD == maHD
                               select hd).FirstOrDefault();
                if (vHoaDon != null)
                    return vHoaDon;
                else
                    return null;
            }
        }
        public void Them_Hoa_Don(HoaDon HoaDonMoi)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                data.HoaDons.Add(HoaDonMoi);
                data.SaveChanges();
            }
        }
        QLKhachSanEntities1 context;

        public List<HoaDon> LoadDL()
        {
            context = new QLKhachSanEntities1();
            var kq = from hd in context.HoaDons.Include("ChiTietHoaDons")
                     join kh in context.KhachHangs on hd.MaKH equals kh.MaKH
                     orderby hd.NgayThanhToan descending
                     select hd;
            
            return kq.ToList();
        }

        public DataTable InHoaDon(string MaHD)
        {
             context = new QLKhachSanEntities1();

            var total = from hd in context.HoaDons
                        join cthd in context.ChiTietHoaDons on hd.MaHD equals cthd.MaHD
                        join kh in context.KhachHangs on hd.MaKH equals kh.MaKH
                        join p in context.Phongs on cthd.MaPhong equals p.MaPhong
                        join nv in context.NhanViens on hd.MaNV equals nv.MaNV
                        where hd.MaHD == MaHD
                        select new
                        {
                            hd.MaHD, hd.NgayThanhToan,
                            kh.MaKH, kh.TenKH, kh.CMND, kh.DiaChi, kh.SDT,
                            p.TenPhong,
                            cthd.NgayDen, cthd.NgayDi, cthd.PhuThu, 
                            cthd.DonGia, cthd.ThanhTien,
                            hd.SoGioThue, hd.TongTien, 
                            nv.TenNV
                        };

            DataTable dt = new DataTable();
            dt.Columns.Add("MaHD");
            dt.Columns.Add("NgayThanhToan");
            dt.Columns.Add("MaKH");
            dt.Columns.Add("TenKH");
            dt.Columns.Add("CMND");
            dt.Columns.Add("DiaChi");
            dt.Columns.Add("SDT");
            dt.Columns.Add("TenPhong");
            dt.Columns.Add("NgayDen");
            dt.Columns.Add("NgayDi");
            dt.Columns.Add("HeSo");
            dt.Columns.Add("PhuThu");
            dt.Columns.Add("DonGia");
            dt.Columns.Add("ThanhTien");
            dt.Columns.Add("SoNgayThue");
            dt.Columns.Add("TongTien");
            dt.Columns.Add("TenNV");
            
            foreach (var x in total)
            {
                DataRow row = dt.NewRow();
                row["MaHD"] = x.MaHD;
                row["NgayThanhToan"] = x.NgayThanhToan;
                row["MaKH"] = x.MaKH;
                row["TenKH"] = x.TenKH;
                row["CMND"] = x.CMND;
                row["DiaChi"] = x.DiaChi;
                row["SDT"] = x.SDT;
                row["TenPhong"] = x.TenPhong;
                row["NgayDen"] = x.NgayDen;
                row["NgayDi"] = x.NgayDi;
                row["PhuThu"] = x.PhuThu;
                row["DonGia"] = x.DonGia;
                row["ThanhTien"] = x.ThanhTien;
                row["SoNgayThue"] = x.SoGioThue;
                row["TongTien"] = x.TongTien;
                row["TenNV"] = x.TenNV;
                dt.Rows.Add(row);
                dt.AcceptChanges();
            }

            return dt;
        }
    }
}
