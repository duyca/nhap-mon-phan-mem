﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class Hinh_Thuc_DAO
    {
        public HinhThuc Tim_Hinh_Thuc(string maHT)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var HinhThuc = (from ht in data.HinhThucs
                             where ht.MaHinhThuc == maHT
                             select ht).FirstOrDefault();
                return HinhThuc;
            }
        }
        public List<HinhThuc> Load_DS_Hinh_Thuc()
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var DS_Hinh_Thuc = from ht in data.HinhThucs
                                   where ht.active == true
                                   select ht;
                return DS_Hinh_Thuc.ToList();
            }
        }
        public void Them_Hinh_Thuc(HinhThuc hinhthucmoi)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                data.HinhThucs.Add(hinhthucmoi);
                data.SaveChanges();
            }
        }
        public void Sua_Hinh_Thuc(HinhThuc hinhthucmoi)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                HinhThuc hinhthuctam = data.HinhThucs.Find(hinhthucmoi.MaHinhThuc);
                hinhthuctam.TenHinhThuc = hinhthucmoi.TenHinhThuc;
                hinhthuctam.GioKetThuc = hinhthucmoi.GioKetThuc;
                hinhthuctam.ThoiGianKeoDai = hinhthucmoi.ThoiGianKeoDai;
                hinhthuctam.active = hinhthucmoi.active;
                data.SaveChanges();
            }
        }
    }
}
