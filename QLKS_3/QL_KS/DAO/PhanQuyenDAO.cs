﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    public class PhanQuyenDAO
    {
        QLKhachSanEntities1 data;
        public List<BangPhanQuyen> Load()
        {
            data = new QLKhachSanEntities1();
            var kq = from pq in data.BangPhanQuyens
                     join loainv in data.LoaiNhanViens
                     on pq.LoaiNV equals loainv.MaLoaiNV
                     select pq;
            return kq.ToList();
        }

        public BangPhanQuyen LoadTheoLoai(string loainv)
        {
            data = new QLKhachSanEntities1();
            var kq = (from p in data.BangPhanQuyens
                      where p.LoaiNV == loainv
                      select p).FirstOrDefault();
            return kq;
        }

        public void Them(BangPhanQuyen p)
        {
            data = new QLKhachSanEntities1();
            data.BangPhanQuyens.Add(p);
            data.SaveChanges();
        }

        public void Xoa(string loainv)
        {
            data = new QLKhachSanEntities1();
            var kq = (from p in data.BangPhanQuyens
                      where p.LoaiNV == loainv
                      select p).FirstOrDefault();
            data.BangPhanQuyens.Remove(kq);
            data.SaveChanges();
        }

        public void CapNhat(List<BangPhanQuyen> list)
        {
            data = new QLKhachSanEntities1();
            for (int i = 0; i < list.Count(); i++)
            {
                var kq = (from p in data.BangPhanQuyens.ToList()
                          where p.STT == list[i].STT
                          select p).FirstOrDefault();
                kq.DanhMucPhong = list[i].DanhMucPhong;
                kq.LapPhieuThue = list[i].LapPhieuThue;
                kq.TraCuuPhong = list[i].TraCuuPhong;
                kq.ThayDoiQuyDinh = list[i].ThayDoiQuyDinh;
                kq.NhatKyHeThong = list[i].NhatKyHeThong;
                kq.QuanLyLoaiPhong = list[i].QuanLyLoaiPhong;
                kq.QuanLyKhachHang = list[i].QuanLyKhachHang;
                kq.QuanLyLoaiKhachHang = list[i].QuanLyLoaiKhachHang;
                kq.QuanLyNhanVien = list[i].QuanLyNhanVien;
                kq.QuanLyLoaiNV = list[i].QuanLyLoaiNV;
                kq.SaoLuuDL = list[i].SaoLuuDL;
                kq.PhucHoiDL = list[i].PhucHoiDL;
                kq.PhanQuyen = list[i].PhanQuyen;
                data.SaveChanges();
            }
        }
    }
}
