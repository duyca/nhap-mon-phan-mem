﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    public class NhanVienDAO
    {
        QLKhachSanEntities1 data;

        public List<NhanVien> LoadDS()
        {
            data = new QLKhachSanEntities1();
            return data.NhanViens.ToList();
        }

        public void CapNhatMatKhau(NhanVien nv)
        {
            data = new QLKhachSanEntities1();
            var kq = data.NhanViens.Find(nv.MaNV);
            //kq.MatKhau = MD5(nv.MatKhau);
            kq.MatKhau = nv.MatKhau;
            data.SaveChanges();
        }

        public NhanVien LoadTheoMaNV(string MaNV)
        {
            data = new QLKhachSanEntities1();
            var kq = data.NhanViens.Find(MaNV);
            return kq;
        }

        public bool CapNhatThongTin(NhanVien nv)
        {
            string manv = nv.MaNV;
            var nhanvien = data.NhanViens.SingleOrDefault(b=>b.MaNV == manv);
            if (nhanvien != null)
            {
                nhanvien.TenNV = nv.TenNV;
                nhanvien.Email = nv.Email;
                nhanvien.SDT = nv.SDT;
                nhanvien.DiaChi = nv.DiaChi;
                nhanvien.CMND = nv.CMND;
                nhanvien.ConQuanLy = true;
                data.SaveChanges();
                return true;
            }
            else
                return false;
        }
        public bool Them(NhanVien nv)
        {
            data = new QLKhachSanEntities1();
            data.NhanViens.Add(nv);
            data.SaveChanges();
            return true;
        }
        public int Xoa(string ma)
        {
            data = new QLKhachSanEntities1();
            var nvXoa = data.NhanViens.First(nv => nv.MaNV == ma);
            data.NhanViens.Remove(nvXoa);
            return data.SaveChanges();
        }
        public NhanVien TimKiem(string ma)
        {
            data = new QLKhachSanEntities1();
            var kq = data.NhanViens.Find(ma);
            return kq;
        }
        public String TaoMa()
        {
            data = new QLKhachSanEntities1();
            String max = data.NhanViens.Max(lnv => lnv.MaNV);
            int index = max.IndexOf('0');
            string chu = max.Substring(0, index);
            int so = Int32.Parse(max.Substring(index, max.Length - index));
            so++;
            return chu + so.ToString("D" + (max.Length - index).ToString());
        }
        public DataTable getNV(string TaiKhoan)
        {
            data = new QLKhachSanEntities1();
            DataTable dt = new DataTable("table");
             dt.Columns.Add("ChucVu");
            var nhanvien = from nv in data.NhanViens
                           join loainv in data.LoaiNhanViens
                           on nv.LoaiNV equals loainv.MaLoaiNV
                           where nv.TaiKhoan == TaiKhoan
                        //join lp in datacontext.LOAIPHONGs on p.LoaiPhong equals lp.MaLP
                        select new {loainv.TenLoaiNV};


            foreach (var x in nhanvien)
            {
                DataRow row = dt.NewRow();
                row["ChucVu"] = x.TenLoaiNV;
                dt.Rows.Add(row);
                dt.AcceptChanges();
            }
            return dt;
        }

    }
}
