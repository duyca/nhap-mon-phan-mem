﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class QuyDinhDAO
    {
        public QuyDinh Lay_Quy_Dinh(int sothutu)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var quydinh = (from qd in data.QuyDinhs
                              where qd.STT == sothutu
                              select qd).FirstOrDefault();
                return quydinh;
            }
        }
    }
}
