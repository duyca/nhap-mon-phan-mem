﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class Loai_Khach_HangDAO
    {
        public List<LoaiKhachHang> Load_DS_Loai_Khach()
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var DS_Loai_Khach = from lk in data.LoaiKhachHangs
                                    select lk;
                return DS_Loai_Khach.ToList();
            }
        }
    }
}
