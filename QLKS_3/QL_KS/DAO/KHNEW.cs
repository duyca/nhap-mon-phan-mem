﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    public class KHNEW
    {
        public string MaKH { get; set; }
        public string TenKH { get; set; }
        public string LoaiKH { get; set; }
        public string CMND { get; set; }
        public string DC { get; set; }
        public string SDT { get; set; }
    }
}
