﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class PhieuThueDAO
    {
        double TongTien = 0;
        public PhieuThue Tim_Phieu_Thue(string ma)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from pt in data.PhieuThues
                             join ph in data.Phongs on pt.MaPhong equals ph.MaPhong
                             where ph.MaPhong == ma && pt.NgayTraPhong == null
                             select pt).FirstOrDefault();
                return phieu;
            }
        }
        public double tinh_tien(string maPhieuThue)
        {
            using (QLKhachSanEntities data = new QLKhachSanEntities())
            {
                var phieu = (from pt in data.PhieuThues
                             join hinhthuc in data.HinhThucs on pt.MaHinhThuc equals hinhthuc.MaHinhThuc
                             join phong in data.Phongs on pt.MaPhong equals phong.MaPhong
                             join loaiphong in data.LoaiPhongs on phong.LoaiPhong equals loaiphong.MaLoaiPhong
                             from hinhthuc_loaiphong in data.HinhThuc_LoaiPhong
                             where pt.MaPhieuThue == maPhieuThue && hinhthuc.MaHinhThuc == hinhthuc_loaiphong.MaHinhThuc && hinhthuc_loaiphong.MaLoaiPhong == loaiphong.MaLoaiPhong
                             select new
                             {
                                 pt.NgayBatDauThue,
                                 loaiphong.TienQuaGio,
                                 hinhthuc.ThoiGianKeoDai,
                                 hinhthuc.GioKetThuc,
                                 hinhthuc_loaiphong.DonGia,
                                 hinhthuc_loaiphong.TiepTheo,
                                 pt.TienChuyenPhong
                             }).FirstOrDefault();

                var qd = (from quydinh in data.QuyDinhs
                          where quydinh.STT == 1
                          select quydinh).FirstOrDefault();
                DateTime giotrongphieu = (DateTime)phieu.NgayBatDauThue;
                DateTime Hientai = DateTime.Now;
                TimeSpan thoigianlech = Hientai.Subtract(giotrongphieu);
                int sogio = (thoigianlech.Days * 24) + thoigianlech.Hours;
                int sophut = thoigianlech.Minutes;
                int boisoThoigian = 0;
                int soduThoigian = 0;
                //Tinh tien theo thoi gian o
                if (phieu.ThoiGianKeoDai != null)
                {
                    if (sophut >= qd.GiaTri)
                    {
                        sogio++;
                    }
                    if (sogio >= phieu.ThoiGianKeoDai)
                    {
                        boisoThoigian = Convert.ToInt32((sogio - phieu.ThoiGianKeoDai) / phieu.ThoiGianKeoDai);
                        soduThoigian = Convert.ToInt32((sogio - phieu.ThoiGianKeoDai) % phieu.ThoiGianKeoDai);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * phieu.TienQuaGio + phieu.TienChuyenPhong);
                    }
                    else
                    {
                        double sogioMien = Convert.ToDouble(phieu.GioKetThuc);
                        TongTien = Convert.ToDouble(phieu.DonGia);
                    }
                }
                //Tinh tien theo moc thoi gian tra phong
                else
                {
                    if (sophut + giotrongphieu.Minute >= qd.GiaTri)
                    {
                        if ((sophut + giotrongphieu.Minute) >= (60 + qd.GiaTri))
                            sogio += 2;
                        sogio++;
                    }
                    int khoangthoigianfree;
                    if (giotrongphieu.Hour < phieu.GioKetThuc)
                    {
                        khoangthoigianfree = Convert.ToInt32(phieu.GioKetThuc - giotrongphieu.Hour);
                    }
                    else
                    {
                        khoangthoigianfree = Convert.ToInt32(phieu.GioKetThuc + 24 - giotrongphieu.Hour);
                    }
                    if (sogio <= khoangthoigianfree)
                    {
                        TongTien = Convert.ToDouble(phieu.DonGia);
                    }
                    else
                    {
                        boisoThoigian = Convert.ToInt32((sogio - khoangthoigianfree) / 24);
                        soduThoigian = Convert.ToInt32((sogio - khoangthoigianfree) % 24);
                        TongTien = Convert.ToDouble(phieu.DonGia + boisoThoigian * phieu.TiepTheo + soduThoigian * phieu.TienQuaGio + phieu.TienChuyenPhong);
                    }
                }

                return TongTien;
            }
        }
    }
}
