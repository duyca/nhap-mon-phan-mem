﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class Loai_Phong_DAO
    {
        public List<LoaiPhong> Load_DS_Loai_Phong()
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var DS_Loai_Phong = from lp in data.LoaiPhongs
                                    where lp.active == true
                                    select lp;
                return DS_Loai_Phong.ToList();
            }
        }
        public LoaiPhong Tim_Loai_Phong( string maLoai)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var Loai_Phong = (from lp in data.LoaiPhongs
                                 where lp.MaLoaiPhong == maLoai
                                 select lp).FirstOrDefault();
                if (Loai_Phong != null)
                    return Loai_Phong;
                else
                    return null;
            }
        }
        public void Them_Loai_Phong(LoaiPhong lphong)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                data.LoaiPhongs.Add(lphong);
                data.SaveChanges();
            }
        }
        public void Sua_Loai_Phong(LoaiPhong lphong)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                LoaiPhong loaitam = (from lp in data.LoaiPhongs
                                          where lp.MaLoaiPhong == lphong.MaLoaiPhong
                                          select lp).FirstOrDefault();
                loaitam.TenLoaiPhong = lphong.TenLoaiPhong;
                loaitam.TienQuaGio = lphong.TienQuaGio;
                loaitam.active = lphong.active;
                data.SaveChanges();
            }
        }

        public static DataTable getTable()
        {
            QLKhachSanEntities1 data = new QLKhachSanEntities1();
            DataTable dt = new DataTable("table");
            dt.Columns.Add("MaLoaiPhong");
            dt.Columns.Add("TenLoaiPhong");
            dt.Columns.Add("DonGia");
            var loaiphong = from lp in data.LoaiPhongs
                            join ht_lp in data.HinhThuc_LoaiPhong
                            on lp.MaLoaiPhong equals ht_lp.MaLoaiPhong
                            select new { lp.MaLoaiPhong, lp.TenLoaiPhong,ht_lp.DonGia
                            };

            foreach (var x in loaiphong)
            {
                DataRow row = dt.NewRow();
                row["MaLoaiPhong"] = x.MaLoaiPhong;
                row["TenLoaiPhong"] = x.TenLoaiPhong;
                row["DonGia"] = x.DonGia;

                dt.Rows.Add(row);
                dt.AcceptChanges();
            }

            return dt;
        }
    }
}
