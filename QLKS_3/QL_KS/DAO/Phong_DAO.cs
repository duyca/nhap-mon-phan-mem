﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.DAO
{
    class Phong_DAO
    {
        QLKhachSanEntities1 data;
        public List<Phong> Load_DS_Phong(string MalP)
        {
            if (MalP != null)
            {
                using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
                {
                    var DS_Phong = from p in data.Phongs
                                   where p.LoaiPhong == MalP && p.active == true 
                                   orderby p.TenPhong
                                   select p;
                    if (DS_Phong.Count() != 0)
                        return DS_Phong.ToList();
                    else
                        return null;
                }
            }
            else
            {
                using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
                {
                    var DS_Phong = from p in data.Phongs
                                   where p.active == true
                                   orderby p.TenPhong
                                   select p;
                    return DS_Phong.ToList();
                }
            }
        }
        public int DemSoPhongTrong()
        {
            int Dem = 0;
            List<Phong> Ds_phong;
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var var_DS_Phong = from p in data.Phongs
                                   select p;
                Ds_phong = var_DS_Phong.ToList();
            }
            foreach (Phong p in Ds_phong)
            {
                if (p.TinhTrang == "MTT0000002")
                    Dem++;
            }
            return Dem;
        }
        public int DemSoPhongThue()
        {
            int Dem = 0;
            List<Phong> Ds_phong;
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var DS_Phong = from p in data.Phongs
                               select p;
                Ds_phong = DS_Phong.ToList();
            }
            foreach (Phong p in Ds_phong)
            {
                if (p.TinhTrang == "MTT0000001")
                    Dem++;
            }
            return Dem;
        }
        public void Cap_Nhat_Tinh_Trang(string maTinhtrang, string map)
        {
            data = new QLKhachSanEntities1();
            var kq = data.Phongs.Find(map);
            //kq.MatKhau = MD5(nv.MatKhau);
            kq.TinhTrang = maTinhtrang;
            data.SaveChanges();
        }
        public Phong Tim_Phong(string map)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var rphong = (from p in data.Phongs
                              where p.MaPhong == map
                              select p).FirstOrDefault();
                if (rphong != null)
                    return rphong;
                else
                    return null;
            }
        }
        public List<Phong> DS_Phong_Trong()
        {
            List<Phong> Ds_phong;
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                var var_DS_Phong = from p in data.Phongs
                                   where p.TinhTrang == "MTT0000002"
                                   select p;
                Ds_phong = var_DS_Phong.ToList();
            }
            return Ds_phong;
        }

        public static List<Phong> TimPhongTrong(string lp)
        {
            QLKhachSanEntities1 data = new QLKhachSanEntities1();
            var kq = from p in data.Phongs
                     join loaip in data.LoaiPhongs on p.LoaiPhong equals loaip.MaLoaiPhong
                     where p.LoaiPhong == lp && p.TinhTrang == "MTT0000002" ||
                        (from pt in data.PhieuThues
                         join p2 in data.Phongs on pt.MaPhong equals p2.MaPhong
                         where p2.LoaiPhong == lp && pt.NgayTraPhong != null
                         select pt.MaPhong).Contains(p.MaPhong)
                     select p;
            return kq.ToList();
        }
        public static List<Phong> TimPhongTrong()
        {
            QLKhachSanEntities1 data = new QLKhachSanEntities1();
            var kq = from p in data.Phongs
                     join loaip in data.LoaiPhongs on p.LoaiPhong equals loaip.MaLoaiPhong
                     where p.TinhTrang.Equals("MTT0000002") ||
                        (from pt in data.PhieuThues
                         where pt.NgayTraPhong != null
                         select pt.MaPhong).Contains(p.MaPhong)
                     select p;
            return kq.ToList();
        }

        public void Them_Phong(Phong phong)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                data.Phongs.Add(phong);
                data.SaveChanges();
            }
        }
        public void Xoa_phong(string map)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                Phong phongtam = data.Phongs.Find(map);
                phongtam.active = false;
                data.SaveChanges();
            }
        }
        public void Sua_phong(Phong phong)
        {
            using (QLKhachSanEntities1 data = new QLKhachSanEntities1())
            {
                Phong phongtam = data.Phongs.Find(phong.MaPhong);
                phongtam.TenPhong = phong.TenPhong;
                phongtam.GhiChu = phong.GhiChu;
                phongtam.LoaiPhong = phong.LoaiPhong;
                phongtam.active = phong.active;
                phongtam.SoNguoiToiDa = phong.SoNguoiToiDa;
                phongtam.TinhTrang = phong.TinhTrang;
                data.SaveChanges();
            }
        }

        public static void CapNhatPhongTheoTrangThai1(Phong p)
        {
            QLKhachSanEntities1 data = new QLKhachSanEntities1();
            string maphong = p.MaPhong;
            Phong phong = new Phong();
            phong = data.Phongs.SingleOrDefault(c => c.MaPhong == maphong);
            if (phong != null)
            {
                phong.TinhTrang = p.TinhTrang;
                data.SaveChanges();
            }
        }
        
        public static List<Phong> TimTheoMaPhong(string lp)
        {
            QLKhachSanEntities1 data = new QLKhachSanEntities1();
            var kq = from p in data.Phongs
                     join loaip in data.LoaiPhongs on p.LoaiPhong equals loaip.MaLoaiPhong
                     where p.MaPhong == lp
                     select p;
            return kq.ToList();
        }
      

    }
}
