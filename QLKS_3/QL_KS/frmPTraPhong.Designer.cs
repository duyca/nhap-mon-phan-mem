﻿namespace QL_KS
{
    partial class frmPTraPhong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDonGiaPhong = new DevExpress.XtraEditors.TextEdit();
            this.RdGKNN = new DevExpress.XtraEditors.RadioGroup();
            this.txtTongTien = new DevExpress.XtraEditors.TextEdit();
            this.txtNGDi = new DevExpress.XtraEditors.TextEdit();
            this.txtSoNguoiO = new DevExpress.XtraEditors.TextEdit();
            this.txtNGDen = new DevExpress.XtraEditors.TextEdit();
            this.txtTenPhong = new DevExpress.XtraEditors.TextEdit();
            this.txtTenKH = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnThanhToan = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonGiaPhong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RdGKNN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNGDi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoNguoiO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNGDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKH.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDonGiaPhong
            // 
            this.txtDonGiaPhong.EditValue = "";
            this.txtDonGiaPhong.Enabled = false;
            this.txtDonGiaPhong.Location = new System.Drawing.Point(157, 274);
            this.txtDonGiaPhong.Name = "txtDonGiaPhong";
            this.txtDonGiaPhong.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaPhong.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtDonGiaPhong.Properties.Appearance.Options.UseFont = true;
            this.txtDonGiaPhong.Properties.Appearance.Options.UseForeColor = true;
            this.txtDonGiaPhong.Properties.MaxLength = 50;
            this.txtDonGiaPhong.Size = new System.Drawing.Size(264, 22);
            this.txtDonGiaPhong.TabIndex = 46;
            // 
            // RdGKNN
            // 
            this.RdGKNN.Location = new System.Drawing.Point(157, 123);
            this.RdGKNN.Name = "RdGKNN";
            this.RdGKNN.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Không"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Có")});
            this.RdGKNN.Size = new System.Drawing.Size(264, 64);
            this.RdGKNN.TabIndex = 45;
            // 
            // txtTongTien
            // 
            this.txtTongTien.Enabled = false;
            this.txtTongTien.Location = new System.Drawing.Point(157, 312);
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTien.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtTongTien.Properties.Appearance.Options.UseFont = true;
            this.txtTongTien.Properties.Appearance.Options.UseForeColor = true;
            this.txtTongTien.Size = new System.Drawing.Size(264, 22);
            this.txtTongTien.TabIndex = 40;
            // 
            // txtNGDi
            // 
            this.txtNGDi.Enabled = false;
            this.txtNGDi.Location = new System.Drawing.Point(157, 239);
            this.txtNGDi.Name = "txtNGDi";
            this.txtNGDi.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNGDi.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtNGDi.Properties.Appearance.Options.UseFont = true;
            this.txtNGDi.Properties.Appearance.Options.UseForeColor = true;
            this.txtNGDi.Size = new System.Drawing.Size(264, 22);
            this.txtNGDi.TabIndex = 41;
            // 
            // txtSoNguoiO
            // 
            this.txtSoNguoiO.Enabled = false;
            this.txtSoNguoiO.Location = new System.Drawing.Point(157, 87);
            this.txtSoNguoiO.Name = "txtSoNguoiO";
            this.txtSoNguoiO.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoNguoiO.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtSoNguoiO.Properties.Appearance.Options.UseFont = true;
            this.txtSoNguoiO.Properties.Appearance.Options.UseForeColor = true;
            this.txtSoNguoiO.Size = new System.Drawing.Size(264, 22);
            this.txtSoNguoiO.TabIndex = 42;
            // 
            // txtNGDen
            // 
            this.txtNGDen.Enabled = false;
            this.txtNGDen.Location = new System.Drawing.Point(157, 203);
            this.txtNGDen.Name = "txtNGDen";
            this.txtNGDen.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNGDen.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtNGDen.Properties.Appearance.Options.UseFont = true;
            this.txtNGDen.Properties.Appearance.Options.UseForeColor = true;
            this.txtNGDen.Properties.MaxLength = 50;
            this.txtNGDen.Size = new System.Drawing.Size(264, 22);
            this.txtNGDen.TabIndex = 37;
            // 
            // txtTenPhong
            // 
            this.txtTenPhong.Enabled = false;
            this.txtTenPhong.Location = new System.Drawing.Point(157, 12);
            this.txtTenPhong.Name = "txtTenPhong";
            this.txtTenPhong.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPhong.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtTenPhong.Properties.Appearance.Options.UseFont = true;
            this.txtTenPhong.Properties.Appearance.Options.UseForeColor = true;
            this.txtTenPhong.Properties.MaxLength = 50;
            this.txtTenPhong.Size = new System.Drawing.Size(264, 22);
            this.txtTenPhong.TabIndex = 38;
            // 
            // txtTenKH
            // 
            this.txtTenKH.Enabled = false;
            this.txtTenKH.Location = new System.Drawing.Point(157, 49);
            this.txtTenKH.Name = "txtTenKH";
            this.txtTenKH.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKH.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtTenKH.Properties.Appearance.Options.UseFont = true;
            this.txtTenKH.Properties.Appearance.Options.UseForeColor = true;
            this.txtTenKH.Properties.MaxLength = 50;
            this.txtTenKH.Size = new System.Drawing.Size(264, 22);
            this.txtTenKH.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 47;
            this.label5.Text = "Phòng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 48;
            this.label1.Text = "Tên khách hàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 49;
            this.label2.Text = "Ngày đi:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 16);
            this.label3.TabIndex = 50;
            this.label3.Text = "Ngày đến:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 16);
            this.label4.TabIndex = 51;
            this.label4.Text = "Khách nước ngoài";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 16);
            this.label6.TabIndex = 52;
            this.label6.Text = "Số người ở";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 16);
            this.label7.TabIndex = 54;
            this.label7.Text = "Đơn giá phòng:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 318);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 53;
            this.label8.Text = "Tổng tiền:";
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Location = new System.Drawing.Point(230, 361);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(89, 23);
            this.btnThanhToan.TabIndex = 55;
            this.btnThanhToan.Text = "Thanh toán";
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // btnDong
            // 
            this.btnDong.Location = new System.Drawing.Point(332, 361);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(89, 23);
            this.btnDong.TabIndex = 56;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // frmPTraPhong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 409);
            this.Controls.Add(this.btnThanhToan);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDonGiaPhong);
            this.Controls.Add(this.RdGKNN);
            this.Controls.Add(this.txtTongTien);
            this.Controls.Add(this.txtNGDi);
            this.Controls.Add(this.txtSoNguoiO);
            this.Controls.Add(this.txtNGDen);
            this.Controls.Add(this.txtTenPhong);
            this.Controls.Add(this.txtTenKH);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmPTraPhong";
            this.Text = "THANH TOÁN TIỀN PHÒNG";
            this.Load += new System.EventHandler(this.frmPTraPhong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtDonGiaPhong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RdGKNN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTongTien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNGDi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoNguoiO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNGDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenPhong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKH.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtDonGiaPhong;
        private DevExpress.XtraEditors.RadioGroup RdGKNN;
        private DevExpress.XtraEditors.TextEdit txtTongTien;
        private DevExpress.XtraEditors.TextEdit txtNGDi;
        private DevExpress.XtraEditors.TextEdit txtSoNguoiO;
        private DevExpress.XtraEditors.TextEdit txtNGDen;
        private DevExpress.XtraEditors.TextEdit txtTenPhong;
        private DevExpress.XtraEditors.TextEdit txtTenKH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.SimpleButton btnThanhToan;
        private DevExpress.XtraEditors.SimpleButton btnDong;
    }
}