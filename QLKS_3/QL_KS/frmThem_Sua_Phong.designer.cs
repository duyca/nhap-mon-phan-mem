﻿namespace QL_KS
{
    partial class frmThem_Sua_Phong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lb_ThongBao_SoNguoi = new System.Windows.Forms.Label();
            this.lbThongbao_ten = new System.Windows.Forms.Label();
            this.txtSoNguoi = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtGhiChu = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cbbLoaiPhong = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtMaPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnThuePhong = new System.Windows.Forms.Button();
            this.btnHuyBo = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label1.Location = new System.Drawing.Point(219, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Thông Tin Phòng";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.lb_ThongBao_SoNguoi);
            this.panel1.Controls.Add(this.lbThongbao_ten);
            this.panel1.Controls.Add(this.txtSoNguoi);
            this.panel1.Controls.Add(this.txtGhiChu);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtTenPhong);
            this.panel1.Controls.Add(this.cbbLoaiPhong);
            this.panel1.Controls.Add(this.txtMaPhong);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(-1, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(632, 203);
            this.panel1.TabIndex = 1;
            // 
            // lb_ThongBao_SoNguoi
            // 
            this.lb_ThongBao_SoNguoi.AutoSize = true;
            this.lb_ThongBao_SoNguoi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lb_ThongBao_SoNguoi.Location = new System.Drawing.Point(399, 81);
            this.lb_ThongBao_SoNguoi.Name = "lb_ThongBao_SoNguoi";
            this.lb_ThongBao_SoNguoi.Size = new System.Drawing.Size(117, 13);
            this.lb_ThongBao_SoNguoi.TabIndex = 35;
            this.lb_ThongBao_SoNguoi.Text = "Hãy nhập vào số người";
            this.lb_ThongBao_SoNguoi.Visible = false;
            // 
            // lbThongbao_ten
            // 
            this.lbThongbao_ten.AutoSize = true;
            this.lbThongbao_ten.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongbao_ten.Location = new System.Drawing.Point(399, 22);
            this.lbThongbao_ten.Name = "lbThongbao_ten";
            this.lbThongbao_ten.Size = new System.Drawing.Size(125, 13);
            this.lbThongbao_ten.TabIndex = 34;
            this.lbThongbao_ten.Text = "Hãy nhập vào tên phòng";
            this.lbThongbao_ten.Visible = false;
            // 
            // txtSoNguoi
            // 
            // 
            // 
            // 
            this.txtSoNguoi.BackgroundStyle.Class = "TextBoxBorder";
            this.txtSoNguoi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSoNguoi.ButtonClear.Visible = true;
            this.txtSoNguoi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoNguoi.Location = new System.Drawing.Point(402, 100);
            this.txtSoNguoi.Mask = "00";
            this.txtSoNguoi.Name = "txtSoNguoi";
            this.txtSoNguoi.Size = new System.Drawing.Size(184, 20);
            this.txtSoNguoi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtSoNguoi.TabIndex = 33;
            this.txtSoNguoi.Text = "";
            // 
            // txtGhiChu
            // 
            // 
            // 
            // 
            this.txtGhiChu.Border.Class = "TextBoxBorder";
            this.txtGhiChu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGhiChu.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(104, 144);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.PreventEnterBeep = true;
            this.txtGhiChu.Size = new System.Drawing.Size(184, 42);
            this.txtGhiChu.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "Ghi Chú:";
            // 
            // txtTenPhong
            // 
            // 
            // 
            // 
            this.txtTenPhong.Border.Class = "TextBoxBorder";
            this.txtTenPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPhong.Location = new System.Drawing.Point(402, 41);
            this.txtTenPhong.Name = "txtTenPhong";
            this.txtTenPhong.PreventEnterBeep = true;
            this.txtTenPhong.Size = new System.Drawing.Size(184, 22);
            this.txtTenPhong.TabIndex = 9;
            // 
            // cbbLoaiPhong
            // 
            this.cbbLoaiPhong.DisplayMember = "Text";
            this.cbbLoaiPhong.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbLoaiPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiPhong.FormattingEnabled = true;
            this.cbbLoaiPhong.ItemHeight = 17;
            this.cbbLoaiPhong.Location = new System.Drawing.Point(104, 97);
            this.cbbLoaiPhong.Name = "cbbLoaiPhong";
            this.cbbLoaiPhong.Size = new System.Drawing.Size(184, 23);
            this.cbbLoaiPhong.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbLoaiPhong.TabIndex = 8;
            // 
            // txtMaPhong
            // 
            // 
            // 
            // 
            this.txtMaPhong.Border.Class = "TextBoxBorder";
            this.txtMaPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaPhong.Enabled = false;
            this.txtMaPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhong.Location = new System.Drawing.Point(104, 41);
            this.txtMaPhong.Name = "txtMaPhong";
            this.txtMaPhong.PreventEnterBeep = true;
            this.txtMaPhong.Size = new System.Drawing.Size(184, 22);
            this.txtMaPhong.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "Loại Phòng:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(313, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tên Phòng(*):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(294, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số Người Tối Đa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã Phòng:";
            // 
            // btnThuePhong
            // 
            this.btnThuePhong.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnThuePhong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnThuePhong.FlatAppearance.BorderSize = 0;
            this.btnThuePhong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThuePhong.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThuePhong.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.btnThuePhong.Location = new System.Drawing.Point(395, 294);
            this.btnThuePhong.Name = "btnThuePhong";
            this.btnThuePhong.Size = new System.Drawing.Size(110, 40);
            this.btnThuePhong.TabIndex = 6;
            this.btnThuePhong.Text = "Thêm Phòng";
            this.btnThuePhong.UseVisualStyleBackColor = false;
            this.btnThuePhong.Click += new System.EventHandler(this.btnDongY_Click);
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnHuyBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHuyBo.FlatAppearance.BorderSize = 0;
            this.btnHuyBo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHuyBo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.ForeColor = System.Drawing.Color.Crimson;
            this.btnHuyBo.Location = new System.Drawing.Point(511, 294);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(110, 40);
            this.btnHuyBo.TabIndex = 7;
            this.btnHuyBo.Text = "Hủy Bỏ";
            this.btnHuyBo.UseVisualStyleBackColor = false;
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // frmThem_Sua_Phong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 346);
            this.Controls.Add(this.btnHuyBo);
            this.Controls.Add(this.btnThuePhong);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "frmThem_Sua_Phong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thêm Phòng";
            this.Load += new System.EventHandler(this.frmThem_Sua_Phong_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenPhong;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbLoaiPhong;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaPhong;
        private System.Windows.Forms.Label label5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGhiChu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnThuePhong;
        private System.Windows.Forms.Button btnHuyBo;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtSoNguoi;
        private System.Windows.Forms.Label lbThongbao_ten;
        private System.Windows.Forms.Label lb_ThongBao_SoNguoi;
    }
}