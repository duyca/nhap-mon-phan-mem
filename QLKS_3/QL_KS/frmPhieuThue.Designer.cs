﻿namespace QL_KS
{
    partial class frmPhieuThue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.gcphieuThue = new DevExpress.XtraGrid.GridControl();
            this.gvDSNguoiThuePhong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MaPhieuThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NGBD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayTra = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoNguoiThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CMND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MaKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TienChuyenPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnTimPhong = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnXuatExcel = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyThuePhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnTraPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnChuyenPhong = new DevExpress.XtraEditors.SimpleButton();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btnThuePhong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbChucVu = new DevExpress.XtraEditors.LabelControl();
            this.lbHoTen = new DevExpress.XtraEditors.LabelControl();
            this.txtNgayDi = new DevExpress.XtraEditors.TextEdit();
            this.txtNgayDen = new DevExpress.XtraEditors.TextEdit();
            this.cbxKhachHang = new DevExpress.XtraEditors.LookUpEdit();
            this.cbxsoluong = new System.Windows.Forms.ComboBox();
            this.lbTenPhong = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcphieuThue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDSNguoiThuePhong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayDi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxKhachHang.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcphieuThue
            // 
            this.gcphieuThue.Dock = System.Windows.Forms.DockStyle.Top;
            gridLevelNode2.RelationName = "Level1";
            this.gcphieuThue.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gcphieuThue.Location = new System.Drawing.Point(0, 0);
            this.gcphieuThue.MainView = this.gvDSNguoiThuePhong;
            this.gcphieuThue.Name = "gcphieuThue";
            this.gcphieuThue.Size = new System.Drawing.Size(920, 408);
            this.gcphieuThue.TabIndex = 9;
            this.gcphieuThue.UseEmbeddedNavigator = true;
            this.gcphieuThue.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDSNguoiThuePhong});
            // 
            // gvDSNguoiThuePhong
            // 
            this.gvDSNguoiThuePhong.Appearance.Row.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gvDSNguoiThuePhong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MaPhieuThue,
            this.TenKH,
            this.MaPhong,
            this.TenPhong,
            this.NGBD,
            this.NgayTra,
            this.SoNguoiThue,
            this.CMND,
            this.DiaChi,
            this.MaKH,
            this.TienChuyenPhong});
            this.gvDSNguoiThuePhong.GridControl = this.gcphieuThue;
            this.gvDSNguoiThuePhong.Name = "gvDSNguoiThuePhong";
            this.gvDSNguoiThuePhong.OptionsBehavior.Editable = false;
            this.gvDSNguoiThuePhong.OptionsView.ShowAutoFilterRow = true;
            this.gvDSNguoiThuePhong.OptionsView.ShowGroupPanel = false;
            this.gvDSNguoiThuePhong.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gvDSNguoiThuePhong_RowStyle);
            this.gvDSNguoiThuePhong.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvDSNguoiThuePhong_FocusedRowChanged);
            // 
            // MaPhieuThue
            // 
            this.MaPhieuThue.Caption = "MaPhieuThue";
            this.MaPhieuThue.FieldName = "MaPhieuThue";
            this.MaPhieuThue.Name = "MaPhieuThue";
            // 
            // TenKH
            // 
            this.TenKH.Caption = "Khách Hàng";
            this.TenKH.FieldName = "TenKH";
            this.TenKH.Name = "TenKH";
            this.TenKH.Visible = true;
            this.TenKH.VisibleIndex = 0;
            // 
            // MaPhong
            // 
            this.MaPhong.Caption = "MaPhong";
            this.MaPhong.FieldName = "MaPhong";
            this.MaPhong.Name = "MaPhong";
            // 
            // TenPhong
            // 
            this.TenPhong.Caption = "Tên Phòng";
            this.TenPhong.FieldName = "TenPhong";
            this.TenPhong.Name = "TenPhong";
            this.TenPhong.Visible = true;
            this.TenPhong.VisibleIndex = 1;
            // 
            // NGBD
            // 
            this.NGBD.Caption = "Ngày Bắt Đầu Thuê";
            this.NGBD.FieldName = "NgayBatDauThue";
            this.NGBD.Name = "NGBD";
            this.NGBD.Visible = true;
            this.NGBD.VisibleIndex = 2;
            // 
            // NgayTra
            // 
            this.NgayTra.Caption = "Ngày Trả Phòng";
            this.NgayTra.FieldName = "NgayTraPhong";
            this.NgayTra.Name = "NgayTra";
            this.NgayTra.Visible = true;
            this.NgayTra.VisibleIndex = 3;
            // 
            // SoNguoiThue
            // 
            this.SoNguoiThue.Caption = "Số Người Thuê";
            this.SoNguoiThue.FieldName = "SoNguoiThue";
            this.SoNguoiThue.Name = "SoNguoiThue";
            this.SoNguoiThue.Visible = true;
            this.SoNguoiThue.VisibleIndex = 4;
            // 
            // CMND
            // 
            this.CMND.Caption = "CMND";
            this.CMND.FieldName = "CMND";
            this.CMND.Name = "CMND";
            this.CMND.Visible = true;
            this.CMND.VisibleIndex = 5;
            // 
            // DiaChi
            // 
            this.DiaChi.Caption = "Địa Chỉ";
            this.DiaChi.FieldName = "DiaChi";
            this.DiaChi.Name = "DiaChi";
            this.DiaChi.Visible = true;
            this.DiaChi.VisibleIndex = 6;
            // 
            // MaKH
            // 
            this.MaKH.Caption = "gridColumn1";
            this.MaKH.FieldName = "MaKH";
            this.MaKH.Name = "MaKH";
            // 
            // TienChuyenPhong
            // 
            this.TienChuyenPhong.Caption = "Tiền Chuyển Phòng";
            this.TienChuyenPhong.FieldName = "TienChuyenPhong";
            this.TienChuyenPhong.Name = "TienChuyenPhong";
            this.TienChuyenPhong.Visible = true;
            this.TienChuyenPhong.VisibleIndex = 7;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnTimPhong);
            this.panelControl1.Controls.Add(this.simpleButton7);
            this.panelControl1.Controls.Add(this.btnDong);
            this.panelControl1.Controls.Add(this.btnXuatExcel);
            this.panelControl1.Controls.Add(this.btnHuyThuePhong);
            this.panelControl1.Controls.Add(this.btnTraPhong);
            this.panelControl1.Controls.Add(this.btnChuyenPhong);
            this.panelControl1.Controls.Add(this.btnCapNhat);
            this.panelControl1.Controls.Add(this.btnThuePhong);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.txtNgayDi);
            this.panelControl1.Controls.Add(this.txtNgayDen);
            this.panelControl1.Controls.Add(this.cbxKhachHang);
            this.panelControl1.Controls.Add(this.cbxsoluong);
            this.panelControl1.Controls.Add(this.lbTenPhong);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 408);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(920, 166);
            this.panelControl1.TabIndex = 10;
            // 
            // btnTimPhong
            // 
            this.btnTimPhong.Location = new System.Drawing.Point(205, 15);
            this.btnTimPhong.Name = "btnTimPhong";
            this.btnTimPhong.Size = new System.Drawing.Size(89, 23);
            this.btnTimPhong.TabIndex = 109;
            this.btnTimPhong.Text = "Tìm phòng";
            this.btnTimPhong.Click += new System.EventHandler(this.btnTimPhong_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(101, 123);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(89, 23);
            this.simpleButton7.TabIndex = 108;
            this.simpleButton7.Text = "Tất cả";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // btnDong
            // 
            this.btnDong.Location = new System.Drawing.Point(819, 123);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(89, 23);
            this.btnDong.TabIndex = 107;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Location = new System.Drawing.Point(714, 123);
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.Size = new System.Drawing.Size(89, 23);
            this.btnXuatExcel.TabIndex = 106;
            this.btnXuatExcel.Text = "Xuất excel";
            this.btnXuatExcel.Click += new System.EventHandler(this.btnXuatExcel_Click);
            // 
            // btnHuyThuePhong
            // 
            this.btnHuyThuePhong.Location = new System.Drawing.Point(610, 123);
            this.btnHuyThuePhong.Name = "btnHuyThuePhong";
            this.btnHuyThuePhong.Size = new System.Drawing.Size(89, 23);
            this.btnHuyThuePhong.TabIndex = 105;
            this.btnHuyThuePhong.Text = "Hủy thuê phòng";
            this.btnHuyThuePhong.Click += new System.EventHandler(this.btnHuyThuePhong_Click);
            // 
            // btnTraPhong
            // 
            this.btnTraPhong.Location = new System.Drawing.Point(511, 123);
            this.btnTraPhong.Name = "btnTraPhong";
            this.btnTraPhong.Size = new System.Drawing.Size(89, 23);
            this.btnTraPhong.TabIndex = 103;
            this.btnTraPhong.Text = "Trả phòng";
            this.btnTraPhong.Click += new System.EventHandler(this.btnTraPhong_Click);
            // 
            // btnChuyenPhong
            // 
            this.btnChuyenPhong.Location = new System.Drawing.Point(409, 123);
            this.btnChuyenPhong.Name = "btnChuyenPhong";
            this.btnChuyenPhong.Size = new System.Drawing.Size(89, 23);
            this.btnChuyenPhong.TabIndex = 102;
            this.btnChuyenPhong.Text = "Chuyển phòng";
            this.btnChuyenPhong.Click += new System.EventHandler(this.btnChuyenPhong_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(308, 123);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(89, 23);
            this.btnCapNhat.TabIndex = 101;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnThuePhong
            // 
            this.btnThuePhong.Location = new System.Drawing.Point(205, 123);
            this.btnThuePhong.Name = "btnThuePhong";
            this.btnThuePhong.Size = new System.Drawing.Size(89, 23);
            this.btnThuePhong.TabIndex = 100;
            this.btnThuePhong.Text = "Thuê phòng";
            this.btnThuePhong.Click += new System.EventHandler(this.btnThuePhong_Click);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(29, 25);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(34, 13);
            this.labelControl7.TabIndex = 97;
            this.labelControl7.Text = "Phòng:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(29, 86);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(40, 13);
            this.labelControl4.TabIndex = 96;
            this.labelControl4.Text = "Ngày đi:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(27, 56);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(50, 13);
            this.labelControl5.TabIndex = 95;
            this.labelControl5.Text = "Ngày đến:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(334, 88);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 13);
            this.labelControl2.TabIndex = 94;
            this.labelControl2.Text = "Khách hàng:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(332, 60);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(46, 13);
            this.labelControl3.TabIndex = 93;
            this.labelControl3.Text = "Số lượng:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.lbChucVu);
            this.groupControl1.Controls.Add(this.lbHoTen);
            this.groupControl1.Location = new System.Drawing.Point(659, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(261, 81);
            this.groupControl1.TabIndex = 75;
            this.groupControl1.Text = "Người lập Phiếu";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(17, 59);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(44, 13);
            this.labelControl6.TabIndex = 92;
            this.labelControl6.Text = "Chức vụ:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 13);
            this.labelControl1.TabIndex = 91;
            this.labelControl1.Text = "Họ tên:";
            // 
            // lbChucVu
            // 
            this.lbChucVu.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbChucVu.Appearance.Options.UseForeColor = true;
            this.lbChucVu.Location = new System.Drawing.Point(99, 63);
            this.lbChucVu.Name = "lbChucVu";
            this.lbChucVu.Size = new System.Drawing.Size(0, 13);
            this.lbChucVu.TabIndex = 6;
            // 
            // lbHoTen
            // 
            this.lbHoTen.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHoTen.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbHoTen.Appearance.Options.UseFont = true;
            this.lbHoTen.Appearance.Options.UseForeColor = true;
            this.lbHoTen.Location = new System.Drawing.Point(99, 31);
            this.lbHoTen.Name = "lbHoTen";
            this.lbHoTen.Size = new System.Drawing.Size(0, 15);
            this.lbHoTen.TabIndex = 6;
            // 
            // txtNgayDi
            // 
            this.txtNgayDi.Enabled = false;
            this.txtNgayDi.Location = new System.Drawing.Point(119, 82);
            this.txtNgayDi.Name = "txtNgayDi";
            this.txtNgayDi.Size = new System.Drawing.Size(175, 20);
            this.txtNgayDi.TabIndex = 74;
            // 
            // txtNgayDen
            // 
            this.txtNgayDen.Enabled = false;
            this.txtNgayDen.Location = new System.Drawing.Point(119, 53);
            this.txtNgayDen.Name = "txtNgayDen";
            this.txtNgayDen.Size = new System.Drawing.Size(175, 20);
            this.txtNgayDen.TabIndex = 73;
            // 
            // cbxKhachHang
            // 
            this.cbxKhachHang.Location = new System.Drawing.Point(409, 85);
            this.cbxKhachHang.Name = "cbxKhachHang";
            this.cbxKhachHang.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cbxKhachHang.Properties.Appearance.Options.UseBackColor = true;
            this.cbxKhachHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxKhachHang.Size = new System.Drawing.Size(191, 20);
            this.cbxKhachHang.TabIndex = 68;
            // 
            // cbxsoluong
            // 
            this.cbxsoluong.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxsoluong.FormattingEnabled = true;
            this.cbxsoluong.Location = new System.Drawing.Point(409, 55);
            this.cbxsoluong.Name = "cbxsoluong";
            this.cbxsoluong.Size = new System.Drawing.Size(191, 21);
            this.cbxsoluong.TabIndex = 67;
            // 
            // lbTenPhong
            // 
            this.lbTenPhong.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenPhong.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbTenPhong.Appearance.Options.UseFont = true;
            this.lbTenPhong.Appearance.Options.UseForeColor = true;
            this.lbTenPhong.Location = new System.Drawing.Point(121, 15);
            this.lbTenPhong.Name = "lbTenPhong";
            this.lbTenPhong.Size = new System.Drawing.Size(0, 18);
            this.lbTenPhong.TabIndex = 61;
            // 
            // frmPhieuThue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 574);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gcphieuThue);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmPhieuThue";
            this.Text = "PHIẾU THUÊ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPhieuThue_FormClosing);
            this.Load += new System.EventHandler(this.frmPhieuThue_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcphieuThue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDSNguoiThuePhong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayDi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxKhachHang.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcphieuThue;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDSNguoiThuePhong;
        private DevExpress.XtraGrid.Columns.GridColumn MaPhieuThue;
        private DevExpress.XtraGrid.Columns.GridColumn TenKH;
        private DevExpress.XtraGrid.Columns.GridColumn MaPhong;
        private DevExpress.XtraGrid.Columns.GridColumn TenPhong;
        private DevExpress.XtraGrid.Columns.GridColumn NGBD;
        private DevExpress.XtraGrid.Columns.GridColumn NgayTra;
        private DevExpress.XtraGrid.Columns.GridColumn SoNguoiThue;
        private DevExpress.XtraGrid.Columns.GridColumn CMND;
        private DevExpress.XtraGrid.Columns.GridColumn DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn MaKH;
        private DevExpress.XtraGrid.Columns.GridColumn TienChuyenPhong;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl lbChucVu;
        private DevExpress.XtraEditors.LabelControl lbHoTen;
        private DevExpress.XtraEditors.TextEdit txtNgayDi;
        private DevExpress.XtraEditors.TextEdit txtNgayDen;
        private DevExpress.XtraEditors.LookUpEdit cbxKhachHang;
        private System.Windows.Forms.ComboBox cbxsoluong;
        private DevExpress.XtraEditors.LabelControl lbTenPhong;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnXuatExcel;
        private DevExpress.XtraEditors.SimpleButton btnHuyThuePhong;
        private DevExpress.XtraEditors.SimpleButton btnTraPhong;
        private DevExpress.XtraEditors.SimpleButton btnChuyenPhong;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private DevExpress.XtraEditors.SimpleButton btnThuePhong;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton btnTimPhong;
    }
}