﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
namespace QL_KS
{
    public partial class frmHoaDon : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmHoaDon()
        {
            InitializeComponent();
        }
        HoaDonBUS bushoadon = new HoaDonBUS();
        public int index;
        public void LoadDL()
        {
            lblMaKH.Text = "";
            lblTenKH.Text = "";
            lblCMND.Text = "";
            lblDiaChi.Text = "";
            lblSoDienThoai.Text = "";
            gcHoaDon.DataSource = bushoadon.LoadDL();
        }

        private void btnLoadLai_Click(object sender, EventArgs e)
        {
            LoadDL();
            gvHoaDon.ClearColumnsFilter();
        }

        private void frmHoaDon_Load(object sender, EventArgs e)
        {
            LoadDL();
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            if (index >= 0)
            {
                string mahd = gvHoaDon.GetFocusedRowCellValue("MaHD").ToString().Trim();
                InHoaDon fr = new InHoaDon();
                fr.DataSource = bushoadon.InHoaDon(mahd);
                fr.ShowPreviewDialog();
            }
            else
            {
                XtraMessageBox.Show("Chưa chọn hóa đơn cần in!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void gvHoaDon_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            index = e.FocusedRowHandle;
        }

        private void gvHoaDon_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (index >= 0)
            {
                lblMaKH.Text = gvHoaDon.GetRowCellValue(index, "MaKH").ToString();
                lblTenKH.Text = gvHoaDon.GetRowCellValue(index, "KhachHang.TenKH").ToString();
                lblCMND.Text = gvHoaDon.GetRowCellValue(index, "KhachHang.CMND").ToString();
                lblDiaChi.Text = gvHoaDon.GetRowCellValue(index, "KhachHang.DiaChi").ToString();
                lblSoDienThoai.Text = gvHoaDon.GetRowCellValue(index, "KhachHang.SDT").ToString();
            }
            else
            {
                lblMaKH.Text = "";
                lblTenKH.Text = "";
                lblCMND.Text = "";
                lblDiaChi.Text = "";
                lblSoDienThoai.Text = "";
            }
        }

        private void gvHoaDon_MasterRowEmpty(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventArgs e)
        {
            HoaDon hd = (HoaDon)gvHoaDon.GetRow(e.RowHandle);
            e.IsEmpty = hd.ChiTietHoaDons.Count == 0;
               
        }

        private void gvHoaDon_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void gvHoaDon_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Chi tiết hóa đơn";
        }

        private void gvHoaDon_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            HoaDon hd = (HoaDon)gvHoaDon.GetRow(e.RowHandle);
            e.ChildList = new BindingSource(hd, "ChiTietHoaDons");
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}