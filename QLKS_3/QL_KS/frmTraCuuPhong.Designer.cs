﻿namespace QL_KS
{
    partial class frmTraCuuPhong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lkLoaiPhong = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.btnTimKiem = new DevExpress.XtraEditors.SimpleButton();
            this.btnThuePhong = new DevExpress.XtraEditors.SimpleButton();
            this.dtNgayDi = new System.Windows.Forms.DateTimePicker();
            this.dtNgayDen = new System.Windows.Forms.DateTimePicker();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcTraCuuPhong = new DevExpress.XtraGrid.GridControl();
            this.gvTraCuuPhong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MaPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LoaiPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayBatDauThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayTraPhong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GioTrong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoNguoiToiDa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DonGia = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkLoaiPhong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTraCuuPhong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTraCuuPhong)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.lkLoaiPhong);
            this.groupControl1.Location = new System.Drawing.Point(-4, 357);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(318, 117);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Tìm phòng trống theo loại phòng";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(18, 40);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(81, 13);
            this.labelControl2.TabIndex = 90;
            this.labelControl2.Text = "Chọn loại phòng:";
            // 
            // lkLoaiPhong
            // 
            this.lkLoaiPhong.Location = new System.Drawing.Point(105, 37);
            this.lkLoaiPhong.Name = "lkLoaiPhong";
            this.lkLoaiPhong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkLoaiPhong.Size = new System.Drawing.Size(183, 20);
            this.lkLoaiPhong.TabIndex = 89;
            this.lkLoaiPhong.EditValueChanged += new System.EventHandler(this.lkLoaiPhong_EditValueChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.radioGroup1);
            this.groupControl2.Controls.Add(this.btnTimKiem);
            this.groupControl2.Controls.Add(this.btnThuePhong);
            this.groupControl2.Controls.Add(this.dtNgayDi);
            this.groupControl2.Controls.Add(this.dtNgayDen);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Location = new System.Drawing.Point(296, 357);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(552, 117);
            this.groupControl2.TabIndex = 91;
            this.groupControl2.Text = "Tìm phòng theo ngày";
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(239, 34);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.radioGroup1.Properties.Appearance.Options.UseFont = true;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Tìm Theo Ngày Đến"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Tìm Theo Cả Ngày Đến Và Đi")});
            this.radioGroup1.Size = new System.Drawing.Size(190, 68);
            this.radioGroup1.TabIndex = 100;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Location = new System.Drawing.Point(447, 34);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(89, 23);
            this.btnTimKiem.TabIndex = 99;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // btnThuePhong
            // 
            this.btnThuePhong.Location = new System.Drawing.Point(447, 70);
            this.btnThuePhong.Name = "btnThuePhong";
            this.btnThuePhong.Size = new System.Drawing.Size(89, 23);
            this.btnThuePhong.TabIndex = 98;
            this.btnThuePhong.Text = "Thuê phòng";
            this.btnThuePhong.Click += new System.EventHandler(this.btnThuePhong_Click);
            // 
            // dtNgayDi
            // 
            this.dtNgayDi.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayDi.Location = new System.Drawing.Point(74, 76);
            this.dtNgayDi.Name = "dtNgayDi";
            this.dtNgayDi.Size = new System.Drawing.Size(159, 21);
            this.dtNgayDi.TabIndex = 95;
            // 
            // dtNgayDen
            // 
            this.dtNgayDen.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayDen.Location = new System.Drawing.Point(74, 34);
            this.dtNgayDen.Name = "dtNgayDen";
            this.dtNgayDen.Size = new System.Drawing.Size(159, 21);
            this.dtNgayDen.TabIndex = 94;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(18, 82);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 13);
            this.labelControl3.TabIndex = 91;
            this.labelControl3.Text = "Ngày đi:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(18, 40);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(50, 13);
            this.labelControl1.TabIndex = 90;
            this.labelControl1.Text = "Ngày đến:";
            // 
            // gcTraCuuPhong
            // 
            this.gcTraCuuPhong.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcTraCuuPhong.Location = new System.Drawing.Point(0, 0);
            this.gcTraCuuPhong.LookAndFeel.SkinMaskColor = System.Drawing.Color.White;
            this.gcTraCuuPhong.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.White;
            this.gcTraCuuPhong.LookAndFeel.UseWindowsXPTheme = true;
            this.gcTraCuuPhong.MainView = this.gvTraCuuPhong;
            this.gcTraCuuPhong.Name = "gcTraCuuPhong";
            this.gcTraCuuPhong.Size = new System.Drawing.Size(848, 351);
            this.gcTraCuuPhong.TabIndex = 92;
            this.gcTraCuuPhong.UseEmbeddedNavigator = true;
            this.gcTraCuuPhong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTraCuuPhong});
            // 
            // gvTraCuuPhong
            // 
            this.gvTraCuuPhong.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvTraCuuPhong.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTraCuuPhong.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvTraCuuPhong.Appearance.EvenRow.Options.UseFont = true;
            this.gvTraCuuPhong.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gvTraCuuPhong.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.gvTraCuuPhong.Appearance.OddRow.Options.UseBackColor = true;
            this.gvTraCuuPhong.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvTraCuuPhong.Appearance.Row.BackColor2 = System.Drawing.Color.LightCoral;
            this.gvTraCuuPhong.Appearance.Row.Options.UseBackColor = true;
            this.gvTraCuuPhong.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MaPhong,
            this.TenPhong,
            this.LoaiPhong,
            this.NgayBatDauThue,
            this.NgayTraPhong,
            this.GioTrong,
            this.SoNguoiToiDa,
            this.DonGia});
            this.gvTraCuuPhong.GridControl = this.gcTraCuuPhong;
            this.gvTraCuuPhong.GroupPanelText = "Tra cứu phòng";
            this.gvTraCuuPhong.Name = "gvTraCuuPhong";
            this.gvTraCuuPhong.OptionsBehavior.Editable = false;
            this.gvTraCuuPhong.OptionsView.ShowAutoFilterRow = true;
            this.gvTraCuuPhong.OptionsView.ShowGroupPanel = false;
            this.gvTraCuuPhong.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gvTraCuuPhong_PopupMenuShowing);
            this.gvTraCuuPhong.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvTraCuuPhong_FocusedRowChanged);
            // 
            // MaPhong
            // 
            this.MaPhong.Caption = "Mã phòng";
            this.MaPhong.FieldName = "MaPhong";
            this.MaPhong.Name = "MaPhong";
            // 
            // TenPhong
            // 
            this.TenPhong.Caption = "Tên Phòng";
            this.TenPhong.FieldName = "TenPhong";
            this.TenPhong.Name = "TenPhong";
            this.TenPhong.Visible = true;
            this.TenPhong.VisibleIndex = 0;
            // 
            // LoaiPhong
            // 
            this.LoaiPhong.Caption = "Loại phòng";
            this.LoaiPhong.FieldName = "LoaiPhong";
            this.LoaiPhong.Name = "LoaiPhong";
            this.LoaiPhong.Visible = true;
            this.LoaiPhong.VisibleIndex = 1;
            // 
            // NgayBatDauThue
            // 
            this.NgayBatDauThue.Caption = "Ngày Thuê";
            this.NgayBatDauThue.FieldName = "NgayBatDauThue";
            this.NgayBatDauThue.Name = "NgayBatDauThue";
            this.NgayBatDauThue.Visible = true;
            this.NgayBatDauThue.VisibleIndex = 2;
            // 
            // NgayTraPhong
            // 
            this.NgayTraPhong.Caption = "Ngày Trả";
            this.NgayTraPhong.FieldName = "NgayTraPhong";
            this.NgayTraPhong.Name = "NgayTraPhong";
            this.NgayTraPhong.Visible = true;
            this.NgayTraPhong.VisibleIndex = 3;
            // 
            // GioTrong
            // 
            this.GioTrong.Caption = "Ngày Giờ Còn Trống";
            this.GioTrong.FieldName = "GioTrong";
            this.GioTrong.Name = "GioTrong";
            this.GioTrong.Visible = true;
            this.GioTrong.VisibleIndex = 4;
            // 
            // SoNguoiToiDa
            // 
            this.SoNguoiToiDa.Caption = "Số Người Tối Đa";
            this.SoNguoiToiDa.FieldName = "SoNguoiToiDa";
            this.SoNguoiToiDa.Name = "SoNguoiToiDa";
            this.SoNguoiToiDa.Visible = true;
            this.SoNguoiToiDa.VisibleIndex = 5;
            // 
            // DonGia
            // 
            this.DonGia.Caption = "Giá Phòng";
            this.DonGia.FieldName = "DonGia";
            this.DonGia.Name = "DonGia";
            this.DonGia.Visible = true;
            this.DonGia.VisibleIndex = 6;
            // 
            // frmTraCuuPhong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 475);
            this.Controls.Add(this.gcTraCuuPhong);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmTraCuuPhong";
            this.Text = "TRA CỨU PHÒNG";
            this.Load += new System.EventHandler(this.frmTraCuuPhong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkLoaiPhong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTraCuuPhong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTraCuuPhong)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lkLoaiPhong;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.DateTimePicker dtNgayDi;
        private System.Windows.Forms.DateTimePicker dtNgayDen;
        private DevExpress.XtraEditors.SimpleButton btnTimKiem;
        private DevExpress.XtraEditors.SimpleButton btnThuePhong;
        private DevExpress.XtraGrid.GridControl gcTraCuuPhong;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTraCuuPhong;
        private DevExpress.XtraGrid.Columns.GridColumn MaPhong;
        private DevExpress.XtraGrid.Columns.GridColumn TenPhong;
        private DevExpress.XtraGrid.Columns.GridColumn LoaiPhong;
        private DevExpress.XtraGrid.Columns.GridColumn NgayBatDauThue;
        private DevExpress.XtraGrid.Columns.GridColumn NgayTraPhong;
        private DevExpress.XtraGrid.Columns.GridColumn GioTrong;
        private DevExpress.XtraGrid.Columns.GridColumn SoNguoiToiDa;
        private DevExpress.XtraGrid.Columns.GridColumn DonGia;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;


    }
}