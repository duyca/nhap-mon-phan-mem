﻿namespace QL_KS
{
    partial class frmSaoLuu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnDuongDan = new System.Windows.Forms.Button();
            this.txtTenTapTin = new System.Windows.Forms.TextBox();
            this.txtDuongDan = new System.Windows.Forms.TextBox();
            this.lblErrorDuongDan = new System.Windows.Forms.Label();
            this.lblErrorTenTapTin = new System.Windows.Forms.Label();
            this.lblPhanTram = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnThayDoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // btnDuongDan
            // 
            this.btnDuongDan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDuongDan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDuongDan.Location = new System.Drawing.Point(337, 180);
            this.btnDuongDan.Name = "btnDuongDan";
            this.btnDuongDan.Size = new System.Drawing.Size(36, 28);
            this.btnDuongDan.TabIndex = 70;
            this.btnDuongDan.Text = ". . .";
            this.btnDuongDan.UseVisualStyleBackColor = true;
            this.btnDuongDan.Click += new System.EventHandler(this.btnDuongDan_Click);
            // 
            // txtTenTapTin
            // 
            this.txtTenTapTin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTenTapTin.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenTapTin.Location = new System.Drawing.Point(131, 122);
            this.txtTenTapTin.Name = "txtTenTapTin";
            this.txtTenTapTin.ReadOnly = true;
            this.txtTenTapTin.Size = new System.Drawing.Size(242, 26);
            this.txtTenTapTin.TabIndex = 71;
            // 
            // txtDuongDan
            // 
            this.txtDuongDan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDuongDan.Enabled = false;
            this.txtDuongDan.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDuongDan.Location = new System.Drawing.Point(131, 180);
            this.txtDuongDan.Name = "txtDuongDan";
            this.txtDuongDan.Size = new System.Drawing.Size(200, 26);
            this.txtDuongDan.TabIndex = 64;
            // 
            // lblErrorDuongDan
            // 
            this.lblErrorDuongDan.AutoSize = true;
            this.lblErrorDuongDan.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorDuongDan.ForeColor = System.Drawing.Color.Red;
            this.lblErrorDuongDan.Location = new System.Drawing.Point(128, 220);
            this.lblErrorDuongDan.Name = "lblErrorDuongDan";
            this.lblErrorDuongDan.Size = new System.Drawing.Size(0, 19);
            this.lblErrorDuongDan.TabIndex = 65;
            // 
            // lblErrorTenTapTin
            // 
            this.lblErrorTenTapTin.AutoSize = true;
            this.lblErrorTenTapTin.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorTenTapTin.ForeColor = System.Drawing.Color.Red;
            this.lblErrorTenTapTin.Location = new System.Drawing.Point(128, 151);
            this.lblErrorTenTapTin.Name = "lblErrorTenTapTin";
            this.lblErrorTenTapTin.Size = new System.Drawing.Size(0, 14);
            this.lblErrorTenTapTin.TabIndex = 66;
            // 
            // lblPhanTram
            // 
            this.lblPhanTram.AutoSize = true;
            this.lblPhanTram.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanTram.Location = new System.Drawing.Point(231, 75);
            this.lblPhanTram.Name = "lblPhanTram";
            this.lblPhanTram.Size = new System.Drawing.Size(34, 19);
            this.lblPhanTram.TabIndex = 94;
            this.lblPhanTram.Text = "0 %";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(131, 71);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(242, 25);
            this.progressBar1.TabIndex = 93;
            // 
            // btnThayDoi
            // 
            this.btnThayDoi.Location = new System.Drawing.Point(203, 253);
            this.btnThayDoi.Name = "btnThayDoi";
            this.btnThayDoi.Size = new System.Drawing.Size(75, 23);
            this.btnThayDoi.TabIndex = 95;
            this.btnThayDoi.Text = "Sao Lưu";
            this.btnThayDoi.Click += new System.EventHandler(this.btnThayDoi_Click);
            // 
            // btnDong
            // 
            this.btnDong.Location = new System.Drawing.Point(298, 253);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 96;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DarkBlue;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(12, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(491, 17);
            this.label7.TabIndex = 97;
            this.label7.Text = "SAO LƯU DỮ LIỆU                                                                  " +
    "                           \r\n";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(26, 80);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 14);
            this.labelControl1.TabIndex = 98;
            this.labelControl1.Text = "Tiến trình:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(25, 134);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 14);
            this.labelControl2.TabIndex = 99;
            this.labelControl2.Text = "Tên tập tin:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(26, 187);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(65, 14);
            this.labelControl3.TabIndex = 100;
            this.labelControl3.Text = "Đường dẫn:";
            // 
            // frmSaoLuu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 293);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnThayDoi);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.lblPhanTram);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnDuongDan);
            this.Controls.Add(this.txtTenTapTin);
            this.Controls.Add(this.txtDuongDan);
            this.Controls.Add(this.lblErrorDuongDan);
            this.Controls.Add(this.lblErrorTenTapTin);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSaoLuu";
            this.Text = "Sao lưu";
            this.Load += new System.EventHandler(this.frmSaoLuu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnDuongDan;
        private System.Windows.Forms.TextBox txtTenTapTin;
        private System.Windows.Forms.TextBox txtDuongDan;
        private System.Windows.Forms.Label lblErrorDuongDan;
        private System.Windows.Forms.Label lblErrorTenTapTin;
        private System.Windows.Forms.Label lblPhanTram;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevExpress.XtraEditors.SimpleButton btnThayDoi;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;

    }
}