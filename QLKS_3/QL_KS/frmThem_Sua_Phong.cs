﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QL_KS.BUS;
using DevComponents.DotNetBar;

namespace QL_KS
{
    public partial class frmThem_Sua_Phong : Form
    {
        Loai_Phong_BUS bloai_phong = new Loai_Phong_BUS();
        Phong_BUS bphong = new Phong_BUS();
        public string frmMaphong;
        public frmThem_Sua_Phong()
        {
            InitializeComponent();
        }
        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmThem_Sua_Phong_Load(object sender, EventArgs e)
        {
            foreach (LoaiPhong lp in bloai_phong.Load_DS_Loai_Phong())
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Text = lp.TenLoaiPhong;
                item.Name = lp.MaLoaiPhong;
                cbbLoaiPhong.Items.Add(item);
                if (frmMaphong == null)
                    cbbLoaiPhong.SelectedIndex = 0;
                else
                {
                    Phong phong_tam = bphong.Tim_Phong(frmMaphong);
                    txtTenPhong.Text = phong_tam.TenPhong;
                    txtGhiChu.Text = phong_tam.GhiChu;
                    txtMaPhong.Text = frmMaphong;
                    txtSoNguoi.Text = phong_tam.SoNguoiToiDa.ToString();
                    if (item.Name == phong_tam.LoaiPhong)
                    {
                        cbbLoaiPhong.SelectedItem = item;
                    }
                }
                    
            }
            this.ActiveControl = txtTenPhong;
            if (frmMaphong == null)
            {
                btnThuePhong.Text = "Thêm Phòng";
            }
            else
            {
                btnThuePhong.Text = "Lưu";
            }
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (txtTenPhong.Text == string.Empty)
            {
                error++;
                lbThongbao_ten.Visible = true;
            }
            else
            {
                lbThongbao_ten.Visible = false;
            }
            if (txtSoNguoi.Text == string.Empty)
            {
                error++;
                lb_ThongBao_SoNguoi.Visible = true;
            }
            else
            {
                lb_ThongBao_SoNguoi.Visible = false;
            }

            if (error == 0)
            {
                Phong phong_moi = new Phong();
                do
                {
                    phong_moi.MaPhong = RandomString(10);
                }
                while (bphong.Tim_Phong(phong_moi.MaPhong) != null);
                phong_moi.TenPhong = txtTenPhong.Text;
                phong_moi.GhiChu = txtGhiChu.Text;
                phong_moi.TinhTrang = "MTT0000002";
                phong_moi.SoNguoiToiDa = Convert.ToInt32(txtSoNguoi.Text);
                phong_moi.LoaiPhong = (cbbLoaiPhong.SelectedItem as ComboBoxItem).Name;
                if(frmMaphong == null)
                {
                    try
                    {
                        bphong.Them_Phong(phong_moi);
                        MessageBox.Show("Thêm phòng thành công");
                        Close();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Chương trình đã xảy ra lỗi nghiêm trọng vui lòng liên hệ quản trị để xử lý");
                    }
                }
                else
                {
                    phong_moi.MaPhong = frmMaphong;
                    bphong.Sua_phong(phong_moi);
                    MessageBox.Show("Sửa thông tin phòng thành công");
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Dữ liệu nhập vào không hợp lệ");
            }
        }
        private string RandomString(int size)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 87)));
                sb.Append(c);
            }
            return sb.ToString();

        }
    }
}
