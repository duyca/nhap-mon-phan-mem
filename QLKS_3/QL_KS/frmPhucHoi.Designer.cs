﻿namespace QL_KS
{
    partial class frmPhucHoi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnDuongDan = new System.Windows.Forms.Button();
            this.txtCoSoDL = new System.Windows.Forms.TextBox();
            this.txtTenTapTin = new System.Windows.Forms.TextBox();
            this.lblErrorDuongDan = new System.Windows.Forms.Label();
            this.lblErrorTenTapTin = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblPhanTram = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnPhucHoi = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // btnDuongDan
            // 
            this.btnDuongDan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDuongDan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDuongDan.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDuongDan.Location = new System.Drawing.Point(347, 129);
            this.btnDuongDan.Name = "btnDuongDan";
            this.btnDuongDan.Size = new System.Drawing.Size(36, 23);
            this.btnDuongDan.TabIndex = 85;
            this.btnDuongDan.Text = ". . .";
            this.btnDuongDan.UseVisualStyleBackColor = true;
            this.btnDuongDan.Click += new System.EventHandler(this.btnDuongDan_Click);
            // 
            // txtCoSoDL
            // 
            this.txtCoSoDL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCoSoDL.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCoSoDL.Location = new System.Drawing.Point(141, 182);
            this.txtCoSoDL.Name = "txtCoSoDL";
            this.txtCoSoDL.Size = new System.Drawing.Size(242, 26);
            this.txtCoSoDL.TabIndex = 86;
            this.txtCoSoDL.Text = "QLKhachSan";
            // 
            // txtTenTapTin
            // 
            this.txtTenTapTin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTenTapTin.Enabled = false;
            this.txtTenTapTin.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenTapTin.Location = new System.Drawing.Point(141, 126);
            this.txtTenTapTin.Name = "txtTenTapTin";
            this.txtTenTapTin.Size = new System.Drawing.Size(200, 26);
            this.txtTenTapTin.TabIndex = 79;
            // 
            // lblErrorDuongDan
            // 
            this.lblErrorDuongDan.AutoSize = true;
            this.lblErrorDuongDan.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorDuongDan.ForeColor = System.Drawing.Color.Red;
            this.lblErrorDuongDan.Location = new System.Drawing.Point(34, 225);
            this.lblErrorDuongDan.Name = "lblErrorDuongDan";
            this.lblErrorDuongDan.Size = new System.Drawing.Size(0, 14);
            this.lblErrorDuongDan.TabIndex = 80;
            // 
            // lblErrorTenTapTin
            // 
            this.lblErrorTenTapTin.AutoSize = true;
            this.lblErrorTenTapTin.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorTenTapTin.ForeColor = System.Drawing.Color.Red;
            this.lblErrorTenTapTin.Location = new System.Drawing.Point(138, 150);
            this.lblErrorTenTapTin.Name = "lblErrorTenTapTin";
            this.lblErrorTenTapTin.Size = new System.Drawing.Size(0, 19);
            this.lblErrorTenTapTin.TabIndex = 81;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(141, 70);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(242, 25);
            this.progressBar1.TabIndex = 91;
            // 
            // lblPhanTram
            // 
            this.lblPhanTram.AutoSize = true;
            this.lblPhanTram.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanTram.Location = new System.Drawing.Point(241, 74);
            this.lblPhanTram.Name = "lblPhanTram";
            this.lblPhanTram.Size = new System.Drawing.Size(34, 19);
            this.lblPhanTram.TabIndex = 92;
            this.lblPhanTram.Text = "0 %";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DarkBlue;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(12, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(498, 17);
            this.label7.TabIndex = 98;
            this.label7.Text = "PHỤC HỒI DỮ LIỆU                                                                 " +
    "                            \r\n";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(15, 77);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 14);
            this.labelControl1.TabIndex = 99;
            this.labelControl1.Text = "Tiến trình:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(15, 135);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 14);
            this.labelControl2.TabIndex = 100;
            this.labelControl2.Text = "Tên tập tin:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(16, 194);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(75, 14);
            this.labelControl3.TabIndex = 101;
            this.labelControl3.Text = "Cơ sở dữ liệu:";
            // 
            // btnPhucHoi
            // 
            this.btnPhucHoi.Location = new System.Drawing.Point(213, 258);
            this.btnPhucHoi.Name = "btnPhucHoi";
            this.btnPhucHoi.Size = new System.Drawing.Size(75, 23);
            this.btnPhucHoi.TabIndex = 102;
            this.btnPhucHoi.Text = "Phục hồi";
            this.btnPhucHoi.Click += new System.EventHandler(this.btnPhucHoi_Click_1);
            // 
            // btnDong
            // 
            this.btnDong.Location = new System.Drawing.Point(308, 258);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 103;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // frmPhucHoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 306);
            this.Controls.Add(this.btnPhucHoi);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblPhanTram);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnDuongDan);
            this.Controls.Add(this.txtCoSoDL);
            this.Controls.Add(this.txtTenTapTin);
            this.Controls.Add(this.lblErrorDuongDan);
            this.Controls.Add(this.lblErrorTenTapTin);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPhucHoi";
            this.Text = "PHỤC HỒI ";
            this.Load += new System.EventHandler(this.frmPhucHoi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnDuongDan;
        private System.Windows.Forms.TextBox txtCoSoDL;
        private System.Windows.Forms.TextBox txtTenTapTin;
        private System.Windows.Forms.Label lblErrorDuongDan;
        private System.Windows.Forms.Label lblErrorTenTapTin;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lblPhanTram;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnPhucHoi;
        private DevExpress.XtraEditors.SimpleButton btnDong;
    }
}