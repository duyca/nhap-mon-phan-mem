﻿using QL_KS.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.BUS
{
    class Phong_BUS
    {
        Phong_DAO phong = new Phong_DAO();
        public List<Phong> Load_DS_Phong(string MaP)
        {
            return phong.Load_DS_Phong(MaP);
        }
        public int DemSoPhongTrong()
        {
            return phong.DemSoPhongTrong();
        }
        public int DemSoPhongThue()
        {
            return phong.DemSoPhongThue();
        }
        public void Cap_Nhat_Tinh_Trang(string maTinhtrang, string map)
        {
            phong.Cap_Nhat_Tinh_Trang(maTinhtrang, map);
        }
        public Phong Tim_Phong(string map)
        {
            return phong.Tim_Phong(map);
        }
        public List<Phong> DS_Phong_Trong()
        {
            return phong.DS_Phong_Trong();
        }
        public void Them_Phong(Phong p)
        {
            phong.Them_Phong(p);
        }
        public void Xoa_phong(string map)
        {
            phong.Xoa_phong(map);
        }
        public void Sua_phong(Phong p)
        {
            phong.Sua_phong(p);
        }
        public static void CapNhatPhongTheoTrangThai1(Phong p)
        {
             Phong_DAO.CapNhatPhongTheoTrangThai1(p);
        }
        public static List<Phong> TimPhongTrong(string lp)
        {
            return Phong_DAO.TimPhongTrong(lp);
        }
        public static List<Phong> TimPhongTrong()
        {
            return Phong_DAO.TimPhongTrong();
        }
        public static List<Phong> TimTheoMaPhong(string lp)
        {
            return Phong_DAO.TimTheoMaPhong(lp);
        }
    }
}
