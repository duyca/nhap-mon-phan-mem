﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;
using System.Data;

namespace QL_KS.BUS
{
    class HoaDonBUS
    {
        HoaDonDAO daoHoaDon = new HoaDonDAO();

        public HoaDon Tim_Hoa_Don(string maHD)
        {
            return daoHoaDon.Tim_Hoa_Don(maHD);
        }
        public void Them_Hoa_Don(HoaDon HoaDonMoi)
        {
            daoHoaDon.Them_Hoa_Don(HoaDonMoi);
        }

        public List<HoaDon> LoadDL()
        {
            return daoHoaDon.LoadDL();
        }
        public DataTable InHoaDon(string MaHD)
        {
            return daoHoaDon.InHoaDon(MaHD);
        }
    }
}
