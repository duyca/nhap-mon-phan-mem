﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;

namespace QL_KS.BUS
{
    public class LoaiKHBUS
    {
        public static List<LoaiKhachHang> LoadLKH()
        {
            return LoaiKHDAO.LoadLKH();
        }

        public static bool Delete(int ma)
        {
            return LoaiKHDAO.Delete(ma);
        }

        public static bool Update(LoaiKhachHang l)
        {
            return LoaiKHDAO.Update(l);
        }

        public static bool Insert(LoaiKhachHang l)
        {
            return LoaiKHDAO.Insert(l);
        }
    }
}
