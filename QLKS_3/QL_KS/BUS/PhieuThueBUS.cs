﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QL_KS.DAO;
using System.Data;

namespace QL_KS.BUS
{
    class PhieuThueBUS
    {
        PhieuThueDAO phieuthue = new PhieuThueDAO();
        public PhieuThue Tim_Phieu_Thue(string maP)
        {
            return phieuthue.Tim_Phieu_Thue(maP);
        }
        public double tinh_tien(string ma)
        {
            return phieuthue.tinh_tien(ma);
        }
        public double tinh_tien(string ma,string shinhthuc,string slloaiphong)
        {
            return phieuthue.tinh_tien(ma,shinhthuc, slloaiphong);
        }
        public PhieuThue Tim_Phieu_Thue_Theo_Ma(string ma)
        {
            return phieuthue.Tim_Phieu_Thue_Theo_Ma(ma);
        }
        public void Them_Phieu_Thue(PhieuThue phieu)
        {
            phieuthue.Them_Phieu_Thue(phieu);
        }
        public void Cap_Nhat_Ngay_Tra(string maphieu)
        {
            phieuthue.Cap_Nhat_Ngay_Tra(maphieu);
        }
        public TimeSpan Gio_Da_Thue(string maphieu)
        {
            return phieuthue.Gio_Da_Thue(maphieu);
        }
        public void Cap_Nhat_Phieu_Thue(PhieuThue phieu)
        {
            phieuthue.Cap_Nhat_Phieu_Thue(phieu);
        }
        public static DataTable getTable()
        {
            return PhieuThueDAO.getTable();
        }
        public static List<PhieuThue> LoadPhieuTheoMP(string map)
        {
            return PhieuThueDAO.LoadPhieuTheoMP(map);
        }
        public static void XoaPhieu(string maph)
        {
             PhieuThueDAO.XoaPhieu(maph);
        }
        public static List<PhieuThue> LoadPhieuThue()
        {
            return PhieuThueDAO.LoadPhieuThue();
        }
    }
}
