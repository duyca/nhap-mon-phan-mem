﻿using QL_KS.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_KS.BUS
{
    class LoaiNhanVienBUS
    {
        LoaiNhanVienDAO nvDao = new LoaiNhanVienDAO();
        public List<LoaiNhanVien> LoadDS()
        {
            return nvDao.LoadDS();
        }
        public String TaoMa()
        {
            return nvDao.TaoMa();
        }
        public LoaiNhanVien TimKiem(string ma)
        {
            return nvDao.TimKiem(ma);
        }
        public int Them(LoaiNhanVien lnv)
        {
            return nvDao.Them(lnv);
        }

        public bool Sua(LoaiNhanVien nv)
        {
            return nvDao.Sua(nv);
        }
        public int Xoa(string ma)
        {
            return nvDao.Xoa(ma);
        }
    }
}
