﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;

namespace QL_KS
{
    public partial class frmLoaiKH : DevComponents.DotNetBar.Metro.MetroForm
    {
        public frmLoaiKH()
        {
            InitializeComponent();
        }

        public static string id = "";

        private void frmLoaiKH_Load(object sender, EventArgs e)
        {
            LoadLKH();

            txtLKH.Text = gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[1]).ToString();
            txtHeSo.Text = gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[2]).ToString();
        }

        private void LoadLKH()
        {
            var ds = LoaiKHBUS.LoadLKH();
            gcDSLKH.DataSource = ds;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            id = gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString();

            int convert = 0;
            if (Int32.TryParse(id, out convert))
            {
                bool kq = LoaiKHBUS.Delete(convert);
                if (kq == true)
                {
                    MessageBox.Show("Xóa thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    LoadLKH();
                }
                else
                {
                    MessageBox.Show("Loại khách hàng đã tồn tại trong hệ thống, không thể xóa.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

        }

        private void txtHeSo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            txtLKH.Text = "";
            txtHeSo.Text = "";
            btnCapNhat.Enabled = false;
            btnThem.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtLKH.Text == "" || txtHeSo.Text == "")
            {
                MessageBox.Show("Nhập đầy đủ các thông tin bắt buộc.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else if (txtLKH.Text != "" && txtHeSo.Text != "")
            {
                LoaiKhachHang l = new LoaiKhachHang();
                l.TenLoaiKH = txtLKH.Text;
                l.HeSo = Convert.ToInt32(txtHeSo.Text);

                bool kq = LoaiKHBUS.Insert(l);
                if (kq == true)
                {
                    MessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtLKH.Text = "";
                    txtHeSo.Text = "";

                    btnThem.Enabled = true;
                    btnCapNhat.Enabled = true;
                    btnXoa.Enabled = true;
                    LoadLKH();
                }
                else
                {
                    MessageBox.Show("Lưu thất bại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (txtLKH.Text == "" || txtHeSo.Text == "")
            {
                MessageBox.Show("Nhập đầy đủ các thông tin bắt buộc.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else if (txtLKH.Text != "" && txtHeSo.Text != "")
            {
                LoaiKhachHang l = new LoaiKhachHang();
                l.MaLoaiKH = Convert.ToInt32(gvData.GetRowCellValue(gvData.FocusedRowHandle, gvData.Columns[0]).ToString());
                l.TenLoaiKH = txtLKH.Text;
                l.HeSo = Convert.ToInt32(txtHeSo.Text);

                bool kq = LoaiKHBUS.Update(l);
                if (kq == true)
                {
                    MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    LoadLKH();

                    btnLuu.Enabled = false; 
                }
                else
                {
                    MessageBox.Show("Cập nhật thất bại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void gvData_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            txtLKH.EditValue = gvData.GetRowCellValue(e.FocusedRowHandle, "TenLoaiKH");
            txtHeSo.EditValue = gvData.GetRowCellValue(e.FocusedRowHandle, "HeSo");
        }
    }
}