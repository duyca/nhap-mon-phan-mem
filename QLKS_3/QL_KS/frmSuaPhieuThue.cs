﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using QL_KS.BUS;
using DevExpress.XtraEditors;
using log4net;

namespace QL_KS
{
    public partial class frmSuaPhieuThue : DevComponents.DotNetBar.Metro.MetroForm
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(frmSuaPhieuThue).Name);
        public frmSuaPhieuThue()
        {
            InitializeComponent();
        }

        public int SLTD, SLNHT;
        public string MaPhieuThue, maphong;
        public string ngayden, ngaydi, makh, TenDN;
        DateTime maxngaydi;



        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        PhieuThueBUS busphieuthue = new PhieuThueBUS();
        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (dtngdi.Enabled == true)
            {
                if (dtngdi.Value >= dtngayden.Value && dtngdi.Value >= DateTime.Now)
                {
                    if (lbGHG.Text != "")
                    {
                        if (dtngdi.Value <= maxngaydi)
                        {
                            PhieuThue p = new PhieuThue();
                            p.MaPhieuThue = MaPhieuThue;
                            p.MaPhong = maphong;
                            p.NgayBatDauThue = dtngayden.Value;
                            p.NgayTraPhong = dtngdi.Value;
                            p.MaKH = cbxKhachHang.GetColumnValue("MaKH").ToString().Trim();
                            p.SoNguoiThue = int.Parse(cbxsoluong.SelectedValue.ToString());
                            try
                            {
                                busphieuthue.Cap_Nhat_Phieu_Thue(p);
                                XtraMessageBox.Show("Cập nhật thành công!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                LuuNhatKyHT.Them(TenDN, "Phiếu Thuê", "Sửa phiếu " + p.MaPhieuThue.ToString());
                                this.Close();
                            }
                            catch (Exception ex)
                            {
                                XtraMessageBox.Show("Cập nhật không thành công!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                _logger.Error(ex.ToString());
                            }
                        }
                        else
                            XtraMessageBox.Show("ngày đi <" + maxngaydi.ToString(), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        PhieuThue p = new PhieuThue();
                        p.MaPhieuThue = MaPhieuThue;
                        p.MaPhong = maphong;
                        p.NgayBatDauThue = dtngayden.Value;
                        p.NgayTraPhong = dtngdi.Value;
                        p.MaKH = cbxKhachHang.GetColumnValue("MaKH").ToString().Trim();
                        p.SoNguoiThue = int.Parse(cbxsoluong.SelectedValue.ToString());
                        try
                        {
                            busphieuthue.Cap_Nhat_Phieu_Thue(p);
                            XtraMessageBox.Show("Cập nhật thành công!", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LuuNhatKyHT.Them(TenDN, "Phiếu Thuê", "Sửa phiếu " + p.MaPhieuThue.ToString());
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("Cập nhật không thành công!", "Báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            _logger.Error(ex.ToString());
                        }
                    }

                }
                else
                    XtraMessageBox.Show("Ngày đi phải lớn hơn ngày đến và ngày hiện tại", "Báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                PhieuThue p = new PhieuThue();
                p.MaPhieuThue = MaPhieuThue;
                p.MaPhong = maphong;
                p.NgayBatDauThue = dtngayden.Value;
                p.MaKH = cbxKhachHang.GetColumnValue("MaKH").ToString().Trim();
                p.SoNguoiThue = int.Parse(cbxsoluong.SelectedValue.ToString());
                try
                {
                    busphieuthue.Cap_Nhat_Phieu_Thue(p);
                    XtraMessageBox.Show("Cập nhật thành công!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LuuNhatKyHT.Them(TenDN, "Phiếu Thuê", "Sửa phiếu " + p.MaPhieuThue.ToString());
                    this.Close();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Cập nhật không thành công!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _logger.Error(ex.ToString());
                }
            }
        }

        private void frmSuaPhieuThue_Load(object sender, EventArgs e)
        {
            //    dtngayden.CustomFormat = "hh:mm:ss tt dd/MM/yyyy";
            //    dtngdi.CustomFormat = "hh:mm:ss tt dd/MM/yyyy";
            //    dtngayden.Value = DateTime.Parse(ngayden);
            //    DataTable dtkh = KhachHangBus.getTable();
            //    cbxKhachHang.Properties.DataSource = dtkh;
            //    for (int i = 0; i < dtkh.Rows.Count; i++)
            //    {
            //        if (dtkh.Rows[i]["MaKH"].ToString() == makh)
            //            cbxKhachHang.ItemIndex = i;
            //    }

            //    List<Phong> ph = Phong_BUS.TimTheoMaPhong(maphong);
            //    SLTD = ph[0].SoNguoiToiDa.Value;
            //    List<int> sl = new List<int>();
            //    for (int i = 1; i <= SLTD; i++)
            //    {
            //        sl.Add(i);
            //    }
            //    cbxsoluong.DataSource = sl;
            //    for (int i = 0; i < sl.Count(); i++)
            //    {
            //        if (sl[i] == SLNHT)
            //            cbxsoluong.SelectedIndex = i;
            //    }
            //    if (ngaydi != "")
            //    {
            //        dtngdi.Value = DateTime.Parse(ngaydi);
            //        List<PhieuThue> list = PhieuThueBUS.LoadPhieuThue();
            //        for (int i = 0; i < list.Count() - 1; i++)
            //        {
            //            if (list[i].MaPhieuThue == MaPhieuThue)
            //            {
            //                var dt = list[i + 1].NgayBatDauThue - list[i].NgayTraPhong;
            //                double giotrong = Math.Round(double.Parse(dt.Value.TotalMinutes.ToString()), 1);
            //                if (giotrong >= 30 || list[i].NgayTraPhong > DateTime.Now)
            //                {
            //                    maxngaydi = list[i].NgayTraPhong.Value.AddMinutes(giotrong - 15);
            //                    lbGHG.Text = "Ngày Đi < " + maxngaydi.ToString();
            //                    break;
            //                }

            //                else
            //                    dtngdi.Enabled = false;
            //            }
            //        }

            //    }
            //    else
            //        dtngdi.Enabled = false;
            //}
           
        }
    }
}
