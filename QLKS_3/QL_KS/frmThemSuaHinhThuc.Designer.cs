﻿namespace QL_KS
{
    partial class frmThemSuaHinhThuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmThemSuaHinhThuc));
            this.btnHuyBo = new System.Windows.Forms.Button();
            this.btnTraPhong = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbGioKetThuc = new System.Windows.Forms.RadioButton();
            this.rbKeoDai = new System.Windows.Forms.RadioButton();
            this.txtGioKetThuc = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.label4 = new System.Windows.Forms.Label();
            this.lbThongBao_ThoiGian = new System.Windows.Forms.Label();
            this.lbThongBao_Ten = new System.Windows.Forms.Label();
            this.txtGioKeoDai = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtTenHinhThuc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaLoaiPhong = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnHuyBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHuyBo.FlatAppearance.BorderSize = 0;
            this.btnHuyBo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHuyBo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo.ForeColor = System.Drawing.Color.Crimson;
            this.btnHuyBo.Location = new System.Drawing.Point(567, 293);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(110, 40);
            this.btnHuyBo.TabIndex = 21;
            this.btnHuyBo.Text = "Hủy Bỏ";
            this.btnHuyBo.UseVisualStyleBackColor = false;
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnTraPhong
            // 
            this.btnTraPhong.BackColor = System.Drawing.Color.MediumTurquoise;
            this.btnTraPhong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTraPhong.FlatAppearance.BorderSize = 0;
            this.btnTraPhong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraPhong.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraPhong.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.btnTraPhong.Location = new System.Drawing.Point(451, 293);
            this.btnTraPhong.Name = "btnTraPhong";
            this.btnTraPhong.Size = new System.Drawing.Size(110, 40);
            this.btnTraPhong.TabIndex = 20;
            this.btnTraPhong.Text = "Thêm Mới";
            this.btnTraPhong.UseVisualStyleBackColor = false;
            this.btnTraPhong.Click += new System.EventHandler(this.btnTraPhong_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label6.Location = new System.Drawing.Point(239, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(208, 22);
            this.label6.TabIndex = 19;
            this.label6.Text = "Thông Tin Hình Thức";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.rbGioKetThuc);
            this.panel1.Controls.Add(this.rbKeoDai);
            this.panel1.Controls.Add(this.txtGioKetThuc);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lbThongBao_ThoiGian);
            this.panel1.Controls.Add(this.lbThongBao_Ten);
            this.panel1.Controls.Add(this.txtGioKeoDai);
            this.panel1.Controls.Add(this.txtTenHinhThuc);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtMaLoaiPhong);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(689, 208);
            this.panel1.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(62, 187);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(488, 15);
            this.label9.TabIndex = 55;
            this.label9.Text = "Chọn \"Giờ Kết Thúc: \" Để định nghĩa giờ kết thúc một đơn vị hình thức vào ngày hô" +
    "m sau";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(62, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(424, 15);
            this.label8.TabIndex = 54;
            this.label8.Text = "Chọn \"Kéo Dài Trong: \" Để định nghĩa số giờ tồn tại của một đơn vị hình thức";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 53;
            this.label5.Text = "Lưu ý:";
            // 
            // rbGioKetThuc
            // 
            this.rbGioKetThuc.AutoSize = true;
            this.rbGioKetThuc.Location = new System.Drawing.Point(322, 130);
            this.rbGioKetThuc.Name = "rbGioKetThuc";
            this.rbGioKetThuc.Size = new System.Drawing.Size(14, 13);
            this.rbGioKetThuc.TabIndex = 52;
            this.rbGioKetThuc.UseVisualStyleBackColor = true;
            this.rbGioKetThuc.CheckedChanged += new System.EventHandler(this.rbGioKetThuc_CheckedChanged);
            // 
            // rbKeoDai
            // 
            this.rbKeoDai.AutoSize = true;
            this.rbKeoDai.Checked = true;
            this.rbKeoDai.Location = new System.Drawing.Point(322, 90);
            this.rbKeoDai.Name = "rbKeoDai";
            this.rbKeoDai.Size = new System.Drawing.Size(14, 13);
            this.rbKeoDai.TabIndex = 51;
            this.rbKeoDai.TabStop = true;
            this.rbKeoDai.UseVisualStyleBackColor = true;
            this.rbKeoDai.CheckedChanged += new System.EventHandler(this.rbKeoDai_CheckedChanged);
            // 
            // txtGioKetThuc
            // 
            // 
            // 
            // 
            this.txtGioKetThuc.BackgroundStyle.Class = "TextBoxBorder";
            this.txtGioKetThuc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGioKetThuc.ButtonClear.Visible = true;
            this.txtGioKetThuc.Enabled = false;
            this.txtGioKetThuc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGioKetThuc.Location = new System.Drawing.Point(106, 126);
            this.txtGioKetThuc.Mask = "00";
            this.txtGioKetThuc.Name = "txtGioKetThuc";
            this.txtGioKetThuc.Size = new System.Drawing.Size(210, 20);
            this.txtGioKetThuc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtGioKetThuc.TabIndex = 50;
            this.txtGioKetThuc.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 15);
            this.label4.TabIndex = 49;
            this.label4.Text = "Giờ Kết Thúc";
            // 
            // lbThongBao_ThoiGian
            // 
            this.lbThongBao_ThoiGian.AutoSize = true;
            this.lbThongBao_ThoiGian.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_ThoiGian.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_ThoiGian.Location = new System.Drawing.Point(103, 67);
            this.lbThongBao_ThoiGian.Name = "lbThongBao_ThoiGian";
            this.lbThongBao_ThoiGian.Size = new System.Drawing.Size(178, 16);
            this.lbThongBao_ThoiGian.TabIndex = 48;
            this.lbThongBao_ThoiGian.Text = "Hãy nhập vào một trong hai ô dưới";
            this.lbThongBao_ThoiGian.Visible = false;
            // 
            // lbThongBao_Ten
            // 
            this.lbThongBao_Ten.AutoSize = true;
            this.lbThongBao_Ten.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbThongBao_Ten.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbThongBao_Ten.Location = new System.Drawing.Point(452, 22);
            this.lbThongBao_Ten.Name = "lbThongBao_Ten";
            this.lbThongBao_Ten.Size = new System.Drawing.Size(144, 16);
            this.lbThongBao_Ten.TabIndex = 47;
            this.lbThongBao_Ten.Text = "Hãy nhập vào tên hình thức:";
            this.lbThongBao_Ten.Visible = false;
            // 
            // txtGioKeoDai
            // 
            // 
            // 
            // 
            this.txtGioKeoDai.BackgroundStyle.Class = "TextBoxBorder";
            this.txtGioKeoDai.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGioKeoDai.ButtonClear.Visible = true;
            this.txtGioKeoDai.Enabled = false;
            this.txtGioKeoDai.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGioKeoDai.Location = new System.Drawing.Point(106, 86);
            this.txtGioKeoDai.Mask = "0000";
            this.txtGioKeoDai.Name = "txtGioKeoDai";
            this.txtGioKeoDai.Size = new System.Drawing.Size(210, 20);
            this.txtGioKeoDai.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtGioKeoDai.TabIndex = 46;
            this.txtGioKeoDai.Text = "";
            // 
            // txtTenHinhThuc
            // 
            // 
            // 
            // 
            this.txtTenHinhThuc.Border.Class = "TextBoxBorder";
            this.txtTenHinhThuc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenHinhThuc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHinhThuc.Location = new System.Drawing.Point(452, 41);
            this.txtTenHinhThuc.MaxLength = 30;
            this.txtTenHinhThuc.Name = "txtTenHinhThuc";
            this.txtTenHinhThuc.PreventEnterBeep = true;
            this.txtTenHinhThuc.Size = new System.Drawing.Size(230, 22);
            this.txtTenHinhThuc.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "Kéo Dài Trong:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(352, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tên Hình Thức:";
            // 
            // txtMaLoaiPhong
            // 
            // 
            // 
            // 
            this.txtMaLoaiPhong.Border.Class = "TextBoxBorder";
            this.txtMaLoaiPhong.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaLoaiPhong.Enabled = false;
            this.txtMaLoaiPhong.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLoaiPhong.Location = new System.Drawing.Point(106, 40);
            this.txtMaLoaiPhong.Name = "txtMaLoaiPhong";
            this.txtMaLoaiPhong.PreventEnterBeep = true;
            this.txtMaLoaiPhong.Size = new System.Drawing.Size(230, 22);
            this.txtMaLoaiPhong.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mã Hình Thức:";
            // 
            // frmThemSuaHinhThuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 345);
            this.Controls.Add(this.btnHuyBo);
            this.Controls.Add(this.btnTraPhong);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmThemSuaHinhThuc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Hình Thức";
            this.Load += new System.EventHandler(this.frmThemSuaHinhThuc_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHuyBo;
        private System.Windows.Forms.Button btnTraPhong;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbThongBao_ThoiGian;
        private System.Windows.Forms.Label lbThongBao_Ten;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtGioKeoDai;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenHinhThuc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMaLoaiPhong;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtGioKetThuc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbGioKetThuc;
        private System.Windows.Forms.RadioButton rbKeoDai;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
    }
}