USE [master]
GO
/****** Object:  Database [QLKhachSan]    Script Date: 5/25/2017 11:37:35 PM ******/
CREATE DATABASE [QLKhachSan]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLKhachSan', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLKhachSan.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLKhachSan_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLKhachSan_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLKhachSan] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLKhachSan].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLKhachSan] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLKhachSan] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLKhachSan] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLKhachSan] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLKhachSan] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLKhachSan] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLKhachSan] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLKhachSan] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLKhachSan] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLKhachSan] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLKhachSan] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLKhachSan] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLKhachSan] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLKhachSan] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLKhachSan] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLKhachSan] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLKhachSan] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLKhachSan] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLKhachSan] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLKhachSan] SET  MULTI_USER 
GO
ALTER DATABASE [QLKhachSan] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLKhachSan] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLKhachSan] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLKhachSan] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QLKhachSan] SET DELAYED_DURABILITY = DISABLED 
GO
USE [QLKhachSan]
GO
/****** Object:  Table [dbo].[BangPhanQuyen]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BangPhanQuyen](
	[STT] [int] IDENTITY(1,1) NOT NULL,
	[LoaiNV] [nchar](10) NULL,
	[DanhMucPhong] [bit] NULL,
	[ThayDoiQuyDinh] [bit] NULL,
	[NhatKyHeThong] [bit] NULL,
	[QuanLyLoaiPhong] [bit] NULL,
	[QuanLyKhachHang] [bit] NULL,
	[QuanLyLoaiKhachHang] [bit] NULL,
	[QuanLyNhanVien] [bit] NULL,
	[QuanLyLoaiNV] [bit] NULL,
	[SaoLuuDL] [bit] NULL,
	[PhucHoiDL] [bit] NULL,
	[PhanQuyen] [bit] NULL,
	[HinhThucThue] [bit] NULL,
 CONSTRAINT [PK_BangPhanQuyen1] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[MaCTHD] [nchar](10) NOT NULL,
	[MaHD] [nchar](10) NULL,
	[MaPhong] [nchar](10) NULL,
	[NgayDen] [datetime] NULL,
	[NgayDi] [datetime] NULL,
	[PhuThu] [float] NULL,
	[DonGia] [float] NULL,
	[ThanhTien] [float] NULL,
 CONSTRAINT [PK_ChiTietHoaDon] PRIMARY KEY CLUSTERED 
(
	[MaCTHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HinhThuc]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HinhThuc](
	[MaHinhThuc] [nchar](10) NOT NULL,
	[TenHinhThuc] [nvarchar](50) NULL,
	[ThoiGianKeoDai] [float] NULL CONSTRAINT [DF_HinhThuc_ThoiGianKeoDai]  DEFAULT ((0)),
	[GioKetThuc] [int] NULL CONSTRAINT [DF_HinhThuc_GioKetThuc]  DEFAULT ((12)),
	[active] [bit] NULL CONSTRAINT [DF_HinhThuc_active]  DEFAULT ((1)),
 CONSTRAINT [PK_HinhThuc] PRIMARY KEY CLUSTERED 
(
	[MaHinhThuc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HinhThuc_LoaiPhong]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HinhThuc_LoaiPhong](
	[MaLoaiPhong] [nchar](10) NOT NULL,
	[MaHinhThuc] [nchar](10) NOT NULL,
	[DonGia] [float] NULL,
	[TiepTheo] [float] NULL,
 CONSTRAINT [PK_HinhThuc_LoaiPhong] PRIMARY KEY CLUSTERED 
(
	[MaLoaiPhong] ASC,
	[MaHinhThuc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDon](
	[MaHD] [nchar](10) NOT NULL,
	[MaKH] [nchar](10) NULL,
	[SoGioThue] [float] NULL,
	[TongTien] [float] NULL,
	[NgayThanhToan] [datetime] NULL,
	[MaNV] [nchar](10) NULL,
	[MaPhieuThue] [nchar](10) NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[MaHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[MaKH] [nchar](10) NOT NULL,
	[TenKH] [nvarchar](50) NULL,
	[CMND] [nchar](15) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[SDT] [nchar](15) NULL,
	[LoaiKH] [int] NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiKhachHang]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiKhachHang](
	[MaLoaiKH] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiKH] [nvarchar](50) NULL,
	[HeSo] [float] NULL,
 CONSTRAINT [PK_LoaiKhachHang] PRIMARY KEY CLUSTERED 
(
	[MaLoaiKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiNhanVien]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiNhanVien](
	[MaLoaiNV] [nchar](10) NOT NULL,
	[TenLoaiNV] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiNhanVien] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiPhong]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiPhong](
	[MaLoaiPhong] [nchar](10) NOT NULL,
	[TenLoaiPhong] [nvarchar](50) NULL,
	[TienQuaGio] [float] NULL,
	[active] [bit] NULL CONSTRAINT [DF_LoaiPhong_active]  DEFAULT ((1)),
 CONSTRAINT [PK_LoaiPhong] PRIMARY KEY CLUSTERED 
(
	[MaLoaiPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MaNV] [nchar](10) NOT NULL,
	[TenNV] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[NgaySinh] [date] NULL,
	[CMND] [nchar](15) NULL,
	[SDT] [nchar](15) NULL,
	[TaiKhoan] [nchar](50) NULL,
	[MatKhau] [nchar](50) NULL,
	[LoaiNV] [nchar](10) NULL,
	[Email] [nchar](50) NULL,
	[ConQuanLy] [bit] NULL,
	[GioiTinh] [int] NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhatKyHoatDong]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhatKyHoatDong](
	[STT] [int] IDENTITY(1,1) NOT NULL,
	[ID] [nchar](50) NULL,
	[TenMayTinh] [nvarchar](100) NULL,
	[ThoiGian] [datetime] NULL,
	[ChucNang] [nvarchar](100) NULL,
	[HanhDong] [nvarchar](100) NULL,
 CONSTRAINT [PK_NhatKyHoatDong] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhieuThue]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuThue](
	[MaPhieuThue] [nchar](10) NOT NULL,
	[MaPhong] [nchar](10) NULL,
	[MaKH] [nchar](10) NULL,
	[NgayBatDauThue] [datetime] NULL,
	[NgayTraPhong] [datetime] NULL,
	[SoNguoiThue] [int] NULL,
	[TienChuyenPhong] [float] NULL,
	[MaHinhThuc] [nchar](10) NULL,
 CONSTRAINT [PK_PhieuThue] PRIMARY KEY CLUSTERED 
(
	[MaPhieuThue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Phong]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Phong](
	[MaPhong] [nchar](10) NOT NULL,
	[TenPhong] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](200) NULL,
	[TinhTrang] [nchar](10) NULL,
	[SoNguoiToiDa] [int] NULL,
	[LoaiPhong] [nchar](10) NULL,
	[active] [bit] NULL CONSTRAINT [DF_Phong_active]  DEFAULT ((1)),
 CONSTRAINT [PK_Phong] PRIMARY KEY CLUSTERED 
(
	[MaPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuyDinh]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyDinh](
	[STT] [int] NOT NULL,
	[TenQuyDinh] [nvarchar](50) NULL,
	[GiaTri] [int] NULL,
 CONSTRAINT [PK_QuyDinh] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TinhTrang]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TinhTrang](
	[MATINHTRANG] [nchar](10) NOT NULL,
	[TENTINHTRANG] [nvarchar](50) NULL,
 CONSTRAINT [PK_TinhTrang] PRIMARY KEY CLUSTERED 
(
	[MATINHTRANG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[VW_BCDT]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_BCDT]
AS
SELECT p.MaPhieuThue, k.TenKH, ph.TenPhong, l.TenLoaiPhong, h.SoGioThue, h.TongTien, h.NgayThanhToan
FROM HoaDon h, KhachHang k, PhieuThue p, Phong ph, LoaiPhong l
WHERE h.MaKH = k.MaKH
AND h.MaPhieuThue = p.MaPhieuThue
AND p.MaPhong = ph.MaPhong
AND ph.LoaiPhong = l.MaLoaiPhong
GO
/****** Object:  View [dbo].[VW_GRAPH]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_GRAPH]
AS
SELECT l.TenLoaiPhong,h.TongTien
FROM HoaDon h, KhachHang k, PhieuThue p, Phong ph, LoaiPhong l
WHERE h.MaKH = k.MaKH
AND h.MaPhieuThue = p.MaPhieuThue
AND p.MaPhong = ph.MaPhong
AND ph.LoaiPhong = l.MaLoaiPhong
GO
/****** Object:  View [dbo].[VW_MDSD]    Script Date: 5/25/2017 11:37:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_MDSD]
AS
SELECT p.MaPhong, ph.TenPhong, COUNT(*) AS SL
FROM PhieuThue p, Phong ph
WHERE p.MaPhong = ph.MaPhong
GROUP BY p.MaPhong, ph.TenPhong
GO
SET IDENTITY_INSERT [dbo].[BangPhanQuyen] ON 

INSERT [dbo].[BangPhanQuyen] ([STT], [LoaiNV], [DanhMucPhong], [ThayDoiQuyDinh], [NhatKyHeThong], [QuanLyLoaiPhong], [QuanLyKhachHang], [QuanLyLoaiKhachHang], [QuanLyNhanVien], [QuanLyLoaiNV], [SaoLuuDL], [PhucHoiDL], [PhanQuyen], [HinhThucThue]) VALUES (2, N'MLNV000001', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
INSERT [dbo].[BangPhanQuyen] ([STT], [LoaiNV], [DanhMucPhong], [ThayDoiQuyDinh], [NhatKyHeThong], [QuanLyLoaiPhong], [QuanLyKhachHang], [QuanLyLoaiKhachHang], [QuanLyNhanVien], [QuanLyLoaiNV], [SaoLuuDL], [PhucHoiDL], [PhanQuyen], [HinhThucThue]) VALUES (3, N'MLNV000002', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[BangPhanQuyen] OFF
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaPhong], [NgayDen], [NgayDi], [PhuThu], [DonGia], [ThanhTien]) VALUES (N'MCTHD00001', N'MHD0000001', N'MP00000001', CAST(N'2016-03-26 00:00:00.000' AS DateTime), CAST(N'2016-03-27 00:00:00.000' AS DateTime), 30000, 30000, 30000)
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaPhong], [NgayDen], [NgayDi], [PhuThu], [DonGia], [ThanhTien]) VALUES (N'MCTHD00002', N'MHD0000001', N'MP00000001', CAST(N'2016-03-26 00:00:00.000' AS DateTime), NULL, 600000, NULL, 600000)
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaPhong], [NgayDen], [NgayDi], [PhuThu], [DonGia], [ThanhTien]) VALUES (N'MCTHD00003', N'MHD0000003', N'MP00000002', CAST(N'2017-05-22 09:34:03.747' AS DateTime), NULL, 0, NULL, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaPhong], [NgayDen], [NgayDi], [PhuThu], [DonGia], [ThanhTien]) VALUES (N'MCTHD00004', N'MHD0000004', N'MP00000002', CAST(N'2017-05-22 09:35:22.327' AS DateTime), NULL, 0, NULL, 0)
INSERT [dbo].[HinhThuc] ([MaHinhThuc], [TenHinhThuc], [ThoiGianKeoDai], [GioKetThuc], [active]) VALUES (N'MHT0000001', N'thuê theo giờ', 1, NULL, 1)
INSERT [dbo].[HinhThuc] ([MaHinhThuc], [TenHinhThuc], [ThoiGianKeoDai], [GioKetThuc], [active]) VALUES (N'MHT0000002', N'Qua Đêm', NULL, 12, 1)
INSERT [dbo].[HinhThuc] ([MaHinhThuc], [TenHinhThuc], [ThoiGianKeoDai], [GioKetThuc], [active]) VALUES (N'MHT0000003', N'Theo Ngày', 24, NULL, 1)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000001', N'MHT0000001', 50000, 10000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000001', N'MHT0000002', 150000, 150000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000001', N'MHT0000003', 250000, 250000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000002', N'MHT0000001', 60000, 10000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000002', N'MHT0000002', 200000, 200000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000002', N'MHT0000003', 300000, 300000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000003', N'MHT0000001', 80000, 20000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000003', N'MHT0000002', 220000, 220000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000003', N'MHT0000003', 350000, 350000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000004', N'MHT0000001', 100000, 30000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000004', N'MHT0000002', 300000, 300000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia], [TiepTheo]) VALUES (N'MLP0000004', N'MHT0000003', 400000, 400000)
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [SoGioThue], [TongTien], [NgayThanhToan], [MaNV], [MaPhieuThue]) VALUES (N'MHD0000001', N'MKH0000001', 24, 9000000, CAST(N'2016-08-08 00:00:00.000' AS DateTime), N'MNV0000001', N'MPT0000001')
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [SoGioThue], [TongTien], [NgayThanhToan], [MaNV], [MaPhieuThue]) VALUES (N'MHD0000002', N'MKH0000005', 0, 200000, CAST(N'2017-05-21 19:39:10.217' AS DateTime), NULL, N'MPT0000005')
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [SoGioThue], [TongTien], [NgayThanhToan], [MaNV], [MaPhieuThue]) VALUES (N'MHD0000003', N'MKH0000006', 0, 60000, CAST(N'2017-05-22 09:34:05.960' AS DateTime), NULL, N'MPT0000006')
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [SoGioThue], [TongTien], [NgayThanhToan], [MaNV], [MaPhieuThue]) VALUES (N'MHD0000004', N'MKH0000003', 4, 100000, CAST(N'2017-05-22 14:29:01.660' AS DateTime), N'MNV0000002', N'MPT0000007')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000001', N'Anh Tùng', N'225674545      ', N'hồ chí minh', N'113            ', 1)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000002', N'Ca', N'344234354      ', N'american', N'114            ', 2)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000003', N'Hiền', N'434342333      ', N'tân bình', N'115            ', 1)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000004', N'Trang', N'3234343553     ', N'hoa kỳ', N'116            ', 2)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000005', N'van c', N'               ', N'', N'               ', 1)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000006', N'van c', N'               ', N'', N'               ', 1)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000007', N'nguyen v', N'               ', N'', N'               ', 1)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000008', N'hhhh', N'               ', N'', N'               ', 1)
SET IDENTITY_INSERT [dbo].[LoaiKhachHang] ON 

INSERT [dbo].[LoaiKhachHang] ([MaLoaiKH], [TenLoaiKH], [HeSo]) VALUES (1, N'nội địa', 1)
INSERT [dbo].[LoaiKhachHang] ([MaLoaiKH], [TenLoaiKH], [HeSo]) VALUES (2, N'ngoại quốc', 2)
SET IDENTITY_INSERT [dbo].[LoaiKhachHang] OFF
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoaiNV]) VALUES (N'MLNV000001', N'nhân viên')
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoaiNV]) VALUES (N'MLNV000002', N'quản lý')
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [TienQuaGio], [active]) VALUES (N'MLP0000001', N'Đơn ', 20000, 1)
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [TienQuaGio], [active]) VALUES (N'MLP0000002', N'Đôi', 10000, 1)
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [TienQuaGio], [active]) VALUES (N'MLP0000003', N'Gia Đình', 20000, 1)
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [TienQuaGio], [active]) VALUES (N'MLP0000004', N'Vip', 30000, 1)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy], [GioiTinh]) VALUES (N'MNV0000001', N'Tùng', N'sài gòn', CAST(N'1994-01-01' AS Date), N'4345435        ', N'016872344      ', N'tung                                              ', N'bb7d4b236b564cf1ec27aa891331e0af                  ', N'MLNV000001', N'tung@gmail.com                                    ', 0, 0)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy], [GioiTinh]) VALUES (N'MNV0000002', N'Ca', N'hồ chí minh', CAST(N'1994-02-02' AS Date), N'876875567      ', N'064328732      ', N'ca                                                ', N'5435c69ed3bcc5b2e4d580e393e373d3                  ', N'MLNV000002', N'ca@gmail.com                                      ', 1, 0)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy], [GioiTinh]) VALUES (N'MNV0000003', N'Trang', N'gia định', CAST(N'1994-03-03' AS Date), N'987967655      ', N'019873684      ', N'trang                                             ', N'c786f96bb9eef05cd4c8641fc7cdeb24                  ', N'MLNV000001', N'trang@gmail.com                                   ', 1, 1)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy], [GioiTinh]) VALUES (N'MNV0000004', N'Xuân Hiền', N'62 Trương Phước Phan, khu phố 7, phường Bình Trị Đông, quận Bình Tân', CAST(N'1994-04-04' AS Date), N'025123002      ', N'0934190085     ', N'hien                                              ', N'380a767a3eb890d7f177538fabd023d6                  ', N'MLNV000002', N'1642017@gmail.com                                 ', 1, 1)
SET IDENTITY_INSERT [dbo].[NhatKyHoatDong] ON 

INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (10, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 11:36:39.767' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (11, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 11:39:32.507' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (12, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 11:44:50.040' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (13, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 11:45:27.787' AS DateTime), N'Cơ sở dữ liệu', N'Phục hồi dữ liệu')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (14, N'trang                                             ', N'USER-VAIO', CAST(N'2017-04-07 11:45:41.853' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (15, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 11:59:59.913' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (16, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 12:10:00.780' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (17, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 12:20:27.820' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (18, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 12:27:58.810' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (19, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 12:30:01.343' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (20, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:12:34.087' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (21, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:14:19.270' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (22, NULL, N'USER-VAIO', CAST(N'2017-04-07 22:14:30.083' AS DateTime), N'Hệ Thống', N'Phân quyền')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (23, N'trang                                             ', N'USER-VAIO', CAST(N'2017-04-07 22:17:24.257' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (24, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:18:30.517' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (25, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:19:40.313' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (26, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:29:10.910' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (27, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:36:47.727' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (28, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:40:14.107' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (29, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:41:57.037' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (30, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-07 22:45:07.107' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (31, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:15:04.210' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (32, NULL, N'USER-VAIO', CAST(N'2017-04-08 22:15:32.550' AS DateTime), N'Hệ Thống', N'Phân quyền')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (33, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:17:26.400' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (34, NULL, N'USER-VAIO', CAST(N'2017-04-08 22:17:46.637' AS DateTime), N'Hệ Thống', N'Phân quyền')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (35, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:22:15.160' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (36, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:42:09.083' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (37, N'trang                                             ', N'USER-VAIO', CAST(N'2017-04-08 22:43:36.777' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (38, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:43:45.353' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (39, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:48:18.097' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (40, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:49:46.750' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (41, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:55:36.817' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (42, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 22:58:04.990' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (43, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 23:01:15.500' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (44, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 23:04:39.843' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (45, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 23:05:50.640' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (46, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 23:06:48.207' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (47, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 23:08:31.530' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (48, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 23:10:21.547' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (49, N'hien                                              ', N'USER-VAIO', CAST(N'2017-04-08 23:10:55.513' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (50, N'ca                                                ', N'KAKA', CAST(N'2017-04-16 03:06:26.160' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (51, N'ca                                                ', N'KAKA', CAST(N'2017-04-16 12:28:09.710' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (52, N'ca                                                ', N'KAKA', CAST(N'2017-04-16 20:46:55.077' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (53, N'ca                                                ', N'KAKA', CAST(N'2017-04-16 21:24:20.790' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (54, N'ca                                                ', N'KAKA', CAST(N'2017-04-16 22:27:59.107' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (55, N'ca                                                ', N'KAKA', CAST(N'2017-04-19 21:14:45.573' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1055, N'ca                                                ', N'KAKA', CAST(N'2017-05-03 01:08:42.253' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1056, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 11:57:41.750' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1057, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 11:58:56.880' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1058, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 12:00:36.800' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1059, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 16:01:28.497' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1060, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 16:03:49.993' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1061, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 16:04:32.243' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1062, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 16:05:55.847' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1063, N'ca                                                ', N'KAKA', CAST(N'2017-05-04 16:06:35.587' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1064, N'ca                                                ', N'CA', CAST(N'2017-05-06 18:47:33.900' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1065, N'ca                                                ', N'CA', CAST(N'2017-05-06 18:48:15.317' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1066, N'ca                                                ', N'CA', CAST(N'2017-05-06 18:49:15.423' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1067, N'ca                                                ', N'CA', CAST(N'2017-05-06 18:50:17.417' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1068, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:35:12.497' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1069, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:37:28.753' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1070, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:43:39.333' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1071, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:44:43.957' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1072, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:47:24.440' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1073, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:50:19.973' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1074, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:50:58.007' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1075, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:53:07.593' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1076, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:55:47.090' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1077, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:56:31.927' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1078, N'ca                                                ', N'CA', CAST(N'2017-05-21 19:58:55.747' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1079, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:01:54.000' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1080, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:02:26.117' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1081, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:03:09.893' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1082, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:04:04.860' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1083, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:04:49.257' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1084, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:06:10.593' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1085, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:07:00.570' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1086, N'ca                                                ', N'CA', CAST(N'2017-05-21 20:07:21.150' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1087, N'ca                                                ', N'CA', CAST(N'2017-05-22 09:22:44.540' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1088, N'ca                                                ', N'CA', CAST(N'2017-05-22 09:33:55.970' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1089, N'ca                                                ', N'CA', CAST(N'2017-05-22 09:35:11.167' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1090, N'ca                                                ', N'CA', CAST(N'2017-05-22 14:28:42.273' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1091, N'ca                                                ', N'CA', CAST(N'2017-05-22 14:30:13.090' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1092, N'ca                                                ', N'CA', CAST(N'2017-05-22 15:36:54.120' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1093, N'ca                                                ', N'CA', CAST(N'2017-05-22 15:39:01.547' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1094, N'hien                                              ', N'CA', CAST(N'2017-05-22 15:54:39.297' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1095, N'ca                                                ', N'CA', CAST(N'2017-05-22 15:55:11.540' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1096, N'ca                                                ', N'CA', CAST(N'2017-05-22 15:58:38.957' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1097, N'ca                                                ', N'CA', CAST(N'2017-05-22 15:58:51.027' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1098, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:01:04.703' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1099, NULL, N'CA', CAST(N'2017-05-22 16:03:13.623' AS DateTime), N'Hệ Thống', N'Phân quyền')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1100, NULL, N'CA', CAST(N'2017-05-22 16:03:20.333' AS DateTime), N'Hệ Thống', N'Phân quyền')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1101, N'hien                                              ', N'CA', CAST(N'2017-05-22 16:04:46.017' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1102, NULL, N'CA', CAST(N'2017-05-22 16:04:54.183' AS DateTime), N'Hệ Thống', N'Phân quyền')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1103, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:07:08.797' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1104, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:07:58.393' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1105, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:10:47.377' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1106, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:16:28.587' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1107, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:18:34.493' AS DateTime), N'Hệ thống', N'Đăng nhập')
GO
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1108, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:21:07.307' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1109, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:22:12.113' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1110, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:24:50.907' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1111, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:29:31.030' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1112, N'ca                                                ', N'CA', CAST(N'2017-05-22 16:30:11.597' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1113, N'ca                                                ', N'CA', CAST(N'2017-05-22 18:05:06.620' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1114, N'ca                                                ', N'CA', CAST(N'2017-05-23 19:57:40.500' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1115, N'trang                                             ', N'DESKTOP-N0ENLN6', CAST(N'2017-05-23 20:57:38.750' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1116, N'trang                                             ', N'DESKTOP-N0ENLN6', CAST(N'2017-05-23 21:51:41.000' AS DateTime), N'Hệ thống', N'Đăng nhập')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1117, N'trang                                             ', N'DESKTOP-N0ENLN6', CAST(N'2017-05-23 22:27:56.927' AS DateTime), N'Hệ thống', N'Đăng nhập')
SET IDENTITY_INSERT [dbo].[NhatKyHoatDong] OFF
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000001', N'MP00000001', N'MKH0000001', CAST(N'2016-05-05 00:00:00.000' AS DateTime), CAST(N'2016-06-06 00:00:00.000' AS DateTime), 3, 3000, N'MHT0000001')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000002', N'MP00000002', N'MKH0000002', CAST(N'2017-04-16 01:31:00.000' AS DateTime), CAST(N'2017-05-03 23:18:36.323' AS DateTime), 2, 10000, N'MHT0000002')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000003', N'MP00000002', N'MKH0000001', CAST(N'2017-04-04 00:00:00.000' AS DateTime), CAST(N'2017-04-15 00:00:00.000' AS DateTime), 1, 10000, N'MHT0000001')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000004', N'MP00000003', N'MKH0000002', CAST(N'2017-04-15 18:18:00.000' AS DateTime), CAST(N'2017-05-03 23:18:38.260' AS DateTime), 2, 0, N'MHT0000003')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000005', N'MP00000002', N'MKH0000005', CAST(N'2017-05-21 19:39:04.013' AS DateTime), CAST(N'2017-05-21 19:39:10.223' AS DateTime), 2, 0, N'MHT0000002')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000006', N'MP00000002', N'MKH0000006', CAST(N'2017-05-22 09:34:03.747' AS DateTime), CAST(N'2017-05-22 09:34:05.967' AS DateTime), 2, 0, N'MHT0000001')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000007', N'MP00000002', N'MKH0000003', CAST(N'2017-05-22 09:35:22.327' AS DateTime), CAST(N'2017-05-22 14:29:01.683' AS DateTime), 2, 0, N'MHT0000001')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000008', N'MP00000002', N'MKH0000007', CAST(N'2017-05-22 14:30:21.307' AS DateTime), NULL, 2, 0, N'MHT0000002')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000009', N'MP00000001', N'MKH0000008', CAST(N'2017-05-22 14:39:08.020' AS DateTime), NULL, 2, 20000, N'MHT0000002')
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong], [active]) VALUES (N'MP00000001', N' 001', N'phòng sạch sẽ', N'MTT0000001', 5, N'MLP0000001', 1)
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong], [active]) VALUES (N'MP00000002', N'002', N'phòng hơi sạch sẽ', N'MTT0000001', 4, N'MLP0000002', 1)
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong], [active]) VALUES (N'MP00000003', N'003', N'phòn', N'MTT0000002', 4, N'MLP0000003', 1)
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong], [active]) VALUES (N'MP00000004', N'004', N'phòng tốt ', N'MTT0000002', 4, N'MLP0000003', 1)
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong], [active]) VALUES (N'MP00000005', N'005', N'ddd', N'MTT0000002', 4, N'MLP0000003', 0)
INSERT [dbo].[QuyDinh] ([STT], [TenQuyDinh], [GiaTri]) VALUES (1, N'SoPhutThem', 15)
INSERT [dbo].[QuyDinh] ([STT], [TenQuyDinh], [GiaTri]) VALUES (2, N'TienChuyenPhong', 20000)
INSERT [dbo].[TinhTrang] ([MATINHTRANG], [TENTINHTRANG]) VALUES (N'MTT0000001', N'Đang Thuê')
INSERT [dbo].[TinhTrang] ([MATINHTRANG], [TENTINHTRANG]) VALUES (N'MTT0000002', N'Trống')
INSERT [dbo].[TinhTrang] ([MATINHTRANG], [TENTINHTRANG]) VALUES (N'MTT0000003', N'Đang Sửa Chữa')
ALTER TABLE [dbo].[BangPhanQuyen]  WITH CHECK ADD  CONSTRAINT [FK_BangPhanQuyen_LoaiNhanVien] FOREIGN KEY([LoaiNV])
REFERENCES [dbo].[LoaiNhanVien] ([MaLoaiNV])
GO
ALTER TABLE [dbo].[BangPhanQuyen] CHECK CONSTRAINT [FK_BangPhanQuyen_LoaiNhanVien]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_HoaDon] FOREIGN KEY([MaHD])
REFERENCES [dbo].[HoaDon] ([MaHD])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_HoaDon]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_Phong] FOREIGN KEY([MaPhong])
REFERENCES [dbo].[Phong] ([MaPhong])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_Phong]
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong]  WITH CHECK ADD  CONSTRAINT [FK_HinhThuc_LoaiPhong_HinhThuc] FOREIGN KEY([MaHinhThuc])
REFERENCES [dbo].[HinhThuc] ([MaHinhThuc])
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong] CHECK CONSTRAINT [FK_HinhThuc_LoaiPhong_HinhThuc]
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong]  WITH CHECK ADD  CONSTRAINT [FK_HinhThuc_LoaiPhong_LoaiPhong] FOREIGN KEY([MaLoaiPhong])
REFERENCES [dbo].[LoaiPhong] ([MaLoaiPhong])
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong] CHECK CONSTRAINT [FK_HinhThuc_LoaiPhong_LoaiPhong]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_NhanVien] FOREIGN KEY([MaNV])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_NhanVien]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_PhieuThue] FOREIGN KEY([MaPhieuThue])
REFERENCES [dbo].[PhieuThue] ([MaPhieuThue])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_PhieuThue]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_LoaiKhachHang] FOREIGN KEY([LoaiKH])
REFERENCES [dbo].[LoaiKhachHang] ([MaLoaiKH])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_LoaiKhachHang]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_LoaiNhanVien] FOREIGN KEY([LoaiNV])
REFERENCES [dbo].[LoaiNhanVien] ([MaLoaiNV])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_LoaiNhanVien]
GO
ALTER TABLE [dbo].[PhieuThue]  WITH CHECK ADD  CONSTRAINT [FK_PhieuThue_HinhThuc] FOREIGN KEY([MaHinhThuc])
REFERENCES [dbo].[HinhThuc] ([MaHinhThuc])
GO
ALTER TABLE [dbo].[PhieuThue] CHECK CONSTRAINT [FK_PhieuThue_HinhThuc]
GO
ALTER TABLE [dbo].[PhieuThue]  WITH CHECK ADD  CONSTRAINT [FK_PhieuThue_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[PhieuThue] CHECK CONSTRAINT [FK_PhieuThue_KhachHang]
GO
ALTER TABLE [dbo].[PhieuThue]  WITH CHECK ADD  CONSTRAINT [FK_PhieuThue_Phong] FOREIGN KEY([MaPhong])
REFERENCES [dbo].[Phong] ([MaPhong])
GO
ALTER TABLE [dbo].[PhieuThue] CHECK CONSTRAINT [FK_PhieuThue_Phong]
GO
ALTER TABLE [dbo].[Phong]  WITH CHECK ADD  CONSTRAINT [FK_Phong_LoaiPhong] FOREIGN KEY([LoaiPhong])
REFERENCES [dbo].[LoaiPhong] ([MaLoaiPhong])
GO
ALTER TABLE [dbo].[Phong] CHECK CONSTRAINT [FK_Phong_LoaiPhong]
GO
ALTER TABLE [dbo].[Phong]  WITH CHECK ADD  CONSTRAINT [FK_Phong_TinhTrang] FOREIGN KEY([TinhTrang])
REFERENCES [dbo].[TinhTrang] ([MATINHTRANG])
GO
ALTER TABLE [dbo].[Phong] CHECK CONSTRAINT [FK_Phong_TinhTrang]
GO
USE [master]
GO
ALTER DATABASE [QLKhachSan] SET  READ_WRITE 
GO
