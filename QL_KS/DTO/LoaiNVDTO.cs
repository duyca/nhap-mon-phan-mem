﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LoaiNVDTO
    {
        private string maloaiNV, tenloaiNV;

        public string MaloaiNV
        {
            get
            {
                return maloaiNV;
            }

            set
            {
                maloaiNV = value;
            }
        }

        public string TenloaiNV
        {
            get
            {
                return tenloaiNV;
            }

            set
            {
                tenloaiNV = value;
            }
        }
    }
}
