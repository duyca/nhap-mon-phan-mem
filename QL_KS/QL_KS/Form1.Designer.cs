﻿namespace QL_KS
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup10 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.ReduceOperation reduceOperation10 = new DevExpress.XtraBars.Ribbon.ReduceOperation();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.Danh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDTTheoNgay = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDTTheoThang = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDTTheoNam = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMatDo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.bbiLoaiNhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.Danh,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.ribbonGalleryBarItem1,
            this.skinRibbonGalleryBarItem1,
            this.barButtonItem4,
            this.bbiDTTheoNgay,
            this.barButtonItem7,
            this.bbiDTTheoThang,
            this.bbiDTTheoNam,
            this.bbiMatDo,
            this.bbiNhanVien,
            this.bbiLoaiNhanVien});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 12;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage3});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(1052, 162);
            // 
            // Danh
            // 
            this.Danh.Caption = "Danh sách khách hàng";
            this.Danh.Glyph = ((System.Drawing.Image)(resources.GetObject("Danh.Glyph")));
            this.Danh.Id = 1;
            this.Danh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("Danh.LargeGlyph")));
            this.Danh.Name = "Danh";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Tìm kiếm khách hàng";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Đổi mật khâu";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.LargeGlyph")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Đăng Xuất";
            this.barButtonItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.Glyph")));
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.LargeGlyph")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "InplaceGallery1";
            // 
            // 
            // 
            galleryItemGroup10.Caption = "Group1";
            this.ribbonGalleryBarItem1.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup10});
            this.ribbonGalleryBarItem1.Id = 1;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 2;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Báo cáo doanh thu";
            this.barButtonItem4.Id = 3;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bbiDTTheoNgay
            // 
            this.bbiDTTheoNgay.Caption = "Doanh Thu Theo Ngày";
            this.bbiDTTheoNgay.Id = 4;
            this.bbiDTTheoNgay.LargeGlyph = global::QL_KS.Properties.Resources.Dtafalonso_Android_Lollipop_Calendar;
            this.bbiDTTheoNgay.Name = "bbiDTTheoNgay";
            this.bbiDTTheoNgay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDTTheoNgay_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Thống Kê Doanh Thu";
            this.barButtonItem7.Id = 6;
            this.barButtonItem7.LargeGlyph = global::QL_KS.Properties.Resources.Aha_Soft_Large_Seo_SEO;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // bbiDTTheoThang
            // 
            this.bbiDTTheoThang.Caption = "Doanh Thu Theo Tháng";
            this.bbiDTTheoThang.Id = 7;
            this.bbiDTTheoThang.LargeGlyph = global::QL_KS.Properties.Resources.Wwalczyszyn_Android_Style_Calendar;
            this.bbiDTTheoThang.Name = "bbiDTTheoThang";
            this.bbiDTTheoThang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDTTheoThang_ItemClick);
            // 
            // bbiDTTheoNam
            // 
            this.bbiDTTheoNam.Caption = "Doanh Thu Theo Năm";
            this.bbiDTTheoNam.Id = 8;
            this.bbiDTTheoNam.LargeGlyph = global::QL_KS.Properties.Resources.Iconsmind_Outline_Calendar_4;
            this.bbiDTTheoNam.Name = "bbiDTTheoNam";
            this.bbiDTTheoNam.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDTTheoNam_ItemClick);
            // 
            // bbiMatDo
            // 
            this.bbiMatDo.Caption = "Mật độ sử dụng phòng";
            this.bbiMatDo.Id = 9;
            this.bbiMatDo.LargeGlyph = global::QL_KS.Properties.Resources.Custom_Icon_Design_Pretty_Office_9_Edit_file;
            this.bbiMatDo.Name = "bbiMatDo";
            this.bbiMatDo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMatDo_ItemClick);
            // 
            // bbiNhanVien
            // 
            this.bbiNhanVien.Caption = "Nhân Viên";
            this.bbiNhanVien.Id = 10;
            this.bbiNhanVien.LargeGlyph = global::QL_KS.Properties.Resources.Aha_Soft_Free_Large_Boss_Manager;
            this.bbiNhanVien.Name = "bbiNhanVien";
            this.bbiNhanVien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNhanVien_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3});
            this.ribbonPage1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage1.Image")));
            this.ribbonPage1.Name = "ribbonPage1";
            reduceOperation10.Behavior = DevExpress.XtraBars.Ribbon.ReduceOperationBehavior.Single;
            reduceOperation10.Group = null;
            reduceOperation10.ItemLinkIndex = 0;
            reduceOperation10.ItemLinksCount = 0;
            reduceOperation10.Operation = DevExpress.XtraBars.Ribbon.ReduceOperationType.LargeButtons;
            this.ribbonPage1.ReduceOperations.Add(reduceOperation10);
            this.ribbonPage1.Text = "Khách sạn";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Glyph = global::QL_KS.Properties.Resources.home_icon;
            this.ribbonPageGroup1.ItemLinks.Add(this.Danh);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Khách hàng";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Giao Dien";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup5,
            this.ribbonPageGroup4});
            this.ribbonPage2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage2.Image")));
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Báo Cáo - Thống Kê";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.bbiDTTheoNgay);
            this.ribbonPageGroup2.ItemLinks.Add(this.bbiDTTheoThang);
            this.ribbonPageGroup2.ItemLinks.Add(this.bbiDTTheoNam);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Báo Cáo";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.bbiMatDo);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Báo Cáo Mật Độ";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Thống Kê";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6});
            this.ribbonPage3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage3.Image")));
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Quản Lý";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.bbiNhanVien);
            this.ribbonPageGroup6.ItemLinks.Add(this.bbiLoaiNhanVien);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Quản Lý Nhân Viên";
            // 
            // bbiLoaiNhanVien
            // 
            this.bbiLoaiNhanVien.Caption = "Loại Nhân Viên";
            this.bbiLoaiNhanVien.Id = 11;
            this.bbiLoaiNhanVien.LargeGlyph = global::QL_KS.Properties.Resources.Icons_Land_Vista_People_Groups_Meeting_Dark;
            this.bbiLoaiNhanVien.Name = "bbiLoaiNhanVien";
            this.bbiLoaiNhanVien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoaiNhanVien_ItemClick);
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // frmMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 459);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbonControl1;
            this.Text = "QUẢN LÝ KHÁCH SẠN";
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.BarButtonItem Danh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem bbiDTTheoNgay;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem bbiDTTheoThang;
        private DevExpress.XtraBars.BarButtonItem bbiDTTheoNam;
        private DevExpress.XtraBars.BarButtonItem bbiMatDo;
        private DevExpress.XtraBars.BarButtonItem bbiNhanVien;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem bbiLoaiNhanVien;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
    }
}

