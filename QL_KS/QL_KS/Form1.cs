﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QL_KS
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void bbiDTTheoNgay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmBCDTTheoNgay rptDay = new frmBCDTTheoNgay();
            rptDay.MdiParent = this;
            rptDay.Show();
        }

        private void bbiDTTheoThang_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmBCDTTheoThang rptMonth = new frmBCDTTheoThang();
            rptMonth.MdiParent = this;
            rptMonth.Show();
        }

        private void bbiDTTheoNam_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmBCDTTheoNam rptYear = new frmBCDTTheoNam();
            rptYear.MdiParent = this;
            rptYear.Show();
        }

        private void bbiMatDo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmMatDoSuDung sd = new frmMatDoSuDung();
            sd.MdiParent = this;
            sd.Show();
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmThongKe tk = new frmThongKe();
            tk.MdiParent = this;
            tk.Show();
        }

        private void bbiNhanVien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmNhanVien nv = new frmNhanVien();
            nv.MdiParent = this;
            nv.Show();
        }

        private void bbiLoaiNhanVien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmLoaiNhanVien type = new frmLoaiNhanVien();
            type.MdiParent = this;
            type.Show();
        }
    }
}
