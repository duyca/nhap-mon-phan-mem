USE [master]
GO
/****** Object:  Database [QLKhachSan]    Script Date: 3/27/2017 12:34:30 AM ******/
CREATE DATABASE [QLKhachSan]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLKhachSan', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\QLKhachSan.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLKhachSan_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\QLKhachSan_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLKhachSan] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLKhachSan].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLKhachSan] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLKhachSan] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLKhachSan] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLKhachSan] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLKhachSan] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLKhachSan] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLKhachSan] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLKhachSan] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLKhachSan] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLKhachSan] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLKhachSan] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLKhachSan] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLKhachSan] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLKhachSan] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLKhachSan] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLKhachSan] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLKhachSan] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLKhachSan] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLKhachSan] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLKhachSan] SET RECOVERY FULL 
GO
ALTER DATABASE [QLKhachSan] SET  MULTI_USER 
GO
ALTER DATABASE [QLKhachSan] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLKhachSan] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLKhachSan] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLKhachSan] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QLKhachSan] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'QLKhachSan', N'ON'
GO
USE [QLKhachSan]
GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[MaCTHD] [nchar](10) NOT NULL,
	[MaHD] [nchar](10) NULL,
	[MaPhong] [nchar](10) NULL,
	[NgayDen] [datetime] NULL,
	[NgayDi] [datetime] NULL,
	[PhuThu] [float] NULL,
	[DonGia] [float] NULL,
	[ThanhTien] [float] NULL,
 CONSTRAINT [PK_ChiTietHoaDon] PRIMARY KEY CLUSTERED 
(
	[MaCTHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HinhThuc]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HinhThuc](
	[MaHinhThuc] [nchar](10) NOT NULL,
	[TenHinhThuc] [nvarchar](50) NULL,
 CONSTRAINT [PK_HinhThuc] PRIMARY KEY CLUSTERED 
(
	[MaHinhThuc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HinhThuc_LoaiPhong]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HinhThuc_LoaiPhong](
	[MaLoaiPhong] [nchar](10) NOT NULL,
	[MaHinhThuc] [nchar](10) NOT NULL,
	[DonGia] [float] NULL,
 CONSTRAINT [PK_HinhThuc_LoaiPhong] PRIMARY KEY CLUSTERED 
(
	[MaLoaiPhong] ASC,
	[MaHinhThuc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDon](
	[MaHD] [nchar](10) NOT NULL,
	[MaKH] [nchar](10) NULL,
	[SoGioThue] [float] NULL,
	[TongTien] [float] NULL,
	[NgayThanhToan] [datetime] NULL,
	[MaNV] [nchar](10) NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[MaHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[MaKH] [nchar](10) NOT NULL,
	[TenKH] [nvarchar](50) NULL,
	[CMND] [nchar](15) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[SDT] [nchar](15) NULL,
	[LoaiKH] [int] NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiKhachHang]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiKhachHang](
	[MaLoaiKH] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiKH] [nvarchar](50) NULL,
	[HeSo] [float] NULL,
 CONSTRAINT [PK_LoaiKhachHang] PRIMARY KEY CLUSTERED 
(
	[MaLoaiKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiNhanVien]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiNhanVien](
	[MaLoaiNV] [nchar](10) NOT NULL,
	[TenLoaiNV] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiNhanVien] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiPhong]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiPhong](
	[MaLoaiPhong] [nchar](10) NOT NULL,
	[TenLoaiPhong] [nvarchar](50) NULL,
	[Anh] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiPhong] PRIMARY KEY CLUSTERED 
(
	[MaLoaiPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MaNV] [nchar](10) NOT NULL,
	[TenNV] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[NgaySinh] [date] NULL,
	[CMND] [nchar](15) NULL,
	[SDT] [nchar](15) NULL,
	[TaiKhoan] [nchar](50) NULL,
	[MatKhau] [nchar](50) NULL,
	[LoaiNV] [nchar](10) NULL,
	[Email] [nchar](50) NULL,
	[ConQuanLy] [bit] NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhatKyHoatDong]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhatKyHoatDong](
	[STT] [int] IDENTITY(1,1) NOT NULL,
	[ID] [nchar](10) NULL,
	[TenMayTinh] [nvarchar](50) NULL,
	[ThoiGian] [datetime] NULL,
	[ChucNang] [nvarchar](50) NULL,
	[HanhDong] [nvarchar](50) NULL,
 CONSTRAINT [PK_NhatKyHoatDong] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhieuThue]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuThue](
	[MaPhieuThue] [nchar](10) NOT NULL,
	[MaPhong] [nchar](10) NULL,
	[MaKH] [nchar](10) NULL,
	[NgayBatDauThue] [datetime] NULL,
	[NgayTraPhong] [datetime] NULL,
	[SoNguoiThue] [int] NULL,
	[TienChuyenPhong] [float] NULL,
	[MaHinhThuc] [nchar](10) NULL,
 CONSTRAINT [PK_PhieuThue] PRIMARY KEY CLUSTERED 
(
	[MaPhieuThue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Phong]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Phong](
	[MaPhong] [nchar](10) NOT NULL,
	[TenPhong] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](200) NULL,
	[TinhTrang] [nchar](10) NULL,
	[SoNguoiToiDa] [int] NULL,
	[LoaiPhong] [nchar](10) NULL,
 CONSTRAINT [PK_Phong] PRIMARY KEY CLUSTERED 
(
	[MaPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuyDinh]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyDinh](
	[STT] [int] IDENTITY(1,1) NOT NULL,
	[PhuThu] [float] NULL,
 CONSTRAINT [PK_QuyDinh] PRIMARY KEY CLUSTERED 
(
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TinhTrang]    Script Date: 3/27/2017 12:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TinhTrang](
	[MATINHTRANG] [nchar](10) NOT NULL,
	[TENTINHTRANG] [nvarchar](50) NULL,
 CONSTRAINT [PK_TinhTrang] PRIMARY KEY CLUSTERED 
(
	[MATINHTRANG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaPhong], [NgayDen], [NgayDi], [PhuThu], [DonGia], [ThanhTien]) VALUES (N'MCTHD00001', N'MHD0000001', N'MP00000001', CAST(N'2016-03-26 00:00:00.000' AS DateTime), CAST(N'2016-03-27 00:00:00.000' AS DateTime), 30000, 30000, 30000)
INSERT [dbo].[ChiTietHoaDon] ([MaCTHD], [MaHD], [MaPhong], [NgayDen], [NgayDi], [PhuThu], [DonGia], [ThanhTien]) VALUES (N'MCTHD00002', N'MHD0000001', N'MP00000001', CAST(N'2016-03-26 00:00:00.000' AS DateTime), NULL, 600000, NULL, 600000)
INSERT [dbo].[HinhThuc] ([MaHinhThuc], [TenHinhThuc]) VALUES (N'MHT0000001', N'thuê theo giờ')
INSERT [dbo].[HinhThuc] ([MaHinhThuc], [TenHinhThuc]) VALUES (N'MHT0000002', N'thuê theo ngày')
INSERT [dbo].[HinhThuc] ([MaHinhThuc], [TenHinhThuc]) VALUES (N'MHT0000003', N'thuê theo tháng')
INSERT [dbo].[HinhThuc] ([MaHinhThuc], [TenHinhThuc]) VALUES (N'MHT0000004', N'thuê theo năm')
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia]) VALUES (N'MLP0000001', N'MHT0000003', 4400000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia]) VALUES (N'MLP0000002', N'MHT0000002', 700000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia]) VALUES (N'MLP0000003', N'MHT0000001', 100000)
INSERT [dbo].[HinhThuc_LoaiPhong] ([MaLoaiPhong], [MaHinhThuc], [DonGia]) VALUES (N'MLP0000004', N'MHT0000004', 9900000)
INSERT [dbo].[HoaDon] ([MaHD], [MaKH], [SoGioThue], [TongTien], [NgayThanhToan], [MaNV]) VALUES (N'MHD0000001', N'MKH0000001', 24, 9000000, CAST(N'2016-08-08 00:00:00.000' AS DateTime), N'MNV0000001')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000001', N'Anh Tùng', N'225674545      ', N'hồ chí minh', N'113            ', 1)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000002', N'Ca', N'344234354      ', N'american', N'114            ', 2)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000003', N'Hiền', N'434342333      ', N'tân bình', N'115            ', 1)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [LoaiKH]) VALUES (N'MKH0000004', N'Trang', N'3234343553     ', N'hoa kỳ', N'116            ', 2)
SET IDENTITY_INSERT [dbo].[LoaiKhachHang] ON 

INSERT [dbo].[LoaiKhachHang] ([MaLoaiKH], [TenLoaiKH], [HeSo]) VALUES (1, N'nội địa', 1)
INSERT [dbo].[LoaiKhachHang] ([MaLoaiKH], [TenLoaiKH], [HeSo]) VALUES (2, N'ngoại quốc', 2)
SET IDENTITY_INSERT [dbo].[LoaiKhachHang] OFF
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoaiNV]) VALUES (N'MLNV000001', N'bảo vệ')
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoaiNV]) VALUES (N'MLNV000002', N'thu ngân')
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoaiNV]) VALUES (N'MLNV000003', N'quản lý')
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoaiNV]) VALUES (N'MLNV000004', N'tổng quản lý')
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [Anh]) VALUES (N'MLP0000001', N'A đơn', NULL)
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [Anh]) VALUES (N'MLP0000002', N'B đơn', NULL)
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [Anh]) VALUES (N'MLP0000003', N'C', NULL)
INSERT [dbo].[LoaiPhong] ([MaLoaiPhong], [TenLoaiPhong], [Anh]) VALUES (N'MLP0000004', N'A đôi', NULL)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy]) VALUES (N'MNV0000001', N'Tùng', N'sài gòn', CAST(N'1994-01-01' AS Date), N'4345435        ', N'016872344      ', N'tung                                              ', N'tung                                              ', N'MLNV000004', N'tung@gmail.com                                    ', 0)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy]) VALUES (N'MNV0000002', N'Ca', N'hồ chí minh', CAST(N'1994-02-02' AS Date), N'876875567      ', N'064328732      ', N'ca                                                ', N'ca                                                ', N'MLNV000001', N'ca@gmail.com                                      ', 1)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy]) VALUES (N'MNV0000003', N'Trang', N'gia định', CAST(N'1994-03-03' AS Date), N'987967655      ', N'019873684      ', N'trang                                             ', N'trang                                             ', N'MLNV000002', N'trang@gmail.com                                   ', 1)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [DiaChi], [NgaySinh], [CMND], [SDT], [TaiKhoan], [MatKhau], [LoaiNV], [Email], [ConQuanLy]) VALUES (N'MNV0000004', N'Hiền', N'tân bình', CAST(N'1994-04-04' AS Date), N'987683434      ', N'012374634      ', N'hien                                              ', N'hien                                              ', N'MLNV000003', N'hien@gmail.com                                    ', 0)
SET IDENTITY_INSERT [dbo].[NhatKyHoatDong] ON 

INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (1, N'1         ', N'MT1', CAST(N'2017-03-26 00:00:00.000' AS DateTime), N'quản lý khác hàng', N'insert')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (2, N'2         ', N'MT1', CAST(N'2017-03-26 00:00:00.000' AS DateTime), N'quản lý khách hàng', N'insert')
INSERT [dbo].[NhatKyHoatDong] ([STT], [ID], [TenMayTinh], [ThoiGian], [ChucNang], [HanhDong]) VALUES (3, N'3         ', N'MT2', CAST(N'2017-03-26 00:00:00.000' AS DateTime), N'quản lý phòng', N'insert')
SET IDENTITY_INSERT [dbo].[NhatKyHoatDong] OFF
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000001', N'MP00000001', N'MKH0000001', CAST(N'2016-05-05 00:00:00.000' AS DateTime), CAST(N'2016-06-06 00:00:00.000' AS DateTime), 3, 3000, N'MHT0000001')
INSERT [dbo].[PhieuThue] ([MaPhieuThue], [MaPhong], [MaKH], [NgayBatDauThue], [NgayTraPhong], [SoNguoiThue], [TienChuyenPhong], [MaHinhThuc]) VALUES (N'MPT0000002', N'MP00000002', N'MKH0000002', CAST(N'2016-06-06 00:00:00.000' AS DateTime), NULL, 2, 60000, N'MHT0000002')
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong]) VALUES (N'MP00000001', N'Phòng 001', N'phòng sạch sẽ', N'MTT0000002', 5, N'MLP0000001')
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong]) VALUES (N'MP00000002', N'Phòng 002', N'phòng hơi sạch sẽ', N'MTT0000001', 4, N'MLP0000003')
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong]) VALUES (N'MP00000003', N'Phòng 003', N'phòng tốt', N'MTT0000003', 4, N'MLP0000002')
INSERT [dbo].[Phong] ([MaPhong], [TenPhong], [GhiChu], [TinhTrang], [SoNguoiToiDa], [LoaiPhong]) VALUES (N'MP00000004', N'Phòng 004', N'phòng tốt ', N'MTT0000003', 4, N'MLP0000004')
INSERT [dbo].[TinhTrang] ([MATINHTRANG], [TENTINHTRANG]) VALUES (N'MTT0000001', N'hết')
INSERT [dbo].[TinhTrang] ([MATINHTRANG], [TENTINHTRANG]) VALUES (N'MTT0000002', N'trống')
INSERT [dbo].[TinhTrang] ([MATINHTRANG], [TENTINHTRANG]) VALUES (N'MTT0000003', N'đang sữa chữa')
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_HoaDon] FOREIGN KEY([MaHD])
REFERENCES [dbo].[HoaDon] ([MaHD])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_HoaDon]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_Phong] FOREIGN KEY([MaPhong])
REFERENCES [dbo].[Phong] ([MaPhong])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_Phong]
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong]  WITH CHECK ADD  CONSTRAINT [FK_HinhThuc_LoaiPhong_HinhThuc] FOREIGN KEY([MaHinhThuc])
REFERENCES [dbo].[HinhThuc] ([MaHinhThuc])
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong] CHECK CONSTRAINT [FK_HinhThuc_LoaiPhong_HinhThuc]
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong]  WITH CHECK ADD  CONSTRAINT [FK_HinhThuc_LoaiPhong_LoaiPhong] FOREIGN KEY([MaLoaiPhong])
REFERENCES [dbo].[LoaiPhong] ([MaLoaiPhong])
GO
ALTER TABLE [dbo].[HinhThuc_LoaiPhong] CHECK CONSTRAINT [FK_HinhThuc_LoaiPhong_LoaiPhong]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_KhachHang]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_NhanVien] FOREIGN KEY([MaNV])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_NhanVien]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_LoaiKhachHang] FOREIGN KEY([LoaiKH])
REFERENCES [dbo].[LoaiKhachHang] ([MaLoaiKH])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_LoaiKhachHang]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_LoaiNhanVien] FOREIGN KEY([LoaiNV])
REFERENCES [dbo].[LoaiNhanVien] ([MaLoaiNV])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_LoaiNhanVien]
GO
ALTER TABLE [dbo].[PhieuThue]  WITH CHECK ADD  CONSTRAINT [FK_PhieuThue_HinhThuc] FOREIGN KEY([MaHinhThuc])
REFERENCES [dbo].[HinhThuc] ([MaHinhThuc])
GO
ALTER TABLE [dbo].[PhieuThue] CHECK CONSTRAINT [FK_PhieuThue_HinhThuc]
GO
ALTER TABLE [dbo].[PhieuThue]  WITH CHECK ADD  CONSTRAINT [FK_PhieuThue_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[PhieuThue] CHECK CONSTRAINT [FK_PhieuThue_KhachHang]
GO
ALTER TABLE [dbo].[PhieuThue]  WITH CHECK ADD  CONSTRAINT [FK_PhieuThue_Phong] FOREIGN KEY([MaPhong])
REFERENCES [dbo].[Phong] ([MaPhong])
GO
ALTER TABLE [dbo].[PhieuThue] CHECK CONSTRAINT [FK_PhieuThue_Phong]
GO
ALTER TABLE [dbo].[Phong]  WITH CHECK ADD  CONSTRAINT [FK_Phong_LoaiPhong] FOREIGN KEY([LoaiPhong])
REFERENCES [dbo].[LoaiPhong] ([MaLoaiPhong])
GO
ALTER TABLE [dbo].[Phong] CHECK CONSTRAINT [FK_Phong_LoaiPhong]
GO
ALTER TABLE [dbo].[Phong]  WITH CHECK ADD  CONSTRAINT [FK_Phong_TinhTrang] FOREIGN KEY([TinhTrang])
REFERENCES [dbo].[TinhTrang] ([MATINHTRANG])
GO
ALTER TABLE [dbo].[Phong] CHECK CONSTRAINT [FK_Phong_TinhTrang]
GO
USE [master]
GO
ALTER DATABASE [QLKhachSan] SET  READ_WRITE 
GO
